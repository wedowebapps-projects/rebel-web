const withImages = require('next-images')
const SWPrecacheWebpackPlugin = require('sw-precache-webpack-plugin')

const nextConfig = {
  devIndicators: {
    autoPrerender: false
  },
  env: {
    APP_BASE_URL: process.env.APP_BASE_URL,
    API_URL: process.env.API_URL,
    AWS_bucketName: process.env.AWS_bucketName,
    AWS_bucketRegion: process.env.AWS_bucketRegion,
    AWS_UserAccessKey: process.env.AWS_UserAccessKey,
    AWS_UserSecret: process.env.AWS_UserSecret,
    FIREBASE_apiKey: process.env.FIREBASE_apiKey,
    FIREBASE_authDomain: process.env.FIREBASE_authDomain,
    FIREBASE_databaseURL: process.env.FIREBASE_databaseURL,
    FIREBASE_projectId: process.env.FIREBASE_projectId,
    FIREBASE_storageBucket: process.env.FIREBASE_storageBucket,
    FIREBASE_messagingSenderId: process.env.FIREBASE_messagingSenderId,
    FIREBASE_appId: process.env.FIREBASE_appId,
    FIREBASE_measurementId: process.env.FIREBASE_measurementId,
    FIREBASE_vapidKey: process.env.FIREBASE_vapidKey,
    STRIPE_secretKey: process.env.STRIPE_secretKey,
    STRIPE_publishableKey: process.env.STRIPE_publishableKey,
    STRIPE_clientId: process.env.STRIPE_clientId,
    STRIPE_authorizeUri: process.env.STRIPE_authorizeUri,
    STRIPE_tokenUri: process.env.STRIPE_tokenUri,
    STRIPE_stripeRedirectDev: process.env.STRIPE_stripeRedirectDev,
    FLUTTERWAVE_SECRET: process.env.FLUTTERWAVE_SECRET,
    PAYSTACK_publicKey: process.env.PAYSTACK_publicKey,
    CHAT_SOCKET_URL: process.env.CHAT_SOCKET_URL,
    PAYMENT_METHOD: process.env.PAYMENT_METHOD
  },
  webpack: config => {
    if (process.env.NODE_ENV === 'production') {
      config.plugins.push(
        new SWPrecacheWebpackPlugin({
          verbose: true,
          staticFileGlobsIgnorePatterns: [/\.next\//],
          runtimeCaching: [
            {
              handler: 'networkFirst',
              urlPattern: /^https?.*/
            }
          ]
        })
      )
    }
    return config
  }
}
module.exports = withImages(nextConfig)
