import pubsub from 'sweet-pubsub'
import { get } from 'lodash'

const show = toast => {
  pubsub.emit('toast', toast)
}

const success = title => show({ title })

const error = err => {
  show({
    type: 'info',
    title: 'Error',
    message: get(err, 'message', err),
    duration: 3
  })
}

const info = title => show({ type: 'info', title })

export default { show, error, success, info }
