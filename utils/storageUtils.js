export const setUserData = (data) => {
  localStorage.setItem('userData', JSON.stringify(data));
};
export const setUserToken = (token) => {
  localStorage.setItem('token', `Bearer ${token}`);
};
export const getUserToken = () => {
  return localStorage.getItem('token');
};
export const getUserData = () => {
  return JSON.parse(localStorage.getItem('userData'));
};

export const unsetUserData = () => {
  localStorage.removeItem('userData');
};
export const unsetUserToken = () => {
  localStorage.removeItem('token');
};

export const announcementStorage = {
  set: async val => {
    let data = await announcementStorage.get()
    if(!data.includes(val)) {
      data.push(val)
      localStorage.setItem('announcements', JSON.stringify(data))
    }
  },
  get: () => {
    return localStorage.getItem('announcements') ? JSON.parse(localStorage.getItem('announcements')) : []
  }
}