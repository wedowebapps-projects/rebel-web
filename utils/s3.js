import AWS from 'aws-sdk'
import { v4 as uuidv4 } from 'uuid'
import { ImageMimes, VideoMimes } from './helpers'

export async function uploadSingle(object, username, type, progressCallback) {
  const s3 = new AWS.S3({
    apiVersion: '2006-03-01',
    params: { Bucket: process.env.AWS_bucketName }
  })

  let path = type
  if (type === 'posts') {
    path = `posts/${ImageMimes.includes(object.type) ? 'photos' : VideoMimes.includes(object.type) ? 'videos' : ''}`
  }
  // const fileName = object.name.replace(/[^a-zA-Z0-9]/g, '');
  const fileName = object.name.replace(/(?:\.(?![^.]+$)|[^\w.])+/g, '-')
  const fullname = `${path}/${username || ''}-${uuidv4()}-${fileName}`

  return new Promise((resolve, reject) => {
    return s3
      .upload(
        {
          Key: fullname,
          Body: object,
          ACL: 'public-read'
        },
        (err, data) => {
          if (err) {
            reject(err)
          }
          resolve({
            location: data.Location,
            type: object.type
          })
        }
      )
      .on('httpUploadProgress', function(evt) {
        const percentage = parseInt((evt.loaded * 100) / evt.total)
        progressCallback(percentage)
      })
  })
}

export async function upload(objects, username, type) {
  if (objects.length > 0) {
    const s3 = new AWS.S3({
      apiVersion: '2006-03-01',
      params: { Bucket: process.env.AWS_bucketName }
    })

    // console.log('objects');
    // console.log(objects);
    // return false;

    const uploads = []
    for (const object of objects) {
      uploads.push(
        new Promise((resolve, reject) => {
          let path = type
          if (type === 'posts') {
            path = `posts/${ImageMimes.includes(object.type) ? 'photos' : VideoMimes.includes(object.type) ? 'videos' : ''}`
          }
          // const fileName = object.name.replace(/[^a-zA-Z0-9]/g, '');
          const fileName = object.name.replace(/(?:\.(?![^.]+$)|[^\w.])+/g, '-')
          const fullname = `${path}/${username || ''}-${uuidv4()}-${fileName}`

          s3.upload(
            {
              Key: fullname,
              Body: object,
              ACL: 'public-read'
            },
            (err, data) => {
              if (err) {
                reject(err)
              }
              resolve({
                location: data.Location,
                type: object.type
              })
            }
          )
          // const params = {
          //   Key: fullname,
          //   Body: object,
          //   ACL: 'public-read'
          // };
          // s3.createMultipartUpload(params, (err, data) => {
          //   if (err) {
          //     console.log(err);
          //     reject(err);
          //   }
          //   resolve({
          //     location: data.Location,
          //     type: object.type
          //   });
          // });
        })
      )
    }
    const uploaded = await Promise.all(uploads)
    return uploaded
  }
}

export async function deleteSingle(object, username, type) {
  const s3 = new AWS.S3({
    apiVersion: '2006-03-01',
    params: { Bucket: process.env.AWS_bucketName }
  })

  let path = type
  if (type === 'posts') {
    path = `posts/${ImageMimes.includes(object.type) ? 'photos' : VideoMimes.includes(object.type) ? 'videos' : ''}`
  }
  // const fileName = object.name.replace(/[^a-zA-Z0-9]/g, '');
  const fileName = object.name.replace(/(?:\.(?![^.]+$)|[^\w.])+/g, '-')
  const fullname = `${path}/${username || ''}-${uuidv4()}-${fileName}`

  return new Promise((resolve, reject) => {
    return s3.deleteObject(
      {
        Key: fileName,
      },
      (err, data) => {
        if (err) {
          reject(err)
        } else {
          resolve(data)
        }
      }
    )
  })
}
