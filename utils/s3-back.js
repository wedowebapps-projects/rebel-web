import AWS from 'aws-sdk'
import { v4 as uuidv4 } from 'uuid'
import { ImageMimes, VideoMimes } from './helpers'

export default async function upload(objects, username, type) {
  if (objects.length > 0) {
    const s3 = new AWS.S3({
      apiVersion: '2006-03-01',
      params: { Bucket: process.env.AWS_bucketName }
    })
    const uploads = []
    for (const object of objects) {
      uploads.push(
        new Promise((resolve, reject) => {
          let path = type;
          if (type === 'posts') {
            path = `posts/${ImageMimes.includes(object.type) ? 'photos' : VideoMimes.includes(object.type) ? 'videos' : ''}`
          }
          const fullname = `${path}/${username || ''}-${uuidv4()}-${object.name}`
          s3.upload(
            {
              Key: fullname,
              Body: object,
              ACL: 'public-read'
            },
            (err, data) => {
              if (err) {
                reject(err)
              }
              resolve({
                location: data.Location,
                type: object.type
              })
            }
          )
          // const params = {
          //   Key: fullname,
          //   Body: object,
          //   ACL: 'public-read'
          // };
          // s3.createMultipartUpload(params, (err, data) => {
          //   if (err) {
          //     console.log(err);
          //     reject(err);
          //   }
          //   resolve({
          //     location: data.Location,
          //     type: object.type
          //   });
          // });
        })
      )
    }
    const uploaded = await Promise.all(uploads)
    return uploaded
  }
}
