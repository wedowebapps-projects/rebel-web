
**Server notes:**

- Add alias in file: `/home/admin/conf/web/rebelyus.com.nginx.ssl.conf`
```

location /_next/static/ {

alias /home/admin/web/rebelyus.com/public_html/.next/static/;

}

```

**Deployment notes:**

- Take pull from `master` branch:

```

git pull origin master

```

- Make a new nextjs build:
```

npm run build

```
- Restart node server by pm2:
```

pm2 restart 6

```

or start new pm2 process:

```

pm2 start 'npm start' --name='rebelyus.com'

```

or npm command to start node server: `npm start` OR `NODE_ENV=production node server.js`


**For PWA:**

1. Configure following plugin in in `next.conf.js` file.
*sw-precache-webpack-plugin* [https://github.com/goldhand/sw-precache-webpack-plugin](https://github.com/goldhand/sw-precache-webpack-plugin)
2. `service-worker.js` file must be at root directory
3. `firebase-messaging-sw.js` file must be at root directory
