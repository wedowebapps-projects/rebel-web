import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { toggleTheme, setTheme } from '../actions/appActions'
import { getTheme } from 'src/utils/storageUtils'

import AppLoading from 'src/components/app/AppLoading'

export const AuthLayout = props => {
  const [isDark, setIsDark] = useState(false)

  useEffect(() => {
    let theme = getTheme()
    if(theme === null){
      theme = 'light'
    }
    props.setTheme(theme)
  }, [])

  useEffect(() => {
    setIsDark(props.theme === 'dark')
  }, [props])

  const onThemeChange = e => {
    props.toggleTheme()
  }

  if (props.appLoading) return <AppLoading theme={props.theme} />

  return (
    <React.Fragment>
      <div className={props.theme == 'dark' ? 'dark-mode' : ''}>
        <header className='mode-wrapper-position'>
          <div>
            <input type='checkbox' checked={isDark} id='toggle-mode-cb' onChange={e => onThemeChange(e)} />
            <div id='mode-wrapper'>
              <label id='toggle-mode' htmlFor='toggle-mode-cb'>
                <span className='toggle-border'>
                  <span className='toggle-indicator' />
                </span>
              </label>
            </div>
          </div>
        </header>
        <div className='wrapper'>
          <div className='page-content'>{props.children}</div>
        </div>
      </div>
    </React.Fragment>
  )
}

const mapStateToProps = state => ({
  theme: state.app.theme,
  appLoading: state.app.appLoading
})

const mapDispatchToProps = dispatch => {
  return {
    toggleTheme: () => dispatch(toggleTheme()),
    setTheme: theme => dispatch(setTheme(theme))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AuthLayout)
