import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  toggleTheme,
  setTheme,
  toggleMenu,
  fetchFeaturedCreators,
  toggleNewPostModal,
  fetchCommissions,
  fetchCountries,
  readNotificationsAction,
  clearNotificationsAction,
  toggleNotificationDrawerAction,
  fetchTopBarNotificationsAction,
  sendContactUsAction,
  fetchAnnouncements
} from '../actions/appActions'
import { logoutUser } from 'src/modules/auth/actions/authActions'
import { getTheme, getUserToken } from 'src/utils/storageUtils'
import { CSSTransition } from 'react-transition-group'
import { Header, LeftSidebar, MobileHeader, MobileSidebar, MobileFooter, WelcomeModal } from 'src/components/app/index.js'
import AppLoading from 'src/components/app/AppLoading'
import CreatePost from 'src/modules/post/container/createPost'
import { fetchCommissionAction, fetchReportReasons } from 'store/actions/app/appActions'
import Router from 'next/router'
import SettingSidebar from 'src/components/app/SettingSidebar'
import { HowItWorks, PrivacyPolicy, TermsConditions } from 'src/modules/auth/components/Setting'
import LogoutModal from 'src/components/app/LogoutModal'
import ContactUs from 'src/modules/auth/components/ContactUs'
import AnnouncementModal from 'src/components/app/AnnouncementModal'

export class AppLayout extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isLogoutModalOpen: false
    }
    this.changeLogoutToggle = this.changeLogoutToggle.bind(this)
  }

  componentDidMount() {
    const token = getUserToken()
    let theme = getTheme()
    if (theme === null) {
      theme = 'light'
    }
    this.props.setTheme(theme)
    this.props.fetchCommissions()
    this.props.fetchCountries()
    this.props.fetchFeaturedCreators({ page: 1, perPage: 50 })
    this.props.fetchCommission()
    this.props.fetchReportReasons()
    this.props.fetchAnnouncements()
    if (token) {
      this.props.fetchTopBarNotifications()
    }
    // setInterval(async () => {
    //   if(!isEmpty(this.props.user)){
    //     this.props.fetchTopBarNotifications()
    //   }
    // }, 5000);
  }

  onThemeChange = e => {
    this.props.toggleTheme()
  }

  changeLogoutToggle = value => {
    this.setState({ isLogoutModalOpen: value })
  }

  render() {
    if (this.props.appLoading) return <AppLoading theme={this.props.theme} />
    return (
      <>
        <AnnouncementModal {...this.props} />
        {Router.router.asPath === '/terms-conditions' ? (
          <TermsConditions />
        ) : Router.router.asPath === '/privacy-policy' ? (
          <PrivacyPolicy />
        ) : Router.router.asPath === '/how-it-works' ? (
          <HowItWorks />
        ) : Router.router.asPath === '/contact-us' ? (
          <ContactUs {...this.props} />
        ) : (
          <div className={this.props.theme == 'dark' ? 'dark-mode' : ''}>
            <WelcomeModal />
            <Header {...this.props} changeLogoutToggle={this.changeLogoutToggle} />
            {Router.router.pathname !== '/chat/[chatId]' && (
              <>
                <MobileHeader {...this.props} />
                <MobileSidebar {...this.props} changeLogoutToggle={this.changeLogoutToggle} />
              </>
            )}
            <main className={`main-class${Router.router.pathname === '/chat/[chatId]' ? ' main-class-hide' : ''}`}>
              <div className='container'>
                <div className={`d-flex justify-content-between`}>
                  <LeftSidebar {...this.props} />
                  {Router.router.route.includes('/settings') && <SettingSidebar />}
                  {this.props.children}
                  {Router.router.pathname !== '/chat/[chatId]' && <MobileFooter {...this.props} />}
                </div>
              </div>
              <CSSTransition in={this.props.is_MenuDrawerVisible} unmountOnExit timeout={300} classNames='animateBodyOverlay'>
                <div className='overlay' style={{ display: 'block' }} onClick={this.props.toggleMenu}></div>
              </CSSTransition>
              {this.props.is_newPostModalVisible && <CreatePost />}
            </main>
            <LogoutModal {...this.props} {...this.state} changeLogoutToggle={this.changeLogoutToggle} />
          </div>
        )}
      </>
    )
  }
}

const mapStateToProps = state => ({
  chatUnreadCount: state.chat.totalUnreadCount,
  user: state.auth.user,
  theme: state.app.theme,
  appLoading: state.app.appLoading,
  is_MenuDrawerVisible: state.app.is_MenuDrawerVisible,
  is_newPostModalVisible: state.app.is_newPostModalVisible,
  topBarNotifications: state.app.topBarNotifications,
  announcements: state.app.announcements
})

const mapDispatchToProps = dispatch => {
  return {
    fetchCommission: () => dispatch(fetchCommissionAction()),
    fetchReportReasons: () => dispatch(fetchReportReasons()),
    toggleNewPostModal: () => dispatch(toggleNewPostModal()),
    fetchAnnouncements: () => dispatch(fetchAnnouncements()),
    toggleTheme: () => dispatch(toggleTheme()),
    setTheme: theme => dispatch(setTheme(theme)),
    toggleMenu: () => dispatch(toggleMenu()),
    logoutUser: () => new Promise((resolve, reject) => dispatch(logoutUser(resolve, reject))),
    fetchFeaturedCreators: filters => dispatch(fetchFeaturedCreators(filters)),
    fetchCountries: () => dispatch(fetchCountries()),
    fetchCommissions: () => dispatch(fetchCommissions()),
    toggleNotificationDrawer: () => dispatch(toggleNotificationDrawerAction()),
    clearNotifications: () => new Promise((resolve, reject) => dispatch(clearNotificationsAction(resolve, reject))),
    readNotifications: () => dispatch(readNotificationsAction()),
    fetchTopBarNotifications: () => dispatch(fetchTopBarNotificationsAction()),
    sendContactUs: data => new Promise((resolve, reject) => dispatch(sendContactUsAction(data, resolve, reject)))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AppLayout)
