import moment from 'moment'
import * as types from '../actions/appTypes'

const initialState = {
  theme: 'light',
  appLoading: true,
  is_NotificationDrawerVisible: false,
  is_MenuDrawerVisible: false,
  is_SearchDrawerVisible: false,
  is_newPostModalVisible: false,
  featuredCreators: [],
  reportReasons: [],
  commissions: {},
  countries: [],
  topBarNotifications: {
    unreadNotifications: 0,
    rows: [],
  },
  announcements: []
}

const AppReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_ANNOUNCEMENTS:
      return {
        ...state,
        announcements: action.data
      }
    case types.SET_REPORT_REASONS:
      return { ...state, reportReasons: action.reasons }

    case types.SET_COUNTRIES:
      return { ...state, countries: action.data }

    case types.SET_COMMISSIONS:
      return { ...state, commissions: action.data }

    case types.SET_FEATURED_CREATORS:
      return { ...state, featuredCreators: action.data }

    case types.SET_APP_LOADING:
      return { ...state, appLoading: action.loading }

    case types.TOGGLE_NEW_POST_MODAL:
      return { ...state, is_newPostModalVisible: !state.is_newPostModalVisible }

    case types.TOGGLE_THEME:
      return { ...state, theme: state.theme === 'light' ? 'dark' : 'light' }

    case types.SET_THEME:
      return { ...state, theme: action.theme }

    case types.TOGGLE_NOTIFICATION_DRAWER:
      return {
        ...state,
        is_NotificationDrawerVisible: !state.is_NotificationDrawerVisible,
        is_MenuDrawerVisible: false,
        is_SearchDrawerVisible: false
      }
    case types.TOGGLE_MENU_DRAWER:
      return {
        ...state,
        is_MenuDrawerVisible: !state.is_MenuDrawerVisible,
        is_NotificationDrawerVisible: false,
        is_SearchDrawerVisible: false
      }
    case types.TOGGLE_SEARCH_DRAWER:
      return {
        ...state,
        is_SearchDrawerVisible: !state.is_SearchDrawerVisible,
        is_NotificationDrawerVisible: false,
        is_MenuDrawerVisible: false
      }
    case types.CLOSE_DRAWERS:
      return {
        ...state,
        is_NotificationDrawerVisible: false,
        is_MenuDrawerVisible: false,
        is_SearchDrawerVisible: false
      }
    case types.SET_NOTIFICATION: {
      return {
        ...state,
        notifications : action.data,
      }
    }
    case types.SET_TOP_BAR_NOTIFICATION: {
      return {
        ...state,
        topBarNotifications : action.data,
      }
    }
    case types.SET_READ_NOTIFICATION: {
      const { topBarNotifications, notifications } = state
      return {
        ...state,
        notifications: notifications.map(n => {
          n.is_read = true
          return n
        }),
        topBarNotifications: {
          rows: topBarNotifications.rows.map(n => {
            n.is_read = true
            return n
          }),
          unreadNotifications: 0,
        }
      }
    }
    default:
      return state
  }
}
export default AppReducer
