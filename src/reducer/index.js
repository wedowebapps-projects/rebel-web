import { combineReducers } from 'redux'
import authReducer from 'src/modules/auth/reducer/authReducer'
// import appReducer from 'store/reducer/app/appReducer'
import postReducer from 'src/modules/post/reducer/postReducer'
import profileReducer from 'src/modules/profile/reducer/profileReducer'
import chatReducer from 'src/modules/chat/reducer/chatReducer'
import bookmarkReducer from 'src/modules/profile/reducer/bookmarkReducer'
import dashboardReducer from 'src/modules/profile/reducer/dashboardReducer'
import AppReducer from './AppReducer'

export default combineReducers({
  auth: authReducer,
  app: AppReducer,
  posts: postReducer,
  profile: profileReducer,
  chat: chatReducer,
  bookmark: bookmarkReducer,
  dashboard: dashboardReducer
})
