import api from 'utils/api'
const _ = require('lodash')
import { convertObjectToQuerystring } from 'utils/helpers'
import { getUserToken } from 'utils/storageUtils'

export const reportPost_api = (postid, data) => {
  const token = getUserToken()
  return api(`/posts/${postid}/report`, data, 'post', token)
}

export const createPostApi = ({ data, token }) => {
  return api(`/posts`, data, 'post', token)
}

export const fetchOpenPosts = filters => {
  let filter = ''
  if (!_.isEmpty(filters)) {
    filter = `${convertObjectToQuerystring(filters)}`
  }
  if (filters.bookmarked || filters.liked) {
    return api(`/user/post?${filter}`, null, 'get')
  } else {
    return api(`/open/posts?${filter}`, null, 'get')
  }
}

export const fetchProtectedPosts = (filters, token) => {
  let filter = ''
  if (!_.isEmpty(filters)) {
    filter = `${convertObjectToQuerystring(filters)}`
  }
  if (filters.bookmarked || filters.liked || filters.paid)
  {
    return api(`/user/post?${filter}`, null, 'get', token)
  }
  else
  {
    return api(`/posts?${filter}`, null, 'get', token)
  }
}

export const fetchPostsByUsername = (filters, username) => {
  let filter = ''
  filters.username = username
  if (!_.isEmpty(filters)) {
    filter = `${convertObjectToQuerystring(filters)}`
  }
  const token = getUserToken()
  if (token)
  {
    return api(`/posts?${filter}`, null, 'get', token)
  }
  else
  {
    return api(`/open/user/profile/${username}/posts?${filter}`, null, 'get')
  }
}

export const addVote_api = (id, data) => {
  const token = getUserToken()
  return api(`/posts/${id}/votes`, data, 'post', token)
}
export const actions_api = (id, data) => {
  const token = getUserToken()
  return api(`/posts/${id}/actions`, data, 'post', token)
}
export const fetchPostDetail_api = uuid => {
  const token = getUserToken()
  if (token) {
    return api(`/posts/${uuid}`, null, 'get', token)
  } else {
    return api(`/open/posts/${uuid}`, null, 'get')
  }
}
export const likeDislikeComment_api = (post_id, commentId, data) => {
  const token = getUserToken()
  return api(`/posts/${post_id}/comment/${commentId}`, data, 'post', token)
}

export const postComment = (post_id, data) => {
  const token = getUserToken()
  return api(`/posts/${post_id}/comment`, data, 'post', token)
}

export const fetchPostComments_api = (post_id, filters) => {
  let filter = ''
  if (!_.isEmpty(filters)) {
    filter = `${convertObjectToQuerystring(filters)}`
  }
  const token = getUserToken()
  if (token) {
    return api(`/posts/${post_id}/comment?${filter}`, null, 'get', token)
  } else {
    return api(`/open/posts/${post_id}/comment?${filter}`, null, 'get', token)
  }
}

export const fetchReplies_api = (post_id, comment_id, filters) => {
  let filter = ''
  if (!_.isEmpty(filters)) {
    filter = `${convertObjectToQuerystring(filters)}`
  }
  const token = getUserToken()
  if (token) {
    return api(`/posts/${post_id}/comment/${comment_id}?${filter}`, null, 'get', token)
  } else {
    return api(`/open/posts/${post_id}/comment/${comment_id}?${filter}`, null, 'get', token)
  }
}

export const submitPurchaseTransaction_api = (post_id, data) => {
  const token = getUserToken()
  if (process.env.PAYMENT_METHOD === 'flutterwave') {
    return api(`/posts/${post_id}/purchase`, data, 'post', token)
  } else {
    return api(`/posts/${post_id}/purchasePayStack`, data, 'post', token)
  }
}

export const submitSendTipTransactionApi = (post_id, data) => {
  const token = getUserToken()
  if (process.env.PAYMENT_METHOD === 'flutterwave') {
    return api(`/posts/${post_id}/tips`, data, 'post', token)
  } else {
    return api(`/posts/${post_id}/tipsPayStack`, data, 'post', token)
  }
}
export const deletePost_api = post_id => {
  const token = getUserToken()
  return api(`/posts/${post_id}`, null, 'delete', token)
}
