import * as types from './postTypes'

export const fetchPostComments = (post, from, resolve, reject) => ({ type: types.FETCH_POST_COMMENTS, post, from, resolve, reject })
export const postInteraction = (data, from, resolve, reject) => ({ type: types.POST_INTERACTION, data, from, resolve, reject })
export const updateFeed = data => ({ type: types.UPDATE_FEED, data })
export const fetchFeed = data => ({ type: types.FETCH_FEED, data })
export const setFeed = data => ({ type: types.SET_FEED, data })
export const setFeedLoading = loading => ({ type: types.SET_FEED_LOADING, loading })
export const createPost = (data, resolve, reject) => ({ type: types.CREATE_POST, data, resolve, reject })
export const appendPost = post => ({ type: types.APPEND_POST, post })
export const reportPost = (postid, data) => ({ type: types.REPORT_POST, postid, data })
export const resetFeed = () => ({ type: types.RESET_FEED })

// Purchase Post
export const togglePurchasePostModalAction = (visible, post, username) => ({
  type: types.TOGGLE_PURCHASE_POST_MODAL,
  post,
  visible,
  username
})
export const setPurchasePostAction = post_id => ({
  type: types.SET_PURCHASE_POST,
  post_id
})
export const submitTransactionAction = (post_id, data, isProfile, resolve, reject) => ({
  type: types.SUBMIT_PURCHASE_TRANSACTION_POST,
  post_id,
  data,
  isProfile,
  resolve,
  reject
})

//send tip
export const setSendTipPostAction = post_id => ({
  type: types.SET_SENDTIP_POST,
  post_id
})

export const deletePostAction = (post_id, resolve, reject, from) => ({
  type: types.DELETE_POST,
  post_id,
  resolve,
  reject,
  from
})
