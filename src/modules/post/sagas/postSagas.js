import { takeLatest, all, fork, call, put, select } from 'redux-saga/effects'
import * as types from '../actions/postTypes'
import * as appApis from 'src/api/appApis'
import * as postApis from '../api/postApis'
import * as postActions from '../actions/postActions'
import * as profileActions from '../../profile/actions/profileActions'
import * as bookmarkActions from '../../profile/actions/bookmarkActions'
import * as dashboardActions from '../../profile/actions/dashbordActions'
import { toggleNewPostModal } from 'src/actions/appActions'
import { feedInteractionLocal } from '../utils/postFunctions'
import Router from 'next/router'

export const getUserToken = state => state.auth.token
export const getFeed = state => state.posts.feed

function* createPost({ data, resolve, reject }) {
  try {
    let token = yield select(getUserToken)
    const result = yield call(postApis.createPostApi, { data, token })
    const post = result.post
    yield put(postActions.appendPost(post))
    yield put(toggleNewPostModal())
    resolve(post)
  } catch (error) {
    reject && reject(error)
  }
}

function* fetchFeed(action) {
  let token = yield select(getUserToken)
  yield put(postActions.setFeedLoading(true))
  try {
    if (token) {
      const response = yield call(postApis.fetchProtectedPosts, action.data, token)
      if (action.data.bookmarked || action.data.liked || action.data.paid) {
        yield put(postActions.setFeed(response.posts))
      } else {
        yield put(postActions.setFeed(response.data.rows))
      }
    } else {
      yield call(Router.replace, '/login')
    }
  } catch (error) {
    console.log('error : ', error)
  }
  yield put(postActions.setFeedLoading(false))
}

function* postInteraction({ data, from, resolve, reject }) {
  try {
    let feed =
      from === 'bookmarked'
        ? yield select(state => state.bookmark.posts)
        : from === 'dashboard'
        ? yield select(state => state.dashboard.toppost)
        : from === 'feed'
        ? yield select(state => state.profile.feed.data)
        : from === 'liked'
        ? yield select(state => state.profile.liked.data)
        : from === 'paid'
        ? yield select(state => state.profile.paid.data)
        : yield select(getFeed)

    switch (data.type) {
      case 'comment':
        const res = yield call(postApis.postComment, data.post_id, data)
        feed = yield call(feedInteractionLocal, feed, data, res.comment)
        if (from === 'bookmarked') {
          yield put(bookmarkActions.updateBookmarkPosts(feed))
        } else if (from === 'dashboard') {
          yield put(dashboardActions.updateTopPosts(feed))
        } else if (from === 'feed' || from === 'liked' || from === 'paid') {
          yield put(profileActions.updateProfilePosts({ posts: feed, type: from }))
        } else {
          yield put(postActions.updateFeed(feed))
        }
        break

      case 'delete-comment':
        yield call(appApis.deleteComment_api, data.post_id, data.comment_id)
        feed = yield call(feedInteractionLocal, feed, data, feed.comments)
        if (from === 'bookmarked') {
          yield put(bookmarkActions.updateBookmarkPosts(feed))
        } else if (from === 'dashboard') {
          yield put(dashboardActions.updateTopPosts(feed))
        } else if (from === 'feed' || from === 'liked' || from === 'paid') {
          yield put(profileActions.updateProfilePosts({ posts: feed, type: from }))
        } else {
          yield put(postActions.updateFeed(feed))
        }
        break

      case 'like-dislike-comment':
        yield call(postApis.likeDislikeComment_api, data.post_id, data.comment_id, { type: data.likeType })
        feed = yield call(feedInteractionLocal, feed, data, feed.comments)
        if (from === 'bookmarked') {
          yield put(bookmarkActions.updateBookmarkPosts(feed))
        } else if (from === 'dashboard') {
          yield put(dashboardActions.updateTopPosts(feed))
        } else if (from === 'feed' || from === 'liked' || from === 'paid') {
          yield put(profileActions.updateProfilePosts({ posts: feed, type: from }))
        } else {
          yield put(postActions.updateFeed(feed))
        }
        break

      case 'toggle-show-comments':
        feed = yield call(feedInteractionLocal, feed, data)
        if (from === 'bookmarked') {
          yield put(bookmarkActions.updateBookmarkPosts(feed))
        } else if (from === 'dashboard') {
          yield put(dashboardActions.updateTopPosts(feed))
        } else if (from === 'feed' || from === 'liked' || from === 'paid') {
          yield put(profileActions.updateProfilePosts({ posts: feed, type: from }))
        } else {
          yield put(postActions.updateFeed(feed))
        }
        break

      case 'vote-poll':
        const vote = yield call(postApis.addVote_api, data.post_id, data)
        feed = yield call(feedInteractionLocal, feed, data)
        yield put(postActions.updateFeed(feed))
        break

      case 'like':
      case 'unlike':
      case 'bookmark':
      case 'remove-bookmark':
      default:
        feed = yield call(feedInteractionLocal, feed, data)
        if (from === 'bookmarked') {
          yield put(bookmarkActions.updateBookmarkPosts(feed))
        } else if (from === 'dashboard') {
          yield put(dashboardActions.updateTopPosts(feed))
        } else if (from === 'feed' || from === 'liked' || from === 'paid') {
          yield put(profileActions.updateProfilePosts({ posts: feed, type: from }))
        } else {
          yield put(postActions.updateFeed(feed))
        }
        yield call(postApis.actions_api, data.post_id, data)
        break
    }
    resolve()
  } catch (error) {
    console.log('error : ', error)
    reject(error)
  }
}

function* fetchPostComments({ post, from, resolve, reject }) {
  try {
    if (!post.commentsState) {
      post.commentsState = {
        show: true,
        page: 1,
        perPage: 5,
        end: false
      }
    } else {
      post.commentsState.show = true
    }
    if (!post.commentsState.end) {
      let comments = yield call(postApis.fetchPostComments_api, post.id, post.commentsState)
      comments = comments.comments.reverse()
      if (comments.length) {
        if (post.commentsState.page === 1) {
          post.comments = comments
        } else {
          // post.comments = [...post.comments, ...comments]
          post.comments = [...comments, ...post.comments]
        }
        if (comments.length >= post.commentsState.perPage) {
          post.commentsState.page++
        } else {
          post.commentsState.end = true
        }
      } else {
        post.commentsState.end = true
      }
      let feed =
        from === 'bookmarked'
          ? yield select(state => state.bookmark.posts)
          : from === 'dashboard'
          ? yield select(state => state.dashboard.toppost)
          : from === 'feed'
          ? yield select(state => state.profile.feed.data)
          : from === 'liked'
          ? yield select(state => state.profile.liked.data)
          : from === 'paid'
          ? yield select(state => state.profile.paid.data)
          : yield select(getFeed)

      if (from === 'bookmarked') {
        yield put(bookmarkActions.updateBookmarkPosts(feed))
      } else if (from === 'dashboard') {
        yield put(dashboardActions.updateTopPosts(feed))
      } else if (from === 'feed' || from === 'liked' || from === 'paid') {
        yield put(profileActions.updateProfilePosts({ posts: feed, type: from }))
      } else {
        yield put(postActions.updateFeed(feed))
      }
    }
    resolve()
  } catch (error) {
    reject(error)
  }
}

function* reportPost({ postid, data }) {
  try {
    const result = yield call(postApis.reportPost_api, postid, data)
    //toast.success(result.message)
  } catch (error) {
    console.log(error)
    let errorMsg = error.errors[0]
    errorMsg = errorMsg.msg
    console.log(errorMsg)
    //toast.error(errorMsg)
  }
}

function* submitTransactionSaga({ post_id, data, resolve, reject, isProfile }) {
  try {
    const result = yield call(postApis.submitPurchaseTransaction_api, post_id, data)
    if (isProfile) {
      yield put(profileActions.setPurchasePostAction_profile(post_id))
    } else {
      yield put(postActions.setPurchasePostAction(post_id))
    }
    resolve(result)
  } catch (error) {
    reject(error)
  }
}

function* deletePostSaga({ post_id, resolve, reject, from }) {
  try {
    let feed =
      from === 'bookmarked'
        ? yield select(state => state.bookmark.posts)
        : from === 'dashboard'
        ? yield select(state => state.dashboard.toppost)
        : from === 'feed'
        ? yield select(state => state.profile.feed.data)
        : from === 'liked'
        ? yield select(state => state.profile.liked.data)
        : from === 'paid'
        ? yield select(state => state.profile.paid.data)
        : yield select(getFeed)

    yield call(postApis.deletePost_api, post_id)
    feed = feed.filter(res => !(res.id === post_id))
    switch (from) {
      case 'bookmarked':
        yield put(bookmarkActions.updateBookmarkPosts(feed))
        break
      case 'dashboard':
        yield put(dashboardActions.updateTopPosts(feed))
        break
      case 'feed':
      case 'liked':
      case 'paid':
        yield put(profileActions.updateProfilePosts({ posts: feed, type: from }))
        break
      default:
        // From feed list
        yield put(postActions.updateFeed(feed))
    }
    resolve()
  } catch (error) {
    console.log(error)
    reject(error)
  }
}

export function* watchSagas() {
  yield takeLatest(types.FETCH_FEED, fetchFeed)
  yield takeLatest(types.FETCH_POST_COMMENTS, fetchPostComments)
  yield takeLatest(types.POST_INTERACTION, postInteraction)
  yield takeLatest(types.CREATE_POST, createPost)
  yield takeLatest(types.REPORT_POST, reportPost)
  yield takeLatest(types.SUBMIT_PURCHASE_TRANSACTION_POST, submitTransactionSaga)
  yield takeLatest(types.DELETE_POST, deletePostSaga)
}

export default function* runSagas() {
  yield all([fork(watchSagas)])
}
