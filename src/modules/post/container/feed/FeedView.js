import React from 'react'
import Feed from '../../components/feed/Feed'
import { AddBankCta } from 'src/components/app'
import images from 'src/config/images'
import PostLoader from '../../components/PostLoader'
import FeaturedCeleb from '../../components/FeaturedCeleb'
import InfiniteScroll from 'react-infinite-scroll-component'

function FeedView(props) {
  const { feedState, feed, app } = props
  const { featuredCreators } = app

  const handleInfiniteScroll = () => {
    props.fetchPosts()
  }

  return (
    <React.Fragment>
      <div className='col-lg-6 col-md-12 mt-4 feed-middle-content'>
        <InfiniteScroll
          dataLength={feed.length} //This is important field to render the next data
          next={handleInfiniteScroll}
          hasMore={!props.feedState.end}>
          <Feed {...props} />
        </InfiniteScroll>
        {feedState.loading && <PostLoader />}
      </div>
      <div className='h-100 bg-color-white featured-wrap p-0 mt-4 sticky-side'>
        <AddBankCta />
        <aside className='home-sidebar d-none d-md-block p-4'>
          <div className='aside-inner-wrap'>
            <div className='sidebar-title'>
              <span>Featured Creators</span>
              <img src={images.app.favStarIcon} alt='favourites' />
            </div>
            <div className='d-flex flex-wrap'>
              {/* <FeaturedCeleb  /> */}
              {featuredCreators.map(user => {
                return <FeaturedCeleb key={user.id} user={user} />
              })}
            </div>
          </div>
        </aside>
      </div>
    </React.Fragment>
  )
}

export default FeedView
