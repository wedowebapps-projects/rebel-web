import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import FeedView from './FeedView'
import { resetFeed, fetchFeed, postInteraction, fetchPostComments, reportPost, deletePostAction } from '../../actions/postActions'

export const FeedContainer = props => {
  useEffect(() => {
    props.resetFeed()
    props.fetchFeed({
      page: 1,
      perPage: props.feedState.perPage
    })
  }, [])

  const fetchPosts = () => {
    const params = {
      page: props.feedState.page,
      perPage: props.feedState.perPage
    }
    props.fetchFeed(params)
  }

  return <FeedView {...props} fetchPosts={fetchPosts} />
}

const mapStateToProps = state => ({
  app: state.app,
  auth: state.auth,
  feed: state.posts.feed,
  feedState: state.posts.feedState
})

const mapDispatchToProps = dispatch => ({
  reportPost: (post_id, data) => dispatch(reportPost(post_id, data)),
  deletePost: post_id => new Promise((resolve, reject) => dispatch(deletePostAction(post_id, resolve, reject, null))),
  postInteraction: data =>
    new Promise((resolve, reject) => {
      dispatch(postInteraction(data, resolve, reject))
    }),
  fetchPostComments: post =>
    new Promise((resolve, reject) => {
      dispatch(fetchPostComments(post, resolve, reject))
    }),
  resetFeed: () => dispatch(resetFeed()),
  fetchFeed: data => dispatch(fetchFeed(data)),
})

export default connect(mapStateToProps, mapDispatchToProps)(FeedContainer)
