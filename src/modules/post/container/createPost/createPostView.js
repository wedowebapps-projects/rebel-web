import React from 'react'
import { Modal } from 'src/components/app'
import images from 'src/config/images'
import { Avatar } from 'src/components/shared'
import { Post, Tabs, Poll, Text } from '../../components/createPost'

function CreatePostView(props) {
  const [postType, setPostType] = React.useState('post')
  const { user } = props

  const closeModal = () => {
    props.toggleNewPostModal()
  }

  return (
    <Modal keyboard={true} size='lg' show={true} onClose={closeModal} className='new-post-modal'>
      <div className='text-center'>
        <div className='broadcast-title-head bg-color-white p-3 mb-4 mobile-show new-post-mobile'>
          <div className='d-flex align-items-center '>
            <h3 className='m-0'>New post</h3>
          </div>
        </div>
        <div className='colse-welcome-modal' onClick={closeModal}>
          <img src={images.app.cross} />
        </div>
        <Tabs postType={postType} setPostType={setPostType} />
        <div className='media align-items-start flex-mobile-column mt-4'>
          <div className='desktop-show'>
            <Avatar name={user.full_name} avatarUrl={user.avatar} size={48} className='mr-3' />
          </div>
          <div className='media-body w-100 text-left'>
            {postType === 'post' && <Post {...props} />}
            {postType === 'poll' && <Poll {...props} />}
            {postType === 'text' && <Text {...props} />}
          </div>
        </div>
      </div>
    </Modal>
  )
}

export default CreatePostView
