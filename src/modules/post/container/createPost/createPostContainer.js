import React from 'react'
import { connect } from 'react-redux'
import CreatePostView from './createPostView'
import { toggleNewPostModal } from 'src/actions/appActions'
import * as postActions from '../../actions/postActions'

export const CreatePostContainer = props => {
  return <CreatePostView {...props} />
}

const mapStateToProps = state => ({
  user: state.auth.user
})

const mapDispatchToProps = dispatch => {
  return {
    toggleNewPostModal: () => dispatch(toggleNewPostModal()),
    createPost: data =>
      new Promise((resolve, reject) => {
        dispatch(postActions.createPost(data, resolve, reject))
      })
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreatePostContainer)
