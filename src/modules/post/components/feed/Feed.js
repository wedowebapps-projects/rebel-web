import React from 'react'
import images from 'src/config/images'
import FeaturedCeleb from '../FeaturedCeleb'
import Post from '../post'
import Empty from './Empty'

function Feed(props) {
  const showEmpty = !props.feedState.loading && props.feed.length == 0

  const [randomN, setRandomN] = React.useState(1)

  React.useEffect(() => {
    setRandomN(Math.floor(Math.random() * 5) + 1)
  }, [])

  return (
    <React.Fragment>
      {!showEmpty > 0 ? (
        <div>
          {props.feed.map((post, i) => {
            return (
              <React.Fragment key={`${post.id}-${i}`}>
                <Post post={post} key={post.id} {...props} />
                {i + 1 === randomN && (
                  <div className='h-100 p-0 sticky-side featured-wrap featured-wrap-slider'>
                    <aside className='home-sidebar pl-3'>
                      <div className='aside-inner-wrap'>
                        <div className='sidebar-title'>
                          <span>Featured Creators</span>
                          <img src={images.app.favStarIcon} alt='favourites' />
                        </div>
                        <div className='featured-wrap-slider-box d-flex'>
                          {props.app.featuredCreators.map((user, i) => {
                            return <FeaturedCeleb key={`user-${user.id}-${i}`} user={user} />
                          })}
                        </div>
                      </div>
                    </aside>
                  </div>
                )}
              </React.Fragment>
            )
          })}
        </div>
      ) : (
        <Empty title='No post found!' />
      )}
    </React.Fragment>
  )
}

export default Feed
