import React from 'react'
import images from 'src/config/images'

function Tabs(props) {
  const changeTab = tab => {
    if (tab !== props.postType) {
      props.setPostType(tab)
    }
  }

  return (
    <div className='tabing mt-2'>
      <ul className='tabs'>
        <li data-tab='tab-1' className={props.postType == 'post' ? 'current' : ''} onClick={() => changeTab('post')}>
          <img src={images.app.ImageGray} />
          <img src={images.app.ImageGrayActive} />
          <a><span className=''>Create</span> post</a>
        </li>
        <li data-tab='tab-2' className={props.postType == 'poll' ? 'current' : ''} onClick={() => changeTab('poll')}>
          <img src={images.app.chart} />
          <img src={images.app.chart} />
          <a><span className=''>Create</span> poll</a>
        </li>
        <li data-tab='tab-3' className={props.postType == 'text' ? 'current' : ''} onClick={() => changeTab('text')}>
          <img src={images.app.media} />
          <img src={images.app.mediaActive} />
          <a>Text <span className=''>Media</span></a>
        </li>
      </ul>
    </div>
  )
}

export default Tabs
