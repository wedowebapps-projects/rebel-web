import React from 'react'
import { Button, Form, Icon } from 'src/components/shared'
import { textPostColors } from 'src/config'
import { Col, Row } from 'react-bootstrap'
import { FieldArray } from 'formik'
import * as Yup from 'yup'

const ColorPicker = props => {
  return (
    <div className='comment-input-wrap color-editor-wrap'>
      <ul>
        {textPostColors.map(color => {
          const colors = color.color.split('-')
          return (
            <li
              className={color == props.bgColor ? 'active' : ''}
              onClick={() => props.setFieldValue('bgColor', color)}
              data-color={`linear-gradient(90deg, ${colors[0]} 0%, ${colors[1]} 100%)`}
              style={{ background: `linear-gradient(90deg, ${colors[0]} 0%, ${colors[1]} 100%)` }}
            />
          )
        })}
      </ul>
    </div>
  )
}

function Text(props) {
  const [loading, setLoading] = React.useState(false)

  return (
    <Form
      initialValues={{
        title: '',
        allowComments: true,
        price: 0,
        isTips: true,
        post_type: 'text',
        bgColor: textPostColors[0]
      }}
      validationSchema={Yup.object().shape({
        title: Yup.string().required('Title is required')
      })}
      onSubmit={async (values, form) => {
        setLoading(true)
        values.bgColor = values.bgColor.color
        await props.createPost(values)
        setLoading(false)
      }}>
      {props => {
        const colors = props.values.bgColor.color.split('-')
        const isDark = props.values.bgColor.isDark
        return (
          <form
            onSubmit={e => {
              e.preventDefault()
              props.submitForm()
              return false
            }}
            noValidate>
            <div className='media-body w-100 text-left'>
              <ColorPicker setFieldValue={props.setFieldValue} bgColor={props.values.bgColor} />

              <div className='comment-input-wrap'>
                {/* <Form.Field.Textarea placeholder='Write Something' name='title' /> */}
                <div
                  className={`color-editor d-flex justify-content-center align-item-center ${isDark ? 'color-white' : ''}`}
                  style={{ background: `linear-gradient(90deg, ${colors[0]} 0%, ${colors[1]} 100%)` }}
                  contentEditable
                  maxLength='160'
                  placeholder='Write something...'
                  onInput={e => {
                    props.setFieldValue('title', e.target.innerHTML)
                  }}
                  onBlur={e => {
                    props.setFieldValue('title', e.target.innerHTML)
                  }}
                />
              </div>
            </div>
            <div className='d-flex justify-content-between align-items-center py-3 new-post-card-footer'>
              <div className='input-box agree-conditions-radio'>
                <input
                  type='checkbox'
                  id='allowComments'
                  name='allowComments'
                  checked={!props.values.allowComments}
                  onChange={e => {
                    props.setFieldValue('allowComments', !props.values.allowComments)
                  }}
                />
                <label htmlFor='allowComments'>Turn off comments?</label>
              </div>
              <div className='full-width-btn'>
                <Button variant='primary' isWorking={loading} medium disabled={!props.isValid} type className='create-post mb-0 mr-0'>
                  Post
                </Button>
              </div>
            </div>
          </form>
        )
      }}
    </Form>
  )
}

export default Text
