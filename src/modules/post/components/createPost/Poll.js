import React from 'react'
import { Button, Form, Icon, Avatar } from 'src/components/shared'
import { Col, Row, Container } from 'react-bootstrap'
import { FieldArray } from 'formik'
import * as Yup from 'yup'
import images from 'src/config/images'
import { pollExpiry } from 'src/config'

function Poll(props) {
  const [loading, setLoading] = React.useState(false)

  const { user } = props

  return (
    <Form
      initialValues={{
        title: '',
        allowComments: true,
        price: 0,
        isTips: false,
        post_type: 'poll',
        pollOptions: ['', ''],
        finish_in_days: 1
      }}
      validationSchema={Yup.object().shape({
        title: Yup.string().required('Comment is required'),
        finish_in_days: Yup.number().required(),
        pollOptions: Yup.array()
          .of(Yup.string().required('Poll options is required'))
          .min(2, 'Minimum of 2 options')
      })}
      onSubmit={async (values, form) => {
        setLoading(true)
        await props.createPost(values)
        setLoading(false)
      }}>
      {props => {
        return (
          <form
            onSubmit={e => {
              e.preventDefault()
              props.submitForm()
              return false
            }}
            noValidate>
            <div className='media-body w-100 text-left'>
              <div className='d-flex comment-input-wrap w-100 mb-4'>
                <Avatar name={user.full_name} avatarUrl={user.avatar} size={48} className='mr-3 mobile-avatar' />
                <Form.Field.Textarea maxLength='160' placeholder='Ask a question..' name='title' className='w-100' />
              </div>
              <Container>
                <Row>
                  <Col className='p-0 mt-3' xs='12' sm='6' lg='6'>
                    <FieldArray
                      name='pollOptions'
                      render={arrayHelpers => (
                        <>
                          {props.values.pollOptions.map((option, i) => (
                            <div className='poll-input-wrap' key={i}>
                              <Form.Field.Input name={`pollOptions.${i}`} placeholder={`Option ${i + 1}`} className='poll-input' />
                              {props.values.pollOptions.length > 2 && (
                                <span
                                  className='poll-input-cancel'
                                  tabIndex='-1'
                                  onClick={() => {
                                    arrayHelpers.remove(i)
                                  }}>
                                  <Icon type='close' size={24} />
                                </span>
                              )}
                            </div>
                          ))}
                        </>
                      )}
                    />
                    <div
                      className='add-opt mt-4'
                      onClick={() => {
                        props.setFieldValue('pollOptions', [...props.values.pollOptions, ''])
                        setTimeout(() => {
                          const lastOption = document.querySelector(`[name="pollOptions.${props.values.pollOptions}"]`)
                          lastOption && lastOption.focus()
                        })
                      }}>
                      <i className='fa fa-plus' aria-hidden='true' /> Add option
                    </div>
                  </Col>
                  <Col className='' xs='12' sm='6' lg='6'>
                    <Form.Field.Select name='finish_in_days' options={pollExpiry} label='Duration' className='pt-0' />
                  </Col>
                </Row>
              </Container>
            </div>
            <div className='d-flex justify-content-between align-items-center py-3 new-post-card-footer'>
              <div className='input-box agree-conditions-radio'>
                <input
                  type='checkbox'
                  id='allowComments'
                  name='allowComments'
                  checked={!props.values.allowComments}
                  onChange={e => {
                    props.setFieldValue('allowComments', !props.values.allowComments)
                  }}
                />
                <label htmlFor='allowComments'>Turn off comments?</label>
              </div>

              <div className='full-width-btn'>
                <Button variant='primary' medium isWorking={loading} disabled={!props.isValid} type className='create-post mb-0 mr-0'>
                  Post
                </Button>
              </div>
            </div>
          </form>
        )
      }}
    </Form>
  )
}

export default Poll
