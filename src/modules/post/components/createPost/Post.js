import React, { Component } from 'react'
import { Form, Button, Avatar } from 'src/components/shared'
import images from 'src/config/images'
import { AllowedMediaTypes, VideoMimes, ImageMimes } from 'src/config'
import { upload, uploadSingle, deleteSingle } from 'utils/s3'
import * as Yup from 'yup'
import imageCompression from 'browser-image-compression'

import ProgressBar from 'react-bootstrap/ProgressBar'

const compressionOptions = {
  maxSizeMB: 1,
  maxWidthOrHeight: 1024,
  maxIteration: 20
}

export default class Post extends Component {
  constructor() {
    super()
    this.state = {
      files: [],
      mediaFiles: [],
      isFormSubmitting: false,
      bgColor: null
    }
  }

  previewFiles(e) {
    const files = e.target.files
    for (const file of files) {
      if (ImageMimes.includes(file.type)) {
        const reader = new FileReader()
        reader.onloadend = () => {
          file.dataUrl = reader.result
          this.setState({
            files: [...this.state.files, file]
          })
        }
        reader.readAsDataURL(file)
      } else {
        file.dataUrl = URL.createObjectURL(file)
        this.setState({
          files: [...this.state.files, file]
        })
      }
    }
  }

  async uploadFile(e) {
    const files = e.target.files

    for (const file of files) {
      if (ImageMimes.includes(file.type)) {
        const reader = new FileReader()
        reader.onloadend = () => {
          file.dataUrl = reader.result
          file.progress = 0
          file.isUploading = false
          file.isUploaded = false
          this.setState(
            {
              files: [...this.state.files, file]
            },
            () => {
              this.startUpload()
            }
          )
        }
        reader.readAsDataURL(file)
      } else {
        file.dataUrl = URL.createObjectURL(file)
        file.progress = 0
        file.isUploading = false
        file.isUploaded = false
        this.setState(
          {
            files: [...this.state.files, file]
          },
          () => {
            this.startUpload()
          }
        )
      }
    }
  }

  async startUpload() {
    // console.log(this.props);
    // return false
    let { files } = this.state
    const username = this.props.user.username
    if (files.length > 0) {
      let uploadedFiles = []
      try {
        this.setState({ isFormSubmitting: true })
        const compressedFiles = await Promise.all(
          files.map(async (imageFile, index) => {
            if (imageFile.isUploaded == false && imageFile.isUploading == false) {
              files[index].isUploading = true
              this.setState({ files })
              if (ImageMimes.includes(imageFile.type)) {
                imageFile = await imageCompression(imageFile, compressionOptions)
                const fileUpload = await uploadSingle(imageFile, username, 'posts', progress => {
                  files[index].progress = progress
                  this.setState({ files })
                })
                if (files[index]) {
                  files[index].isUploading = false
                  files[index].isUploaded = true
                  this.setState({ files })
                  this.setState({
                    mediaFiles: [
                      ...this.state.mediaFiles,
                      {
                        type: ImageMimes.includes(fileUpload.type) ? 'photo' : VideoMimes.includes(fileUpload.type) ? 'video' : '',
                        media_path: fileUpload.location
                      }
                    ]
                  })
                }
              } else {
                const fileUpload = await uploadSingle(imageFile, username, 'posts', progress => {
                  files[index].progress = progress
                  this.setState({ files })
                })
                if (files[index]) {
                  files[index].isUploading = false
                  files[index].isUploaded = true
                  this.setState({ files })
                  this.setState({
                    mediaFiles: [
                      ...this.state.mediaFiles,
                      {
                        type: ImageMimes.includes(fileUpload.type) ? 'photo' : VideoMimes.includes(fileUpload.type) ? 'video' : '',
                        media_path: fileUpload.location
                      }
                    ]
                  })
                }
              }
            }
          })
        )
      } catch (err) {
        console.log('error', err)
        // toast.error('Something went wrong. please try again later.')
      }
      this.setState({ isFormSubmitting: false })
    }
  }

  async removeMedia(i, file) {
    const username = this.props.user.username
    const { files, mediaFiles } = this.state
    await deleteSingle(file, username, 'posts')
    files[i].isUploading = false
    files.splice(i, 1)
    mediaFiles.splice(i, 1)
    this.setState({ files })
    this.setState({ mediaFiles })
  }

  render() {
    const { files, isFormSubmitting, bgColor, mediaFiles } = this.state
    const { user } = this.props

    return (
      <Form
        initialValues={{
          title: '',
          allowComments: true,
          mediaFiles: [],
          price: 0,
          isTips: true,
          post_type: 'post'
        }}
        validationSchema={Yup.object().shape({
          title: Yup.string().required('Comment is required')
        })}
        onSubmit={async (values, form) => {
          this.setState({ isFormSubmitting: true })
          values.mediaFiles = this.state.mediaFiles
          await this.props.createPost(values)
          this.setState({ isFormSubmitting: false })
        }}>
        {props => {
          const isValid = this.state.mediaFiles.length && props.isValid
          return (
            <form
              onSubmit={e => {
                e.preventDefault()
                props.submitForm()
                return false
              }}
              noValidate>
              <div className='media-body w-100 text-left'>
                <div className='d-flex comment-input-wrap w-100'>
                  <Avatar name={user.full_name} avatarUrl={user.avatar} size={48} className='mr-3 mobile-avatar' />
                  <Form.Field.Textarea maxLength='160' placeholder='Write Something' name='title' className='w-100' />
                </div>
                <div className='d-flex flex-wrap mt-4 media-full-wrap'>
                  <input
                    style={{ display: 'none' }}
                    type='file'
                    id='new-image'
                    multiple
                    name='new-image'
                    accept={AllowedMediaTypes.join(',')}
                    onChange={e => this.uploadFile(e)}
                    disabled={this.state.isFormSubmitting}
                  />
                  {files.map((file, i) => (
                    <div className='media-wrap mb-3 d-flex align-items-center justify-content-center' key={i}>
                      {file.isUploading && <ProgressBar striped variant='success' animated={true} label={file.progress + '%'} now={file.progress} />}
                      {file.isUploaded && <ProgressBar striped={false} variant='success' animated={false} label={'Uploaded!'} now={100} />}
                      {ImageMimes.includes(file.type) && <img src={file.dataUrl} alt={i} />}
                      {VideoMimes.includes(file.type) && (
                        <video controls width='100%' style={{ maxHeight: '100%', maxWidth: '100%' }}>
                          <source src={file.dataUrl} type={file.type} />
                          Your browser does not support the video tag.
                        </video>
                      )}
                      <div className='remove-media' onClick={() => this.removeMedia(i, file)}>
                        <img src={images.app.close} />
                      </div>
                    </div>
                  ))}

                  <label htmlFor='new-image' className='media-wrap mb-3 d-flex align-items-center justify-content-center'>
                    <div className='plus-icon-wrap'>
                      <img src={images.app.plus} />
                    </div>
                  </label>
                </div>
                <div className='d-flex justify-content-between price-view-wrp'>
                  <span className='alpha50 color-dark'>Price to view:</span>
                  <div className='d-flex align-items-center'>
                    <strong className="naira-currency">₦</strong>
                    <Form.Field.Input type="number"  className='ml-2' name='price' style={{ width: 100 }} />
                  </div>
                </div>
              </div>
              <div className='d-flex justify-content-between align-items-center py-3 new-post-card-footer'>
                <div className='input-box agree-conditions-radio'>
                  <input
                    type='checkbox'
                    id='allowComments'
                    name='allowComments'
                    checked={!props.values.allowComments}
                    onChange={e => {
                      props.setFieldValue('allowComments', !props.values.allowComments)
                    }}
                  />
                  <label htmlFor='allowComments'>Turn off comments?</label>
                </div>

                <div className='full-width-btn'>
                  <Button variant='primary' isWorking={this.state.isFormSubmitting} medium disabled={!isValid} className='create-post mb-0 mr-0'>
                    Post
                  </Button>
                </div>
              </div>
            </form>
          )
        }}
      </Form>
    )
  }
}
