import React from 'react'
import ContentLoader, { Facebook, Instagram } from 'react-content-loader'
import { connect } from 'react-redux'

function PostLoader(props) {
  const backgroundColor = props.theme == 'dark' ? '#2A9DF420' : '#2A9DF430'
  const foregroundColor = props.theme == 'dark' ? '#2A9DF435' : '#2A9DF445'

  return (
    <div className='feed-card sm-card mb-5 bg-color-white'>
      <React.Fragment>
        <ContentLoader speed={2} viewBox='0 0 100% 40' height={40} width={'100%'} backgroundColor={backgroundColor} foregroundColor={foregroundColor}>
          <rect x='55' y='5' rx='3' ry='3' width='120' height='10' />
          <rect x='55' y='26' rx='3' ry='3' width='80' height='10' />
          <circle cx='20' cy='20' r='20' />
        </ContentLoader>
        <div className='mt-4'>
          <ContentLoader speed={2} viewBox='0 0 100% 300' height={300} width={'100%'} backgroundColor={backgroundColor} foregroundColor={foregroundColor}>
            <rect x='0' y='0' rx='3' ry='3' width={'100%'} height='200' />
            <rect x='0' y='220' rx='3' ry='3' width={'100%'} height='4' />
            <rect x='0' y='280' rx='3' ry='3' width={'100%'} height='4' />
            <rect x='60' y='246' rx='3' ry='3' width='60' height='10' />
            <rect x={'45%'} y='246' rx='3' ry='3' width='60' height='10' />
            <rect x={'77%'} y='240' rx='3' ry='3' width='84' height='28' />
            <rect x='60' y='246' rx='3' ry='3' width='60' height='10' />
            <circle cx='36' cy='252' r='14' />
            <circle cx={'39%'} cy='252' r='14' />
          </ContentLoader>
        </div>
      </React.Fragment>
    </div>
  )
}

const mapStateToProps = state => ({
  theme: state.app.theme
})

const mapDispatchToProps = {}

export default connect(mapStateToProps, mapDispatchToProps)(PostLoader)
