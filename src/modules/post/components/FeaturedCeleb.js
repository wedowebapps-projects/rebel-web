import React from 'react'
import PropTypes from 'prop-types'
import Link from 'next/link'

import images from 'src/config/images'

const FeaturedCeleb = props => {
  return (
    <Link href='/[username]' as={`/${props.user.username}`}>
      <div className='card suggestion-card-fluid' style={{ cursor: 'pointer' }}>
        {props.user.cover_image ? (
          <div className='card-img'>
            <img src={props.user.cover_image} alt={props.user.name} />
            {/* {props.user.subscribed && <button className='btn subscrib-btn'>Subscribed</button>} */}
          </div>
        ) : (
          <div className='card-img'>
            <img src={images.app.featuredPlaceholder} alt={props.user.name} />
            {/* {props.user.subscribed && <button className='btn subscrib-btn'>Subscribed</button>} */}
          </div>
        )}
        <div>
          <div className='media align-items-center'>
            <div className='user-avtar avtar-64 mt--30'>
              <img src={props.user.avatar} alt={props.user.name} />
            </div>
            <div className='media-body text-left ml-3 mb-3'>
              <h5 className='mb-1'>
                <a href='#' className='mr-2'>
                  {props.user.first_name} {props.user.last_name}
                </a>
                <img src={images.app.verifiedIcon} alt={props.user.name} />
              </h5>
              <p className='mt-0'>@{props.user.username} </p>
            </div>
          </div>
        </div>
      </div>
    </Link>
  )
}

// FeaturedCeleb.propTypes = {
//   user: {
//     name: PropTypes.string,
//     username: PropTypes.string,
//     avatar: PropTypes.string,
//     cover: PropTypes.string,
//     subscribed: PropTypes.bool,
//   },
// }

export default FeaturedCeleb
