import React from 'react'
import PropTypes from 'prop-types'
import images from 'src/config/images'
import { Spinner } from 'src/components/shared/index'
import { getUserToken } from 'src/utils/storageUtils'

const propTypes = {
  className: PropTypes.string,
  hideText: PropTypes.bool,
  post: PropTypes.object
}

const defaultProps = {
  className: undefined,
  hideText: false,
  post: {}
}

const PostFooter = ({ post, ...props }) => {
  const token = getUserToken()
  const [commentsLoading, setCommentsLoading] = React.useState(false)
  const personal_info = props.auth?.user?.personal_info ? JSON.parse(props.auth.user.personal_info) : {}

  const likePost = e => {
    e.preventDefault()
    const data = { type: post.isLiked ? 'unlike' : 'like' }
    props.postInteraction(data)
  }

  const showComments = e => {
    e.preventDefault()
    if (post.commentsState) {
      const data = { type: 'toggle-show-comments' }
      props.postInteraction(data)
    } else {
      setCommentsLoading(true)
      props
        .fetchPostComments(post, props.activeTab)
        .then(res => {
          setCommentsLoading(false)
        })
        .catch(err => {
          setCommentsLoading(false)
        })
    }
  }

  return (
    <div className='feed-social'>
      <div className='d-flex justify-content-between align-items-center py-3'>
        <div className='feed-like d-flex cursor-pointer'>
          <div className='like-img mr-3' onClick={e => (token ? likePost(e) : null)}>
            <span>{post.isLiked ? <img src={images.app.like} alt='Like' /> : <img src={images.app.heartBlank} alt='Like' />}</span>
          </div>
          <span className='likes-number-wrp'>
            <span className='likes-number'>{post.likesCount}</span>
            {!props.hideText && <React.Fragment>{post.likesCount > 1 ? ' Likes' : ' Like'}</React.Fragment>}
          </span>
        </div>
        {post.allowComments && (
          <div className='feed-comment d-flex cursor-pointer' onClick={e => (token ? showComments(e) : null)}>
            <div className='comment-img mr-3'>{commentsLoading ? <Spinner color='#2A9DF4' size={20} /> : <img src={images.app.comment} alt='comment' />}</div>
            <span className='comment-number-wrp'>
              <span className='comment-number'>{post.commentsCount}</span>
              {!props.hideText && <React.Fragment>{post.commentsCount > 1 ? ' Comments' : ' Comment'}</React.Fragment>}
            </span>
          </div>
        )}
        {post.isTips && post.isSubscribed && personal_info?.bank && props.auth?.user?.personal_info_status === 'approved' && !props.meta.isMyPost ? (
          <div className='tip-btn-wrp'>
            <button
              className='tip-btn'
              onClick={e => {
                e.preventDefault()
                if (!post.isTipsAdded) props.setTipModalVisible(true)
              }}
              data-modal='sendTipModal'>
              Send Tip <strong className='naira-currency'>₦</strong>
            </button>
          </div>
        ) : null}
      </div>
    </div>
  )
}

PostFooter.propTypes = propTypes
PostFooter.defaultProps = defaultProps

export default PostFooter
