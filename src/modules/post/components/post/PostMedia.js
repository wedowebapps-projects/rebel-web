import React, { Component } from 'react'
import { Image, Row, Col } from 'react-bootstrap'
import Container from 'react-bootstrap/Container'
import Modal from './Modal'
import PropTypes from 'prop-types'
import ReactPlayer from 'react-player'

class post_media extends Component {
  static defaultProps = {
    post_media: [],
    hideOverlay: false,
    renderOverlay: () => '',
    overlayBackgroundColor: '#222222',
    countFrom: 5
  }

  constructor(props) {
    super(props)

    this.state = {
      modal: false,
      countFrom: props.countFrom > 0 && props.countFrom < 5 ? props.countFrom : 5,
      conditionalRender: false
    }

    this.openModal = this.openModal.bind(this)
    this.onClose = this.onClose.bind(this)
  }

  openModal(index) {
    // const { post_media } = this.props
    // this.props.setModalVisible(true)
    // this.props.setModalVisible(true);
    this.props.openModal(index)
    // if (post_media[index].type !== 'video') {
    //   this.setState({ modal: true, url: post_media[index], index })
    // }
  }

  onClose() {
    this.setState({ modal: false })
  }

  renderOne() {
    const { post_media } = this.props
    const { countFrom } = this.state
    const overlay = post_media.length > countFrom && countFrom == 1 ? this.renderCountOverlay(true) : this.renderOverlay()

    return (
      <Container>
        <Row>
          <Col xs={12} md={12} className={`height-one px-2 grid-img`} onClick={this.openModal.bind(this, 0)}>
            {/* <img src={post_media[0].processed_path} /> */}
            {this.renderMedia(post_media[0])}
            {/* {overlay} */}
          </Col>
        </Row>
      </Container>
    )
  }

  renderTwo() {
    const { post_media } = this.props
    const { countFrom } = this.state
    const overlay = post_media.length > countFrom && [2, 3].includes(+countFrom) ? this.renderCountOverlay(true) : this.renderOverlay()
    const conditionalRender = [3, 4].includes(post_media.length) || (post_media.length > +countFrom && [3, 4].includes(+countFrom))

    return (
      <Container>
        <Row>
          <Col xs={6} md={6} className='height-two px-2 grid-img' onClick={this.openModal.bind(this, conditionalRender ? 1 : 0)}>
            {/* <img src={conditionalRender ? post_media[1].processed_path : post_media[0].processed_path} /> */}
            {this.renderMedia(conditionalRender ? post_media[1] : post_media[0])}
            {/* {this.renderOverlay()} */}
          </Col>
          <Col xs={6} md={6} className='height-two px-2 grid-img' onClick={this.openModal.bind(this, conditionalRender ? 2 : 1)}>
            {/* <img src={conditionalRender ? post_media[2].processed_path : post_media[1].processed_path} /> */}
            {this.renderMedia(conditionalRender ? post_media[2] : post_media[1])}
            {overlay}
          </Col>
        </Row>
      </Container>
    )
  }

  renderThree(more) {
    const { post_media } = this.props
    const { countFrom } = this.state
    const overlay = !countFrom || countFrom > 5 || (post_media.length > countFrom && [4, 5].includes(+countFrom)) ? this.renderCountOverlay(true) : this.renderOverlay(conditionalRender ? 3 : 4)
    const conditionalRender = post_media.length == 4 || (post_media.length > +countFrom && +countFrom == 4)

    return (
      <Container>
        <Row className=''>
          <Col xs={4} md={4} className='height-three px-2 grid-img' onClick={this.openModal.bind(this, conditionalRender ? 1 : 2)}>
            {/* <img src={conditionalRender ? post_media[1].processed_path : post_media[2].processed_path} /> */}
            {this.renderMedia(conditionalRender ? post_media[1] : post_media[2])}
            {/* {this.renderOverlay(conditionalRender ? 1 : 2)} */}
          </Col>
          <Col xs={4} md={4} className='height-three px-2 grid-img' onClick={this.openModal.bind(this, conditionalRender ? 2 : 3)}>
            {/* <img src={conditionalRender ? post_media[2].processed_path : post_media[3].processed_path} /> */}
            {this.renderMedia(conditionalRender ? post_media[2] : post_media[3])}
            {/* {this.renderOverlay(conditionalRender ? 2 : 3)} */}
          </Col>
          <Col xs={4} md={4} className='height-three px-2 grid-img' onClick={this.openModal.bind(this, conditionalRender ? 3 : 4)}>
            {/* <img src={conditionalRender ? post_media[3].processed_path : post_media[4].processed_path} /> */}
            {this.renderMedia(conditionalRender ? post_media[3] : post_media[4])}
            {overlay}
          </Col>
        </Row>
      </Container>
    )
  }

  renderOverlay(id) {
    const { hideOverlay, renderOverlay, overlayBackgroundColor } = this.props

    if (hideOverlay) {
      return false
    }

    return [
      <div key={`cover-${id}`} className='cover slide' style={{ backgroundColor: 'transparent' }}></div>,
      <div key={`cover-text-${id}`} className='cover-text slide animate-text' style={{ fontSize: '100%' }}>
        {renderOverlay()}
      </div>
    ]
  }

  renderMedia(post) {
    return (
      <React.Fragment>
        {post.type === 'video' ? (
          <ReactPlayer
            url={post.processed_path}
            width='100%'
            height='100%'
            controls={true}
            config={{
              file: {
                playerVars: { forceHLS: true }
              }
            }}
          />
        ) : (
          <img src={post.processed_path} />
        )}
      </React.Fragment>
    )
  }

  renderCountOverlay(more) {
    const { post_media } = this.props
    const { countFrom } = this.state
    const extra = post_media.length - (countFrom && countFrom > 5 ? 5 : countFrom)

    return [
      more && <div key='count' className='cover'></div>,
      more && (
        <div key='count-sub' className='cover-text' style={{ fontSize: '200%' }}>
          <p>+{extra}</p>
        </div>
      )
    ]
  }

  render() {
    const { modal, index, countFrom } = this.state
    const { post_media } = this.props
    const post_mediaToShow = [...post_media]
    if (countFrom && post_media.length > countFrom) {
      post_mediaToShow.length = countFrom
    }

    return (
      <div className={`feed-post-wrap feed-grid pb-3 post-media posts-${post_media.length > 5 ? 5 : post_media.length}`}>
        {[1, 3, 4].includes(post_mediaToShow.length) && this.renderOne()}
        {post_mediaToShow.length >= 2 && post_mediaToShow.length != 4 && this.renderTwo()}
        {post_mediaToShow.length >= 4 && this.renderThree()}
        {modal && <Modal onClose={this.onClose} index={index} post_media={post_media} />}
      </div>
    )
  }
}

post_media.propTypes = {
  post_media: PropTypes.array.isRequired,
  hideOverlay: PropTypes.bool,
  renderOverlay: PropTypes.func,
  overlayBackgroundColor: PropTypes.string,
  countFrom: PropTypes.number
}

export default post_media
