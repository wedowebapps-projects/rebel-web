import React from 'react'
import { checkAndRedirectLogin, constants } from 'src/utils/helpers'
import images from 'src/config/images'
import { useRouter } from 'next/router'
import { followUserApi } from 'src/modules/profile/api/profileApi'
import { Modal } from 'src/components/app'

function PostLocked({ notSubscribed, notPurchased, post, ...props }) {
  let profile = props.profile ? props.profile : post.user
  const [isModalVisible, setIsModalVisible] = React.useState(false)
  const router = useRouter()
  const openPurchasePostPrompt = e => {
    if (e) e.preventDefault()
    if (checkAndRedirectLogin()) {
      props.togglePurchasePostModal(true, post, router?.query?.username)
    }
  }

  const plans = profile.plans ? profile.plans : []
  const subscribeUser = async e => {
    e.preventDefault()
    if (checkAndRedirectLogin()) {
      if (plans.length === 0) {
        setIsModalVisible(true)
      } else {
        props.toggleSubscriptionModal()
      }
    } else {
      router.push(`/login${router.asPath !== '/login' && router.asPath !== '/' ? `?return=${router.asPath}` : ''}`)
    }
  }

  return (
    <>
      <div className='feed-body'>
        <div className='d-flex feed-post-wrap pb-3'>
          <div className='d-flex w-100'>
            <div className='sky-bg w-100 d-flex flex-column align-items-center justify-content-center h-250 mt-2 purchage-post'>
              <img src={images.app.lockedPost} alt='Lock' />
              {!notPurchased && notSubscribed && (
                <a className='btn sky-bg primary-color subscribe-btn subscribe-btn mt-4' onClick={subscribeUser}>
                  Subscribe to view this post.
                </a>
              )}
              {notPurchased && (
                <a className='btn sky-bg primary-color subscribe-btn subscribe-btn mt-4' onClick={openPurchasePostPrompt}>
                  Purchase this post at {constants.NAIRA}
                  {post.price}
                </a>
              )}
            </div>
          </div>
        </div>
      </div>
      <Modal keyboard={true} size='md' onClose={() => setIsModalVisible(false)} show={isModalVisible}>
        <div className='d-flex mb-2 justify-content-between align-items-center flex-column'>
          <div className='colse-welcome-modal' onClick={() => setIsModalVisible(false)}>
            <img src={images.app.cross} alt='cross' />
          </div>
          <h3 className='mt-4'>Subscribe For Free</h3>
          <a
            href={null}
            title=''
            className='btn primary-bg-color color-white mt-2'
            onClick={async () => {
              await followUserApi(profile.id, { isFree: true })
              location.reload()
            }}>
            Subscribe
          </a>
        </div>
      </Modal>
    </>
  )
}

export default PostLocked
