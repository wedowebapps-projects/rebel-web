import React from 'react'
import { textPostColors } from 'src/config'
import { decodeHTMLEntities } from '../../utils/postFunctions'

function PostText({ post }) {
  let colorOrig = textPostColors.filter(col1 => {
    return col1.color == post.bgColor
  })
  colorOrig = colorOrig.length ? colorOrig[0] : textPostColors[0]

  let colors = colorOrig.color.split('-')

  const title = decodeHTMLEntities(post.title)

  return (
    <div className='d-flex feed-post-wrap pb-3 text-post'>
      <div className='d-flex w-100'>
        <div className={`color-editor mt-4 w-100 ${colorOrig.isDark ? 'color-white' : 'color-dark'}`} style={{ background: `linear-gradient(90deg, ${colors[0]} 0%, ${colors[1]} 100%)` }}>
          <h2 className={`text-post-content ${colorOrig.isDark ? 'color-white' : 'color-dark'}`}>{title}</h2>
        </div>
      </div>
    </div>
  )
}

export default PostText
