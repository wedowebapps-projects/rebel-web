import React from 'react'
import { v4 as uuidv4 } from 'uuid'
import { FlutterWaveButton, closePaymentModal } from 'flutterwave-react-v3'
import { PaystackButton } from 'react-paystack'
import images from 'src/config/images'
import { constants } from 'src/utils/helpers'
import { Modal } from 'src/components/app'

function PurchasePostModal(props) {
  const {
    purchaseModal,
    auth: { user }
  } = props

  const closePurchaseModal = () => {
    props.togglePurchasePostModal(false)
  }
  const handlePaystackSuccessAction = async response => {
    try {
      const data = {
        transaction_id: response.trxref,
        tx_ref: response.transaction
      }
      await props.submitTransaction(purchaseModal.post.id, data, purchaseModal.username)
      closePurchaseModal()
    } catch (error) {
      console.log(error)
    }
  }

  const flutterwaveConfig = {
    public_key: process.env.FLUTTERWAVE_SECRET,
    tx_ref: `post-${purchaseModal.post.id}-${uuidv4()}`,
    amount: purchaseModal.post.price,
    currency: 'NGN',
    payment_options: 'card',
    customer: {
      email: user.email,
      name: `${user.first_name} ${user.last_name}`
    },
    customizations: {
      title: 'Rebelyus.com',
      description: 'Payment for purchase post',
      logo: images.app.logo
    },
    callback: async response => {
      try {
        closePaymentModal()
        await props.submitTransaction(
          purchaseModal.post.id,
          {
            tx_ref: response.tx_ref,
            transaction_id: response.transaction_id
          },
          purchaseModal.username || ''
        )
        closePurchaseModal()
      } catch (error) {
        console.log(error)
      }
    },
    onClose: () => {
      closePurchaseModal()
    }
  }
  const paysackConfig = {
    publicKey: process.env.PAYSTACK_publicKey,
    tx_ref: `post-${purchaseModal.post.id}-${uuidv4()}`,
    amount: purchaseModal.post.price * 100,
    email: user.email,
    onSuccess: reference => handlePaystackSuccessAction(reference),
    onClose: () => closePurchaseModal()
  }

  return (
    <Modal keyboard={true} size='md' onClose={closePurchaseModal} show={purchaseModal.visible}>
      <div className='d-flex mb-2 justify-content-between align-items-center flex-column'>
        <div className='colse-welcome-modal' onClick={closePurchaseModal}>
          <img src={images.app.cross} alt='cross' />
        </div>
        <h3 className='mt-4'>Purchase Post</h3>
        <h2 className='mb-4'>
          {constants.NAIRA} {process.env.PAYMENT_METHOD === 'flutterwave' ? purchaseModal.post.price : purchaseModal.post.price}
        </h2>
        {purchaseModal.post && (
          <>
            {process.env.PAYMENT_METHOD === 'flutterwave' ? (
              <FlutterWaveButton {...flutterwaveConfig} text={'Make Payment'} className='btn btn-success rounded px-4' />
            ) : (
              <PaystackButton {...paysackConfig} text={'Make Payment'} className='btn btn-success rounded px-4' />
            )}
          </>
        )}
      </div>
    </Modal>
  )
}

export default PurchasePostModal
