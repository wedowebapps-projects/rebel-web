import React, { Component } from 'react'
import { Form } from 'react-bootstrap'
import TextareaAutosize from 'react-textarea-autosize'
import { Modal } from 'src/components/app'
import { Button } from 'src/components/shared'
import images from 'src/config/images'

export default class ReportPost extends Component {
  constructor(props) {
    super(props)
    this.state = {
      reasonId: 0,
      btnLoader: '',
      message: ''
    }
    this.submitReport = this.submitReport.bind(this)
  }

  submitReport() {
    const data = {
      reasonId: this.state.reasonId,
      message: this.state.message
    }
    const postId = this.props.post.id
    this.props.reportPost(postId, data)
    this.props.closeReportModal()
  }

  render() {
    const { reportReasons } = this.props
    const { btnLoader } = this.state

    return (
      <div>
        {this.props.renderReportModal && (
          <Modal keyboard={true} size='md' show={this.props.renderReportModal} onClose={this.props.closeReportModal}>
            <div className='text-start p-2'>
              <div className=' bg-color-white pb-3'>
                <div className='d-flex align-items-center '>
                  <h3 className='m-0'>Report post</h3>
                </div>
              </div>
              <div className='colse-welcome-modal' onClick={this.props.closeReportModal}>
                <img src={images.app.cross} />
              </div>
              {reportReasons.map(item => {
                return (
                  <div key={`report-reason-${item.id}`}>
                    <Form.Check
                      custom
                      type='radio'
                      name='reportReason'
                      defaultChecked={1}
                      label={item.reason}
                      data-reason={item.id}
                      id={`report-reason-${item.id}`}
                      onChange={() => {
                        this.setState({
                          reasonId: item.id
                        })
                      }}
                    />
                  </div>
                )
              })}
              <div style={{ height: 20 }} />
              <TextareaAutosize
                placeholder='Type your message...'
                minRows={2}
                autoFocus
                onChange={e => {
                  this.setState({
                    message: e.target.value
                  })
                }}
              />
              <div className='d-flex justify-content-end align-items-center pt-2 '>
                <Button variant='primary' onClick={this.submitReport} disabled={btnLoader === 'follow'} medium>
                  Report
                </Button>
              </div>
            </div>
          </Modal>
        )}
      </div>
    )
  }
}
