import React from 'react'
import moment from 'moment'
import * as Yup from 'yup'
import Link from 'next/link'
import { Avatar, Spinner, Form } from 'src/components/shared'

function PostComments({ post, ...props }) {
  const { comments } = post
  const [cmtLoading, setCmtLoading] = React.useState(false)
  const [commentsLoading, setCommentsLoading] = React.useState(false)
  const [deletingComment, setDeletingComment] = React.useState(false)
  const [likeComment, setLikeComment] = React.useState(false)

  const loadMoreComments = e => {
    e.preventDefault()
    setCommentsLoading(true)
    props
      .fetchPostComments(post, props.activeTab)
      .then(res => {
        setCommentsLoading(false)
      })
      .catch(err => {
        setCommentsLoading(false)
      })
  }

  // Delete Comment
  const onDeleteComment = async comment_id => {
    setDeletingComment(true)
    let data = { type: 'delete-comment', comment_id }
    await props
      .postInteraction(data)
      .then(res => {
        setDeletingComment(false)
      })
      .catch(err => {
        setDeletingComment(false)
      })
  }

  // Like and dislike Comment
  const onLikeComment = async (comment_id, type) => {
    setLikeComment(true)
    let data = { type: 'like-dislike-comment', likeType: type, comment_id }
    await props
      .postInteraction(data)
      .then(res => {
        setLikeComment(false)
      })
      .catch(err => {
        setLikeComment(false)
      })
  }

  const showMore = post.commentsState ? !post.commentsState.end : false
  return (
    <React.Fragment>
      <div className='new-comment-wrap mt-4'>
        {post.comments.length ? (
          <div className='comments_list'>
            {showMore && (
              <React.Fragment>
                {commentsLoading ? (
                  <div className='view-all-comment cursor-pointer' style={{ textAlign: 'center' }}>
                    <Spinner color='#187BCD' size='24' /> Loading
                  </div>
                ) : (
                  <div className='view-all-comment cursor-pointer' style={{ textAlign: 'center' }} onClick={e => loadMoreComments(e)}>
                    Load more...
                  </div>
                )}
              </React.Fragment>
            )}
            {post.comments.map(comment => {
              return (
                <div className='d-flex mb-3' key={`post-${post.id}-comment-${comment.id}`}>
                  <Link href={`${comment.user.id === props.auth.user.id  ? '/account' : `/${comment.user.username}`}`} to={`${comment.user.id === props.auth.user.id  ? '/account' : `/${comment.user.username}`}`}>
                    <Avatar className='cursor-pointer' name={`${comment.user.first_name} ${comment.user.last_name}`} avatarUrl={comment.user.avatar} />
                  </Link>
                  <div className='ml-3 w-100'>
                    <div className='comment-wrap'>
                      <h4 className='mt-0'>
                        <Link href={`${comment.user.id === props.auth.user.id  ? '/account' : `/${comment.user.username}`}`} to={`${comment.user.id === props.auth.user.id  ? '/account' : `/${comment.user.username}`}`}>
                          <div className='d-inline-flex cursor-pointer'>@{comment.user.username}</div>
                        </Link>
                        <span className='comment-time'>{moment(comment.createdAt).fromNow()}</span>
                        {comment.user.id === props.auth.user.id || post.user.id === props.auth.user.id ? (
                          <>
                            {deletingComment ? (
                              <Spinner color='#187BCD' size='20' />
                            ) : (
                              <span
                                className='comment-time cursor-pointer'
                                onClick={e => {
                                  e.preventDefault()
                                  onDeleteComment(comment.id)
                                }}>
                                Delete
                              </span>
                            )}
                          </>
                        ) : null}
                        {likeComment ? (
                          <Spinner color='#187BCD' size='20' />
                        ) : comment.isLiked ? (
                          <span
                            className='comment-time cursor-pointer'
                            onClick={e => {
                              e.preventDefault()
                              onLikeComment(comment.id, 'unlike')
                            }}>
                            Unlike({comment.likesCount})
                          </span>
                        ) : (
                          <span
                            className='comment-time cursor-pointer'
                            onClick={e => {
                              e.preventDefault()
                              onLikeComment(comment.id, 'like')
                            }}>
                            Like({comment.likesCount})
                          </span>
                        )}
                      </h4>
                      <div className='comment-text'>{comment.comment}</div>
                    </div>
                  </div>
                </div>
              )
            })}
          </div>
        ) : (
          <div className='d-flex justify-content-center mt-4 mb-3'>
            <p style={{ opacity: 0.25 }}>No comments found</p>
          </div>
        )}

        <Form
          initialValues={{
            comment: ''
          }}
          validationSchema={Yup.object().shape({
            comment: Yup.string().required('Comment is required')
          })}
          onSubmit={async (values, form) => {
            values.type = 'comment'
            setCmtLoading(true)
            props
              .postInteraction(values)
              .then(res => {
                setCmtLoading(false)
              })
              .catch(err => {
                setCmtLoading(false)
              })
            form.resetForm({ comment: '' })
          }}>
          {props => {
            return (
              <form
                onSubmit={e => {
                  e.preventDefault()
                  props.submitForm()
                  return false
                }}
                noValidate>
                <div className='comment-input-wrap'>
                  <Form.Field.Input name='comment' autoComplete='off' placeholder='Add a comment' />
                  {!cmtLoading ? <button type='submit'>Post</button> : <Spinner color='#187BCD' className='spinner' size={20} />}
                </div>
              </form>
            )
          }}
        </Form>
      </div>
    </React.Fragment>
  )
}

export default PostComments
