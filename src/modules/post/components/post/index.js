import React from 'react'
import { constants } from 'src/utils/helpers'
import PostHeader from './PostHeader'
import PostMedia from './PostMedia'
import PostFooter from './PostFooter'
import PostComments from './PostComments'
import PostText from './PostText'
import PostPoll from './PostPoll'
import PostModal from './PostModal/index'
import PostLocked from './PostLocked'
import PostTipModal from './PostTipModal'
import PostPurchaseModal from './PostPurchaseModal'
import { submitTransactionAction, togglePurchasePostModalAction } from '../../actions/postActions'
import { connect } from 'react-redux'

function Post(props) {
  const [modalVisible, setModalVisible] = React.useState(false)
  const [tipModalVisible, setTipModalVisible] = React.useState(false)
  const [currentIndex, setCurrentIndex] = React.useState(0)

  const postInteraction = data => {
    return new Promise((resolve, reject) => {
      data.post_id = props.post.id
      props
        .postInteraction(data, props.activeTab)
        .then(res => {
          resolve(res)
        })
        .catch(err => {
          reject(err)
        })
    })
  }

  const openModal = async index => {
    await setCurrentIndex(index)
    setModalVisible(true)
    const data = { type: 'toggle-show-comments' }
    postInteraction(data)
  }

  const showComments = props.post.commentsState ? props.post.commentsState.show : false

  const meta = {
    isMyPost: props.post.user.id === props.auth.user.id,
    isPaidPost: props.post.price > 0 && props.post.isPaid,
    isSubscribed: props.post.isSubscribed || props.post.planPurchased
  }

  const notSubscribed = !meta.isMyPost && !meta.isSubscribed && !meta.isPaidPost
  const notPurchased = !meta.isMyPost && props.post.price > 0 && !meta.isPaidPost
  
  return (
    <div className={`feed-card sm-card mb-5 bg-color-white ${props.post.post_type == 'poll' ? 'vote-card' : ''}`}>
      <PostHeader {...props} notSubscribed={notSubscribed} notPurchased={notPurchased} post={props.post} postInteraction={postInteraction} meta={meta} />
      <div className='feed-body'>
          <p className='mt-2 text-right'>
            {(meta.isPaidPost || props?.post?.price > 0) && (
              <button className='purchased-btn'>
                {meta.isMyPost && `${constants.NAIRA} ${props.post.price || 0}`}
                {!meta.isMyPost && !notPurchased && `Purchased for ${constants.NAIRA} ${props.post.price || 0}`}
                {notPurchased && `Purchase for ${constants.NAIRA} ${props.post.price || 0}`}
              </button>
            )}
            {(!props?.post?.price) && (
              <button className='purchased-btn'>Free</button>
            )}
          </p>

        {props.post.post_type != 'text' && <p className='mt-4 mb-3'>{props.post.title}</p>}

        {(meta.isMyPost || (meta.isSubscribed && !props.post.price) || meta.isPaidPost) && (
          <React.Fragment>
            {props.post.post_type == 'post' && <PostMedia post_media={props.post.post_media} setModalVisible={setModalVisible} openModal={openModal} />}
            {props.post.post_type == 'text' && <PostText post={props.post} />}
            {props.post.post_type == 'poll' && <PostPoll post={props.post} postInteraction={postInteraction} />}
          </React.Fragment>
        )}

        {(notSubscribed || notPurchased) && (
          <React.Fragment>
            <PostLocked notSubscribed={notSubscribed} notPurchased={notPurchased} meta={meta} post={props.post} {...props} />
          </React.Fragment>
        )}
      </div>
      <PostFooter {...props} setTipModalVisible={setTipModalVisible} postInteraction={postInteraction} meta={meta} />
      {showComments && <PostComments {...props} postInteraction={postInteraction} meta={meta} notSubscribed={notSubscribed} notPurchased={notPurchased} />}
      {modalVisible && <PostModal currentIndex={currentIndex} modalVisible={modalVisible} setModalVisible={setModalVisible} {...props} postInteraction={postInteraction} />}
      {<PostTipModal {...props} tipModalVisible={tipModalVisible} setTipModalVisible={setTipModalVisible} />}
      {props.purchaseModal.visible && props.post.id === props.purchaseModal.post.id && <PostPurchaseModal {...props} />}
    </div>
  )
}

const mapStateToProps = state => ({
  purchaseModal: state.posts.purchaseModal
})

const mapDispatchToProps = dispatch => ({
  togglePurchasePostModal: (visible, post, username) => dispatch(togglePurchasePostModalAction(visible, post, username)),
  submitTransaction: (post_id, data, isProfile) => new Promise((resolve, reject) => dispatch(submitTransactionAction(post_id, data, isProfile, resolve, reject)))
})

export default connect(mapStateToProps, mapDispatchToProps)(Post)
