import React from 'react'
import { UserMedia } from 'src/components/shared'
import moment from 'moment'
import images from 'src/config/images'
import Dropdown from 'react-bootstrap/Dropdown'
import ReportPost from './ReportPost'
import { getUserToken } from 'src/utils/storageUtils'

const PostHeader = ({ post, type, postInteraction, ...props }) => {
  const token = getUserToken()
  const [renderReportModal, setRenderReportModal] = React.useState(false)
  const time = moment(post.createdAt).fromNow()

  const bookmarkPost = e => {
    if (e) {
      e.preventDefault()
    }
    const data = { type: post.isBookmarked ? 'remove-bookmark' : 'bookmark' }
    postInteraction(data)
  }

  const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
    <a
      href=''
      ref={ref}
      onClick={e => {
        e.preventDefault()
        onClick(e)
      }}>
      {children}
    </a>
  ))

  const onDeletePost = async () => {
    try {
      await props.deletePost(post.id, props?.activeTab)
    } catch (error) {
      console.log(error)
    }
  }

  return (
    <div className='feed-head'>
      <div className='d-flex align-items-center justify-content-between'>
        <UserMedia user={post.user} size={39} />
        <div className='d-flex align-items-center'>
          <div className='post-time mr-4'>{time}</div>
          <p className=' text-right' style={{ maxWidth: '15px', margin: '0 8px' }}>
            {post.price > 0 && <img src={images.app.smLockedPost} />}
          </p>
          {token && (
            <div className='bookmark-wrp mr-2'>
              <a href='#' onClick={e => bookmarkPost(e)}>
                {post.isBookmarked ? <img src={images.app.activeBookmark} /> : <img src={images.app.colorBookmark} />}
              </a>
            </div>
          )}
          <div className='more-settings-wrp'>
            <Dropdown>
              <Dropdown.Toggle as={CustomToggle} id='dropdown-custom-components'>
                <img src={images.app.moreCircle} />
              </Dropdown.Toggle>

              <Dropdown.Menu className='pt-3 pb-3'>
                <Dropdown.Item
                  className='pt-2 pb-2'
                  onSelect={() => {
                    const url = `${process.env.APP_BASE_URL}posts/${post.uuid}`
                    navigator.clipboard.writeText(url)
                    // toast.success('Post URL is copied to your clipboard!')
                  }}>
                  Copy link to post
                </Dropdown.Item>
                {token && (
                  <Dropdown.Item
                    eventKey='2'
                    className='pt-2 pb-2'
                    onSelect={() => {
                      bookmarkPost()
                    }}>
                    {post.isBookmarked ? (
                      <React.Fragment>
                        <img src={images.app.activeBookmark} className='mr-2' /> Remove Bookmark
                      </React.Fragment>
                    ) : (
                      <React.Fragment>
                        <img src={images.app.colorBookmark} className='mr-2' /> Bookmark
                      </React.Fragment>
                    )}
                  </Dropdown.Item>
                )}
                {post.user.id !== props.auth.user.id && post.isSubscribed && (
                  <Dropdown.Item onSelect={() => setRenderReportModal(true)} className='pt-2 pb-2'>
                    Report
                  </Dropdown.Item>
                )}
                {props.meta.isMyPost && (
                  <Dropdown.Item onSelect={onDeletePost} className='pt-2 pb-2'>
                    Delete
                  </Dropdown.Item>
                )}
              </Dropdown.Menu>
            </Dropdown>
          </div>
        </div>
      </div>
      <ReportPost
        {...props}
        renderReportModal={renderReportModal}
        post={post}
        reportReasons={props.app.reportReasons}
        reportPost={props.reportPost}
        closeReportModal={() => setRenderReportModal(false)}
      />
    </div>
  )
}

export default PostHeader
