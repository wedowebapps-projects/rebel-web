import React from 'react'
import { Swiper, SwiperSlide } from 'swiper/react'
import SwiperCore, { Pagination, Navigation } from 'swiper'
import ReactPlayer from 'react-player'
SwiperCore.use([Navigation])

function Media({ post, currentIndex }) {
  const { post_media, id } = post
  // const swiper = React.useRef(null)

  // React.useEffect(() => {
  //   setTimeout(() => {
  //     // swiper.current.update()
  //     console.log('swiper')
  //     console.log(swiper)
  //   }, 1000)
  // }, [])

  return (
    <div className='postSlider swiper-container-horizontal'>
      {post_media.length > 1 && <div className={`swiper-pagination swiper-pagination-${id}`}></div>}
      <Swiper spaceBetween={0} slidesPerView={1} navigation initialSlide={currentIndex} lazy={true} preloadImages={false} observer={'true'}>
        {post_media.map((m, index) => (
          <SwiperSlide key={`${m.processed_path}-${index}`}>
            {m.type === 'photo' && <img src={m.processed_path} alt='' />}
            {m.type === 'video' && (
              <ReactPlayer
                url={m.processed_path}
                width='100%'
                height='100%'
                controls={true}
                config={{
                  file: {
                    playerVars: { forceHLS: true }
                  }
                }}
              />
            )}
          </SwiperSlide>
        ))}
      </Swiper>
    </div>
  )
}

export default Media
