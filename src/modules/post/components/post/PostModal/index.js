import React from 'react'
import { Modal } from 'src/components/app'
import { UserMedia, Spinner } from 'src/components/shared'
import images from 'src/config/images'
import PostFooter from '../PostFooter'
import PostComments from '../PostComments'
import Media from './Media'
function PostModal(props) {
  const [commentsLoading, setCommentsLoading] = React.useState(false)

  const closeModal = () => {
    props.setModalVisible(false)
  }

  React.useEffect(() => {
    if (!props.post.commentsState) {
      setCommentsLoading(true)
      props
        .fetchPostComments(props.post)
        .then(res => {
          setCommentsLoading(false)
        })
        .catch(err => {
          setCommentsLoading(false)
        })
    }
  }, [props])

  const meta = {
    isMyPost: props.post.user.id === props.auth.user.id,
    isPaidPost: props.post.price > 0 && props.post.isPaid,
    isSubscribed: props.post.isSubscribed || props.post.planPurchased
  }
  return (
    <Modal keyboard={true} show={props.modalVisible} onClose={closeModal} className='post-modal'>
      <div className='p-0'>
        <div className='modal-wrp d-flex'>
          <div className='feed-head-mobile'>
            <div className='d-flex align-items-center justify-content-between'>
              <UserMedia user={props.post.user} size={39} />
            </div>
            <div className='post-popup-close colse-welcome-modal' onClick={closeModal}>
                <img src={images.app.close} />
              </div>
            <p className='mt-4 post-details-title'>{props.post.title}</p>
          </div>
          <div className='post-popup-img comman-bg'>
            <Media post={props.post} currentIndex={props.currentIndex} />
          </div>
          <div className='post-details'>
            <div className='feed-head'>
              <div className='d-flex align-items-center justify-content-between'>
                <UserMedia user={props.post.user} size={39} />
              </div>
              <p className='mt-4 post-details-title'>{props.post.title}</p>
              <div className='post-popup-close colse-welcome-modal' onClick={closeModal}>
                <img src={images.app.close} />
              </div>
            </div>
            <PostFooter {...props} postInteraction={props.postInteraction} hideText={true} meta={meta} />
            {commentsLoading ? (
              <div className='d-flex justify-content-center mt-4'>
                <Spinner color='#2A9DF4' size={36} />
              </div>
            ) : (
              <PostComments {...props} postInteraction={props.postInteraction} />
            )}
          </div>
        </div>
      </div>
    </Modal>
  )
}

export default PostModal
