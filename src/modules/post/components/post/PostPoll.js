import React from 'react'
import { Formik, Field, Form } from 'formik'
import { Button } from 'src/components/shared'
import moment from 'moment'

function PollOpen({ post, ...props }) {
  const [loading, setLoading] = React.useState(false)
  return (
    <Formik
      initialValues={{
        poll_option_id: ''
      }}
      onSubmit={async values => {
        values.type = 'vote-poll'
        setLoading(true)
        await props.postInteraction(values)
        setLoading(false)
      }}>
      {({ values }) => (
        <Form>
          {post.post_options.map(pollOption => {
            return (
              <div
                key={`poll-${post.id}-option${pollOption.id}`}
                className={`custom-control custom-radio cursor-pointer mt-4 ${values.poll_option_id == pollOption.id ? 'active' : ''}`}
                htmlFor={`option-${pollOption.id}`}>
                <Field type='radio' name='poll_option_id' value={pollOption.id} id={`option-${pollOption.id}`} className='custom-control-input' />
                <label htmlFor={`option-${pollOption.id}`} className='custom-control-label'>
                  {pollOption.name}
                </label>
              </div>
            )
          })}
          <Button variant='primary' disabled={post.isExpired || post.isVoted || !values.poll_option_id} isWorking={loading} medium type='submit'>
            Vote
          </Button>
        </Form>
      )}
    </Formik>
  )
}

function PollAnswered({ post }) {
  return (
    <div className='row'>
      <div className='col-12'>
        {post.post_options.map(pollOption => {
          const vote_percent = post.votesCount > 0 ? (pollOption.voteCount * 100) / post.votesCount : 0
          return (
            <div className='row d-flex align-items-center' key={`poll-${post.id}-answer-${pollOption.id}`}>
              <div className='col-10'>
                <div className='poll-item'>
                  <div
                    className={`custom-control custom-radio cursor-pointer mt-4 py-3 border-0 color-dark-grey ${pollOption.isVoted ? 'primary-bg-color' : ''}
                `}
                    style={{ width: `${vote_percent}%` }}></div>
                  <div className={`poll-item-label ${pollOption.isVoted ? 'color-white' : ''}`}>{pollOption.name}</div>
                </div>
              </div>
              <div className='col-2'>
                <span>{pollOption.voteCount}</span>
                <small>/{post.votesCount}</small>
              </div>
            </div>
          )
        })}
      </div>
    </div>
  )
}

function PostPoll(props) {
  return (
    <React.Fragment>
      {!props.post.isVoted && !props.post.isExpired ? <PollOpen {...props} /> : <PollAnswered {...props} />}
      <div className='post-time mb-3 mt-3'>
        <span>{props.post.votesCount} Votes</span>
        <i className='dot rounded active-ago-color-vote alpha50' />
        {`${!props.post.isExpired ? 'expires' : 'expired'} ${moment(props.post.finishedAt).fromNow()}`}
      </div>
    </React.Fragment>
  )
}

export default PostPoll
