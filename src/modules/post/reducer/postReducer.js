import * as types from '../actions/postTypes'
import { constants } from 'utils/helpers'

const initialState = {
  feed: [],
  feedState: {
    page: 1,
    perPage: 5,
    loading: false,
    end: false
  },
  purchaseModal: {
    post: {},
    visible: false,
    username: null
  }
}

const postReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.RESET_FEED: 
      return {
        ...state,
        feed: [],
        feedState: {
          page: 1,
          perPage: 5,
          loading: true,
          end: false
        }
      }

    case types.SET_FEED:
      return {
        ...state,
        feed: [...state.feed, ...action.data],
        feedState: {
          ...state.feedState,
          end: action.data.length === state.feedState.perPage ? false : true,
          page: action.data.length === state.feedState.perPage ? state.feedState.page + 1 : state.feedState.page
        }
      }

    case types.APPEND_POST:
      return {
        ...state,
        feed: [action.post, ...state.feed]
      }

    case types.UPDATE_FEED:
      return {
        ...state,
        feed: [...action.data]
      }

    case types.SET_FEED_LOADING:
        return { ...state, feedState: { ...state.feedState, loading: action.loading }
      }

    case types.TOGGLE_PURCHASE_POST_MODAL: {
      console.log(action)
      return {
        ...state,
        purchaseModal: {
          post: action.post || {},
          visible: action.visible,
          username: action.username || null
        }
      }
      }

    case types.SET_PURCHASE_POST: {
      let feed = state.feed
      const postIndex = feed.map(post => post.id).indexOf(action.post_id)
      feed[postIndex].isPaid = true
      return {
        ...state,
        feed
      }
      }

    default:
      return state
  }
}
export default postReducer
