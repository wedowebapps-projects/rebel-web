export function feedInteractionLocal(feed, data, comment) {
  feed.map(post => {
    if (post.id == data.post_id) {
      switch (data.type) {
        case 'vote-poll':
          post.isVoted = true
          post.post_options.map(item => {
            if (item.id == data.poll_option_id) {
              item.isVoted = true
              item.voteCount++
            }
          })
          post.votesCount++
          break

        case 'toggle-show-comments':
          post.commentsState.show = !post.commentsState.show
          break

        case 'comment':
          post.comments.push(comment)
          post.commentsCount++
          break

        case 'delete-comment':
          post.comments = post.comments.filter(res => !(res.id === data.comment_id))
          post.commentsCount--
          break

        case 'like-dislike-comment':
          post.comments = post.comments.map(res =>
            res.id === data.comment_id ? { ...res, isLiked: data.likeType === 'like' ? true : false, likesCount: data.likeType === 'like' ? res.likesCount + 1 : res.likesCount - 1 } : res
          )
          break

        // case 'dislike-comment':
        //   post.comments = post.comments.filter(res => (res.id === data.comment_id ? { ...res, isLiked: false, likesCount: res.likesCount-- } : res))
        //   break

        case 'like':
          post.isLiked = true
          post.likesCount++
          break

        case 'unlike':
          post.isLiked = false
          post.likesCount--
          break

        case 'bookmark':
          post.isBookmarked = true
          break

        case 'remove-bookmark':
          post.isBookmarked = false
          break

        default:
          break
      }
      post.random = Math.random()
    }
  })
  return feed
}

export function decodeHTMLEntities(text) {
  var entities = [
    ['amp', '&'],
    ['apos', "'"],
    ['#x27', "'"],
    ['#x2F', '/'],
    ['#39', "'"],
    ['#47', '/'],
    ['lt', '<'],
    ['gt', '>'],
    ['nbsp', ' '],
    ['quot', '"']
  ]

  for (var i = 0, max = entities.length; i < max; ++i) text = text.replace(new RegExp('&' + entities[i][0] + ';', 'g'), entities[i][1])

  return text
}
