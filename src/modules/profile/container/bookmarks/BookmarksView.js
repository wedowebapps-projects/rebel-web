import React from 'react'
import { constants } from 'src/utils/helpers'
import { BookmarkedPosts, NoBookmarksView } from '../../components/bookmarks'

function BookmarksView(props) {
  React.useEffect(() => {
    if (props.listType.type === 'bookmarks') callFetchFun()
  }, [props.listType])

  const callFetchFun = () => props.fetchPosts({ page: props.bookmark.page, perPage: constants.POSTS_PER_PAGE, bookmarked: true, end: props.listType.count })

  return <>{props.bookmark.posts.length === 0 ? <NoBookmarksView {...props} /> : <BookmarkedPosts {...props} callFetchFun={callFetchFun} />}</>
}

export default BookmarksView
