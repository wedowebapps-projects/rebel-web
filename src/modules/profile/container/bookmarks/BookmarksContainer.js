import React from 'react'
import { connect } from 'react-redux'
import BookmarksView from './BookmarksView'
import * as bookmarkActions from '../../actions/bookmarkActions'
import { fetchListNumbers_api } from 'src/api/appApis'
import _ from 'lodash'
import { deletePostAction, fetchPostComments, postInteraction } from 'src/modules/post/actions/postActions'

export const BookmarksContainer = props => {
  const [listType, setListType] = React.useState({})
  React.useEffect(() => {
    fetchListNumbers_api().then(data => {
      if (data && data.lists) setListType(_.find(data.lists, list => list.type === 'bookmarks') || {})
    })
  }, [])
  return <BookmarksView {...props} listType={listType} />
}

const mapStateToProps = state => ({
  auth: state.auth,
  app: state.app,
  bookmark: state.bookmark
})

const mapDispatchToProps = dispatch => {
  return {
    deletePost: post_id => new Promise((resolve, reject) => dispatch(deletePostAction(post_id, resolve, reject, 'bookmarked'))),
    postInteraction: data =>
      new Promise((resolve, reject) => {
        dispatch(postInteraction(data, 'bookmarked', resolve, reject))
      }),
    fetchPostComments: post =>
      new Promise((resolve, reject) => {
        dispatch(fetchPostComments(post, 'bookmarked', resolve, reject))
      }),
    fetchPosts: (filters, end) => new Promise((resolve, reject) => dispatch(bookmarkActions.fetchBookmarkPosts(filters, end, resolve, reject)))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(BookmarksContainer)
