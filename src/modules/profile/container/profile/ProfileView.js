import _ from 'lodash'
import React from 'react'
import { ProfileBanner, ProfileInfo, ProfileLoader, ProfileTabs, ProfileFeed, ProfileImages, ProfileVideos, ProfileTipModal } from '../../components/profile'
``
function ProfileView(props) {
  const [activeTab, setActiveTab] = React.useState('feed')

  const fetchPosts = () => {
    props.fetchPosts(activeTab)
  }

  React.useEffect(() => {
    fetchPosts()
  }, [activeTab])

  const [modalVisible, setModalVisible] = React.useState(false)
  const [profileTipModalVisible, setProfileTipModalVisible] = React.useState(false)
  const posts = props[activeTab].data
  const state = props[activeTab].state

  return (
    <div className='w-100'>
      {props.isProfileLoading && <ProfileLoader />}
      {!props.isProfileLoading && _.isEmpty(props.profile) && (
        <div className='p-5 bg-color-secondary text-center'>
          <h2>Profile not found</h2>
        </div>
      )}
      {!props.isProfileLoading && props.profile.isBlocked === true && (
        <div className='p-5 bg-color-secondary text-center'>
          <h2>Profile not found</h2>
        </div>
      )}
      {!props.isProfileLoading && props?.auth?.user?.username !== props?.profile?.username && !_.isEmpty(props.profile) && !props?.profile?.isBlocked && (
        <React.Fragment>
          <ProfileBanner profile={props.profile} />
          <ProfileInfo {...props} setProfileTipModalVisible={setProfileTipModalVisible} />
          {<ProfileTipModal {...props} profileTipModalVisible={profileTipModalVisible} setProfileTipModalVisible={setProfileTipModalVisible} />}
          <div className='tabing mt-2'>
            <ProfileTabs {...props} activeTab={activeTab} setActiveTab={setActiveTab} />
            {activeTab == 'feed' && <ProfileFeed activeTab={activeTab} posts={posts} state={state} {...props} fetchPosts={fetchPosts} />}
            {activeTab == 'images' && <ProfileImages posts={posts} state={state} {...props} fetchPosts={fetchPosts} />}
            {activeTab == 'videos' && <ProfileVideos posts={posts} state={state} {...props} fetchPosts={fetchPosts} />}
          </div>
        </React.Fragment>
      )}
    </div>
  )
}

export default ProfileView
