import React from 'react'
import { useRouter } from 'next/router'
import { connect } from 'react-redux'
import * as profileActions from '../../actions/profileActions'
import * as profileTypes from '../../actions/profileTypes'
import ProfileView from './ProfileView'
import { resetFeed, postInteraction, fetchPostComments, reportPost } from 'src/modules/post/actions/postActions'

export const ProfileContainer = props => {
  const router = useRouter()
  let username = router.query.username

  React.useEffect(() => {
    if (props.auth.user.username === username)
    {
      router.push('/account') 
    }
    props.resetProfile()
    props.fetchProfile(username)
    props.resetFeed()
    props.fetchProfilePosts({
      username: username,
      type: 'feed',
      page: 1,
      perPage: props.feed.state.perPage
    })
  }, [username])

  const fetchPosts = type => {
    const state = props[type].state
    if (!state.end) {
      const params = {
        username: username,
        type: type,
        page: state.page,
        perPage: state.perPage
      }
      props.fetchProfilePosts(params)
    }
  }

  return <ProfileView {...props} fetchPosts={fetchPosts} />
}

const mapStateToProps = state => ({
  auth: state.auth,
  app: state.app,
  profile: state.profile.profile,
  isProfileLoading: state.profile.isProfileLoading,
  feed: state.profile.feed,
  images: state.profile.images,
  videos: state.profile.videos,
  is_subscriptionModalVisible: state.profile.is_subscriptionModalVisible
})

const mapDispatchToProps = dispatch => {
  return {
    reportPost: (post_id, data) => dispatch(reportPost(post_id, data)),
    updateProfile: data => dispatch({ type: profileTypes.UPDATE_PROFILE, data }),
    toggleSubscriptionModal: () => dispatch(profileActions.toggleSubscriptionModal()),
    fetchUserAction: data => new Promise((resolve, reject) => dispatch(profileActions.fetchUserAction(data, resolve, reject))),
    resetProfile: () => dispatch(profileActions.resetProfile()),
    fetchProfile: username => dispatch(profileActions.fetchProfile(username)),
    postInteraction: (data, type) =>
      new Promise((resolve, reject) => {
        dispatch(postInteraction(data, type, resolve, reject))
      }),
    fetchPostComments: (post, type) =>
      new Promise((resolve, reject) => {
        dispatch(fetchPostComments(post, type, resolve, reject))
      }),
    resetFeed: () => dispatch(resetFeed()),
    fetchProfilePosts: data => dispatch(profileActions.fetchProfilePosts(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfileContainer)
