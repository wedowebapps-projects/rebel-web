import React from 'react'
import { connect } from 'react-redux'
import * as profileActions from '../../actions/profileActions'
import * as profileTypes from '../../actions/profileTypes'
import MyProfileView from './MyProfileView'
import { resetFeed, postInteraction, fetchPostComments, deletePostAction } from 'src/modules/post/actions/postActions'
import { fetchListNumbers_api } from 'src/api/appApis'
import { useRouter } from 'next/router'

export const ProfileContainer = props => {
  const router = useRouter()
  const [listType, setListType] = React.useState([])
  React.useEffect(() => {
    if (props.auth.user.username)
    {
      props.resetProfile()
      props.fetchProfile(props.auth.user.username)
      props.resetFeed()
      props.fetchProfilePosts({
        username: props.auth.user.username,
        type: 'feed',
        page: 1,
        perPage: props.feed.state.perPage
      })
    }
  }, [props.auth.user.username])

  React.useEffect(() => {
    fetchListData()
  }, [])

  const fetchListData = () => {
    fetchListNumbers_api().then(data => {
      if (data && data.lists) setListType(data.lists)
    })
  }

  const fetchPosts = type => {
    const state = props[type].state
    if (!state.end) {
      const params = {
        username: props.auth.user.username,
        type: type,
        page: state.page,
        perPage: state.perPage
      }
      props.fetchProfilePosts(params)
    }
  }

  return <MyProfileView {...props} fetchPosts={fetchPosts} fetchListData={fetchListData} listType={listType} />
}

const mapStateToProps = state => ({
  auth: state.auth,
  app: state.app,
  profile: state.profile.profile,
  isProfileLoading: state.profile.isProfileLoading,
  feed: state.profile.feed,
  images: state.profile.images,
  videos: state.profile.videos,
  liked: state.profile.liked,
  paid: state.profile.paid,
  fans: state.profile.fans,
  subscriptions: state.profile.subscriptions,
  favorites: state.profile.favorites,
  is_subscriptionModalVisible: state.profile.is_subscriptionModalVisible
})

const mapDispatchToProps = dispatch => {
  return {
    deletePost: (post_id, type) => new Promise((resolve, reject) => dispatch(deletePostAction(post_id, resolve, reject, type))),
    updateProfile: data => dispatch({ type: profileTypes.UPDATE_PROFILE, data }),
    toggleSubscriptionModal: () => dispatch(profileActions.toggleSubscriptionModal()),
    fetchUserAction: data => new Promise((resolve, reject) => dispatch(profileActions.fetchUserAction(data, resolve, reject))),
    resetProfile: () => dispatch(profileActions.resetProfile()),
    fetchProfile: username => dispatch(profileActions.fetchProfile(username)),
    postInteraction: (data, type) =>
      new Promise((resolve, reject) => {
        dispatch(postInteraction(data, type, resolve, reject))
      }),
    fetchPostComments: (post, type) =>
      new Promise((resolve, reject) => {
        dispatch(fetchPostComments(post, type, resolve, reject))
      }),
    resetFeed: () => dispatch(resetFeed()),
    fetchProfilePosts: data => dispatch(profileActions.fetchProfilePosts(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfileContainer)
