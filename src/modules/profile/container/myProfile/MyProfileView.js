import React from 'react'
import {
  ProfileBanner,
  ProfileInfo,
  ProfileLoader,
  ProfileFeed,
  ProfileImages,
  ProfileVideos,
  ProfileActiveFans,
  ProfileActiveSubscriber,
  ProfileFavorites,
  ProfileTabs
} from '../../components/profile'

function ProfileView(props) {
  const [activeTab, setActiveTab] = React.useState('feed')

  const fetchPosts = () => {
    props.fetchPosts(activeTab)
  }

  React.useEffect(() => {
    fetchPosts()
  }, [activeTab])

  const posts = props[activeTab].data
  const state = props[activeTab].state

  return (
    <div className='w-100'>
      {props.isProfileLoading ? (
        <ProfileLoader />
      ) : (
        <React.Fragment>
          <ProfileBanner profile={props.profile} />
          <ProfileInfo {...props} />
          <div className='tabing mt-2 pb-5'>
            <ProfileTabs {...props} activeTab={activeTab} setActiveTab={setActiveTab} />
            {activeTab == 'images' ? (
              <ProfileImages posts={posts} state={state} {...props} fetchPosts={fetchPosts} />
            ) : activeTab == 'videos' ? (
              <ProfileVideos posts={posts} state={state} {...props} fetchPosts={fetchPosts} />
            ) : activeTab == 'fans' ? (
              <ProfileActiveFans posts={posts} state={state} {...props} fetchPosts={fetchPosts} />
            ) : activeTab == 'subscriptions' ? (
              <ProfileActiveSubscriber posts={posts} state={state} {...props} fetchPosts={fetchPosts} />
            ) : activeTab == 'favorites' ? (
              <ProfileFavorites posts={posts} state={state} {...props} fetchPosts={fetchPosts} />
            ) : (
              // activeTab == 'feed' && <ProfileFeed posts={posts} state={state} {...props} fetchPosts={fetchPosts} />
              // activeTab == 'likedPosts' && <ProfileVideos posts={posts} state={state} {...props} fetchPosts={fetchPosts} />
              //activeTab == 'paidPosts' && <ProfileVideos posts={posts} state={state} {...props} fetchPosts={fetchPosts} />}
              <ProfileFeed activeTab={activeTab} posts={posts} state={state} {...props} fetchPosts={fetchPosts} />
            )}
          </div>
        </React.Fragment>
      )}
    </div>
  )
}

export default ProfileView
