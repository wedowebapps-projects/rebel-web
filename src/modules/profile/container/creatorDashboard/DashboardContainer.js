import moment from 'moment'
import { useRouter } from 'next/router'
import React from 'react'
import { connect } from 'react-redux'
import { deletePostAction, fetchPostComments, postInteraction } from 'src/modules/post/actions/postActions'
import * as dashboardActions from '../../actions/dashbordActions'
import DashboardView from './DashboardView'

export const DashboardContainer = props => {
  const router = useRouter()
  const [activeTab, setActiveTab] = React.useState('overview')
  const [dateChange, onDateChange] = React.useState({
    startDate: moment()
      .subtract(31, 'days')
      .format('YYYY-MM-DD'),
    endDate: moment()
      .subtract(1, 'days')
      .format('YYYY-MM-DD')
  })

  const callFetchFun = (tab, dateChange) => {
    if (tab === 'overview') {
      props.fetchOverview({ start_date: dateChange.startDate, end_date: dateChange.endDate })
    } else if (tab === 'contentAnalytics') {
      props.fetchAnalytics({ start_date: dateChange.startDate, end_date: dateChange.endDate })
    }
  }

  React.useEffect(() => {
    if (props.auth.user.personal_info_status !== 'approved') {
      router.push('/')
    }
  }, [props?.auth?.user?.personal_info_status])

  React.useEffect(() => {
    callFetchFun(activeTab, dateChange)
  }, [])

  return <DashboardView {...props} callFetchFun={callFetchFun} dateChange={dateChange} onDateChange={onDateChange} activeTab={activeTab} setActiveTab={setActiveTab} />
}

const mapStateToProps = state => ({
  auth: state.auth,
  app: state.app,
  dashboard: state.dashboard
})

const mapDispatchToProps = dispatch => {
  return {
    deletePost: post_id => new Promise((resolve, reject) => dispatch(deletePostAction(post_id, resolve, reject, 'dashboard'))),
    postInteraction: data =>
      new Promise((resolve, reject) => {
        dispatch(postInteraction(data, 'dashboard', resolve, reject))
      }),
    fetchPostComments: post =>
      new Promise((resolve, reject) => {
        dispatch(fetchPostComments(post, 'dashboard', resolve, reject))
      }),
    fetchOverview: data => dispatch(dashboardActions.fetchOverviewReportAction(data)),
    fetchAnalytics: data => dispatch(dashboardActions.fetchAnalyticsReportAction(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DashboardContainer)
