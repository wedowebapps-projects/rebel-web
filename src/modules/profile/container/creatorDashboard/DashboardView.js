import React from 'react'
import { ContentAnalytics, Overview } from '../../components/creatorDashboard'
import moment from 'moment'
import { RangeDatePicker } from 'react-google-flight-datepicker'

function DashboardView({ activeTab, setActiveTab, dateChange, onDateChange, ...props }) {
  return (
    <div className='wallet-add-card-col w-100 mt-4 mb-4 ml-4 border-wrap'>
      <div className='dashboard-option-title card-option'>
        <div className='dashboard-title'>
          <h3 className='m-0'>Creator Dashboard</h3>
        </div>
        <ul className='tabs'>
          <li
            className={activeTab === 'overview' ? 'current' : ''}
            onClick={() => {
              setActiveTab('overview')
              let start = moment()
                .subtract(31, 'days')
                .format('YYYY-MM-DD')
              let end = moment()
                .subtract(1, 'days')
                .format('YYYY-MM-DD')
              onDateChange({ startDate: start, endDate: end })
              props.callFetchFun('overview', {
                startDate: start,
                endDate: end
              })
            }}>
            Overview
          </li>
          <li
            className={activeTab === 'contentAnalytics' ? 'current' : ''}
            onClick={() => {
              setActiveTab('contentAnalytics')
              let start = moment()
                .subtract(31, 'days')
                .format('YYYY-MM-DD')
              let end = moment()
                .subtract(1, 'days')
                .format('YYYY-MM-DD')
              onDateChange({ startDate: start, endDate: end })
              props.callFetchFun('contentAnalytics', {
                startDate: start,
                endDate: end
              })
            }}>
            Content Analytics
          </li>
        </ul>
      </div>
      <div className='dashboard-tabs'>
        <div className='dashboard-time-limit'>
          <p className='d-flex align-items-center'>
            Showing analytics for:
            <RangeDatePicker
              startDate={dateChange.startDate}
              endDate={dateChange.endDate}
              onChange={(startDate, endDate) => {
                let start = moment(startDate).format('YYYY-MM-DD')
                let end = moment(endDate).format('YYYY-MM-DD')
                onDateChange({ startDate: start, endDate: end })
                props.callFetchFun(activeTab, {
                  startDate: start,
                  endDate: end
                })
              }}
              minDate={new Date(1900, 0, 1)}
              maxDate={new Date()}
              //dateFormat={moment().format('YYYY-MM-DD')}
              monthFormat='MMM YYYY'
              highlightToday
              style={{ background: '#fcfcfc' }}
              startDatePlaceholder='Start Date'
              endDatePlaceholder='End Date'
              disabled={false}
              className='my-own-class-name'
              startWeekDay='monday'
            />
          </p>
        </div>
        {activeTab === 'overview' ? <Overview {...props} /> : activeTab === 'contentAnalytics' ? <ContentAnalytics {...props} /> : null}
      </div>
    </div>
  )
}

export default DashboardView
