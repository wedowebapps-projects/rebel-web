import api from 'src/api'
const _ = require('lodash')
import { convertObjectToQuerystring } from 'src/utils/helpers'
import { getUserToken } from 'src/utils/storageUtils'

export const fetchProfile = (username, token) => {
  if (token) {
    return api(`/user/profile/${username}`, null, 'get', token)
  } else {
    return api(`/open/user/profile/${username}`, null, 'get')
  }
}

export const fetchPostsByUsername = (filters, username) => {
  let filter = ''
  filters.username = username
  if (!_.isEmpty(filters)) {
    filter = `${convertObjectToQuerystring(filters)}`
  }
  const token = getUserToken()
  if (token) {
    return api(`/posts?${filter}`, null, 'get', token)
  } else {
    return api(`/open/user/profile/${username}/posts?${filter}`, null, 'get')
  }
}

export const fetchUserImages = (filters, username) => {
  let filter = ''
  if (!_.isEmpty(filters)) {
    filter = `${convertObjectToQuerystring(filters)}`
  }
  const token = getUserToken()
  if (token) {
    return api(`/user/profile/${username}/images?${filter}`, null, 'get', token)
  } else {
    return api(`/open/user/profile/${username}/images?${filter}`, null, 'get', token)
  }
}

export const fetchUserVideos = (filters, username) => {
  let filter = ''
  if (!_.isEmpty(filters)) {
    filter = `${convertObjectToQuerystring(filters)}`
  }
  const token = getUserToken()
  if (token) {
    return api(`/user/profile/${username}/videos?${filter}`, null, 'get', token)
  } else {
    return api(`/open/user/profile/${username}/videos?${filter}`, null, 'get', token)
  }
}

export const setUserActionApi = data => {
  const token = getUserToken()
  return api(`/user/lists/add`, data, 'post', token, false)
}

export const followUserApi = (id, data) => {
  const token = getUserToken()
  return api(`/user/follow/${id}`, data, 'post', token)
}

export const unFollowUserApi = id => {
  const token = getUserToken()
  return api(`/user/unfollow/${id}`, null, 'post', token)
}

export const submitSubscriptionTransactionApi = (profile_id, data) => {
  const token = getUserToken()
  if (process.env.PAYMENT_METHOD === 'flutterwave') {
    return api(`/user/follow/${profile_id}/paid`, data, 'post', token)
  } else {
    return api(`/user/follow/${profile_id}/paidpaystack`, data, 'post', token)
  }
}

export const blockUserApi = id => {
  const token = getUserToken()
  return api(`/user/blocked/${id}`, null, 'post', token)
}

export const unBlockedUserApi = id => {
  const token = getUserToken()
  return api(`/user/unblocked/${id}`, null, 'post', token)
}

export const removeSubscriptionApi = id => {
  const token = getUserToken()
  if (process.env.PAYMENT_METHOD === 'flutterwave') {
    return api(`/user/remove/subscription/${id}`, null, 'post', token)
  } else {
    return api(`/user/remove/subscriptionpaystack/${id}`, null, 'post', token)
  }
}

export const submitSendUserTipTransactionApi = (user_id, data) => {
  const token = getUserToken()
  return api(`/user/tips/${user_id}`, data, 'post', token)
}
