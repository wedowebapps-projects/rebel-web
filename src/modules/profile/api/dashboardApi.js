import _ from 'lodash'
import api from 'src/api'
import { convertObjectToQuerystring } from 'src/utils/helpers'
import { getUserToken } from 'src/utils/storageUtils'

export const fetchOverviewReport = filters => {
  let filter = ''
  if (!_.isEmpty(filters)) {
    filter = `${convertObjectToQuerystring(filters)}`
  }
  const token = getUserToken()
  return api(`/user/report/overview?${filter}`, null, 'get', token)
}

export const fetchAnalyticsReport = filters => {
  let filter = ''
  if (!_.isEmpty(filters)) {
    filter = `${convertObjectToQuerystring(filters)}`
  }
  const token = getUserToken()
  return api(`/user/report/analytics?${filter}`, null, 'get', token)
}
