import { constants } from 'src/utils/helpers'
import * as types from '../actions/dashbordTypes'

const initialState = { toppost: [], overview_loading: false, analytics_loading: false, post_state: { page: 1, perPage: constants.POSTS_PER_PAGE, loading: false, end: false } }

const profileReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.OVERVIEW_LOADING: {
      return {
        ...state,
        overview_loading: action.data
      }
    }

    case types.ANALYTICS_LOADING: {
      return {
        ...state,
        analytics_loading: action.data
      }
    }

    case types.SET_OVERVIEW_REPORT: {
      return {
        ...state,
        ...action.data
      }
    }

    case types.UPDATE_TOP_POSTS: {
      return {
        ...state,
        toppost: action.data
      }
    }

    case types.SET_ANALYTICS_REPORT: {
      console.log(state)
      return {
        ...state,
        ...action.data,
        post_state: {
          ...state.post_state,
          page: state.post_state.page + 1,
          loading: false,
          end: false
        }
      }
    }

    default:
      return state
  }
}

export default profileReducer
