import { constants } from 'src/utils/helpers'
import * as types from '../actions/profileTypes'

const defaultPaging = {
  page: 1,
  perPage: constants.POSTS_PER_PAGE,
  loading: false,
  end: false
}

const initialState = {
  profile: {},
  isProfileLoading: false,
  feed: {
    data: [],
    state: defaultPaging
  },
  images: {
    data: [],
    state: {
      ...defaultPaging,
      perPage: 12
    }
  },
  videos: {
    data: [],
    state: {
      ...defaultPaging,
      perPage: 12
    }
  },
  paid: {
    data: [],
    state: {
      ...defaultPaging,
      perPage: constants.POSTS_PER_PAGE
    }
  },
  liked: {
    data: [],
    state: {
      ...defaultPaging,
      perPage: constants.POSTS_PER_PAGE
    }
  },
  fans: {
    data: [],
    state: {
      ...defaultPaging,
      perPage: constants.POSTS_PER_PAGE
    }
  },
  blocked: {
    data: [],
    state: {
      ...defaultPaging,
      perPage: constants.POSTS_PER_PAGE
    }
  },
  subscriptions: {
    data: [],
    state: {
      ...defaultPaging,
      perPage: constants.POSTS_PER_PAGE
    }
  },
  favorites: {
    data: [],
    state: {
      ...defaultPaging,
      perPage: constants.POSTS_PER_PAGE
    }
  }
}
const profileReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.RESET_PROFILE: {
      return {
        ...state,
        profile: {},
        feed: initialState.feed,
        images: initialState.images,
        videos: initialState.videos,
        paid: initialState.paid,
        liked: initialState.liked,
        fans: initialState.fans,
        blocked: initialState.blocked,
        subscriptions: initialState.subscriptions,
        favorites: initialState.favorites
      }
    }

    case types.SET_PROFILE_POSTS: {
      const type = action.data.type
      return {
        ...state,
        [type]: {
          data: [...state[type].data, ...action.data.posts],
          state: {
            ...state[type].state,
            end: action.data.posts.length === state[type].state.perPage ? false : true,
            page: action.data.posts.length === state[type].state.perPage ? state[type].state.page + 1 : state[type].state.page
          }
        }
      }
    }
    case types.UPDATE_PROFILE_POSTS: {
      const type = action.data.type
      return {
        ...state,
        [type]: {
          ...state[type],
          data: [...action.data.posts]
        }
      }
    }
    case types.SET_PURCHASE_POST_PROFILE: {
      let list = state.feed.data
      const postIndex = list.map(post => post.id).indexOf(action.post_id)
      list[postIndex].isPaid = true
      return {
        ...state,
        feed: {
          ...state.feed,
          data: [...state.feed.data, ...list]
        }
      }
    }

    case types.SET_PROFILE_POST_LOADING: {
      const type = action.data.type
      return {
        ...state,
        [type]: {
          ...state[type],
          state: {
            ...state[type].state,
            loading: action.data.loading
          }
        }
      }
    }

    case types.SET_PROFILE: {
      return {
        ...state,
        profile: action.data
      }
    }

    case types.SET_USER_ACTION: {
      return {
        ...state,
        profile: action.data
      }
    }

    case types.SET_PROFILE_LOADING: {
      return {
        ...state,
        isProfileLoading: action.loading
      }
    }

    case types.UPDATE_PROFILE: {
      return {
        ...state,
        profile: { ...state.profile, ...action.data }
      }
    }

    case types.TOGGLE_SUBSCRIPTION_MODAL: {
      return {
        ...state,
        is_subscriptionModalVisible: !state.is_subscriptionModalVisible
      }
    }

    default:
      return state
  }
}

export default profileReducer
