import * as types from '../actions/bookmarkTypes'

const initialState = {
  posts: [],
  page: 1,
  isPostsLoading: false,
  end: false
}

const profileReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_BOOKMARK_POST_LOADING: {
      return {
        ...state,
        isPostsLoading: action.loading
      }
    }

    case types.SET_BOOKMARK_POST: {
      let old = state.posts
      return {
        ...state,
        posts: [...old, ...action.posts],
        page: state.page + 1,
        end: action.end
      }
    }

    case types.UPDATE_BOOKMARK_POST:
      console.log(action)
      return {
        ...state,
        posts: [...action.data]
      }

    default:
      return state
  }
}

export default profileReducer
