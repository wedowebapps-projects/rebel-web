import React from 'react'
import images from 'src/config/images'

function Empty() {
  return (
    <div className='text-center no-post-found'>
      <img src={images.app.emptyFeed} style={{ maxWidth: 300 }} />
      <h3>No posts found!</h3>
    </div>
  )
}

export default Empty
