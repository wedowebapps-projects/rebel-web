import React from 'react'
import Post from 'src/modules/post/components/post'
import Empty from './Empty'

function Feed(props) {
  const showEmpty = !props?.dashboard?.analytics_loading && props.posts.length == 0

  return (
    <React.Fragment>
      {!showEmpty > 0 ? (
        <div>
          {props.posts.map((post, i) => {
            return <Post post={post} key={`post-${post.id}-${i}`} {...props} />
          })}
        </div>
      ) : (
        <div className='mt-2 mb-5'>
          <Empty title='No post found!' />
        </div>
      )}
    </React.Fragment>
  )
}

export default Feed
