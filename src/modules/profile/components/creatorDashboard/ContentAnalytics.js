import React from 'react'
import images from 'src/config/images'
import PostLoader from 'src/modules/post/components/PostLoader'
import Feed from './feed'

const ContentAnalytics = props => {
  return (
    <div className='content-analytics-contain current tab-content'>
      <div className='row'>
        <div className='col-md-7 analytics-col'>
          <div className='p-4 border-wrap analytics-col-inner'>
            <div className='feed-card-topper sm-card d-flex justify-content-between mb-3'>
              <h4>Top Post</h4>
              <h5 className='parrot-color'>Earned {props?.dashboard?.engagements?.post_impressions || 0} impressions</h5>
            </div>
            <Feed posts={props?.dashboard?.toppost || []} state={props?.dashboard?.post_state} {...props} />
            {props?.dashboard?.analytics_loading && <PostLoader />}
          </div>
        </div>
        <div className='col-md-5 analytics-col'>
          <div className='engagements-col border-wrap'>
            <div className='sidebar-title pt-4'>
              <span>
                <strong>Engagements</strong>
              </span>
            </div>
            <div className='engagements-box d-flex justify-content-between align-items-center'>
              <div className='engagements-box-icon-inner d-flex align-items-center'>
                <div className='engagements-box-icon mr-4'>
                  <img src={images.dashboard.engagements1} />
                </div>
                <div className='engagements-box-info'>
                  <h6>Likes</h6>
                  <h2 className='primary-color'>{props?.dashboard?.engagements?.likes || 0}</h2>
                </div>
              </div>
              <div className='engagements-box-rate'>
                <h4 className={`${props?.dashboard?.engagements?.likes_status === 'down' ? 'red-color' : 'green-color'}`}>
                  <span>{props?.dashboard?.engagements?.likes_status === 'down' ? <img src={images.dashboard.tredingDown} /> : <img src={images.dashboard.tredingUp} />}</span>
                  {props?.dashboard?.engagements?.likesper || 0}%
                </h4>
              </div>
            </div>
            <div className='engagements-box d-flex justify-content-between align-items-center'>
              <div className='engagements-box-icon-inner d-flex align-items-center'>
                <div className='engagements-box-icon mr-4'>
                  <img src={images.dashboard.engagements2} />
                </div>
                <div className='engagements-box-info'>
                  <h6>COMMENTS</h6>
                  <h2 className='primary-color'>{props?.dashboard?.engagements?.comments || 0}</h2>
                </div>
              </div>
              <div className='engagements-box-rate'>
                <h4 className={`${props?.dashboard?.engagements?.comments_status === 'down' ? 'red-color' : 'green-color'}`}>
                  <span>{props?.dashboard?.engagements?.comments_status === 'down' ? <img src={images.dashboard.tredingDown} /> : <img src={images.dashboard.tredingUp} />}</span>
                  {props?.dashboard?.engagements?.commentsper || 0}%
                </h4>
              </div>
            </div>
            <div className='engagements-box d-flex justify-content-between align-items-center'>
              <div className='engagements-box-icon-inner d-flex align-items-center'>
                <div className='engagements-box-icon mr-4'>
                  <img src={images.dashboard.engagements3} />
                </div>
                <div className='engagements-box-info'>
                  <h6>POST IMPRESSIONS</h6>
                  <h2 className='primary-color'>{props?.dashboard?.engagements?.post_impressions || 0}</h2>
                </div>
              </div>
              <div className='engagements-box-rate'>
                <h4 className={`${props?.dashboard?.engagements?.post_impressions_status === 'down' ? 'red-color' : 'green-color'}`}>
                  <span>{props?.dashboard?.engagements?.post_impressions_status === 'down' ? <img src={images.dashboard.tredingDown} /> : <img src={images.dashboard.tredingUp} />}</span>
                  {props?.dashboard?.engagements?.post_impressionsper || 0}%
                </h4>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ContentAnalytics
