import React from 'react'
import { constants } from 'src/utils/helpers'
import images from 'src/config/images'
import { Spinner, UserMedia } from 'src/components/shared'
import Link from 'next/link'
import moment from 'moment'

const Overview = props => {
  return (
    <div className='overview-contain current tab-content'>
      <div className='d-flex wallet-row pt-5 pb-5'>
        <div className='wallet-box'>
          <div className='wallet-box-inner'>
            <div className='wallet-box-info'>
              <h6>Earnings</h6>
              <h2 className='primary-color'>
                <sup>{constants.NAIRA}</sup> {props?.dashboard?.topdata?.earnings || 0}
              </h2>
              <h4 className={`${props?.dashboard?.topdata?.earnings_status === 'down' ? 'red-color' : 'green-color'}`}>
                <span>
                  <span>{props?.dashboard?.topdata?.earnings_status === 'down' ? <img src={images.dashboard.tredingDown} /> : <img src={images.dashboard.tredingUp} />}</span>
                </span>
                {props?.dashboard?.topdata?.earningsper || 0}%
              </h4>
            </div>
          </div>
        </div>
        <div className='wallet-box'>
          <div className='wallet-box-inner'>
            <div className='wallet-box-info'>
              <h6>SUBSCRIBERS</h6>
              <h2 className='primary-color'>{props?.dashboard?.topdata?.subscribers || 0}</h2>
              <h4 className={`${props?.dashboard?.topdata?.subscriber_status === 'down' ? 'red-color' : 'green-color'}`}>
                <span>
                  <span>{props?.dashboard?.topdata?.subscriber_status === 'down' ? <img src={images.dashboard.tredingDown} /> : <img src={images.dashboard.tredingUp} />}</span>
                </span>
                {props?.dashboard?.topdata?.subscribersper || 0}%
              </h4>
            </div>
          </div>
        </div>
        <div className='wallet-box'>
          <div className='wallet-box-inner'>
            <div className='wallet-box-info'>
              <h6>LIKES</h6>
              <h2 className='primary-color'>{props?.dashboard?.topdata?.likes || 0}</h2>
              <h4 className={`${props?.dashboard?.topdata?.likes_status === 'down' ? 'red-color' : 'green-color'}`}>
                <span>
                  <span>{props?.dashboard?.topdata?.likes_status === 'down' ? <img src={images.dashboard.tredingDown} /> : <img src={images.dashboard.tredingUp} />}</span>
                </span>
                {props?.dashboard?.topdata?.likesper || 0}%
              </h4>
            </div>
          </div>
        </div>
      </div>
      <div className='overview-contain-subscribers'>
        <div className='sidebar-title'>
          <span>
            <strong> Top Subscribers</strong>
          </span>
        </div>
        <div className='top-subscribers-col'>
          {props?.dashboard?.topsubscribers?.map((sub, i) => {
            return (
              <div className='row top-subscribers-box align-items-center' key={`sub-${sub.user.username}-${i}`}>
                <div className='col-md-4'>
                  <UserMedia user={sub.user} size={64} />
                </div>
                <div className='col-md-4'>
                  <div className='since-date text-center'>
                    <p>{sub?.createdAt && `Since ${moment(sub?.createdAt).format('MMM YYYY')}`}</p>
                  </div>
                </div>
                <div className='col-md-4'>
                  <div className='custom-link-button-box'>
                    <Link href={'/[username]'} to={`/${sub.user.username}`}>
                      <a href={`/${sub.user.username}`} className='custom-link-button'>
                        View Profile
                      </a>
                    </Link>
                  </div>
                </div>
              </div>
            )
          })}
          {props?.dashboard?.overview_loading && (
            <div className='d-flex align-items-center justify-content-center'>
              <Spinner color='#2A9DF4' size={40} />
            </div>
          )}
        </div>
      </div>
    </div>
  )
}

export default Overview
