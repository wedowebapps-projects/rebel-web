import React from 'react'
import InfiniteScroll from 'react-infinite-scroll-component'
import PostLoader from 'src/modules/post/components/PostLoader'
import Feed from './Feed'

const BookmarkedPosts = props => {
  return (
    <div className='w-100 mt-4 recents-bookmark-wrap bg-color-white'>
      <h3>All Bookmarks</h3>
      <InfiniteScroll
        dataLength={props.bookmark.posts.length} //This is important field to render the next data
        next={() => {
          props.callFetchFun()
        }}
        hasMore={props.bookmark.posts.length < props.bookmark.end ? true : false}>
        <Feed posts={props.bookmark.posts} {...props} />
      </InfiniteScroll>
      {props.bookmark.isPostsLoading && <PostLoader />}
    </div>
  )
}

export default BookmarkedPosts
