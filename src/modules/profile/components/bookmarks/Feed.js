import React from 'react'
import Post from 'src/modules/post/components/post'

const Feed = props => {
  return (
    <React.Fragment>
      {props.posts.length > 0 && (
        <div>
          {props.posts.map((post, i) => {
            return <Post post={post} key={`post-${post.id}-${i}`} {...props} />
          })}
        </div>
      )}
    </React.Fragment>
  )
}

export default Feed
