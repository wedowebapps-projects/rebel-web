import React from 'react'
import images from 'src/config/images'

const NoBookmarksView = () => {
  return (
    <div className='w-100 havent-bookmark'>
      <div className='text-center no-post-found'>
        <img src={images.sidebar.bookmarkPng} alt='bookmark' />
        <h3>You haven’t bookmarked any post</h3>
        <p className='alpha50'>Click the bookmark icon on posts to save items to your bookmarks.</p>
      </div>
    </div>
  )
}

export default NoBookmarksView
