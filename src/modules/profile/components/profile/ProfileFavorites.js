import _ from 'lodash'
import React from 'react'
import InfiniteScroll from 'react-infinite-scroll-component'
import { Spinner, UserMedia } from 'src/components/shared'
import images from 'src/config/images'
import { checkAndRedirectLogin, copyToClipboard } from 'src/utils/helpers'
import Empty from './feed/Empty'
import { SearchPost } from './SearchPost'

const ProfileFavorites = props => {
  const url = new URL(window.location)
  const [favoriteLoader, setFavoriteLoader] = React.useState(false)
  const [unFavorite, setUnFavorite] = React.useState(0)
  const [list, setList] = React.useState([])
  var maxLength = props?.listType?.filter(l => l.type === 'favorites')?.count || 0

  React.useEffect(() => {
    if (props.favorites.data) {
      setList(props.favorites.data)
    }
  }, [props.favorites.data])

  const onMarkUnFavorite = async id => {
    if (checkAndRedirectLogin()) {
      try {
        setFavoriteLoader(true)
        props
          .fetchUserAction({ user_id: id, list_type: 'unfavorites' })
          .then(() => {
            setFavoriteLoader(false)
            location.reload()
          })
          .catch(() => {
            setFavoriteLoader(false)
          })
        setUnFavorite(0)
      } catch (error) {
        console.log(error)
        setUnFavorite(0)
        setFavoriteLoader(false)
      }
    }
  }

  return (
    <div id='tab-6' className='user-profile-wrp mt-3 ml-auto tab-content current'>
      <div className='d-flex justify-content-between search-post-wrp align-items-center mb-3 fan-search-wrp mb-4'>
        <div className='post-number'>FAVOURITES</div>
        <SearchPost list={list} setList={setList} data={props.favorites.data} placeholder='Search Users' />
      </div>

      <div className='feed-card lg-card mb-3 bg-color-white fan-list'>
        {!props.favorites.state.loading && list.length === 0 && <Empty title='Empty!' />}
        <InfiniteScroll
          dataLength={maxLength} //This is important field to render the next data
          next={() => props.fetchPosts()}
          hasMore={!props.favorites.state.end}>
          {list.map((user, i) => {
            return (
              <div className='feed-head row d-flex align-items-center mx-0' key={`user-${user.userId.id}-${i}`}>
                <div className='col-10 col-sm-6'>
                  <UserMedia user={user.userId} size={64} />
                </div>
                <div className='col-2 col-sm-6'>
                  <div className='copy-profile-link desktop-show' onClick={() => copyToClipboard(`${url.origin}/${user.userId.username}`)}>
                    <img src={images.profile.copyIcon} alt='' />
                    <span>Copy profile link</span>
                  </div>
                  <div
                    className='favourites'
                    onClick={() => {
                      onMarkUnFavorite(user.userId.id)
                      setUnFavorite(user.userId.id)
                    }}>
                    {favoriteLoader && user.userId.id === unFavorite ? <Spinner color='#2A9DF4' size={36} /> : user.userId.isStarred && <img src={images.profile.starFillRound} alt='' />}
                  </div>
                </div>
              </div>
            )
          })}
        </InfiniteScroll>
        {props.favorites.state.loading && (
          <div className='d-flex align-items-center justify-content-center'>
            <Spinner color='#2A9DF4' size={40} />
          </div>
        )}
      </div>
    </div>
  )
}

export default ProfileFavorites
