import React from 'react'
import { Modal } from 'src/components/app'
import images from 'src/config/images'
import { constants } from 'src/utils/helpers'

function SubscriptionModal({ profile, toggleSubscriptionModal, ...props }) {
  const closeModal = () => {
    toggleSubscriptionModal()
  }

  return (
    <Modal keyboard={true} size='lg' show={true} onClose={closeModal} className='new-post-modal'>
      <div className='subscription-modal'>
        <div className='modal-wrp'>
          <div className='d-flex'>
            <h3>Subscription plans</h3>
            <div className='colse-welcome-modal' onClick={closeModal}>
              <img src={images.app.cross} alt='cross' />
            </div>
          </div>
          {profile?.plans?.map(plan => {
            const plan_data = JSON.parse(plan.plan_data)
            return (
              <div className='subscription-plan-card sky-bg' key={plan.id}>
                <div className='d-flex flex-wrap subscription-plan-title justify-content-between'>
                  <h2>
                    <span>{constants.NAIRA}</span>
                    {process.env.PAYMENT_METHOD === 'flutterwave' ? plan_data.amount : plan_data.amount / 100}
                    <i>/{plan_data.interval}</i>
                  </h2>
                </div>
                <ul>
                  <li>Access to posts {plan_data.interval}</li>
                  <li>Enable direct messaging</li>
                  <li>Cancel auto renewal at anytime</li>
                </ul>
                <a href={null} title='' className='btn primary-bg-color color-white mt-2' onClick={() => props.MakePayment(plan_data)}>
                  Subscribe
                </a>
              </div>
            )
          })}
        </div>
      </div>
    </Modal>
  )
}

export default SubscriptionModal
