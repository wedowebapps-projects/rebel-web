import { useRouter } from 'next/router'
import React from 'react'
import images from 'src/config/images'

function ProfileTabs(props) {
  const [isMenuOpen, setIsMenuOopen] = React.useState(false)
  const router = useRouter()
  const onChange = tab => {
    if (tab !== props.activeTab) {
      props.setActiveTab(tab)
    }
  }
  const activeTab = props.activeTab
  const getCounts = t => props.listType.find(ty => ty.type === t)?.count
  return (
    <ul className='tabs justify-content-around'>
      <li className={activeTab === 'feed' ? 'current' : ''} data-tab='tab-1' onClick={() => onChange('feed')}>
        All posts
      </li>
      <li data-tab='tab-2' className={activeTab === 'images' ? 'current' : ''} onClick={() => onChange('images')}>
        <img src={images.profile.imageGray} />
        <img src={images.profile.imageGrayActive} />
        Images
      </li>
      <li data-tab='tab-3' className={`${router.asPath === '/account' ? 'desktop-show' : ''} ${activeTab === 'videos' ? 'current' : ''}`} onClick={() => onChange('videos')}>
        <img src={images.profile.videoGray} />
        <img src={images.profile.videoGrayActive} />
        Videos
      </li>
      {router.route === '/account' && (
        <>
          <li data-tab='tab-4' className={`desktop-show ${activeTab === 'liked' ? 'current' : ''}`} onClick={() => onChange('liked')}>
            <img src={images.profile.heartGrey} />
            <img src={images.profile.heartBlue} />
            Liked Posts
          </li>
          <li data-tab='tab-5' className={`desktop-show ${activeTab === 'paid' ? 'current' : ''}`} onClick={() => onChange('paid')}>
            <img src={images.profile.NGrey} alt='' />
            <img src={images.profile.N} alt='' />
            Paid Posts
          </li>
          <li
            data-tab='tab-6'
            onClick={() => setIsMenuOopen(!isMenuOpen)}
            className={`more-settings-wrp ${activeTab === 'fans' || activeTab === 'subscription' || activeTab === 'favorites' ? 'current' : ''}`}>
            <img src={images.app.arrowDown} className='gry-down-ic' alt='' />
            <img src={images.app.arrowDownSvg} className='color-down-ic' alt='' />
            More
            <div className='more-setings-link mobile-settings-popup more-option' style={{ display: isMenuOpen ? 'block' : 'none' }}>
              <button className='popupclose mobile-show'>
                <img src={images.app.popupClose} />
              </button>
              <ul>
                <li className={`mobile-show ${activeTab === 'videos' ? 'current' : ''}`} onClick={() => onChange('videos')}>
                  {props.app.theme === 'dark' ? <img src={images.profile.videoGrayDarkMode} /> : <img src={images.profile.videoGray} /> }
                  <img src={images.profile.videoGrayActive} />
                  Videos
                </li>
                <li className={`mobile-show ${activeTab === 'liked' ? 'current' : ''}`} onClick={() => onChange('liked')}>
                  <img src={images.profile.heartGrey} />
                  <img src={images.profile.heartBlue} />
                  Liked Posts
                </li>
                <li className={`mobile-show ${activeTab === 'paid' ? 'current' : ''}`} onClick={() => onChange('paid')}>
                  <img src={images.profile.NGrey} alt='' />
                  <img src={images.profile.N} alt='' />
                  Paid Posts
                </li>
                <li className={activeTab === 'fans' ? 'current' : ''} onClick={() => onChange('fans')}>
                  <a title=''>
                    Your Active Fans<span>{getCounts('fans')}</span>
                  </a>
                </li>
                <li disabled className={activeTab === 'subscriptions' ? 'current' : ''} onClick={() => onChange('subscriptions')}>
                  <a title=''>
                    Your Active Subscriptions<span>{getCounts('subscriptions')}</span>
                  </a>
                </li>
                <li className={activeTab === 'favorites' ? 'current' : ''} onClick={() => onChange('favorites')}>
                  <a title=''>
                    Favourites<span>{getCounts('favorites')}</span>
                  </a>
                </li>
              </ul>
            </div>
          </li>
        </>
      )}
    </ul>
  )
}

export default ProfileTabs
