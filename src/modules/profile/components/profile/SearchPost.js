import React from 'react'
import images from 'src/config/images'

export const SearchPost = ({ setList, data, placeholder = 'Search posts' }) => {
  const [input, setInput] = React.useState('')

  React.useEffect(() => {
    if (input) {
      searchInput()
    } else {
      setList(data)
    }
  }, [input])

  const searchInput = () => {
    let temp = _.filter(data, function(e) {
      return _.includes(_.lowerCase(`${e?.userId?.first_name} ${e?.userId?.last_name}`), _.lowerCase(input))
    })
    setList(temp)
  }
  return (
    <div className='search-input-wrap'>
      <input type='search' name='' value={input} onChange={e => setInput(e.target.value)} placeholder={placeholder} />
      <img src={images.app.search} />
    </div>
  )
}
