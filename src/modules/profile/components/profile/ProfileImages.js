import React from 'react'
import InfiniteScroll from 'react-infinite-scroll-component'
import images from 'src/config/images'
import { Spinner } from 'src/components/shared'
import Modal from '../../../post/components/post/Modal'

function ProfileImages({ posts, state, ...props }) {
  const [modalOpen, setModalOpen] = React.useState(false)
  const [indexOpened, setIndexOpened] = React.useState(-1)

  const handleInfiniteScroll = () => {
    props.fetchPosts()
  }

  const isLocked = !props.profile.isSubscribed && props.profile.username != props.auth.user.username

  const openModal = index => {
    setIndexOpened(index)
    setModalOpen(true)
  }

  const closeModal = () => {
    setIndexOpened(-1)
    setModalOpen(false)
  }
  return (
    <div className='user-profile-wrp mt-3 ml-auto tab-content current'>
      <div className='flex-wrap row subscribed-post mb-3 profile-images'>
        {!(posts && posts[0]) && !state.loading && (
          <div className='text-center text-muted'>
            <i className='fas fa-images fa-3x mb-1'></i>
            <h3 className='text-muted'> No data found. </h3>
          </div>
        )}
        <InfiniteScroll
          dataLength={posts.length} //This is important field to render the next data
          next={handleInfiniteScroll}
          hasMore={!state.end}>
          {posts.map((post, index) => {
            return (
              <div className='w-25 subscribe-user-post float-left' key={`profile-images-${post.id}-${index}`}>
                {post?.isSubscribed && post?.isPurchased ? (
                  <div onClick={() => openModal(index)} className='sky-bg d-flex flex-column align-items-center justify-content-center h-250 rounded cursor-pointer'>
                    <img src={post.processed_path} />
                  </div>
                ) : (
                  <div className='sky-bg d-flex flex-column align-items-center justify-content-center h-250 rounded'>
                    <img src={images.app.lockedPost} alt='Lock' style={{ height: 60, width: 60 }} />
                    <a href='#' title className='btn sky-bg primary-color subscribe-btn subscribe-btn mt-4'>
                      {!post.isSubscribed && `Subscribe to view user’s post`}
                      {post.isSubscribed && !post.isPurchased && `Purchase post`}
                    </a>
                  </div>
                )}
              </div>
            )
          })}
        </InfiniteScroll>
        {state.loading && (
          <div className='d-flex align-items-center justify-content-center'>
            <Spinner size={40} color='#187BCD' />
          </div>
        )}
      </div>
      {modalOpen && indexOpened !== -1 && <Modal onClose={closeModal} index={indexOpened} post_media={posts} />}
    </div>
  )
}

export default ProfileImages
