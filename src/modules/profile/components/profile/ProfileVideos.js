import React from 'react'
import InfiniteScroll from 'react-infinite-scroll-component'
import ReactPlayer from 'react-player'
import images from 'src/config/images'
import { Spinner } from 'src/components/shared'
import { Modal } from 'src/components/app'

function ProfileVideos({ posts, state, ...props }) {
  const [modalOpen, setModalOpen] = React.useState(false)
  const [urlOpened, setUrlOpened] = React.useState('')

  const handleInfiniteScroll = () => {
    props.fetchPosts()
  }

  const isLocked = !props.profile.isSubscribed && props.profile.username != props.auth.user.username

  const openModal = url => {
    setModalOpen(true)
    setUrlOpened(url)
  }

  const closeModal = () => {
    setModalOpen(false)
    setUrlOpened('')
  }

  return (
    <div className='user-profile-wrp mt-3 ml-auto tab-content current'>
      <div className='flex-wrap row subscribed-post mb-3 profile-videos'>
        {!(posts && posts[0]) && !state.loading && (
          <div className='text-center text-muted'>
            <i className='fas fa-images fa-3x mb-1'></i>
            <h3 className='text-muted'> No data found. </h3>
          </div>
        )}
        <InfiniteScroll
          dataLength={posts.length} //This is important field to render the next data
          next={handleInfiniteScroll}
          hasMore={!state.end}
          style={{ width: '100%' }}>
          {posts.map((post, i) => {
            return (
              <div className='w-25 subscribe-user-post float-left' key={`profile-images-${post.id}-${i}`}>
                {post?.isSubscribed && post?.isPurchased ? (
                  <div
                    onClick={() => openModal(post.media_path)}
                    className='sky-bg d-flex flex-column align-items-center justify-content-center h-250 rounded cursor-pointer'
                    style={{ position: 'relative' }}>
                    <ReactPlayer
                      url={post.processed_path}
                      width='100%'
                      height='100%'
                      controls={false}
                      config={{
                        file: {
                          playerVars: { forceHLS: true }
                        }
                      }}
                    />
                    <div className='play-icon-wrap'>
                      <img src={images.profile.playIcon} />
                    </div>
                  </div>
                ) : (
                  <div className='sky-bg d-flex flex-column align-items-center justify-content-center h-250 rounded'>
                    <img src={images.app.lockedPost} alt='Lock' style={{ height: 60, width: 60 }} />
                    <a href='#' title className='btn sky-bg primary-color subscribe-btn subscribe-btn mt-4'>
                      {!post.isSubscribed && `Subscribe to view user’s post`}
                      {post.isSubscribed && !post.isPurchased && `Purchase post`}
                    </a>
                  </div>
                )}
              </div>
            )
          })}
        </InfiniteScroll>
        {state.loading && (
          <div className='d-flex align-items-center justify-content-center'>
            <Spinner size={40} color='#187BCD' />
          </div>
        )}
      </div>
      {modalOpen && urlOpened && (
        <Modal keyboard={true} size='xl' height='500px' show={modalOpen} onClose={closeModal}>
          <div className='subscription-modal'>
            <div className='modal-wrp'>
              <div className='d-flex my-4'>
                <div className='colse-welcome-modal' onClick={closeModal}>
                  <img src={images.app.cross} alt='cross' />
                </div>
              </div>
              <ReactPlayer
                url={urlOpened}
                width='100%'
                height='600px'
                controls={true}
                config={{
                  file: {
                    playerVars: { forceHLS: true }
                  }
                }}
              />
            </div>
          </div>
        </Modal>
      )}
    </div>
  )
}

export default ProfileVideos
