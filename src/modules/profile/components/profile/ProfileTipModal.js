import React from 'react'
import { PaystackButton } from 'react-paystack'
import { closePaymentModal, FlutterWaveButton } from 'flutterwave-react-v3'
import { Modal } from 'src/components/app'
import images from 'src/config/images'
import { constants, isValidPrice } from 'src/utils/helpers'
import { v4 as uuidv4 } from 'uuid'
import { submitSendUserTipTransactionApi } from '../../api/profileApi'
import { Alert } from 'react-bootstrap'

function ProfileTipModal({ profileTipModalVisible, setProfileTipModalVisible, ...props }) {
  const [amountToTip, setAmountToTip] = React.useState(Number)
  const [error, setError] = React.useState({ msg: '', type: '' })

  const getFees = amount => {
    let newAmount = 20
    if (process.env.PAYMENT_METHOD === 'flutterwave') {
      newAmount = amount * props.app.commissions.flutterwave_fees
    } else {
      newAmount = (amount * props.app.commissions.paystack_fees) / 100
    }
    return newAmount.toFixed(2)
  }

  //payment methods
  const config = {
    publicKey: process.env.PAYSTACK_publicKey,
    tx_ref: `tip-${props.profile.id}-${uuidv4()}`,
    email: props?.auth?.user?.email,
    amount: amountToTip * 100,
    onSuccess: reference => handlePaystackSuccessAction(reference),
    onClose: closeModal
  }
  const flutterwaveConfig = {
    public_key: process.env.FLUTTERWAVE_SECRET,
    tx_ref: `tip-${props.profile.id}-${uuidv4()}`,
    amount: amountToTip,
    currency: 'NGN',
    payment_options: 'card',
    customer: {
      email: props?.auth?.user?.email,
      name: `${props?.auth?.user?.first_name} ${props?.auth?.user?.last_name}`
    },
    customizations: {
      title: 'Rebelyus.com',
      description: 'Payment for tip',
      logo: images.app.logo
    },
    text: 'Pay with Flutterwave!',
    callback: async response => {
      try {
        const data = {
          transaction_id: response.transaction_id,
          tx_ref: response.tx_ref,
          amount: amountToTip,
          payment_method: 'flutterwave'
        }
        closePaymentModal()
        await submitSendUserTipTransactionApi(props.profile.id, data)
        setError({ msg: 'Payment successful', type: 'success' })
      } catch (error) {
        closePaymentModal()
        if(error?.errors !== undefined)
        {
          setError({ msg: error?.errors[0]?.msg, type: 'danger' })
        }
        else
        {
          setError({ msg: 'Something went wrong', type: 'danger' })
        }
        console.log(error)
      }
    },
    onClose: () => {
      setProfileTipModalVisible(false)
    }
  }

  const handlePaystackSuccessAction = async response => {
    try {
      const data = {
        transaction_id: response.trxref,
        tx_ref: response.transaction,
        amount: amountToTip,
        payment_method: 'paystack'
      }
      await submitSendUserTipTransactionApi(props.profile.id, data)
      setProfileTipModalVisible(false)
    } catch (error) {
      console.log(error)
    }
  }

  const closeModal = () => {
    setProfileTipModalVisible(false)
  }

  return (
    <Modal keyboard={true} show={profileTipModalVisible} onClose={closeModal}>
      <div id='sendProfileTipModal' className='send-tip-popups visible'>
        <div className='modal-wrp text-center'>
          <div className='modal-inner-wrap'>
            <div className='colse-welcome-modal' onClick={closeModal}>
              <img src={images.app.cross} alt='cross' />
            </div>
            <h2>Send tip</h2>
            <p>
              Enter amount to <strong> send </strong>below
            </p>
            <div className='tip-input-wrap'>
              <span>{constants.NAIRA}</span>
              <input
                type='number'
                name='Price'
                value={amountToTip}
                className={`fa-2x ${!isValidPrice(amountToTip) ? 'text-danger' : ''}`}
                onChange={e => setAmountToTip(e.target.value)}
                min={props?.app?.commissions?.min_tip_amount}
                max={props?.app?.commissions?.max_tip_amount}
              />
            </div>
            {error.type && (
              <Alert variant={error.type} className='text-center rounded-0 mb-2'>
                {error.msg}
              </Alert>
            )}
            <small>{`Min allowed value ${props?.app?.commissions?.min_tip_amount} & max allowed value ${props?.app?.commissions?.max_tip_amount}`}</small>
            {process.env.PAYMENT_METHOD === 'flutterwave' ? (
              <FlutterWaveButton
                {...flutterwaveConfig}
                text={'Send Tip'}
                className='btn btn-success btn-md px-4 w-80'
                disabled={!isValidPrice(amountToTip) || !(amountToTip >= props.app.commissions.min_tip_amount && amountToTip <= props.app.commissions.max_tip_amount)}
              />
            ) : (
              <PaystackButton
                {...config}
                text={'Send Tip'}
                className='btn btn-success btn-md px-4 w-80'
                disabled={!isValidPrice(amountToTip) || !(amountToTip >= props.app.commissions.min_tip_amount && amountToTip <= props.app.commissions.max_tip_amount)}
              />
            )}
            <p>
              Bank charge of{' '}
              <strong>
                <u>{`NGN ${getFees(amountToTip)}`}</u>
              </strong>{' '}
              as banking fees applicable
            </p>
          </div>
        </div>
      </div>
    </Modal>
  )
}

export default ProfileTipModal
