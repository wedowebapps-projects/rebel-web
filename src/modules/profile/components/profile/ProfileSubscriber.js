import Link from 'next/link'
import React from 'react'
import InfiniteScroll from 'react-infinite-scroll-component'
import { Spinner, UserMedia } from 'src/components/shared'
import Empty from './feed/Empty'
import images from 'src/config/images'

const ProfileActiveSubscriber = props => {
  const [postList, setPostList] = React.useState([])
  const [input, setInput] = React.useState('')

  var maxLength = props?.listType?.filter(l => l.type === 'subscriptions')?.count || 0

  React.useEffect(() => {
    if (props.subscriptions.data) {
      setPostList(props.subscriptions.data)
    }
  }, [props.subscriptions.data])

  React.useEffect(() => {
    if (input) {
      searchInput()
    } else {
      setPostList(props.subscriptions.data)
    }
  }, [input])

  const searchInput = () => {
    let temp = _.filter(props.subscriptions.data, function(e) {
      return _.includes(_.lowerCase(`${e?.followedId?.first_name} ${e?.followedId?.last_name}`), _.lowerCase(input))
    })
    setPostList(temp)
  }

  return (
    <div className='user-profile-wrp mt-3 ml-auto tab-content current'>
      <div className='d-flex justify-content-between search-post-wrp align-items-center mb-3 fan-search-wrp mb-4'>
        <div className='post-number'>ACTIVE SUBSCRIPTIONS</div>
        <div className='search-input-wrap'>
          <input type='search' name='' value={input} onChange={e => setInput(e.target.value)} placeholder='Search Users' />
          <img src={images.app.search} />
        </div>
      </div>
      <div className='feed-card lg-card mb-3 bg-color-white fan-list'>
        {!props.subscriptions.state.loading && postList.length === 0 && <Empty title='Empty!' />}
        <InfiniteScroll
          dataLength={maxLength} //This is important field to render the next data
          next={() => props.fetchPosts()}
          style={{ overflow: 'hidden' }}
          hasMore={!props.subscriptions.state.end}>
          {postList.map((user, i) => {
            return (
              <div className='feed-head row d-flex align-items-center mx-0' key={`subscriptions-post-${user.followed_id}-${i}`}>
                <div className='col-8 col-sm-5 col-md-4'>
                  <UserMedia user={user.followedId} size={64} />
                </div>
                <div className='col-4 col-sm-3 col-md-2'>
                  <Link href='/[username]' as={`/${user.followedId.username}`}>
                    <a className='view-profile cursor-pointer'>View profile</a>
                  </Link>
                </div>
                <div className='col-3 col-sm-6 col-md-6 px-0 desktop-show'>
                  {user.subscription_id ? (
                    <div className='desktop-show'>
                      <div className='date-wrap'>
                        <span>
                          Start date <span>{user.start_date}</span>
                          {/* <input type='date' value={user.start_date} disabled className='ml-2' /> */}
                        </span>
                        <span>
                          Expiry date <span>{user.end_date}</span>
                          {/* <input type='date' value={user.end_date} disabled className='ml-2' /> */}
                        </span>
                      </div>
                    </div>
                  ) : (
                    <p className='mx-auto'>Free User</p>
                  )}
                </div>
              </div>
            )
          })}
        </InfiniteScroll>
        {props.subscriptions.state.loading && (
          <div className='d-flex align-items-center justify-content-center'>
            <Spinner color='#2A9DF4' size={40} />
          </div>
        )}
      </div>
    </div>
  )
}

export default ProfileActiveSubscriber
