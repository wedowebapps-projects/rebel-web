import { useRouter } from 'next/router'
import React from 'react'
import images from 'src/config/images'

function ProfileBanner(props) {
  const { profile } = props
  const router = useRouter()
  const coverImage = profile.cover_image && profile.cover_image !== '' ? profile.cover_image : images.profile.profileCoverPlaceholder

  return (
    <div className='profile-banner comman-bg d-flex justify-content-between' style={{ backgroundImage: `url("${coverImage}")` }}>
      <div className='go-back cursor-pointer' onClick={() => router.back()}>
        <img src={images.profile.profileBack} />
      </div>
      <div className='profile-social-team d-flex'>
        <div className='d-flex align-items-center'>
          <div className='profile-icon'>
            <img src={images.profile.profileVideo} />
          </div>
          <div>
            <p>{profile.video || 0}</p>
          </div>
        </div>
        <div className='d-flex align-items-center'>
          <div className='profile-icon'>
            <img src={images.profile.profileImages} />
          </div>
          <div>
            <p>{profile.images || 0}</p>
          </div>
        </div>
        <div className='d-flex align-items-center'>
          <div className='profile-icon'>
            <img src={images.profile.profileHearts} />
          </div>
          <div>
            <p>{profile.postLikesCount || 0}</p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ProfileBanner
