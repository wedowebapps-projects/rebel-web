import React from 'react'
import PostLoader from 'src/modules/post/components/PostLoader'
import InfiniteScroll from 'react-infinite-scroll-component'
import Feed from './feed'

function ProfileFeed({ posts, state, ...props }) {
  const handleInfiniteScroll = () => {
    props.fetchPosts()
  }

  return (
    <div className='user-profile-wrp mt-3 ml-auto tab-content current'>
      <InfiniteScroll
        dataLength={posts.length} //This is important field to render the next data
        next={handleInfiniteScroll}
        hasMore={!state.end}>
        {/* <Feed feed={feed} feedState={feedState} postInteraction={props.postInteraction} fetchPostComments={props.fetchPostComments} /> */}
        <Feed posts={posts} state={state} {...props} />
      </InfiniteScroll>
      {state.loading && <PostLoader />}
    </div>
  )
}

export default ProfileFeed
