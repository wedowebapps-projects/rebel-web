import { Col, Modal, Row } from 'react-bootstrap'
import Link from 'next/link'
import React from 'react'
import InfiniteScroll from 'react-infinite-scroll-component'
import { Button, Spinner, UserMedia } from 'src/components/shared'
import images from 'src/config/images'
import { checkAndRedirectLogin } from 'src/utils/helpers'
import { PaystackButton } from 'react-paystack'
import { FlutterWaveButton, closePaymentModal } from 'flutterwave-react-v3'
import { v4 as uuidv4 } from 'uuid'
import moment from 'moment'
import { addMoneyToWallet_api, blockUser_api, removeSubscription_api } from 'src/modules/auth/api/authApi'
import { getSubscriptions } from 'src/api/appApis'
import { SearchPost } from './SearchPost'
import Empty from './feed/Empty'

const ProfileActiveFans = props => {
  const [isOptionOpen, setIsOptionOpen] = React.useState(0)
  const [isOpen, setIsOpen] = React.useState(false)
  const [showBlockPrompt, setShowBlockPrompt] = React.useState(false)
  const [actionLoader, setActionLoader] = React.useState(false)
  const [removeAction, setRemoveAction] = React.useState('')
  const [actionUser, setActionUser] = React.useState(null)
  const [filter, setFilter] = React.useState({
    page: 1,
    perPage: 10
  })
  const [list, setList] = React.useState([])
  const [postList, setPostList] = React.useState([])
  const [isLoadingData, setIsLoadingData] = React.useState(false)

  var maxLength = props?.listType?.filter(l => l.type === 'fans')?.count || 0
  const hasPlan_data = actionUser && actionUser.userId.hasPlan ? JSON.parse(actionUser.userId.hasPlan.plan_data) : {}

  React.useEffect(() => {
    if (props.fans.data) {
      setPostList(props.fans.data)
    }
  }, [props.fans.data])

  React.useEffect(() => {
    resetFilters()
  }, [])

  const resetFilters = () => {
    setIsLoadingData(false)
    setList([])
    setFilter({
      page: 1,
      perPage: 10
    })
  }

  const getRefundableAmount = () => {
    const hasPlan_data = actionUser && actionUser.userId.hasPlan ? JSON.parse(actionUser.userId.hasPlan.plan_data) : {}
    const commissionAndCharges = actionUser && actionUser.userId.hasPlan ? JSON.parse(actionUser.userId.hasPlan.commissionAndCharges) : {}
    const commission = ((hasPlan_data.amount * commissionAndCharges.commmission) / 100).toFixed(2)
    let fees
    if (process.env.PAYMENT_METHOD === 'flutterwave') {
      fees = ((hasPlan_data.amount * commissionAndCharges.flutterwave_fees) / 100).toFixed(2)
    } else {
      fees = ((hasPlan_data.amount * commissionAndCharges.paystack_fees) / 100).toFixed(2)
    }
    let finalAmount
    if (process.env.PAYMENT_METHOD === 'flutterwave') {
      finalAmount = (hasPlan_data.amount - commission - fees).toFixed(2)
    } else {
      finalAmount = ((hasPlan_data.amount - commission - fees) / 100).toFixed(2)
    }
    return finalAmount
  }

  const reloadData = async () => {
    try {
      await setFilter({
        perPage: 10,
        page: 1
      })
      const result = await getSubscriptions('fans', filter)
      setList(result.data)
      setFilter({
        ...filters,
        page: result.data.length > 0 ? filters.page + 1 : filters.page
      })
      setTimeout(() => setIsLoadingData(list.length === 0 && filter.page === 1 ? 'noData' : false))
    } catch (error) {
      console.log(error)
      setIsLoadingData(false)
    }
  }

  const refreshData = async () => {
    try {
      setIsLoadingData(true)
      const result = await getSubscriptions(props.page, filter)
      setList([...list, ...result.data])
      setFilter({
        ...filter,
        page: result.data.length > 0 ? filter.page + 1 : filter.page
      })
      setTimeout(() => setIsLoadingData(list.length === 0 && filter.page === 1 ? 'noData' : false))
    } catch (error) {
      console.log(error)
      setIsLoadingData(false)
    }
  }

  // Block / remove user
  const setBlockPrompt = (e, removeAction) => {
    e.preventDefault()
    if (checkAndRedirectLogin()) {
      setShowBlockPrompt(true)
      setRemoveAction(removeAction)
    }
  }

  const closeBlockPrompt = () => {
    setShowBlockPrompt(false)
    setActionUser(null)
    setRemoveAction('')
  }

  const onRemoveUser = async () => {
    try {
      setActionLoader('removeSubscription')
      if (removeAction === 'block') {
        await blockUser_api(actionUser.user_id)
        await reloadData()
        props.fetchListData()
      } else {
        await removeSubscription_api(actionUser.user_id)
        props.fetchListData()
      }
      location.reload()
    } catch (error) {
      console.log(error)
      //if (error.errors.length > 0) {
      // error.errors.map(error => toast.error(error.msg))
      //  }
    }
    setActionLoader('removeSubscription')
    closeBlockPrompt()
  }

  //paystack
  const topupConfig = {
    publicKey: process.env.PAYSTACK_publicKey,
    tx_ref: `topup-${props.auth.user.id}-${uuidv4()}`,
    amount: getRefundableAmount() * 100,
    email: props.auth.user.email,
    onSuccess: reference => handlePaystackSuccessAction(reference),
    onClose: () => handlePaystackCloseAction()
  }
  const flutterwaveConfig = {
    public_key: process.env.FLUTTERWAVE_SECRET,
    tx_ref: `topup-${props.auth.user.id}-${uuidv4()}`,
    amount: getRefundableAmount(),
    currency: 'NGN',
    payment_options: 'card',
    customer: {
      email: props.auth.user.email,
      name: `${props.auth.user.first_name} ${props.auth.user.last_name}`
    },
    customizations: {
      title: 'Rebelyus.com',
      description: 'Add money to wallet',
      logo: images.app.logo
    },
    text: 'Pay with Flutterwave!',
    callback: async response => {
      try {
        const data = {
          transaction_id: response.transaction_id,
          tx_ref: response.tx_ref,
          amount: getRefundableAmount()
        }
        await addMoneyToWallet_api(data)
        location.reload()
      } catch (error) {
        console.log(error)
      }
    },
    onClose: () => {
      setShowBlockPrompt(false)
      setActionUser(null)
      setTimeout(() => {
        setRemoveAction('')
      }, 500)
    }
  }

  const handlePaystackSuccessAction = async response => {
    try {
      const data = {
        transaction_id: response.trxref,
        tx_ref: response.transaction,
        amount: getRefundableAmount()
      }
      await addMoneyToWallet_api(data)
      location.reload()
    } catch (error) {
      console.log(error)
    }
  }

  const handlePaystackCloseAction = () => {
    setShowBlockPrompt(false)
    setActionUser(null)
    setTimeout(() => {
      setRemoveAction('')
    }, 500)
  }

  const extra_charge = () => {
    if (process.env.PAYMENT_METHOD === 'flutterwave') {
      return ((getRefundableAmount() * props.app.commission.flutterwave_fees) / 100).toFixed(2)
    } else {
      return ((getRefundableAmount() * props.app.commission.paystack_fees) / 100).toFixed(2)
    }
  }

  return (
    <div id='tab-6' className='user-profile-wrp mt-3 ml-auto tab-content current'>
      <div className='d-flex justify-content-between search-post-wrp align-items-center mb-3 fan-search-wrp mb-4'>
        <div className='post-number'>ACTIVE FANS</div>
        <SearchPost list={postList} setList={setPostList} data={props.fans.data} placeholder='Search Users' />
      </div>

      <div className='feed-card lg-card mb-3 bg-color-white fan-list'>
        {!props.fans.state.loading && postList.length === 0 && <Empty title='Empty!' />}
        <InfiniteScroll
          dataLength={maxLength} //This is important field to render the next data
          next={() => props.fetchPosts()}
          hasMore={!props.fans.state.end}>
          {postList.map((user, i) => {
            return (
              <div className='feed-head row d-flex align-items-center mx-0' key={`fans-post-${user.user_id}-${i}`}>
                <div className='col-8 col-sm-4'>
                  <UserMedia user={user.userId} size={64} />
                </div>
                <div className='col-3 col-sm-2'>
                  <Link href='/[username]' as={`/${user.userId.username}`}>
                    <a className='view-profile cursor-pointer'>View profile</a>
                  </Link>
                </div>
                <div className='col-1 col-sm-6 px-0 justify-content-end'>
                  <div className='d-flex align-items-center justify-content-end'>
                    <div className='desktop-show mx-auto'>
                      {user.subscription_id ? (
                        <div className='desktop-show align-items-center justify-content-center'>
                          <div className='date-wrap'>
                            <span>
                              Start date <span>{user.start_date}</span>
                              {/* <input type='date' value={user.start_date} disabled className='ml-2' /> */}
                            </span>
                            <span>
                              Expiry date <span>{user.end_date}</span>
                              {/* <input type='date' value={user.end_date} disabled className='ml-2' /> */}
                            </span>
                          </div>
                        </div>
                      ) : (
                        <p className='mx-auto'>Free User</p>
                      )}
                    </div>
                    <div className='d-inline-flex align-items-center'>
                      <div className='more-settings-wrp'>
                        <a
                          onClick={e => {
                            e.preventDefault()
                            isOptionOpen !== 0 ? setIsOptionOpen(0) : setIsOptionOpen(user.id)
                            setIsOpen(!isOpen)
                          }}>
                          <img src={images.app.moreCircle} />
                        </a>
                        {isOpen && user.id === isOptionOpen && (
                          <div className='more-setings-link' style={{ display: isOpen ? 'block' : 'none' }}>
                            {user.userId.hasSubscribed && (
                              <a
                                className='cursor-pointer'
                                onClick={e => {
                                  setBlockPrompt(e, 'removeSubscription', user)
                                  setActionUser(user)
                                }}
                                title=''>
                                Remove user
                              </a>
                            )}
                            <a
                              className='cursor-pointer'
                              title=''
                              onClick={e => {
                                setBlockPrompt(e, 'block', user)
                                setActionUser(user)
                              }}>
                              Block user
                            </a>
                          </div>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            )
          })}
        </InfiniteScroll>
        {props.fans.state.loading && (
          <div className='d-flex align-items-center justify-content-center'>
            <Spinner color='#2A9DF4' size={40} />
          </div>
        )}
      </div>

      <Modal keyboard={true} show={showBlockPrompt && actionUser} onHide={() => closeBlockPrompt()} centered size='lg'>
        {actionUser && (
          <Modal.Body>
            <h2 className='text-center'>
              {removeAction === 'block' ? `Block @${actionUser.userId.username}?` : `Remove ${actionUser.userId.hasPlan ? 'subscriber' : 'fan'} @${actionUser.userId.username}?`}
            </h2>
            <div className='colse-welcome-modal' onClick={closeBlockPrompt}>
              <img src={images.app.cross} alt='' />
            </div>
            <br />
            {/* Paid follower */}
            {actionUser.userId.hasPlan && (
              <>
                <div className='text-center my-2'>
                  <strong>
                    {hasPlan_data.name} - NGN {process.env.PAYMENT_METHOD === 'flutterwave' ? hasPlan_data.amount : hasPlan_data.amount / 100} - {hasPlan_data.interval}
                  </strong>
                </div>
                <Row className='my-2'>
                  <Col xs={5} md={6} className='text-right'>
                    Subscribed On:
                  </Col>
                  <Col xs={7} md={6}>
                    <strong>
                      {moment
                        .utc(actionUser.userId.hasPlan.createdAt)
                        .local()
                        .format('DD MMM, YYYY HH:MM')}
                    </strong>
                  </Col>
                </Row>
                <Row className='my-2'>
                  <Col xs={5} md={6} className='text-right'>
                    Refundable:
                  </Col>
                  <Col xs={7} md={6}>
                    <strong>NGN {getRefundableAmount()}</strong>
                  </Col>
                </Row>
                <br />

                {/* Add Money to wallet */}
                {props.auth.user.wallet && props.auth.user.wallet.available < getRefundableAmount() && (
                  <>
                    <p className='text-center'>
                      To proceed, please add{' '}
                      <span className='fa-lg'>
                        <strong>NGN {getRefundableAmount()}</strong>
                      </span>{' '}
                      to your wallet
                    </p>
                    <div className='text-center my-4'>
                      {process.env.PAYMENT_METHOD === 'flutterwave' ? (
                        <FlutterWaveButton {...flutterwaveConfig} text={'Add Money'} className='btn btn-success rounded px-4' />
                      ) : (
                        <PaystackButton {...topupConfig} text={'Add Money'} className='btn btn-success rounded px-4' />
                      )}
                    </div>
                    <p className='mb-0 mt-4 text-center'>
                      <small className='text-muted'>
                        Extra charge of NGN <strong> {extra_charge()}</strong> as banking fees applicable.
                      </small>
                    </p>
                  </>
                )}

                {/* Confirm Block */}
                {props.auth.user.wallet && props.auth.user.wallet.available >= getRefundableAmount() && (
                  <div className='text-center mb-4'>
                    <Button variant='success' className='btn btn-success rounded mx-auto px-4' onClick={onRemoveUser}>
                      Confirm {actionLoader === 'removeSubscription' && <Spinner color='#2A9DF4' size={32} />}
                    </Button>
                  </div>
                )}
              </>
            )}
            {/* Free follower */}
            {!actionUser.userId.hasPlan && (
              <div className='text-center mb-4'>
                <Button variant='success' className='btn btn-success rounded mx-auto px-4' onClick={onRemoveUser}>
                  Confirm {actionLoader === 'removeSubscription' && <Spinner color='#2A9DF4' size={32} />}
                </Button>
              </div>
            )}
          </Modal.Body>
        )}
      </Modal>
    </div>
  )
}

export default ProfileActiveFans
