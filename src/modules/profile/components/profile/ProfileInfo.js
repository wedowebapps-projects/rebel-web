import React, { useState } from 'react'
import { Spinner, UserMedia, Button, ReadMore } from 'src/components/shared'
import images from 'src/config/images'
import moment from 'moment'
import { followUserApi, unFollowUserApi, blockUserApi, submitSubscriptionTransactionApi, removeSubscriptionApi } from '../../api/profileApi'
import { useRouter } from 'next/router'
import { Modal } from 'src/components/app'
import { PaystackButton } from 'react-paystack'
import { v4 as uuidv4 } from 'uuid'
import { FlutterWaveButton } from 'flutterwave-react-v3'
import { constants, checkAndRedirectLogin, copyToClipboard } from 'src/utils/helpers'
import SubscriptionModelContainer from './SubscriptionModal'
import Link from 'next/link'
import { Col, Row } from 'react-bootstrap'
import { initiateChatApi } from 'store/apis/chatApis'
import toast from 'src/utils/toast'

const dateFormat = 'YYYY-MM-DD'
function ProfileInfo({ profile, fetchUserAction, ...props }) {
  const Router = useRouter()
  const url = new URL(window.location)
  const [btnLoading, setBtnLoading] = useState(false)
  const [chatBtnLoading, setChatBtnLoading] = useState(false)
  const [paymentState, setPaymentState] = useState({})
  const [isConformationModalOpen, setIsConformationModalOpen] = useState(false)
  const [favoriteLoader, setFavoriteLoader] = useState(false)
  const [visibleOption, setVisibleOption] = useState(false)
  const [blockPrompt, setBlockPrompt] = useState(false)
  const [removeAction, setRemoveAction] = useState('')
  const [isRemoving, setIsRemoving] = useState(false)

  const toggleOption = () => {
    setVisibleOption(state => !state)
  }

  const last_seen = moment.utc(profile.last_seen).local()
  const duration = moment.duration(
    moment().diff(
      moment
        .utc(last_seen)
        .local()
        .format()
    )
  )
  const online_status = duration.asMinutes() < 2 ? 'online' : last_seen.fromNow()

  const plans = profile.plans ? profile.plans : []
  if (plans.length > 0) {
    plans.map(plan => {
      plan.plan = JSON.parse(plan.plan_data)
    })
  }
  let subBtnText = 'Follow Free'

  if (plans.length > 0) {
    if (plans.length > 1) {
      subBtnText = `View Subscription Plans`
    } else {
      subBtnText = `Subscribe for ${plans[0].plan.currency} ${process.env.PAYMENT_METHOD === 'flutterwave' ? plans[0].plan.amount : plans[0].plan.amount / 100} (${plans[0].plan.interval})`
    }
  }

  const redirectLogin = () => {
    Router.push(`/login${Router.asPath !== '/login' && Router.asPath !== '/' ? `?return=${Router.asPath}` : ''}`)
  }

  const onMarkFavorite = async () => {
    if (checkAndRedirectLogin()) {
      try {
        setFavoriteLoader(true)
        const data = { user_id: profile.id }
        data.list_type = !profile.isStarred ? 'favorites' : 'unfavorites'
        fetchUserAction(data)
          .then(() => {
            setFavoriteLoader(false)
          })
          .catch(() => {
            setFavoriteLoader(false)
          })
      } catch (error) {
        console.log(error)
      }
    } else {
      redirectLogin()
    }
  }

  const subscribeUser = async () => {
    if (checkAndRedirectLogin()) {
      setBtnLoading(true)
      if (plans.length === 0) {
        try {
          await followUserApi(profile.id, { isFree: true })
          location.reload()
        } catch (error) {
          console.log(error)
        }
      } else if (plans.length === 1) {
        try {
          const plan_data = JSON.parse(plans[0].plan_data)
          await MakePayment(plan_data)
        } catch (error) {
          console.log(error)
        }
      } else {
        props.toggleSubscriptionModal()
      }
      setBtnLoading(false)
    } else {
      if (plans.length !== 0) {
        props.toggleSubscriptionModal()
      } else {
        redirectLogin()
      }
    }
  }

  const unSubscribeUser = async () => {
    if (checkAndRedirectLogin()) {
      setBtnLoading(true)
      try {
        await unFollowUserApi(profile.id)
        location.reload()
      } catch (error) {
        console.log(error)
      }
      setBtnLoading(false)
    } else {
      redirectLogin()
    }
  }

  const initiateNewChat = async () => {
    if (checkAndRedirectLogin()) {
      setChatBtnLoading(true)
      try {
        let chat = await initiateChatApi({ from_id: props.auth.user.id, to_id: profile.id })
        if (chat?.data) {
          Router.push(`/chat/${chat?.data?.room_id}`)
        }
      } catch (error) {
        console.log(error)
      }
      setChatBtnLoading(false)
    } else {
      redirectLogin()
    }
  }

  const onRemoveUser = async () => {
    try {
      setIsRemoving(true)
      let data = {}
      if (removeAction === 'block') {
        await blockUserApi(profile.id)
        data = { isBlockedByYou: true }
        Router.push('/settings/accountPrivacy/blocked-accounts')
      } else {
        await removeSubscriptionApi(profile.id)
      }
      props.updateProfile({ ...profile, ...data, hasPlan: null, hasSubscribed: false })
      setBlockPrompt(false)
      setIsRemoving(false)
    } catch (error) {
      setIsRemoving(false)
      console.log(error)
    }
  }

  //payment methods
  const MakePayment = plan_data => {
    if (checkAndRedirectLogin()) {
      const paystackConfig = {
        reference: new Date().getTime(),
        email: props.auth.user.email,
        amount: plan_data.amount,
        publicKey: process.env.PAYSTACK_publicKey,
        onSuccess: reference => handlePaystackSuccessAction(reference, plan_data),
        onClose: error => handlePaystackCloseAction(error)
      }
      const flutterwaveConfig = {
        public_key: process.env.FLUTTERWAVE_SECRET,
        tx_ref: `profile-${profile.id}-${uuidv4()}`,
        currency: 'NGN',
        amount: plan_data.amount,
        payment_options: 'card',
        payment_plan: plan_data.id,
        customer: {
          email: props.auth.user.email,
          name: `${props.auth.user.first_name} ${props.auth.user.last_name}`
        },
        customizations: {
          title: 'Rebelyus.com',
          description: 'Payment for subcription',
          logo: images.app.logo
        },
        callback: async response => {
          try {
            const data = {
              plan_id: plan_data.id,
              amount: plan_data.amount,
              tx_ref: response.tx_ref,
              transaction_id: response.transaction_id
            }
            await submitSubscriptionTransactionApi(profile.id, data)
            location.reload()
          } catch (error) {
            console.log(error)
          }
        },
        onClose: () => {
          setIsConformationModalOpen(false)
          setPaymentState({})
        }
      }
      setPaymentState({ plan_data, paystackConfig, flutterwaveConfig })
      setIsConformationModalOpen(true)
    } else {
      redirectLogin()
    }
  }

  const handlePaystackSuccessAction = async (reference, plan_data) => {
    try {
      const data = {
        plan_id: plan_data.id,
        amount: plan_data.amount / 100,
        transaction_id: reference.trxref,
        tx_ref: reference.transaction
      }
      await submitSubscriptionTransactionApi(profile.id, data)
      location.reload()
    } catch (error) {
      console.log(error)
      if (error?.errors?.length) {
        error?.errors?.map(err => toast.error(err.msg))
      }
    }
  }

  const handlePaystackCloseAction = () => {
    setIsConformationModalOpen(false)
    setPaymentState({})
  }

  const getRefundableAmount = () => {
    const hasPlan_data = profile?.hasPlan ? JSON.parse(profile.hasPlan.plan_data) : {}
    const commissionAndCharges = profile?.hasPlan ? JSON.parse(profile.hasPlan.commissionAndCharges) : {}
    const commissions = ((hasPlan_data.amount * commissionAndCharges.commmission) / 100).toFixed(2)
    let fees
    if (process.env.PAYMENT_METHOD === 'flutterwave') {
      fees = ((hasPlan_data.amount * commissionAndCharges.flutterwave_fees) / 100).toFixed(2)
    } else {
      fees = ((hasPlan_data.amount * commissionAndCharges.paystack_fees) / 100).toFixed(2)
    }
    let finalAmount
    if (process.env.PAYMENT_METHOD === 'flutterwave') {
      finalAmount = (hasPlan_data.amount - commissions - fees).toFixed(2)
    } else {
      finalAmount = ((hasPlan_data.amount - commissions - fees) / 100).toFixed(2)
    }
    return finalAmount
  }
  const hasPlan_data = profile?.hasPlan ? JSON.parse(profile.hasPlan.plan_data) : {}
  const personal_info = profile?.personal_info ? JSON.parse(profile.personal_info) : {}
  const extra_charge = () => {
    if (process.env.PAYMENT_METHOD === 'flutterwave') {
      return ((getRefundableAmount() * props.app.commission.flutterwave_fees) / 100).toFixed(2)
    } else {
      return ((getRefundableAmount() * props.app.commission.paystack_fees) / 100).toFixed(2)
    }
  }

  return (
    <div className='profile-bottom-section'>
      {props.is_subscriptionModalVisible && (
        <SubscriptionModelContainer
          {...props}
          profile={profile}
          MakePayment={MakePayment}
          paymentState={paymentState}
          setPaymentState={setPaymentState}
          isConformationModalOpen={isConformationModalOpen}
          setIsConformationModalOpen={setIsConformationModalOpen}
        />
      )}
      <div className='d-flex align-items-center justify-content-between mb-t-20 flex-mobile-column-reverse'>
        <div className='d-flex align-items-center w-100 justify-content-between justify-content-sm-start' style={{ flex: 1 }}>
          <UserMedia user={profile} size={64} />
          <div className='copy-profile-link ml-4' onClick={() => copyToClipboard(`${url.origin}/${profile.username}`)}>
            <img src={images.profile.copyIcon} />
            <span>Copy profile link</span>
          </div>
        </div>

        {Router.route !== '/account' && (
          <>
            <div className='d-flex align-items-center w-mobile-100 justify-content-between mb-t-20'>
              {online_status == 'online' ? (
                <div className='post-time green-color green-bg-faded mr-4'>
                  <i className='dot rounded green-bg' />
                  Active Now
                </div>
              ) : (
                <div className='post-time secondary-color secondary-bg-color-faded mr-4'>
                  <i className='dot rounded secondary-bg-color' />
                  {online_status}
                </div>
              )}

              <div className='d-flex'>
                {/* Block Button */}
                {!profile.isBlockedByYou && (
                  <div className='more-settings-wrp'>
                    <a href='javascript:;' onClick={toggleOption}>
                      <img src={images.profile.contextIcon} />
                    </a>
                    <div className='more-setings-link' style={{ display: visibleOption ? 'block' : 'none' }}>
                      <a
                        href='javascript:;'
                        onClick={e => {
                          e.preventDefault()
                          setBlockPrompt(true)
                          setRemoveAction('block')
                        }}>
                        {' '}
                        Block user{' '}
                      </a>
                    </div>
                  </div>
                )}

                <div className='ml-2'>
                  <a href='#' onClick={onMarkFavorite}>
                    {favoriteLoader ? <Spinner color='#2A9DF4' size={36} /> : profile.isStarred ? <img src={images.profile.iconFavActive} /> : <img src={images.profile.iconFav} />}
                  </a>
                </div>
              </div>
            </div>
          </>
        )}
      </div>
      <div className='d-flex align-items-start flex-column flex-sm-row mt-4'>
        <div style={{ flex: 1.5 }}>
          <ReadMore words={140} text={profile.bio} />
          <div className='d-flex flex-wrap mt-t-30 mt-3'>
            {profile.website && (
              <a href={profile.website} target='_blank' className='attachment primary-color mr-5'>
                <img src={images.profile.website} className='mr-2' />
                {profile.website}
              </a>
            )}
            {profile.address && (
              <a className='location primary-color'>
                <img src={images.profile.location} className='mr-2' />
                {profile.address}
              </a>
            )}
          </div>
        </div>

        {Router.route !== '/account' ? (
          <div className='profile-buttons'>
            {/* {profile?.isSubscribed && profile?.plans && profile?.subscriptionData && moment(profile?.subscriptionData?.end_date).diff(moment(), 'days') > 0 && (
              <Button variant='secondary' className='btn msg-btn' onClick={initiateNewChat}>
                {chatBtnLoading ? <Spinner color='#2A9DF4' size={36} /> : <img src={images.profile.dmIcon} />}
              </Button>
            )} */}
            {profile?.isSubscribed && (
              <Button variant='secondary' className='btn msg-btn' onClick={initiateNewChat}>
                {chatBtnLoading ? <Spinner color='#2A9DF4' size={36} /> : <img src={images.profile.dmIcon} />}
              </Button>
            )}
            {profile.isSubscribed ? (
              profile.plans && profile?.subscriptionData && profile.planPurchased ? (
                <Button variant='secondary' className='btn'>
                  Subscribed
                  {btnLoading && <Spinner color='#2A9DF4' size={36} />}
                </Button>
              ) : (
                <Button variant='secondary' className='btn' onClick={unSubscribeUser}>
                  Following
                  {btnLoading && <Spinner color='#2A9DF4' size={36} />}
                </Button>
              )
            ) : (
              <Button className='btn profile-subscribe-btn' onClick={subscribeUser}>
                {btnLoading && <Spinner color='#2A9DF4' size={36} />}
                {subBtnText}
              </Button>
            )}

            {profile.isSubscribed && personal_info?.bank && (
              <Button
                className='btn'
                onClick={e => {
                  e.preventDefault()
                  if (!profile.isTipsAdded) props.setProfileTipModalVisible(true)
                }}
                data-modal='sendTipModal'>
                Send Tip <span className='naira-currency text-primary'>₦</span>
              </Button>
            )}
          </div>
        ) : (
          <div className='mt-t-30 d-flex justify-content-end' style={{ flex: 1 }}>
            <Link href={'/settings/your-account'} to={'/settings/your-account'}>
              <button type='' className='edit-pro-btn primary-bg-color btn color-white'>
                <img src={images.profile.editSqure} alt='' />
                Edit Profile
              </button>
            </Link>
          </div>
        )}
      </div>

      {/* SHOW AUTO RENEWS BUTTON */}
      {Router.route !== '/account' && profile.plans && profile?.subscriptionData && profile.planPurchased && profile.isSubscribed && (
        <div className='d-flex align-items-center justify-content-end flex-mobile-column-reverse flex-wrap'>
          <div className='date-wrap'>
            Renews on {profile?.subscriptionData?.end_date}
            &nbsp;&nbsp; ({' '}
            <a href='javascript:;' className='text-primary mr-0' onClick={unSubscribeUser}>
              Cancel Subscription
            </a>{' '}
            )
          </div>
        </div>
      )}

      {/* SHOW START DATE - END DATE IF SUBSCRIPTION IS CANCELED */}
      {Router.route !== '/account' && profile.plans && profile?.subscriptionData && !profile.planPurchased && profile.isSubscribed && (
        <div className='d-flex align-items-center justify-content-end flex-mobile-column-reverse flex-wrap'>
          <div className='date-wrap'>
            <span>
              Start date <span className='ml-2 mr-0'>{profile?.subscriptionData?.start_date}</span>
            </span>
            <span className='mr-0'>
              Expiry date <span className='ml-2 mr-0'>{profile?.subscriptionData?.end_date}</span>
            </span>
          </div>
        </div>
      )}

      {/*------------------ To subscribe user ------------------- */}
      <Modal show={isConformationModalOpen} keyboard={true} size='md' onClose={() => setIsConformationModalOpen(false)} centered backdropClassName='subscription-checkout-backdrop'>
        <div className='d-flex mb-2 justify-content-between align-items-center flex-column'>
          <h2 className='mb-4'>
            {constants.NAIRA} {process.env.PAYMENT_METHOD === 'flutterwave' ? paymentState?.plan_data?.amount : paymentState?.plan_data?.amount / 100}
          </h2>
          {process.env.PAYMENT_METHOD === 'flutterwave' ? (
            <FlutterWaveButton {...paymentState?.flutterwaveConfig} text={'Make Payment'} className='btn btn-success btn-lg rounded px-4' />
          ) : (
            <PaystackButton {...paymentState?.paystackConfig} text={'Make Payment'} className='btn btn-success btn-lg rounded px-4' />
          )}
          <a
            href='/'
            className='btn-link btn mt-3'
            onClick={e => {
              e.preventDefault()
              setIsConformationModalOpen(false)
              setPaymentState({})
            }}>
            Cancel
          </a>
        </div>
      </Modal>

      <Modal show={blockPrompt} onClose={() => setBlockPrompt(false)} centered keyboard={true} size='lg'>
        {/* For add money */}
        <h2 className='text-center'>{removeAction === 'block' ? `Block @${profile.username}?` : `Remove ${profile.hasPlan ? 'subscriber' : 'fan'} @${profile.username}?`}</h2>
        <br />
        {profile.hasPlan && (
          <>
            <div className='text-center my-2'>
              <strong>
                {hasPlan_data.name} - NGN {process.env.PAYMENT_METHOD === 'flutterwave' ? hasPlan_data.amount : hasPlan_data.amount / 100} - {hasPlan_data.interval}
              </strong>
            </div>
            <Row className='my-2'>
              <Col xs={5} md={6} className='text-right'>
                Subscribed On:
              </Col>
              <Col xs={7} md={6}>
                <strong>
                  {moment
                    .utc(profile.hasPlan.createdAt)
                    .local()
                    .format('DD MMM, YYYY HH:mm')}
                </strong>
              </Col>
            </Row>
            <Row className='my-2'>
              <Col xs={5} md={6} className='text-right'>
                Refundable:
              </Col>
              <Col xs={7} md={6}>
                <strong>NGN {getRefundableAmount()}</strong>
              </Col>
            </Row>
            <br />

            {/* Add Money to wallet */}
            {props.auth.user.wallet.available < getRefundableAmount() && (
              <>
                <p className='text-center'>
                  To proceed, please add{' '}
                  <span className='fa-lg'>
                    <strong>NGN {getRefundableAmount()}</strong>
                  </span>{' '}
                  to your wallet
                </p>
                <div className='text-center my-4'>
                  {process.env.PAYMENT_METHOD === 'flutterwave' ? (
                    <FlutterWaveButton {...flutterwaveConfig} text={'Add Money'} className='btn btn-success btn-lg rounded px-4' />
                  ) : (
                    <PaystackButton {...topupConfig} text={'Add Money'} className='btn btn-success rounded px-4' />
                  )}
                </div>
                <p className='mb-0 mt-4 text-center'>
                  <small className='text-muted'>
                    Extra charge of NGN <strong> {extra_charge()}</strong> as banking fees applicable.
                  </small>
                </p>
              </>
            )}

            {/* Confirm Block */}
            {props.auth.user.wallet.available >= getRefundableAmount() && (
              <div className='text-center mb-4'>
                <Button className='btn  rounded px-4 m-auto' onClick={onRemoveUser}>
                  Confirm
                  {isRemoving && <Spinner color='#2A9DF4' size={36} />}
                </Button>
              </div>
            )}
          </>
        )}
        {!profile.hasPlan && (
          <div className='text-center mb-4'>
            <Button className='btn  rounded px-4 m-auto' onClick={onRemoveUser}>
              Confirm
              {isRemoving && <Spinner color='#2A9DF4' size={36} />}
            </Button>
          </div>
        )}
      </Modal>
    </div>
  )
}

export default ProfileInfo
