import React from 'react'
import ContentLoader, { Facebook, Instagram } from 'react-content-loader'
import { connect } from 'react-redux'

function ProfileLoader(props) {
  const backgroundColor = props.theme == 'dark' ? '#2A9DF420' : '#2A9DF430'
  const foregroundColor = props.theme == 'dark' ? '#2A9DF435' : '#2A9DF445'

  return (
    <React.Fragment>
      <ContentLoader speed={2} viewBox='0 0 100% 156' height={156} width={'100%'} backgroundColor={backgroundColor} foregroundColor={foregroundColor}>
        <rect x='0' y='0' rx='3' ry='3' width={'100%'} height='156' />
      </ContentLoader>

      <div className='profile-bottom-section'>
        <div className='d-flex align-items-start justify-content-between mb-t-20 flex-mobile-column-reverse'>
          <ContentLoader speed={2} viewBox='0 0 100% 200' height={200} width={'100%'} backgroundColor={backgroundColor} foregroundColor={foregroundColor}>
            <rect x='80' y='18' rx='3' ry='3' width='200' height='10' />
            <rect x='80' y='38' rx='3' ry='3' width='100' height='10' />
            <rect x='0' y='90' rx='3' ry='3' width={'100%'} height='10' />
            <rect x='0' y='110' rx='3' ry='3' width={'50%'} height='10' />
            <circle cx='32' cy='32' r='32' />
          </ContentLoader>
        </div>
      </div>
    </React.Fragment>
  )
}

const mapStateToProps = state => ({
  theme: state.app.theme
})

const mapDispatchToProps = {}

export default connect(mapStateToProps, mapDispatchToProps)(ProfileLoader)
