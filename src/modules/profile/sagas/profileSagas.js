import { takeLatest, all, fork, call, put, select } from 'redux-saga/effects'
import * as types from '../actions/profileTypes'
import * as profileApi from '../api/profileApi'
import * as postApi from 'src/modules/post/api/postApis'
import * as profileActions from '../actions/profileActions'
import { getSubscriptions } from 'src/api/appApis'

export const getUserToken = state => state.auth.token

function* fetchProfile({ username }) {
  yield put(profileActions.setProfileLoading(true))
  try {
    let token = yield select(getUserToken)
    const profile = yield call(profileApi.fetchProfile, username, token)
    yield put(profileActions.setProfile(profile.user))
  } catch (error) {
    console.log('error : ', error)
  }
  yield put(profileActions.setProfileLoading(false))
}

function* setUserActions({ data, resolve, reject }) {
  try {
    const apiData = yield call(profileApi.setUserActionApi, data)
    let profile = yield select(state => state.profile.profile)

    switch (data.list_type) {
      case 'favorites':
        yield put(profileActions.setUserAction({ ...profile, isStarred: true }))
        break

      case 'unfavorites':
        yield put(profileActions.setUserAction({ ...profile, isStarred: false }))
        break

      case 'bookmarks':
        yield put(profileActions.setUserAction({ ...profile }))
        break

      case 'remove-bookmarks':
        yield put(profileActions.setUserAction({ ...profile }))
        break

      default:
        break
    }

    resolve()
  } catch (error) {
    reject()
    console.log('error : ', error)
  }
}

function* fetchProfilePosts({ data }) {
  yield put(
    profileActions.setProfilePostsLoading({
      type: data.type,
      loading: true
    })
  )
  try {
    let token = yield select(getUserToken)
    let posts
    let { username, type, ...otherFilter } = data
    const res = {
      type: data.type
    }
    switch (data.type) {
      case 'images':
        posts = yield call(profileApi.fetchUserImages, data, data.username)
        res.posts = posts.posts
        break

      case 'videos':
        posts = yield call(profileApi.fetchUserVideos, data, data.username)
        res.posts = posts.posts
        break

      case 'favorites':
      case 'subscriptions':
      case 'fans':
      case 'blocked':
        posts = yield call(getSubscriptions, data.type, { ...otherFilter })
        res.posts = posts.data
        break

      case 'paid':
      case 'liked':
        posts = yield call(postApi.fetchProtectedPosts, { [type]: true, ...otherFilter }, token)
        res.posts = posts.posts
        break

      case 'feed':
      default:
        posts = yield call(profileApi.fetchPostsByUsername, data, data.username)
        if(posts.posts)
        {
          res.posts = posts.posts
        }
        else{
          res.posts = posts.data.rows
        }
        
        break
    }
    yield put(profileActions.setProfilePosts(res))
  } catch (error) {
    console.log('error : ', error)
  }
  yield put(
    profileActions.setProfilePostsLoading({
      type: data.type,
      loading: false
    })
  )
}

export function* watchSagas() {
  yield takeLatest(types.FETCH_PROFILE, fetchProfile)
  yield takeLatest(types.FETCH_PROFILE_POSTS, fetchProfilePosts)
  yield takeLatest(types.FETCH_USER_ACTION, setUserActions)
  //   yield takeLatest(types.LIKE_UNLIKE_COMMENT_PROFILE_POST, likeUnlikeCommentSaga)
  //   yield takeLatest(types.FETCH_PROFILE_POST_COMMENTS, fetchProfilePostCommentsSaga)
  //   yield takeLatest(types.FETCH_REPLIES_PROFILE, fetchRepliessSaga)
}

export default function* runSagas() {
  yield all([fork(watchSagas)])
}
