import { takeLatest, all, fork, call, put, select } from 'redux-saga/effects'
import * as types from '../actions/dashbordTypes'
import * as actions from '../actions/dashbordActions'
import * as apis from '../api/dashboardApi'

function* fetchOverviewReportSaga({ filters }) {
  yield put(actions.OverviewLoading(true))
  try {
    const report = yield call(apis.fetchOverviewReport, filters)
    yield put(actions.setOverviewReportAction(report.data))
  } catch (error) {
    console.log('error : ', error)
  }
  yield put(actions.OverviewLoading(false))
}

function* fetchAnalyticsReportSaga({ filters }) {
  yield put(actions.AnalyticsLoading(true))
  try {
    const report = yield call(apis.fetchAnalyticsReport, filters)
    yield put(actions.setAnalyticsReportAction(report.data))
  } catch (error) {
    console.log('error : ', error)
  }
  yield put(actions.AnalyticsLoading(false))
}

export function* watchSagas() {
  yield takeLatest(types.FETCH_OVERVIEW_REPORT, fetchOverviewReportSaga)
  yield takeLatest(types.FETCH_ANALYTICS_REPORT, fetchAnalyticsReportSaga)
}

export default function* runSagas() {
  yield all([fork(watchSagas)])
}
