import { takeLatest, all, fork, call, put, select } from 'redux-saga/effects'
import * as types from '../actions/bookmarkTypes'
import * as bookmarkApi from '../api/bookmarkApi'
import * as bookmarkActions from '../actions/bookmarkActions'
import { fetchProtectedPosts } from 'src/modules/post/api/postApis'

export const getUserToken = state => state.auth.token

function* fetchPosts({ filters: { end, ...filter } }) {
  yield put(bookmarkActions.setBookmarkPostLoading(true))
  try {
    let token = yield select(getUserToken)
    const post = yield call(fetchProtectedPosts, filter, token)
    yield put(bookmarkActions.setBookmarkPosts(post.posts, end))
  } catch (error) {
    console.log('error : ', error)
  }
  yield put(bookmarkActions.setBookmarkPostLoading(false))
}

export function* watchSagas() {
  yield takeLatest(types.FETCH_BOOKMARK_POST, fetchPosts)
}

export default function* runSagas() {
  yield all([fork(watchSagas)])
}
