import * as types from './dashbordTypes'

export const OverviewLoading = data => ({ type: types.OVERVIEW_LOADING, data })
export const fetchOverviewReportAction = filters => ({ type: types.FETCH_OVERVIEW_REPORT, filters })
export const setOverviewReportAction = data => ({ type: types.SET_OVERVIEW_REPORT, data })

export const AnalyticsLoading = data => ({ type: types.ANALYTICS_LOADING, data })
export const fetchAnalyticsReportAction = filters => ({ type: types.FETCH_ANALYTICS_REPORT, filters })
export const setAnalyticsReportAction = data => ({ type: types.SET_ANALYTICS_REPORT, data })
export const updateTopPosts = data => ({ type: types.UPDATE_TOP_POSTS, data })
