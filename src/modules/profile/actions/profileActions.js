import * as types from './profileTypes'

export const resetProfile = () => ({ type: types.RESET_PROFILE })

export const fetchUserAction = (data, resolve, reject) => ({ type: types.FETCH_USER_ACTION, data, resolve, reject })
export const setUserAction = data => ({ type: types.SET_USER_ACTION, data })
export const fetchProfilePosts = data => ({ type: types.FETCH_PROFILE_POSTS, data })
export const setProfilePosts = data => ({ type: types.SET_PROFILE_POSTS, data })
export const setProfilePostsLoading = data => ({ type: types.SET_PROFILE_POST_LOADING, data })
export const updateProfilePosts = (data) => ({ type: types.UPDATE_PROFILE_POSTS, data })

export const fetchProfile = username => ({ type: types.FETCH_PROFILE, username })
export const setProfile = data => ({ type: types.SET_PROFILE, data })
export const setProfileLoading = loading => ({ type: types.SET_PROFILE_LOADING, loading })

export const toggleSubscriptionModal = () => ({ type: types.TOGGLE_SUBSCRIPTION_MODAL })

// Purchase Post
export const setPurchasePostAction_profile = post_id => ({
  type: types.SET_PURCHASE_POST_PROFILE,
  post_id
})
export const setSendTipPostAction_profile = post_id => ({
  type: types.SET_SENDTIP_POST_PROFILE,
  post_id
})
