import * as types from './bookmarkTypes'

export const fetchBookmarkPosts = filters => ({ type: types.FETCH_BOOKMARK_POST, filters })
export const setBookmarkPosts = (posts, end) => ({ type: types.SET_BOOKMARK_POST, posts, end })

export const setBookmarkPostLoading = loading => ({ type: types.SET_BOOKMARK_POST_LOADING, loading })

export const updateBookmarkPosts = data => ({ type: types.UPDATE_BOOKMARK_POST, data })