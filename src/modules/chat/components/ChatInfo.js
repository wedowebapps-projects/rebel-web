import _ from 'lodash'
import Link from 'next/link'
import React from 'react'
import { Avatar, Spinner } from 'src/components/shared'
import images from 'src/config/images'

const ChatInfo = props => {
  const toId = _.get(props, ['chat', 'activeChat', 'toId'], '')
  const interest = _.get(props, ['chat', 'activeChat', 'toId', 'interest'], '')

  return (
    <div className='home-sidebar-col h-100 bg-color-white p-0 ml-2 mt-4 sticky-side'>
      <aside className='home-sidebar d-none d-md-block px-3'>
        {props.chat.showLoader && props.chat.pages === 1 ? (
          <div className='d-flex justify-content-center my-5'>
            <Spinner color='#2A9DF4' size={36} />
          </div>
        ) : (
          <div className='aside-inner-wrap'>
            <div className='sidebar-title'>
              <span>Info</span>
            </div>
            <div className='d-flex flex-wrap'>
              <div className='card suggestion-card-fluid'>
                <div className='card-img'>
                  <img src={toId?.cover_image} />
                </div>
                <div className=''>
                  <div className='ml-3'>
                    <div className='user-avtar avtar-42 mt--20'>
                      <Avatar name={toId?.first_name} avatarUrl={toId?.avatar} size={43} />
                    </div>
                    <div className='media-body text-left  mb-4'>
                      <h5 className='mb-1'>
                        <Link href='/[username]' as={`/${props?.chat?.activeChat?.toId?.username}`}>
                          <a href={null} className='mr-2'>
                            {`${toId?.first_name} ${toId?.last_name}`}
                          </a>
                        </Link>
                        {toId.isFeatured && <img src={images.app.verifiedIcon} />}
                      </h5>
                      <p className='mt-0'>{` @${toId?.username}`} </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className='sidebar-title'>
              <span>Interests</span>
            </div>

            <div className='interests-menu'>
              {(interest && interest.length === 0) || !interest ? (
                <p>No interest available</p>
              ) : (
                <ul>
                  {interest && interest.map(interest => {
                      return (
                        <li key={`interest-${interest}`}>
                          <a href='#'>{interest}</a>
                        </li>
                      )
                    })}
                </ul>
              )}
            </div>
          </div>
        )}
      </aside>
    </div>
  )
}

export default ChatInfo
