import imageCompression from 'browser-image-compression'
import { useRouter } from 'next/router'
import React, { useRef, useState } from 'react'
import InfiniteScroll from 'react-infinite-scroll-component'
import io from 'socket.io-client'
import { Modal } from 'src/components/app'
import { Avatar, Form } from 'src/components/shared'
import images from 'src/config/images'
import { AllowedMediaTypes, ImageMimes, VideoMimes } from 'src/utils/helpers'
import { upload } from 'src/utils/s3'
import * as Yup from 'yup'

const BroadcastModal = props => {
  const router = useRouter()
  const myimageinput = useRef()
  const myvideoinput = useRef()
  const content = useRef()
  const [files, setFiles] = useState([])
  const [isFormSubmitting, setIsFormSubmitting] = useState(false)
  const [isUserlistVisible, setIsUserlistVisible] = useState(false)
  const [allSelected, setAllSelected] = useState(false)
  const [selectedUser, setSelectUser] = useState([])
  const [tempUser, setTempUser] = useState([])
  const [socket, setSocket] = useState(null)
  var maxLength = props?.listType?.filter(l => l.type === 'fans')?.count || 0

  React.useEffect(() => {
    setSocket(null)
    reinitiateSocket()
    return () => {
      socket?.off('sendBroadcastMessage')
      socket?.disconnect()
      setSocket(null)
    }
  }, [props?.auth?.user])

  const reinitiateSocket = () => {
    const username = props?.auth?.user?.username
    const endpoint = `${process.env.CHAT_SOCKET_URL}?username=${username}` // PRODUCTION
    setTimeout(() => {
      setSocket(io(endpoint, { transports: ['polling'] }))
    })
  }

  const removeFromList = (e, val) => {
    e.preventDefault()
    if (Array.isArray(tempUser) && tempUser.length > 0) {
      setAllSelected(false)
      let temp = [...tempUser]
      let i = temp.indexOf(val)
      temp.splice(i, 1)
      setTempUser(temp)
    }
  }
  const removeFromSelected = (e, val) => {
    e.preventDefault()
    if (Array.isArray(selectedUser) && selectedUser.length > 0) {
      setAllSelected(false)
      let temp = [...selectedUser]
      let i = temp.indexOf(val)
      temp.splice(i, 1)
      setSelectUser(temp)
      setTempUser(temp)
    }
  }
  const addToList = (e, val) => {
    e.preventDefault()
    if (Array.isArray(tempUser)) {
      setAllSelected(false)
      let temp = [...tempUser]
      temp.push(val)
      setTempUser(temp)
    }
  }

  const previewFiles = e => {
    const selectedFile = e.target.files
    for (const file of selectedFile) {
      if (ImageMimes.includes(file.type)) {
        const reader = new FileReader()
        reader.onloadend = () => {
          file.dataUrl = reader.result
          setFiles([...files, file])
        }
        reader.readAsDataURL(file)
      } else {
        file.dataUrl = URL.createObjectURL(file)
        setFiles([...files, file])
      }
    }
  }
  const options = {
    maxSizeMB: 1,
    maxWidthOrHeight: 2000,
    maxIteration: 20
  }

  React.useEffect(() => {
    if (allSelected) {
      let allIds = props.fans.data.map(f => f.user_id)
      setTempUser(allIds)
    } else {
      setTempUser([])
    }
  }, [allSelected])

  return (
    <div className='send-broadcast-popups m-3'>
      <div className='modal-wrp'>
        <div className='modal-inner-wrap'>
          <div className='send-broadcast-popups-header'>
            <h3 className='m-0'>
              Send Broadcast
              <img src={images.chat.megaphone} />
            </h3>
            <button className='popupclose' onClick={props.closeModal}>
              <img src={images.chat.popupclose} />
            </button>
          </div>
          <Form
            initialValues={{
              content: ''
            }}
            validationSchema={Yup.object().shape({
              content: Yup.string('')
            })}
            onSubmit={async (values, form) => {
              // UPLOAD MEDIA FILES TO S3
              let uploadedFiles = []
              let data = {}
              try {
                if (values.content) {
                  setIsFormSubmitting(true)
                  if (files.length > 0) {
                    const compressedFiles = await Promise.all(
                      files.map(async imageFile => {
                        if (ImageMimes.includes(imageFile.type)) {
                          imageFile = await imageCompression(imageFile, options)
                          return imageFile
                        } else {
                          return imageFile
                        }
                      })
                    )
                    uploadedFiles = await upload(compressedFiles, props.auth.user.username, 'posts')
                    const mediaFiles = uploadedFiles.map(file => ({
                      type: ImageMimes.includes(file.type) ? 'image' : VideoMimes.includes(file.type) ? 'video' : '',
                      media_path: file.location
                    }))
                    data.mediaFiles = mediaFiles
                    data.processed_path = mediaFiles
                  }
                  data.user_ids = selectedUser
                  data.content = values.content
                  data.type = files.length === 0 ? 'text' : 'file'
                  data.from_id = props.auth.user.id
                  data.fromId = props.auth.user.id
                  setIsFormSubmitting(false)
                  if (socket) {
                    socket.emit('sendBroadcastMessage', data)
                    socket.off('sendBroadcastMessage')
                    setSocket(null)
                    props.fetchChatList()
                  }
                  setAllSelected(false)
                  setSelectUser([])
                  setTempUser([])
                  props.closeModal()
                  router.push('/chat')
                }
              } catch (err) {
                console.log('error', err)
                // toast.error('Something went wrong. please try again later.')
              }
              setIsFormSubmitting(false)
            }}>
            {prop => {
              return (
                <form
                  onSubmit={e => {
                    e.preventDefault()
                    prop.submitForm()
                    return false
                  }}
                  noValidate>
                  {/* <p>Welcome to my page everyone ! 😁</p> */}
                  <div className='broadcast-popups-inner border-wrap'>
                    <div className='followers-list'>
                      <div className='followers-search mb-2'>
                        <input type='text' name='search-username' onClick={() => setIsUserlistVisible(true)} placeholder='Search Username' />
                      </div>
                      {allSelected && (
                        <ul>
                          <li className='followers-yeallow'>
                            <span className='followers-user-avtar pr-1'>
                              <Avatar name='All' avatarUrl={null} size={18} />
                            </span>
                            All
                            <span
                              className='followers-user-close cursor-pointer'
                              onClick={e => {
                                e.preventDefault()
                                setAllSelected(false)
                              }}>
                              <img src={images.chat.f_close} />
                            </span>
                          </li>
                        </ul>
                      )}
                      {Array.isArray(selectedUser) && !allSelected && selectedUser.length > 0 && (
                        <ul>
                          {selectedUser.map((id, i) => {
                            let user = props?.fans?.data?.find(fn => fn.user_id === id)
                            return (
                              <li key={`follower-${id}-${i}`} className='followers-purpal'>
                                <span className='followers-user-avtar'>
                                  <Avatar name={user?.userId?.first_name} avatarUrl={user?.userId?.avatar} size={18} />
                                </span>
                                {` ${user?.userId?.first_name} ${user?.userId?.last_name}`}
                                <span className='followers-user-close cursor-pointer' onClick={e => removeFromSelected(e, user.userId.id)}>
                                  <img src={images.chat.f_close} />
                                </span>
                              </li>
                            )
                          })}
                        </ul>
                      )}
                      {isUserlistVisible && (
                        <div className='followers-search-suggestion bg-color-white' style={{ display: isUserlistVisible ? 'block' : 'none' }}>
                          <div className='followers-select-box mt-5' style={{ maxHeight: '270px', overflow: 'scroll' }}>
                            <button
                              className='popupclose'
                              onClick={e => {
                                e.preventDefault()
                                setIsUserlistVisible(false)
                              }}>
                              <img src={images.chat.popupclose} />
                            </button>
                            <div className='d-flex align-items-center justify-content-between pb-5'>
                              <div className='media align-items-center'>
                                <div className='user-avtar avtar-48 mr-3'>
                                  <Avatar name={'All'} size={47} avatarUrl={null} />
                                </div>
                                <div className='media-body text-left'>
                                  <h4 className='font-weight-normal m-0'>
                                    <a href={null} className='mr-2'>
                                      All
                                    </a>
                                  </h4>
                                  {/* <p className='m-0'>{`@${fan.userId.username}`}</p> */}
                                </div>
                              </div>
                              <div className='d-flex align-items-center'>
                                <div className='followers-select'>
                                  {allSelected ? (
                                    <button
                                      className='followers-selected-button'
                                      onClick={e => {
                                        e.preventDefault()
                                        setAllSelected(false)
                                      }}>
                                      Selected
                                    </button>
                                  ) : (
                                    <button
                                      className='followers-select-button'
                                      onClick={e => {
                                        e.preventDefault()
                                        setTempUser([])
                                        setAllSelected(true)
                                      }}>
                                      Select<span>+</span>
                                    </button>
                                  )}
                                </div>
                              </div>
                            </div>
                            <InfiniteScroll
                              dataLength={maxLength} //This is important field to render the next data
                              next={() => props.fetchPosts()}
                              hasMore={!props.fans.state.end}>
                              {props?.fans?.data?.map((fan, i) => {
                                return (
                                  <div key={`users-fans-${fan.id}-${i}`} className='d-flex align-items-center justify-content-between pb-5'>
                                    <div className='media align-items-center'>
                                      <div className='user-avtar avtar-48 mr-3'>
                                        <Avatar name={fan.userId.first_name} size={47} avatarUrl={fan.userId.avatar} />
                                      </div>
                                      <div className='media-body text-left'>
                                        <h4 className='font-weight-normal m-0'>
                                          <a href={null} className='mr-2'>
                                            {`${fan.userId.first_name} ${fan.userId.last_name}`}
                                          </a>
                                        </h4>
                                        <p className='m-0'>{`@${fan.userId.username}`}</p>
                                      </div>
                                    </div>
                                    <div className='d-flex align-items-center'>
                                      <div className='followers-select'>
                                        {tempUser.find(u => u === fan.userId.id) ? (
                                          <button className='followers-selected-button' onClick={e => removeFromList(e, fan.userId.id)}>
                                            Selected
                                          </button>
                                        ) : (
                                          <button className='followers-select-button' onClick={e => addToList(e, fan.userId.id)}>
                                            Select<span>+</span>
                                          </button>
                                        )}
                                      </div>
                                    </div>
                                  </div>
                                )
                              })}
                            </InfiniteScroll>
                          </div>
                          <button
                            className='custom-button primary-bg-color color-white w-100'
                            onClick={() => {
                              setSelectUser([...tempUser])
                              setIsUserlistVisible(false)
                            }}>
                            Add Selected
                          </button>
                        </div>
                      )}
                    </div>

                    <div className='followers-center-box' ref={content}>
                      <Form.Field.Textarea name='content' placeholder='Write something...' />
                    </div>

                    <ul className='chat-media-files'>
                      {files.map((file, i) => (
                        <li className='uploaded d-inline-block m-2' key={file.dataUrl + i}>
                          <div className='uploaded-image' data-index={1}>
                            {ImageMimes.includes(file.type) && <img src={file.dataUrl} alt={i} />}
                            {VideoMimes.includes(file.type) && (
                              <video controls width='100%' style={{ maxHeight: '100%', maxWidth: '100%' }}>
                                <source src={file.dataUrl} type={file.type} />
                                Your browser does not support the video tag.
                              </video>
                            )}
                            <button
                              className='delete-image'
                              onClick={e => {
                                let temp = files.splice(i + 1, 1)
                                setFiles(temp)
                              }}>
                              <img src={images.app.close} title='Delete' alt='Delete' />
                            </button>
                          </div>
                        </li>
                      ))}
                    </ul>
                  </div>
                  <div className='broadcast-popups-footer'>
                    <div className='broadcast-button-group d-flex'>
                      <button type='submit' disabled={isFormSubmitting} className='broadcast-send primary-bg-color custom-button '>
                        Send
                      </button>
                      {files.length !== 1 && (
                        <>
                          <input
                            type='file'
                            id='new-image'
                            style={{ display: 'none' }}
                            accept='image/*'
                            multiple={false}
                            accept={AllowedMediaTypes.join(',')}
                            ref={myimageinput}
                            onChange={e => {
                              e.preventDefault()
                              previewFiles(e)
                            }}
                          />
                          <button
                            className='broadcast-image'
                            onClick={e => {
                              e.preventDefault()
                              myimageinput.current.click()
                            }}>
                            <img src={images.chat.Image_icon} />
                          </button>
                          <input
                            type='file'
                            id='new-image'
                            style={{ display: 'none' }}
                            accept='video/*'
                            multiple={false}
                            accept={AllowedMediaTypes.join(',')}
                            ref={myvideoinput}
                            onChange={e => {
                              e.preventDefault()
                              previewFiles(e)
                            }}
                          />
                          <button
                            className='broadcast-video'
                            onClick={e => {
                              e.preventDefault()
                              myvideoinput.current.click()
                            }}>
                            <img src={images.chat.Video_icon} />
                          </button>
                        </>
                      )}
                    </div>
                    {/* <div className='input-box agree-conditions-radio'>
                          <input
                            type='radio'
                            id='conditions'
                            name='conditions'
                            value='Welcome to my page everyone ! 😁'
                            onChange={e => {
                              //e.preventDefault()
                              console.log(content, e.target.value)
                            }}
                          />
                          <label htmlFor='conditions'>Default welcome message for new fans</label>
                        </div> */}
                  </div>
                </form>
              )
            }}
          </Form>
        </div>
      </div>
    </div>
  )
}

export default BroadcastModal
