import _ from 'lodash'
import moment from 'moment'
import Link from 'next/link'
import { useRouter } from 'next/router'
import React, { useState } from 'react'
import { Modal } from 'src/components/app'
import { Avatar } from 'src/components/shared'
import images from 'src/config/images'
import { BroadcastModal } from '.'

const ChatList = ({ chat = [], ...props }) => {
  const { chatList } = chat
  const router = useRouter()
  const [searchText, setSearchText] = useState('')
  const [newChatList, setNewChatList] = useState([])
  const [isBroadcastModalOpen, setBroadcastModalOpen] = useState(false)

  const openModal = () => setBroadcastModalOpen(true)
  const closeModal = () => setBroadcastModalOpen(false)

  React.useEffect(() => {
    setNewChatList(chatList)
  }, [chatList])

  React.useEffect(() => {
    if (searchText) {
      let temp = _.filter(chatList, function(e) {
        return _.includes(_.lowerCase(`${e.toId.first_name} ${e.toId.last_name}`), _.lowerCase(searchText))
      })
      setNewChatList(temp)
    } else {
      setNewChatList(chatList)
    }
  }, [searchText])

  return (
    <aside className={`chat-list-box-col ml-2 mt-4 border-wrap ${router.pathname === '/chat/[chatId]' ? 'd-none d-md-block' : ''}`}>
      <Modal show={isBroadcastModalOpen} keyboard={true} size='lg' onClose={closeModal} centered>
        <BroadcastModal {...props} isBroadcastModalOpen={isBroadcastModalOpen} closeModal={closeModal} />
      </Modal>
      <div className='chat__and__list-box'>
        <div className='chat__and__list-toper'>
          <div className='sidebar-title'>
            <span>Conversations</span>
            {props.user.personal_info_status === 'approved' && (
              <button className='send-broadcast-button' onClick={openModal}>
                <img src={images.chat.voice} />
              </button>
            )}
          </div>
          <div className='search-input-wrap chat-list-search-input'>
            <input type='search' name='' placeholder='Search' value={searchText} onChange={e => setSearchText(e.target.value)} />
            {searchText ? (
              <img
                src={images.app.cross}
                onClick={() => {
                  setSearchText('')
                }}
              />
            ) : (
              <img src={images.app.search} />
            )}
          </div>
        </div>
        <div className='chat__and__list py-2'>
          <ul className='pb-5'>
            {_.map(newChatList, (chat, i) => {
              let messageTime = ''
              if (chat.lastMessage) {
                var duration = moment.duration(
                  moment().diff(
                    moment
                      .utc(chat.lastMessage.createdAt)
                      .local()
                      .format()
                  )
                )
                const mins = parseInt(duration.asMinutes())
                const hours = duration.asHours()
                const days = duration.asDays()
                if (days > 7) {
                  messageTime = moment
                    .utc(chat.lastMessage.createdAt)
                    .local()
                    .format('DD MMM')
                } else if (days > 1) {
                  messageTime = moment
                    .utc(chat.lastMessage.createdAt)
                    .local()
                    .format('ddd')
                } else if (hours > 1) {
                  messageTime = moment
                    .utc(chat.lastMessage.createdAt)
                    .local()
                    .format('hh:mm a')
                } else if (mins > 1) {
                  messageTime = `${mins} mins`
                } else {
                  messageTime = 'just now'
                }
              }
              return (
                <li className={`tablinks ${chat.room_id === props.chatId ? 'active' : ''}`} key={chat.id}>
                  <div className='media chat_list'>
                    <Link href='/[username]' as={`/${chat?.toId?.username}`}>
                      <div className='user-pic mr-2 cursor-pointer'>
                        <Avatar avatarUrl={chat.toId?.avatar} name={`${chat.toId?.first_name} ${chat.toId?.last_name}`} size={50} />
                      </div>
                    </Link>
                    <Link href='/chat/[roomId]' as={`/chat/${chat.room_id}`}>
                      <div className='media-body chat cursor-pointer'>
                        <div className='user_name'>
                          {chat.toId && (
                            <h5 className='cursor-pointer'>
                              {`${chat.toId?.first_name} ${chat.toId?.last_name}`}
                              {chat.toId?.personal_info !== null && chat.toId?.personal_info !== '' && <img src={images.app.verifiedIcon} className='mx-1' />}
                              <span>{` @${chat.toId?.username}`}</span>
                            </h5>
                          )}
                          <p className=''>{chat.lastMessage ? (chat.lastMessage.content === null ? 'File' : chat.lastMessage.content) : '-- No Message --'}</p>
                        </div>
                        <div className='time'>
                          {messageTime && <p className=''>{messageTime}</p>}
                          {!chat.isSubscribed && (
                            <span className='warning'>
                              <img src={images.chat.emojione_warning} />
                            </span>
                          )}
                          {chat.unreadCount > 0 && <span className='massage'></span>}
                        </div>
                      </div>
                    </Link>
                  </div>
                </li>
              )
            })}
          </ul>
        </div>
      </div>
    </aside>
  )
}

export default ChatList
