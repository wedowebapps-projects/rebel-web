import Link from 'next/link'
import { useRouter } from 'next/router'
import React, { useState } from 'react'
import LightBoxModal from '../../post/components/post/Modal'
import images from 'src/config/images'
import io from 'socket.io-client'
import { upload } from 'utils/s3'
import { AllowedMediaTypes, constants, ImageMimes, VideoMimes, copyToClipboard } from 'src/utils/helpers'
import { Avatar, Spinner } from 'src/components/shared'
import moment from 'moment'
import { Picker } from 'emoji-mart'
import imageCompression from 'browser-image-compression'
import _ from 'lodash'
import { Dropdown } from 'react-bootstrap'

const ChatMessageBox = ({ chatId, ...props }) => {
  const activeChat = _.get(props, ['chat', 'activeChat'], '')

  const [message, setMessage] = useState('')
  const [typing, setTyping] = useState('')
  const [lastTyping, setLastTyping] = useState('')
  const [showDots, setShowDots] = useState(false)
  const [chatMediaLoader, setChatMediaLoader] = useState(false)
  const [chatOptionOpen, setChatOptionOpen] = useState(false)
  const [files, setFiles] = useState([])
  const [newMessages, setNewMessages] = useState([])
  const [showPostLightBox, setPostLightBox] = useState(false)
  const [isEmojiPickerOpen, setIsEmojiPickerOpen] = useState(false)
  const [chatImageLightBox, setChatImageLightBox] = useState([])
  const [socket, setSocket] = React.useState(null)
  const [lastSeen, setLastSeen] = useState('offline')

  const router = useRouter()
  const input = React.useRef()
  const myInput = React.useRef()
  const messagesEnd = React.useRef()
  const lastPage = React.useRef()

  let postMediaImagesArr = []

  React.useEffect(() => {
    setMessage('')
    setTyping('')
    setLastTyping('')
    callSocket()
  }, [chatId])

  const callSocket = () => {
    if (props && router.query.chatId && props?.user?.username) {
      setTimeout(() => {
        const temp = io(`${process.env.CHAT_SOCKET_URL}?room_id=${router.query.chatId}&username=${props.user.username}`, { transports: ['polling'] })
        setSocket(temp)
        temp?.on('getMessage', onMessage)
        temp?.on('getBroadcastMessage', onMessage)
        temp?.on('getTyping', onTyping)
        temp?.on('getDeleteMessage', onDelete)
      })
      if (activeChat?.toId?.last_seen) {
        const last_seen = moment.utc(activeChat?.toId?.last_seen).local()
        const duration = moment.duration(
          moment().diff(
            moment
              .utc(last_seen)
              .local()
              .format()
          )
        )
        const online_status = duration.asMinutes() < 2 ? 'online' : last_seen.fromNow()
        setLastSeen(online_status)
      } else {
        setLastSeen('offline')
      }
    }
  }

  React.useEffect(() => {
    input?.current?.focus()
    socket?.on('connect', () => {})
    socket?.on('disconnect', data => {})
    return () => {
      socket?.off('getMessage')
      socket?.off('getBroadcastMessage')
      socket?.off('getTyping')
      socket?.off('getDeleteMessage')
      socket?.disconnect()
      setSocket(null)
    }
  }, [])

  React.useEffect(() => {
    let newMsgArray = []
    _.forEachRight(props?.chat?.chatRoom, function(value, index, array) {
      let temp = { ...value }
      if (index === 0) {
        temp['showDateStamp'] = true
      } else if (array[index - 1] && moment(value.createdAt).format('MMMM Do YYYY') !== moment(array[index - 1].createdAt).format('MMMM Do YYYY')) {
        temp['showDateStamp'] = true
      } else {
        temp['showDateStamp'] = false
      }
      newMsgArray.push(temp)
    })
    setNewMessages(_.reverse(newMsgArray || []))
  }, [props?.chat])

  React.useEffect(() => {
    if (messagesEnd.current) {
      if (props.chat.page === 1) scrollToBottom()
      else scrollToLastPage()
    }
  }, [newMessages, messagesEnd.current])

  const onTyping = typing => {
    if (typing.username !== props.auth.user.username && typing.room_id === activeChat?.room_id) {
      setTyping(typing.message)
      //  setLastTyping(moment())
      clearTimeout(time)
      let time = setTimeout(() => {
        // let duration = moment.duration(moment().diff(moment(lastTyping)))
        // if (duration.asSeconds() >= 1)
        setTyping('')
      }, 1000)
    }
  }
  const onMessage = message => {
    if (message.room_id === activeChat?.room_id) {
      const data = {
        room_id: chatId,
        from_id: message.from_id,
        fromId: activeChat.toId,
        to_id: message.to_id,
        type: message.type,
        is_broadcasting: message.is_broadcasting || false,
        processed_path: message.processed_path,
        content: message.content
      }
      props.appendChat(data)
      setTyping('')
      setTimeout(() => scrollToBottom())
    }
  }
  const onDelete = message => {
    if (message.room_id === activeChat?.room_id) {
      props.deleteMessage(message)
      setTyping('')
    }
  }

  const handleScrollTop = e => {
    if (e.target.scrollTop === 0 && newMessages.length >= constants.CHATS_PER_PAGE && props.chat.page !== 'noData') {
      props.loadMoreMessages()
    }
  }
  const onChangeInput = e => {
    setMessage(e)
    socket?.emit('setTyping', {})
  }
  const scrollToBottom = () => {
    messagesEnd?.current?.scrollIntoView()
  }
  const scrollToLastPage = () => {
    lastPage?.current?.scrollIntoView()
  }

  const closePostLightbox = () => {
    setPostLightBox(false)
    setChatImageLightBox([])
  }

  const previewChatFiles = e => {
    const selectedFile = e.target.files
    for (const file of selectedFile) {
      if (ImageMimes.includes(file.type)) {
        const reader = new FileReader()
        reader.onloadend = () => {
          file.dataUrl = reader.result
          setFiles([...files, file])
        }
        reader.readAsDataURL(file)
      } else {
        file.dataUrl = URL.createObjectURL(file)
        setFiles([...files, file])
      }
    }
  }

  const removeMedia = i => {
    let temp = files.splice(i + 1, 1)
    setFiles(temp)
  }

  const onSubmit = async values => {
    // IMAGE COMPRESSION OPTIONS
    const options = {
      maxSizeMB: 1,
      maxWidthOrHeight: 2000,
      maxIteration: 20
    }
    // UPLOAD MEDIA FILES TO S3
    let uploadedFiles = [],
      mediaFiles = [],
      data = {}

    try {
      if (message === '' && message === null && files.length === 0) {
        return false
      }
      const from_id = activeChat.from_id
      const to_id = activeChat.to_id
      data = {
        room_id: chatId,
        from_id: from_id,
        fromId: activeChat.fromId,
        to_id: to_id,
        content: message
      }
      if (files.length > 0) {
        setChatMediaLoader(true)
        const compressedFiles = await Promise.all(
          files.map(async imageFile => {
            if (ImageMimes.includes(imageFile.type)) {
              imageFile = await imageCompression(imageFile, options)
              return imageFile
            } else {
              return imageFile
            }
          })
        )
        uploadedFiles = await upload(compressedFiles, props.auth.user.username, 'posts')
        mediaFiles = uploadedFiles.map(file => ({
          type: ImageMimes.includes(file.type) ? 'image' : VideoMimes.includes(file.type) ? 'video' : '',
          media_path: file.location
        }))
        data.mediaFiles = mediaFiles
        data.processed_path = mediaFiles
        if (message === '' || message === null) {
          data.content = null
        }
      }
      data.type = files.length === 0 ? 'text' : 'file'
      if (socket !== null) {
        await socket?.emit('sendMessage', data)
      }

      // props.appendChat(data)
      setChatMediaLoader(false)
      setMessage('')
      setFiles([])
      setTimeout(() => scrollToBottom())
      input?.current?.focus()
    } catch (err) {
      console.log('error', err)
      //toast.error('Something went wrong. please try again later.')
    }
    setChatMediaLoader(false)
  }
  const ChatItemMenu = React.forwardRef(({ onClick }, ref) => (
    <div
      className='d-inline-flex cursor-pointer'
      ref={ref}
      onClick={e => {
        e.preventDefault()
        onClick(e)
      }}>
      <img src={images.chat.more_circle_dots} />
    </div>
  ))
  const ChatMoreMenu = React.forwardRef(({ onClick }, ref) => (
    <div
      className='d-inline-flex cursor-pointer'
      ref={ref}
      onClick={e => {
        e.preventDefault()
        onClick(e)
      }}>
      <img src={images.app.moreCircle} />
    </div>
  ))
  return (
    <div className={`chat-box-col chat-box-col-mobile ml-2 mt-4 mb-4 border-wrap ${router.pathname !== '/chat/[chatId]' ? 'd-none d-md-block' : ''}`}>
      <div className='chatbox_colum'>
        {props?.chat?.showLoader ? (
          <div className='d-flex justify-content-center mt-5'>
            <Spinner color='#2A9DF4' size={36} />
          </div>
        ) : (
          <section className='chatbox'>
            <div className='feed-head chat-feed-head bg-color-white'>
              <div className='d-flex align-items-center justify-content-between'>
                <div className='media align-items-center'>
                  <div className='back-arrow-mobile mr-3 d-md-none d-sm-block'>
                    <Link href='/chat'>
                      <a href='/chat'>
                        <img src={images.chat.back_arrow_mobile} alt='back' />
                      </a>
                    </Link>
                  </div>
                  <div className='user-avtar avtar-48 mr-3'>
                    <Avatar avatarUrl={activeChat?.toId?.avatar} name={`${activeChat?.toId?.first_name} ${activeChat?.toId?.last_name}`} size={50} />
                  </div>
                  <div className='media-body text-left'>
                    <h5 className='font-weight-normal m-0'>
                      <Link href='/[username]' as={`/${activeChat?.toId?.username}`}>
                        <a className='mr-2'>{`${activeChat?.toId?.first_name} ${activeChat?.toId?.last_name}`}</a>
                      </Link>
                      {activeChat?.toId.isFeatured && <img src={images.app.verifiedIcon} />}
                    </h5>
                    <p className='m-0'>{`@${activeChat?.toId?.username}`}</p>
                  </div>
                </div>
                <div className='d-flex align-items-center'>
                  <div className={`post-online mr-4 ${lastSeen !== 'online' ? 'secondary-color' : ''}`}>{lastSeen}</div>
                  <div className='more-settings-wrp'>
                    <Dropdown>
                      <Dropdown.Toggle as={ChatMoreMenu} id={`dropdown-custom-components-chat-item`} />
                      <Dropdown.Menu className='more-circle-dots-link p-2'>
                        <Dropdown.Item
                          eventKey='1'
                          onClick={async () => {
                            await props.clearChat(router.query.chatId)
                          }}>
                          Clear chat
                        </Dropdown.Item>
                        <Dropdown.Item
                          eventKey='2'
                          onClick={async () => {
                            await props.deleteChat(activeChat.id)
                            router.push('/chat')
                          }}>
                          Delete conversation
                        </Dropdown.Item>
                        <Dropdown.Item eventKey='3' onClick={() => {}}>
                          Report user
                        </Dropdown.Item>
                      </Dropdown.Menu>
                    </Dropdown>
                  </div>
                </div>
              </div>
            </div>
            <section className='chat-window' onScroll={handleScrollTop}>
              {newMessages.map((chat, i) => {
                const isRemote = props.auth.user.id !== chat.from_id
                return (
                  <React.Fragment key={`message-map-${chat.room_id}-${i}`}>
                    {chat.showDateStamp && (
                      <div className='chat-date-center text-center py-2'>
                        <span>{moment(chat.createdAt).format('MMMM DD, YYYY')}</span>
                      </div>
                    )}
                    <article
                      key={`message-${chat.room_id}-${i}`}
                      className={`msg-container ${isRemote ? 'msg-remote' : 'msg-self'} ${i > 0 && newMessages[i - 1]?.fromId?.username !== chat?.fromId?.username ? 'mt-3' : ''}`}
                      id={`msg-${i}`}>
                      <div className='msg-box'>
                        <div className='flr'>
                          <div className='messages alignment'>
                            {!isRemote && (
                              <div className='more-settings-wrp  more-circle-dots'>
                                <Dropdown>
                                  <Dropdown.Toggle as={ChatItemMenu} id={`dropdown-custom-components-${i}`} />
                                  <Dropdown.Menu className='more-circle-dots-link'>
                                    <Dropdown.Item
                                      eventKey='1'
                                      onClick={async () => {
                                        const data = {
                                          message_id: chat.id,
                                          room_id: activeChat?.room_id,
                                          from_id: props.auth.user.id
                                        }
                                        await socket?.emit('onDeleteMessage', data)
                                        setShowDots(null)
                                      }}>
                                      Delete message
                                    </Dropdown.Item>
                                    <Dropdown.Item
                                      eventKey='2'
                                      onClick={() => {
                                        copyToClipboard(chat.content)
                                        setShowDots(null)
                                      }}>
                                      Copy message
                                    </Dropdown.Item>
                                  </Dropdown.Menu>
                                </Dropdown>
                              </div>
                            )}
                            <p className='msg' id={`msg-${i}`}>
                              {chat.type === 'file' &&
                                chat.processed_path !== null &&
                                chat.processed_path.length > 0 &&
                                chat.processed_path.map((m, index) => (
                                  <div
                                    className='mb-2'
                                    key={index}
                                    onClick={() => {
                                      if (chat.type === 'file' && chat.processed_path.length > 0) {
                                        if (chat.processed_path.length > 0) {
                                          chat.processed_path.map(p => {
                                            if (p.type === 'image') {
                                              postMediaImagesArr.push({ processed_path: p.media_path })
                                            }
                                          })
                                        }
                                        setPostLightBox(true)
                                        setChatImageLightBox(postMediaImagesArr)
                                      }
                                    }}>
                                    {m.type === 'image' && (
                                      <div
                                        className='chat-media'
                                        style={{
                                          backgroundImage: `url(${m.media_path})`
                                        }}></div>
                                    )}
                                    {m.type === 'video' && (
                                      <video controls width='100%' style={{ maxHeight: '100%', maxWidth: '100%' }}>
                                        <source src={m.media_path} />
                                        Your browser does not support the video tag.
                                      </video>
                                    )}
                                  </div>
                                ))}
                              {chat.content}
                            </p>
                            <span className='timestamp'>
                              <span className='posttime'>
                                {chat.is_broadcasting && <div>BROADCAST</div>} {moment(chat.createdAt).format('DD/MM/YYYY h:mm A')}
                              </span>
                            </span>
                          </div>
                        </div>
                      </div>
                    </article>
                    {i + 1 === props?.chat?.resultLength && <div id='lastPage' key='lastPage' ref={lastPage}></div>}
                  </React.Fragment>
                )
              })}
              <div ref={messagesEnd}></div>
            </section>
            {isEmojiPickerOpen && (
              <Picker
                set='apple'
                onSelect={e => {
                  onChangeInput(message + ` ${e.native} `)
                  input.current.focus()
                }}
                title='Pick your emoji…'
                emoji='point_up'
                i18n={{ search: 'Recherche', categories: { search: 'Résultats de recherche', recent: 'Récents' } }}
              />
            )}
            <ul className='chat-media-files'>
              {files.map((file, i) => (
                <li className='uploaded d-inline-block m-2' key={file.dataUrl + i}>
                  <div className='uploaded-image' data-index={1}>
                    {ImageMimes.includes(file.type) && <img src={file.dataUrl} alt={i} />}
                    {VideoMimes.includes(file.type) && (
                      <video controls width='100%' style={{ maxHeight: '100%', maxWidth: '100%' }}>
                        <source src={file.dataUrl} type={file.type} />
                        Your browser does not support the video tag.
                      </video>
                    )}
                    <button className='delete-image' onClick={() => removeMedia(i)}>
                      <img src={images.app.close} title='Delete' alt='Delete' />
                    </button>
                  </div>
                </li>
              ))}
              {chatMediaLoader && (
                <div className='pageLoader chat-media-loader loader d-flex justify-content-center align-items-center'>{chatMediaLoader === true && <img src={images.Loader} width={35} alt='' />}</div>
              )}
            </ul>
            {typing && <div className='typing mx-auto text-muted'> {typing}... </div>}
            {activeChat?.isSubscribed === false ? (
              <div className='chat-error-message py-3'>
                <p>
                  you cannot message this person any longer. Please{' '}
                  <Link href={`/${activeChat?.toId?.username}`} to={`/${activeChat?.toId?.username}`}>
                    <button className='chat-subscribe-button'> subscribe </button>
                  </Link>
                  to resume messaging
                </p>
              </div>
            ) : (
              <form className='chat-input bg-color-white' id='chat-input1'>
                <>
                  <input
                    type='text'
                    autoComplete='off'
                    disabled={!activeChat?.isSubscribed}
                    onChange={e => onChangeInput(e.target.value)}
                    value={message}
                    ref={input}
                    // className='p-0'
                    // name='content'
                    placeholder='Send a message'
                  />
                  <button
                    type='submit'
                    className='chat-input-send-button'
                    onClick={e => {
                      e.preventDefault()
                      setIsEmojiPickerOpen(false)
                      if (message || files.length > 0) onSubmit()
                    }}
                    disabled={!activeChat?.isSubscribed || chatMediaLoader}>
                    send
                  </button>
                  <input
                    type='file'
                    accept='image/*, video/*'
                    multiple={false}
                    ref={myInput}
                    style={{ display: 'none' }}
                    id='chat_files'
                    // name='chat-files'
                    accept={AllowedMediaTypes.join(',')}
                    onChange={e => {
                      e.preventDefault()
                      previewChatFiles(e)
                    }}
                    disabled={!activeChat?.isSubscribed}
                  />
                  <button
                    className='chat-input-msd-pin'
                    onClick={e => {
                      e.preventDefault()
                      myInput.current.click()
                    }}>
                    <img src={images.chat.send_msd_pin} />
                  </button>
                  <button
                    className='chat-input-msd-smile'
                    onClick={e => {
                      e.preventDefault()
                      setIsEmojiPickerOpen(!isEmojiPickerOpen)
                    }}>
                    <img src={images.chat.send_msd_smile} />
                  </button>
                </>
              </form>
            )}
          </section>
        )}
      </div>

      {showPostLightBox && chatImageLightBox.length > 0 && <LightBoxModal onClose={closePostLightbox} index={0} post_media={chatImageLightBox} />}
    </div>
  )
}

export default ChatMessageBox
