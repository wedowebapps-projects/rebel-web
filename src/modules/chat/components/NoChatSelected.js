import { useRouter } from 'next/router'
import React from 'react'
import images from 'src/config/images'

const NoChatSelected = () => {
  const router = useRouter()
  return (
    <div className={`massege-create-col border-wrap ml-lg-3 mt-4 ${router.pathname !== '/chat/[chatId]' ? 'd-none d-md-block' : ''}`}>
      <div className=' d-flex align-items-center justify-content-center'>
        <div className='massege-create text-center'>
          <img src={images.chat.chat_bubble} />
          <h3>You haven’t selected any message</h3>
          <p>Click on one or click below to create a new message</p>
          {/* <button className='custom-button primary-bg-color'>Create new</button> */}
        </div>
      </div>
    </div>
  )
}

export default NoChatSelected
