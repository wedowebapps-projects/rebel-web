import { takeLatest, all, fork, call, put, select } from 'redux-saga/effects'
import * as chatActions from '../actions/chatActions'
import * as chatTypes from '../actions/chatTypes'
import * as chatApis from '../api/chatApis'
import { constants } from 'src/utils/helpers'

function* fetchChatList() {
  try {
    const response = yield call(chatApis.fetchChatsApi)
    yield put(chatActions.setChatList(response.chats))
  } catch (error) {
    console.log(error)
  }
}

function* fetchChatRoom(action) {
  const roomId = action.chatId
  const filters = action.filters
  try {
    yield put(chatActions.setChatLoader(true))
    filters.perPage = constants.CHATS_PER_PAGE
    const response = yield call(chatApis.fetchChatRoomApi, roomId, filters)
    //yield put(chatActions.setActiveChat(response.data))
    yield put(chatActions.setChatRoom(response))
    action.resolve(response)
  } catch (error) {
    yield put(chatActions.setChatRoom([]))
    action.reject(error)
  }
}

function* deleteChatSaga({ chat_id, resolve, reject }) {
  try {
    yield call(chatApis.deleteChat_api, chat_id)
    yield put(chatActions.setDeletedChat_action(chat_id))
    yield put(chatActions.setChatPageAction(1))
    resolve()
  } catch (error) {
    console.log(error)
    reject(error)
  }
}

function* clearChatSaga({ chat_id, resolve, reject }) {
  try {
    yield call(chatApis.clearChat_api, chat_id)
    yield put(chatActions.setClearChat_action(chat_id))
    resolve()
  } catch (error) {
    console.log(error)
    reject(error)
  }
}

export function* watchSagas() {
  yield takeLatest(chatTypes.FETCH_CHAT_LIST, fetchChatList)
  yield takeLatest(chatTypes.FETCH_CHAT_ROOM, fetchChatRoom)
  yield takeLatest(chatTypes.DELETE_CHAT, deleteChatSaga)
  yield takeLatest(chatTypes.UPDATE_CLEAR_CHAT, clearChatSaga)
}

export default function* runSagas() {
  yield all([fork(watchSagas)])
}
