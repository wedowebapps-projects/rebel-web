import React from 'react'
import { connect } from 'react-redux'
import * as profileActions from 'src/modules/profile/actions/profileActions'
import { restoreAuthentication } from 'src/modules/auth/actions/authActions'
import { appendChat, deleteChat_action, deleteMessage, fetchChatList, fetchChatRoomAction, setActiveChat, setChatPageAction, updateClearChat_action } from '../actions/chatActions'
import ChatView from './ChatView'
import { fetchListNumbers_api } from 'src/api/appApis'

const ChatContainer = props => {
  const [listType, setListType] = React.useState([])

  React.useEffect(() => {
    props.fetchChatList()
    fetchListNumbers_api().then(data => {
      if (data && data.lists) setListType(data.lists)
    })
    fetchPosts()
  }, [])

  const fetchPosts = () => {
    const state = props?.fans?.state
    if (!state.end) {
      props.fetchProfilePosts({
        username: props.user.username,
        type: 'fans',
        page: state.page,
        perPage: state.perPage
      })
    }
  }

  return <ChatView {...props} listType={listType} fetchPosts={fetchPosts} />
}

const mapStateToProps = state => ({
  auth: state.auth,
  user: state.auth.user,
  app: state.app,
  chat: state.chat,
  fans: state.profile.fans
})

const mapDispatchToProps = dispatch => ({
  fetchChatList: () => dispatch(fetchChatList()),
  restoreSession: () => dispatch(restoreAuthentication()),
  setChatPage: page => dispatch(setChatPageAction(page)),
  fetchChatRoom: (chatId, filters) => new Promise((resolve, reject) => dispatch(fetchChatRoomAction(chatId, filters, resolve, reject))),
  setActiveChat: data => dispatch(setActiveChat(data)),
  appendChat: data => dispatch(appendChat(data)),
  deleteMessage: data => dispatch(deleteMessage(data)),
  clearChat: chat_id => new Promise((resolve, reject) => dispatch(updateClearChat_action(chat_id, resolve, reject))),
  deleteChat: chat_id => new Promise((resolve, reject) => dispatch(deleteChat_action(chat_id, resolve, reject))),
  fetchProfilePosts: data => dispatch(profileActions.fetchProfilePosts(data))
})

export default connect(mapStateToProps, mapDispatchToProps)(ChatContainer)
