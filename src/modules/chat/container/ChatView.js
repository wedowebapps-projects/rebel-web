import { useRouter } from 'next/router'
import React from 'react'
import { Spinner } from 'src/components/shared'
import { constants } from 'src/utils/helpers'
import { ChatInfo, ChatList, ChatMessageBox, NoChatSelected } from '../components'
const ChatView = props => {
  const router = useRouter()

  React.useEffect(() => {
    props.setChatPage(1)
    router.query.chatId && setTimeout(() => loadMoreMessages(true), 1000)
  }, [router.query.chatId])

  const loadMoreMessages = async (p = false) => {
    try {
      const { page, showLoader } = props.chat
      if (showLoader !== 'noData') {
        if (p) {
          await props.fetchChatRoom(router.query.chatId, { page: 1 })
        } else {
          await props.fetchChatRoom(router.query.chatId, { page })
        }
      }
    } catch (error) {
      console.log(error)
    }
  }

  return (
    <>
      <ChatList {...props} chatId={router.query.chatId} />
      {router.asPath === '/chat' && <NoChatSelected />}
      {router.query.chatId &&
        (props?.chat?.activeChat?.id ? (
          <>
            <ChatMessageBox {...props} chatId={router.query.chatId} loadMoreMessages={loadMoreMessages} />
            <ChatInfo {...props} chatId={router.query.chatId} />
          </>
        ) : (
          <div className='ml-lg-3 mt-4 border-wrap d-flex align-items-center justify-content-center w-100' style={{ height: 'calc(100vh - 120px)' }}>
            <Spinner color='#2A9DF4' size={36} />
          </div>
        ))}
    </>
  )
}

export default ChatView
