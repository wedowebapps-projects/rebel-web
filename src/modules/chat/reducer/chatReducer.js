import * as chatTypes from 'src/modules/chat/actions/chatTypes'
import moment from 'moment'

const initialState = {
  chatList: [],
  activeChat: {},
  chatRoom: [],
  showLoader: false,
  page: 1,
  resultLength: 0,
  totalUnreadCount: 0
}

const chatReducer = (state = initialState, action) => {
  switch (action.type) {
    case chatTypes.SET_CHAT_LOADER: {
      return {
        ...state,
        showLoader: action.data
      }
    }
    case chatTypes.APPEND_CHAT: {
      const { chatList } = state
      const i = chatList.map(c => c.room_id).indexOf(action.data.room_id)
      const chatListItem = chatList[i]
      if (chatListItem) {
        chatListItem.lastMessage = chatListItem.lastMessage || {}
        chatListItem.lastMessage.createdAt = moment().utc()
        chatListItem.lastMessage.content = action.data.content === null ? 'File' : action.data.content
        chatListItem.lastMessage.type = action.data.type
        chatList[i] = chatListItem
      }
      return {
        ...state,
        chatRoom: [...state.chatRoom, action.data],
        chatList
      }
    }
    case chatTypes.APPEND_CHAT: {
      let { chatRoom } = state
      let temp = [...chatRoom]
      let i = temp.findIndex(v => v.id === action.data.id)
      temp.splice(i, 1)
      return {
        ...state,
        chatRoom: [...temp]
      }
    }

    case chatTypes.SET_ACTIVE_CHAT:
      return {
        ...state,
        activeChat: action.data
      }

    case chatTypes.SET_CHAT_LIST: {
      let totalUnreadCount = 0
      for (const chat of action.data) {
        totalUnreadCount = totalUnreadCount + chat.unreadCount
      }
      return {
        ...state,
        chatList: action.data,
        totalUnreadCount
      }
    }
    case chatTypes.SET_PAGE:
      return {
        ...state,
        page: action.page,
        activeChat: {},
        chatRoom: [],
        showLoader: false,
        resultLength: 0
      }
    case chatTypes.SET_CHAT_ROOM: {
      const chats = state.chatRoom
      action.chats.forEach(chat => chats.unshift(chat))
      return {
        ...state,
        chatRoom: chats,
        activeChat: action.data,
        page: action.chats.length > 0 ? state.page + 1 : 'noData',
        resultLength: action.chats.length,
        showLoader: false
      }
    }
    case chatTypes.SET_DELETED_CHAT: {
      const chatList = state.chatList
      const i = chatList.map(c => c.id).indexOf(action.chat_id)
      if (i !== -1) {
        chatList.splice(i, 1)
        return {
          ...state,
          chatList
        }
      } else {
        return state
      }
    }

    case chatTypes.DELETE_MESSAGE: {
      const newChatRoom = state.chatRoom.filter(msg => msg.id !== action.data.message_id)
      return {
        ...state,
        chatRoom: newChatRoom
      }
    }

    case chatTypes.SET_CLEAR_CHAT: {
      let { chatList } = state
      const i = chatList.map(c => c.room_id).indexOf(action.room_id)
      const chatListItem = chatList[i]
      if (chatListItem) {
        chatListItem.lastMessage = null
        chatList[i] = chatListItem
      }
      return {
        ...state,
        chatRoom: [],
        chatList
      }
    }

    default:
      return state
  }
}

export default chatReducer
