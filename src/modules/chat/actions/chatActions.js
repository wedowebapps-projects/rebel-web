import * as chatTypes from './chatTypes'

export const setChatLoader = data => {
  return {
    type: chatTypes.SET_CHAT_LOADER,
    data
  }
}
export const fetchChatList = () => {
  return {
    type: chatTypes.FETCH_CHAT_LIST
  }
}

export const setChatList = data => {
  return {
    type: chatTypes.SET_CHAT_LIST,
    data
  }
}

export const setActiveChat = data => {
  return {
    type: chatTypes.SET_ACTIVE_CHAT,
    data
  }
}

export const appendChat = data => {
  return {
    type: chatTypes.APPEND_CHAT,
    data
  }
}
export const deleteMessage = data => {
  return {
    type: chatTypes.DELETE_MESSAGE,
    data
  }
}

export const setChatPageAction = page => ({
  type: chatTypes.SET_PAGE,
  page
})

export const fetchChatRoomAction = (chatId, filters, resolve, reject) => {
  return {
    type: chatTypes.FETCH_CHAT_ROOM,
    chatId,
    filters,
    resolve,
    reject
  }
}

export const setChatRoom = data => {
  return {
    type: chatTypes.SET_CHAT_ROOM,
    ...data
  }
}

export const deleteChat_action = (chat_id, resolve, reject) => ({
  type: chatTypes.DELETE_CHAT,
  chat_id,
  resolve,
  reject
})
export const setDeletedChat_action = chat_id => ({
  type: chatTypes.SET_DELETED_CHAT,
  chat_id
})

export const updateClearChat_action = (chat_id, resolve, reject) => ({
  type: chatTypes.UPDATE_CLEAR_CHAT,
  chat_id,
  resolve,
  reject
})
export const setClearChat_action = room_id => ({
  type: chatTypes.SET_CLEAR_CHAT,
  room_id
})
