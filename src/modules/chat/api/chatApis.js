import api from 'src/api'
import _ from 'lodash'
import { convertObjectToQuerystring } from 'src/utils/helpers'
import { getUserToken } from 'src/utils/storageUtils'

export const initiateChatApi = data => {
  const token = getUserToken()
  return api(`/user/initiate/chat`, data, 'post', token)
}

export const fetchChatsApi = () => {
  const token = getUserToken()
  return api(`/user/chats`, '', 'get', token)
}

export const fetchChatRoomApi = (roomId, filters) => {
  let filter = ''
  if (!_.isEmpty(filters)) {
    filter = `${convertObjectToQuerystring(filters)}`
  }
  const token = getUserToken()
  return api(`/user/chats/${roomId}?${filter}`, '', 'get', token)
}

export const deleteChat_api = chat_id => {
  const token = getUserToken()
  return api(`/user/chats/${chat_id}`, null, 'delete', token)
}
export const clearChat_api = chat_id => {
  const token = getUserToken()
  return api(`/user/chat/clear/${chat_id}`, null, 'put', token)
}
