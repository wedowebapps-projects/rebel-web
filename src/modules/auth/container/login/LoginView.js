import React from 'react'
import images from 'src/config/images.js'
import Link from 'next/link'

import LoginSlides from '../../components/LoginSlides'
import LoginForm from '../../components/LoginForm'
import { Button } from 'src/components/shared/index.js'
import { isMobile } from 'react-device-detect'

function LoginView(props) {
  const [showLoginForm, setShowLoginForm] = React.useState(false)
  React.useEffect(() => {
    if (isMobile) {
      setShowLoginForm(false)
    } else {
      setShowLoginForm(true)
    }
  }, [])

  const showLogin = () => {
    setShowLoginForm(true)
  }

  return (
    <React.Fragment>
      <section className={`sign-in-bg authentification authe-11 ${!showLoginForm ? 'pt-0' : ''}`}>
        {!showLoginForm ? (
          <div className='row no-gutters'>
            <div className='col-md-6'>
              <div className='front-slider swiper-container'>
                <div className='lock-img-box'>
                  <img src={images.app.logo} className='lock-img-desktop' />
                  <img src={images.app.logoMobile} className='lock-img-mobile' />
                </div>
                <LoginSlides showLogin={showLogin} />
              </div>
            </div>
          </div>
        ) : (
          <div className='row no-gutters'>
            <div className='col-md-6 auth-slides'>
              <div className='front-slider swiper-container'>
                <div className='lock-img-box'>
                  <img src={images.app.logo} className='lock-img-desktop' />
                  <img src={images.app.logoMobile} className='lock-img-mobile' />
                </div>
                <LoginSlides showLogin={showLogin} />
              </div>
            </div>
            <div className='col-md-6'>
              <div className='sign-up-box'>
                <div className='sign-up-button-top-box d-flex justify-content-between'>
                  <div>
                    <img className='mobileLogo' src={images.app.logo} />
                  </div>
                  <Link href='/register'>
                    <Button variant='primary'>Sign up</Button>
                  </Link>
                </div>
                <div className='sign-up-box-inner'>
                  <h2>Welcome back!</h2>
                  <p>Welcome back to Rebelyus. Sign in to access your profile </p>
                  <div className='form'>
                    <LoginForm {...props} />
                    <Link href='/mypay' to='mypay'>
                      <button className='custom-button my-pay-button primary-color'>
                        My pay calculator
                        <img src={images.app.myPay} />
                      </button>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}
      </section>
    </React.Fragment>
  )
}

export default LoginView
