import React, { Component } from 'react'
import { connect } from 'react-redux'
import LoginView from './LoginView'
import { loginUser, restoreAuthentication, setSocialDataAction, socialLoginAction } from '../../actions/authActions'
import { storeAuthenticationAction } from '../../actions/authActions'
export class LoginContainer extends Component {
  render() {
    return <LoginView {...this.props} />
  }
}

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({
  loginUser: data =>
    new Promise((resolve, reject) => {
      dispatch(loginUser(data, resolve, reject))
    }),
  restoreAuthentication: () => dispatch(restoreAuthentication()),
  setSocialData: data => dispatch(setSocialDataAction(data)),
  storeAuthentication: data => dispatch(storeAuthenticationAction(data)),
  socialLogin: data => new Promise((resolve, reject) => {
    dispatch(socialLoginAction(data, resolve, reject))
  }),
})

export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer)
