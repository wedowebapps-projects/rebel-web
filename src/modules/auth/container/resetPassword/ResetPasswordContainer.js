import React, { Component } from 'react'
import { connect } from 'react-redux'
import ResetPasswordView from './ResetPasswordView'
import { forgotPassword, resetPassword } from '../../actions/authActions'

export class ResetPasswordContainer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: '',
      otpSent: false
    }
  }

  requestOTP = data => {
    return new Promise((resolve, reject) => {
      this.setState({ email: data.email })
      this.props
        .forgotPassword(data)
        .then(res => {
          console.log(res)
          if (res.emailSent == true) {
            this.setState({ otpSent: true })
          }
          resolve(res)
        })
        .catch(err => {
          this.setState({ otpSent: false })
          reject(err)
        })
    })
  }

  render() {
    return <ResetPasswordView {...this.props} {...this.state} requestOTP={this.requestOTP} />
  }
}

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({
  forgotPassword: data =>
    new Promise((resolve, reject) => {
      dispatch(forgotPassword(data, resolve, reject))
    }),
  resetPassword: data =>
    new Promise((resolve, reject) => {
      dispatch(resetPassword(data, resolve, reject))
    })
})

export default connect(mapStateToProps, mapDispatchToProps)(ResetPasswordContainer)
