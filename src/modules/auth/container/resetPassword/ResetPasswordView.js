import React from 'react'

import images from 'src/config/images.js'
import Link from 'next/link'

import LoginSlides from '../../components/LoginSlides'
import ForgotPasswordForm from '../../components/ForgotPasswordForm'
import ResetPasswordForm from '../../components/ResetPasswordForm'

import { Button } from 'src/components/shared/index.js'

function ResetPasswordView(props) {
  const { otpSent, requestOTP, email } = props

  return (
    <React.Fragment>
      <section className='sign-in-bg authentification authe-13'>
        <div className='row no-gutters'>
          <div className='col-md-6 auth-slides'>
            <div className='front-slider swiper-container'>
              <div className='lock-img-box'>
                <img src={images.app.logo} className='lock-img-desktop' />
                <img src={images.app.logoMobile} className='lock-img-mobile' />
              </div>
              <LoginSlides />
            </div>
          </div>
          <div className='col-md-6'>
            <div className='sign-up-box'>
              <div className='sign-up-button-top-box d-flex justify-content-between'>
                <div>
                  <img className='mobileLogo' src={images.app.logo} />
                </div>
                <Link href='/login'>
                  <Button variant='secondary'>Sign in</Button>
                </Link>
              </div>
              <div className='sign-up-box-inner'>
                <h2>Reset your password</h2>
                {!otpSent ? <p>Enter your email to reset your password</p> : <p>You'll receive a code on {email} to verify here so you can reset your account password.</p>}
                <div className='form'>{!otpSent ? <ForgotPasswordForm {...props} requestOTP={requestOTP} /> : <ResetPasswordForm {...props} />}</div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </React.Fragment>
  )
}

export default ResetPasswordView
