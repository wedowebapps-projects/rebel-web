import React from 'react'
import { Tabs, BankApplication, BankDetail, Wallet, NoBankAddedWallet } from '../../components/Bank'
import { useRouter } from 'next/router'
import { getBankList_api } from '../../api/authApi'
import { sortBy } from 'lodash'

export const BankView = props => {
  const [banks, setBanks] = React.useState([])
  const router = useRouter()

  React.useEffect(() => {
    if (router.asPath === '/wallet') {
      router.push('/wallet/[page]', props.allRoutes.BankApplication)
    }
    getBanks()
  }, [])

  let getBanks = async () => {
    try {
      let bank = await getBankList_api()
      bank = (bank.banks || []).map((b, i) => {
        return { value: b.code, label: b.name, key: `${i + 1}${b.code}` }
      })
      setBanks(sortBy(bank, ['name']))
    } catch (error) {
      console.log(error)
    }
  }

  return (
    <div className='wallet-add-card-col w-100 mt-4 mb-4 ml-4 border-wrap '>
      <Tabs {...props} />
      <div className='wallet-tabs'>
        <div className='add-bank-contain tab-content current pt-4'>
          {router.asPath === props.allRoutes.BankApplication && <BankApplication {...props} />}
          {router.asPath === props.allRoutes.BankDetail && <BankDetail {...props} banks={banks} />}
          {[props.allRoutes.Transaction, props.allRoutes.Summary].includes(router.asPath) ? props?.user_personal_info?.bank ? <Wallet {...props} /> : <NoBankAddedWallet {...props} /> : null}
        </div>
      </div>
    </div>
  )
}

export default BankView
