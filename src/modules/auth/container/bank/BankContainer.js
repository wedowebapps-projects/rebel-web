import React from 'react'
import { connect } from 'react-redux'
import { submitBankDetailsAction, submitBankApplicationAction } from '../../actions/authActions'
import BankView from './BankView'

export const BankContainer = props => {
  return <BankView {...props} />
}

const mapStateToProps = state => ({
  user: state.auth.user,
  countries: state.app.countries,
  commissions: state.app.commissions,
  user_personal_info: state?.auth?.user?.personal_info ? JSON.parse(state.auth.user.personal_info) : '',
  allRoutes: {
    BankApplication: '/wallet/bank-application',
    BankDetail: '/wallet/bank-detail',
    Transaction: '/wallet/transaction',
    Summary: '/wallet/summary',
    Cards: '/wallet/cards'
  }
})

const mapDispatchToProps = dispatch => ({
  submitBankApplication: data => new Promise((resolve, reject) => dispatch(submitBankApplicationAction(data, resolve, reject))),
  submitBankDetails: data => new Promise((resolve, reject) => dispatch(submitBankDetailsAction(data, resolve, reject)))
})
export default connect(mapStateToProps, mapDispatchToProps)(BankContainer)
