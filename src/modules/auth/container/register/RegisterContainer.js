import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { useRouter } from 'next/router'

import { registerUser, registerOtpSubmit, setRegisterData, patchUser, storeAuthenticationAction, setSocialDataAction, socialSignupAction } from '../../actions/authActions'
import { getRegisterData } from 'src/utils/storageUtils';

import RegisterView from './RegisterView'
import RegisterVerify from './RegisterVerify'
import RegisterUsername from './RegisterUsername'
import RegisterImage from './RegisterImage'

export const RegisterContainer = props => {
  const { asPath } = useRouter()
  const [page, setPage] = useState('register')
  const components = {
    register: RegisterView,
    verify: RegisterVerify,
    username: RegisterUsername,
    image: RegisterImage
  }

  useEffect(() => {
    const registerData = getRegisterData()
    props.setRegisterData(registerData)
  }, [])

  const pageSlug = asPath.split('/').pop()

  const PageComponent = components[pageSlug]
  return <PageComponent {...props} />
}

const mapStateToProps = state => ({
  auth: state.auth,
  registerError: state.auth.registerError,
  registerData: state.auth.registerData,
  socialData: state.auth.socialData
})

const mapDispatchToProps = dispatch => ({
  registerUser: data => dispatch(registerUser(data)),
  registerOtpSubmit: data => dispatch(registerOtpSubmit(data)),
  setRegisterData: data => dispatch(setRegisterData(data)),
  patchUser: data =>
    new Promise((resolve, reject) => {
      dispatch(patchUser(data, resolve, reject))
    }),
  storeAuthentication: data => dispatch(storeAuthenticationAction(data)),
  setSocialData: data => dispatch(setSocialDataAction(data)),
  socialSignup: data => {
    return new Promise((resolve, reject) => {
      dispatch(socialSignupAction(data, resolve, reject))
    })
  },
})

export default connect(mapStateToProps, mapDispatchToProps)(RegisterContainer)
