import React from 'react'
import images from 'src/config/images.js'
import Link from 'next/link'
import LoginSlides from '../../components/LoginSlides'
import RegisterForm from '../../components/RegisterForm'
import { Button } from 'src/components/shared/index.js'
// import { isMobile } from 'react-device-detect'

export default function RegisterView(props) {
  const classes = 'sign-in-bg authentification authe-13'
  return (
    <React.Fragment>
      <section className={classes}>
        <div className='row no-gutters'>
          <div className='col-md-6 auth-slides'>
            <div className='front-slider swiper-container'>
              <div className='lock-img-box'>
                <img src={images.app.logo} className='lock-img-desktop' />
                <img src={images.app.logoMobile} className='lock-img-mobile' />
              </div>
              <LoginSlides />
            </div>
          </div>

          <div className='col-md-6'>
            <div className='sign-up-box'>
              <div className='sign-up-button-top-box d-flex justify-content-between'>
                <div>
                  <img className='mobileLogo' src={images.app.logo} />
                </div>
                <Link href='/login'>
                  <Button variant='secondary'>Sign in</Button>
                </Link>
              </div>
              <div className='sign-up-box-inner'>
                <h2>Create an account</h2>
                <p>Welcome back to Rebelyus. Create an account to continue </p>
                <div className='form'>
                  <RegisterForm {...props} />
                  <Link href='/mypay' to='mypay'>
                    <button className='custom-button my-pay-button primary-color'>
                      My pay calculator
                      <img src={images.app.myPay} />
                    </button>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </React.Fragment>
  )
}
