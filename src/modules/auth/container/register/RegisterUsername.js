import React from 'react'
import images from 'src/config/images.js'
import { Form, Button } from 'src/components/shared/index.js'
import Router from 'next/router'

function RegisterUsername(props) {

  const [errors, setErrors] = React.useState([])
  const [loading, setLoading] = React.useState(false)
  
  const error = errors && errors.length > 0 && errors[0].msg !== '' ? errors[0].msg : false

  return (
    <section className='enter-username-bg'>
      <div className='container'>
        <div className='row'>
          <div className='col-12'>
            <div className='enter-username-box'>
              <div className='enter-username-top-logo'>
                <img src={images.app.rebelLogo} style={{ width:'70px' }} className='logo-lock-img desktop-show' />
                <img src={images.app.logo} className='lock-img mobile-show' />
              </div>
              <h3>Welcome to Rebelyus. 👋🏽</h3>
              <Form
                initialValues={{
                  username: ''
                }}
                validations={{
                  username: [Form.is.required()]
                }}
                onSubmit={async (values, form) => {
                  setLoading(true)
                  setErrors([])
                  const data = { key: 'username', value: values.username }
                  await props.patchUser(data).then(res => {
                    if(props.socialData !== null){
                      Router.replace('/')
                    } else {
                      Router.replace('image')
                    }
                  }).catch(err => {
                    setErrors(err)
                  })
                  setLoading(false)
                }}>
                {props => (
                  <React.Fragment>
                    <Form.Field.Input floating errorMsg={error} name='username' label='Enter your username' />
                    <Button variant='primary' block isWorking={loading} onClick={props.submitForm}>
                      Continue
                    </Button>
                  </React.Fragment>
                )}
              </Form>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default RegisterUsername
