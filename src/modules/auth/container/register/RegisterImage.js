import React from 'react'
import images from 'src/config/images.js'
import { Form, Button, Spinner } from 'src/components/shared'
import Router from 'next/router'
import { ImageMimes } from 'src/utils/helpers'
import { uploadSingle } from 'src/utils/s3'

function RegisterImage(props) {
  const [uploading, setUploading] = React.useState(false)
  const [errors, setErrors] = React.useState([])
  const [loading, setLoading] = React.useState(false)
  const [avatar, setAvatar] = React.useState('')

  const getUuid = () => {
    return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c => (c ^ (crypto.getRandomValues(new Uint8Array(1))[0] & (15 >> (c / 4)))).toString(16))
  }

  const previewFiles = async e => {
    setUploading(true)
    let file = e.target.files
    file = file[0]

    const uploadedFiles = await uploadSingle(file, getUuid(), 'profile_photos')
      .then(res => {
        setAvatar(res.location)
      })
      .catch(err => {
        console.log(err)
      })
    setUploading(false)
  }

  return (
    <section className='enter-username-bg upload-profile-bg'>
      <div className='container'>
        <div className='row'>
          <div className='col-12'>
            <div className='upload-profile-box'>
              <div className='enter-username-top-logo'>
                <img src={images.app.rebelLogo} style={{ width:'70px' }} className='logo-lock-img desktop-show' />
                <img src={images.app.logo} className='lock-img mobile-show' />
              </div>
              <h3>
                <span>Kindly</span> Upload a profile image 📸
              </h3>
              <form>
                <input type='file' name='upload-profile-image' id='new-avatar' name='avatar' style={{ display: 'none' }} onChange={e => previewFiles(e)} />
                {avatar && avatar !== '' ? (
                  <div className='input-box input-upload-profile-image-selected'>
                    <label>
                      <img src={avatar} />
                    </label>
                  </div>
                ) : (
                  <div className='input-box input-upload-profile-image'>
                    {/* <img src={avatarPreview || initialValues.avatar || images.IconCamera} alt='' /> */}
                    <label className='new-avatar' className='pickerBg' htmlFor='new-avatar'></label>
                    {uploading ? <Spinner classNam='loading' color='#187BCD' /> : <img src={images.app.camera} />}
                  </div>
                )}
                <Button
                  variant='primary'
                  block
                  isWorking={loading || uploading}
                  onClick={async (e) => {
                    e && e.preventDefault()
                    setLoading(true)
                    setErrors([])
                    const payload = { key: 'avatar', value: avatar }
                    await props
                      .patchUser(payload)
                      .then(res => {
                        console.log(res)
                        Router.replace('/')
                      })
                      .catch(err => {
                        setErrors(err)
                      })
                    setLoading(false)
                  }}>
                  Continue
                </Button>
                <Button
                  variant='secondary'
                  block
                  onClick={() => {
                    Router.replace('/')
                  }}>
                  Skip
                </Button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default RegisterImage
