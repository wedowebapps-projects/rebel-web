import React from 'react'
import images from 'src/config/images.js'
import { Form, Button } from 'src/components/shared/index.js'
import Router from 'next/router'

import { resendOTP } from 'src/modules/auth/api/authApi'

function RegisterVerify(props) {
  const [seconds, setSeconds] = React.useState(60)
  const [loading, setLoading] = React.useState(false)
  const [otpLoading, setOtpLoading] = React.useState(false)
  const [error, setError] = React.useState('')

  const { registerData } = props

  React.useEffect(() => {
    if (!seconds) return
    const intervalId = setInterval(() => {
      setSeconds(seconds - 1)
    }, 1000)
    return () => clearInterval(intervalId)
  }, [seconds])

  const onResendOtp = async () => {
    setOtpLoading(true)
    const data = {
      email: registerData.email
    }
    const sent = await resendOTP(data)
      .then(res => {
        console.log(res)
      })
      .catch(err => {
        console.log(err)
      })
    setOtpLoading(false)
    setSeconds(60)
  }

  React.useEffect(() => {
    if (props.registerError.invalidOtp) {
      setLoading(false)
      setError('OTP does not match! Please try again.')
    } else {
      setError('')
    }
  }, [props])

  return (
    <section className='verification-code-bg'>
      <div className='container'>
        <div className='row'>
          <div className='col-12'>
            <div className='verification-code-box'>
              <div className='sign-up-button-top-box'>
                <img src={images.app.logo} />
              </div>
              <button
                className='r-close-button'
                onClick={() => {
                  Router.replace('/register')
                }}>
                <img src={images.app.cross} />
              </button>
              <h2 className='primary-color'>Enter Verification Code.</h2>
              <p>Kindly enter the verification code we sent to {registerData.email} below.</p>
              <Form
                initialValues={{
                  otp: ''
                }}
                validations={{
                  otp: [Form.is.required()]
                }}
                onSubmit={async (values, form) => {
                  setLoading(true)
                  values.email = registerData.email
                  props.registerOtpSubmit(values)
                }}>
                {props => (
                  <React.Fragment>
                    <Form.Field.OtpInput isInputNum errorMsg={error} numInputs={6} name='otp' label='OTP' />
                    <p>Kindly try this step after 60 minutes, if you don't get verification code.</p>
                    <div className='d-flex justify-content-between mb-4 mt-4'>
                      <Button medium className='resendOtpButton' variant='secondary' isWorking={otpLoading} onClick={() => onResendOtp()} disabled={seconds > 0 ? true : false}>
                        Resend Code{' '}
                        {seconds > 0 && (
                          <React.Fragment>
                            in <span className='counter'>0:{seconds}</span>
                          </React.Fragment>
                        )}
                      </Button>

                      <Button
                        medium
                        variant='secondary'
                        onClick={() => {
                          props.setFieldValue('otp', '')
                        }}>
                        Clear
                      </Button>
                    </div>
                    <Button variant='primary' block isWorking={loading} onClick={props.submitForm}>
                      Continue
                    </Button>
                  </React.Fragment>
                )}
              </Form>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default RegisterVerify
