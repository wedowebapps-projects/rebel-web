import React from 'react'

export const SettingContainer = props => {
  const allRoutes = {
    Settings: '/settings',
    YourAccount: '/settings/your-account',
    SubscriptionPrices: '/settings/subscription-prices',
    ChangePassword: '/settings/change-password',
    AccountPrivacy: '/settings/accountPrivacy',
    BlockedAccounts: '/settings/accountPrivacy/blocked-accounts',
    PrivacyPolicy: '/privacy-policy',
    TearmsConditions: '/terms-conditions',
    HowItWorks: '/how-it-works',
    YourPosts: '/settings/accountPrivacy/your-posts'
  }

  return <div className='w-100 d-none d-sm-none d-md-block d-lg-block d-xl-block'></div>
}

export default SettingContainer
