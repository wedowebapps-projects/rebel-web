import * as authTypes from '../actions/authTypes'
const initialState = {
  user: {},
  registerData: {},
  registerError: {
    emailExists: false,
    usernameExists: false,
    invalidOtp: false,
    errors: []
  },
  token: '',
  isLoggedIn: false,
  socialData: null
}

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case authTypes.SET_AUTHENTICATION:
      return {
        ...state,
        user: action.user ? action.user : state.user,
        // token: action.token ? `Bearer ${action.token}` : state.token
        token: action.token ? action.token : state.token
      }

    case authTypes.STORE_AUTHENTICATION:
      return {
        ...state,
        isLoggedIn: true,
        user: {
          ...state.user,
          ...action.data.user
        },
        token: action.data.token
      }

    case authTypes.SET_REGISTER_DATA:
      return {
        ...state,
        registerData: action.data
      }

    case authTypes.SET_REGISTER_ERROR:
      return {
        ...state,
        registerError: {
          ...state.registerError,
          emailExists: action.data.emailExists ? action.data.emailExists : state.emailExists,
          usernameExists: action.data.usernameExists ? action.data.usernameExists : state.usernameExists,
          errors: action.data.errors ? action.data.errors : state.errors,
          invalidOtp: action.data.invalidOtp ? action.data.invalidOtp : state.invalidOtp
        }
      }

    case authTypes.SET_MY_PROFILE:
      return {
        ...state,
        user: {
          ...state.user,
          ...action.data
        }
      }

    //wallet module
    case authTypes.SET_BANK_APPLICATION:
      return {
        ...state,
        user: {
          ...state.user,
          personal_info: action.data
        }
      }

    //subscription price
    case authTypes.SET_SUBSCRIPTION_PRICE:
      return {
        ...state,
        subscriptionPrices: action.reset ? action.data : [...state.subscriptionPrices, ...action.data]
      }
    case authTypes.UPDATE_SUBSCRIPTION_PRICE: {
      const subscriptionPrices = state.subscriptionPrices
      const planIndex = subscriptionPrices.map(p => p.id).indexOf(action.data.id)
      subscriptionPrices[planIndex] = action.data
      return {
        ...state,
        subscriptionPrices
      }
    }

    case authTypes.SET_SOCIAL_DATA:
      return {
        ...state,
        socialData: action.data
      }
    default:
      return state
  }
}

export default authReducer
