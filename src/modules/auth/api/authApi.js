import api from 'utils/api'
import { getUserToken } from 'src/utils/storageUtils'
import { convertObjectToQuerystring } from 'src/utils/helpers'
import { isEmpty } from 'lodash'

export const signup = data => {
  return api('/auth/register', data, 'post')
}

export const checkEmailUsername = data => {
  return api('/auth/check/exists', data, 'post')
}

export const resetPasswordOTP = data => {
  return api('/auth/requestOTP', data, 'post')
}

export const socialLogin = data => {
  return api('/auth/socialLogin', data, 'post')
}

export const login = data => {
  return api('/auth/login', data, 'post')
}

export const verifyOTP = data => {
  return api('/auth/confirm', data, 'post')
}

export const logout_api = () => {
  const token = getUserToken()
  return api('/user/logout', null, 'get', token)
}

export const requestOtp = data => api('/auth/requestOTP', data, 'post')
export const resetPassword = data => api('/auth/resetPassword', data, 'post')
export const resendOTP = data => api('/auth/resend/confirm', data, 'post')
export const getMyProfile = token => api('/user/me', null, 'get', token)
export const patchUser = (data, token) => api('/user/profileUpdate', data, 'put', token)

export const sendOtpToEmail = data => {
  return api('/auth/resendotp', data, 'post')
}

export const socialSignupApi = data => {
  return api('/auth/socialSignUp', data, 'post')
}

export const updateProfileApi = data => {
  const token = getUserToken()
  return api('/user/updateProfile', data, 'put', token)
}

export const changeProfileMedia_api = data => {
  const token = getUserToken()
  return api('/user/updateProfile/media', data, 'put', token)
}

export const getBankList_api = () => {
  return api('/banks', null, 'get')
}
export const submitBankApplication_api = data => {
  const token = getUserToken()
  return api('/user/application', data, 'post', token)
}
export const submitBankAccount_api = data => {
  const token = getUserToken()
  return api('/user/bank', data, 'post', token)
}
export const addSubscriptionPrice_api = data => {
  const token = getUserToken()
  return api('/user/subscriptions', data, 'post', token)
}
export const updateSubscriptionPrice_api = (data, uuid) => {
  const token = getUserToken()
  return api(`/user/subscriptions/${uuid}`, data, 'put', token)
}
export const fetchSubscriptionPrice_api = () => {
  const token = getUserToken()
  return api('/user/subscriptions', null, 'get', token)
}
export const deleteSubscriptionPrice_api = (uuid, data) => {
  const token = getUserToken()
  return api(`/user/subscriptions/${uuid}`, data, 'delete', token)
}
export const submitSubscriptionTransaction_api = (profile_id, data) => {
  const token = getUserToken()
  if (process.env.PAYMENT_METHOD === 'flutterwave') {
    return api(`/user/follow/${profile_id}/paid`, data, 'post', token)
  } else {
    return api(`/user/follow/${profile_id}/paidpaystack`, data, 'post', token)
  }
}
export const getTransactionList_api = filters => {
  let filter = ''
  if (!isEmpty(filters)) {
    filter = `${convertObjectToQuerystring(filters)}`
  }
  const token = getUserToken()
  return api(`/user/transactions?${filter}`, null, 'get', token)
}
export const getTransactionMonths_api = () => {
  const token = getUserToken()
  return api(`/user/transactionsmonthly`, null, 'get', token)
}
export const addMoneyToWallet_api = data => {
  const token = getUserToken()
  if (process.env.PAYMENT_METHOD === 'flutterwave') {
    return api(`/user/wallet/add`, data, 'post', token)
  } else {
    return api(`/user/walletPayStack/add`, data, 'post', token)
  }
}
export const withdraw_api = data => {
  const token = getUserToken()
  if (process.env.PAYMENT_METHOD === 'flutterwave') {
    return api(`/user/wallet/withdraw`, data, 'post', token)
  } else {
    return api(`/user/walletPayStack/withdraw`, data, 'post', token)
  }
}
export const getWithdrawalFees_api = amount => {
  return api(`/transfer/fee/${amount}`, null, 'get')
}

export const removeSubscription_api = id => {
  const token = getUserToken()
  if (process.env.PAYMENT_METHOD === 'flutterwave') {
    return api(`/user/remove/subscription/${id}`, null, 'post', token)
  } else {
    return api(`/user/remove/subscriptionpaystack/${id}`, null, 'post', token)
  }
}
export const changePassword_api = data => {
  const token = getUserToken()
  return api(`/user/changePassword`, data, 'put', token)
}
export const deleteAccount_api = () => {
  const token = getUserToken()
  return api(`/user/remove/account`, null, 'delete', token)
}

export const updateFCMToken_api = data => {
  const token = getUserToken()
  return api(`/user/updateFcmToken`, data, 'put', token)
}

export const checkUsernameExist_api = data => {
  const token = getUserToken()
  return api(`/user/checkUsername`, data, 'put', token)
}

export const verifyPassword_api = data => {
  const token = getUserToken()
  return api(`/user/checkPassword`, data, 'post', token)
}

export const blockUser_api = id => {
  const token = getUserToken()
  if (process.env.PAYMENT_METHOD === 'flutterwave') {
    return api(`/user/blocked/${id}`, null, 'post', token)
  } else {
    return api(`/user/blockedpaystack/${id}`, null, 'post', token)
  }
}

export const unBlockUser_api = id => {
  const token = getUserToken()
  return api(`/user/blocked/${id}`, null, 'post', token)
}

export const unBlockedUserFromBlockList_api = id => {
  const token = getUserToken()
  return api(`/user/unblocked/${id}`, null, 'post', token)
}
