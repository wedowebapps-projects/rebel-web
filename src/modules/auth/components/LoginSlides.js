import React from 'react'
import SwiperCore, { Navigation, Pagination, Scrollbar, A11y } from 'swiper'
import { Swiper, SwiperSlide } from 'swiper/react'
import { useRouter } from 'next/router'
import images from 'src/config/images.js'
import Link from 'next/link'

SwiperCore.use([Navigation, Pagination, Scrollbar, A11y])

function LoginSlides(props) {
  const router = useRouter()
  const path = router.asPath

  return (
    <React.Fragment>
      <Swiper slidesPerView={1} pagination={{ clickable: true }}>
        <SwiperSlide>
          <div className='front-slider-box swiper-slide'>
            <div className='front-slider-box-img'>
              <img src={images.login.slider1} />
            </div>
            <div className='front-slider-box-info'>
              <h1>
                Discover the best <br />
                content creators.
              </h1>
              <div className='mobile-account-button-group'>
                <Link href='/register'>
                  <button className='custom-button primary-bg-color color-white'>Create an account</button>
                </Link>
                <button className='custom-button bg-color-white primary-color' onClick={props.showLogin}>
                  Already have an account
                </button>
              </div>
            </div>
          </div>
        </SwiperSlide>
        <SwiperSlide>
          <div className='front-slider-box swiper-slide'>
            <div className='front-slider-box-img'>
              <img src={images.login.slider2} />
            </div>
            <div className='front-slider-box-info'>
              <h1>
                Publish content <br />
                and earn easily.
              </h1>
              <div className='mobile-account-button-group'>
                <Link href='/register'>
                  <button className='custom-button primary-bg-color color-white'>Create an account</button>
                </Link>
                <button className='custom-button bg-color-white primary-color' onClick={props.showLogin}>
                  Already have an account
                </button>
              </div>
            </div>
          </div>
        </SwiperSlide>
      </Swiper>
    </React.Fragment>
  )
}

export default LoginSlides
