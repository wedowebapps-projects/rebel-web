import Link from 'next/link'
import React from 'react'
import { Modal } from 'src/components/app'
import images from 'src/config/images'
import { constants, numberWithCommas } from 'src/utils/helpers'
import { getCmsPages_Api } from 'src/api/appApis'

const MyPay = () => {
  const minAmount = 2000
  const maxAmount = 50000
  const [isModalOpen, setIsModalOpen] = React.useState(false)
  const [slug, setSlug] = React.useState('mypay')
  const [title, setTitle] = React.useState('')
  const [slugImage, setSlugImage] = React.useState('')
  const [loading, setLoading] = React.useState(true)
  const [minPrice, setMinPrice] = React.useState(0)
  const [maxPrice, setMaxPrice] = React.useState(0)
  const [subscriptionSliderValue, setSubscriptionSliderValue] = React.useState(minAmount)
  const [followerSliderValue, setFollowerSliderValue] = React.useState(1000)
  const [contentPage, setContentPage] = React.useState('<p></p>')

  const closeModal = () => {
    setIsModalOpen(!isModalOpen)
  }

  React.useEffect(() => {
    getCmsPages()
    calculateMinPrice()
    calculateMaxPrice()
  }, [])

  const getCmsPages = async () => {
    try {
      const filters = { slug: slug }
      const result = await getCmsPages_Api(filters)
      setContentPage(result.page.content)
      setLoading(false)
      setTitle(result.page.title)
      setSlugImage(result.page.image)
    } catch (error) {
      if (error.errors.length > 0) {
        error.errors.map(error => toast.error(error.msg))
      }
      setLoading(false)
    }
  }
  const handleSubscriptionChange = e => {
    setSubscriptionSliderValue(e.target.value)
    calculateMinPrice()
    calculateMaxPrice()
  }
  const handleFollowerChange = e => {
    setFollowerSliderValue(e.target.value)
    calculateMinPrice()
    calculateMaxPrice()
  }

  const calculateMinPrice = () => {
    let minprice = (followerSliderValue * subscriptionSliderValue) / 100
    setMinPrice(minprice)
  }
  const calculateMaxPrice = () => {
    let maxprice = (followerSliderValue * subscriptionSliderValue * 5) / 100
    setMaxPrice(maxprice)
  }

  return (
    <section className='min-vh-100'>
      <div className='pay-calculator-bg' style={{ backgroundImage: `url(https://rebelyus-prod.s3.amazonaws.com/${slugImage})` }}>
        <div className='container'>
          <div className='row'>
            <div className='col-12'>
              <div className='banner-box'>
                <div className='back-button'>
                  <Link href='/login' to='/login'>
                    <a href='/login'>
                      <img src={images.settings.arrowBack} />
                    </a>
                  </Link>
                </div>
                <div className='banner-title-box mt-5 d-flex align-items-center'>
                  <div className='banner-title-icon mr-4'>
                    <img src={images.settings.titleIconLock} />
                  </div>
                  <div className='banner-title-text'>
                    <h2 className='color-white'>{title}</h2>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className='pay-calculator-info'>
        <div className='container'>
          <div className='row'>
            <div className='col-md-6 mt-3'>
              <div className='pay-calculator-left'>
                <p dangerouslySetInnerHTML={{ __html: contentPage }}></p>
              </div>
            </div>
            <div className='col-md-6 mt-5'>
              <div className='pay-calculator-right ml-auto mt--20'>
                <div className='ml-auto d-flex align-items-center'>
                  <div className='pay-calculator-img'>
                    <img src={images.login.Ellipse265} />
                  </div>
                  <div className='pay-calculator-img'>
                    <img src={images.login.nigeriaNairaCurrency} />
                  </div>
                  <div className='pay-calculator-img'>
                    <img src={images.login.Ellipse266} />
                  </div>
                </div>
                <div className='pay-calculator-right-button mt-4 mb-2'>
                  <a
                    href='#'
                    onClick={e => {
                      e.preventDefault()
                      setIsModalOpen(true)
                    }}
                    className='custom-button primary-bg-color use-calculator-button'>
                    Use calculator
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className='overlay'></div>
      <Modal keyboard={true} size='xl' show={isModalOpen} onClose={closeModal} showHeader={true}>
        <div className='calculator-popups p-4'>
          <div className='modal-wrp'>
            <div className='modal-inner-wrap'>
              <div className='calculator-popups-mobile calculator-popups-row row align-items-center mt-3'>
                <div className='calculator-popups-left-col col-md-7'>
                  <div className='calculator-popups-left'>
                    <h2 className='primary-color'>My pay calculator</h2>
                    <div className='d-flex align-items-center mt-4'>
                      <h4 className='primary-color mr-2'># of followers?</h4>
                      <ul>
                        <li>
                          <a href='#'>
                            <i className='fa fa-instagram' aria-hidden='true'></i>
                          </a>
                        </li>
                        <li>
                          <a href='#'>
                            <i className='fa fa-twitter' aria-hidden='true'></i>
                          </a>
                        </li>
                      </ul>
                    </div>
                    <div className='rate-bar'>
                      <div className='range-slider'>
                        <span className='range-slider__value'>{numberWithCommas(followerSliderValue)}</span>
                        <input className='range-slider__range' type='range' min={1000} max={1000000} step={1000} value={followerSliderValue} onChange={handleFollowerChange} />
                        <div className='range-slider__value__range d-flex align-items-center'>
                          <span>1000</span>
                          <span className='ml-auto'>1,000,000</span>
                        </div>
                      </div>
                    </div>
                    <h4 className='primary-color mr-2'>Monthly subscription price?</h4>
                    <div className='rate-bar'>
                      <div className='range-slider'>
                        <span className='range-slider__value'>₦ {numberWithCommas(subscriptionSliderValue)}</span>
                        <input className='range-slider__range' type='range' value={subscriptionSliderValue} onChange={handleSubscriptionChange} min={minAmount} max={maxAmount} step={0.05} />
                        <div className='range-slider__value__range d-flex align-items-center'>
                          <span> {numberWithCommas(minAmount)}</span>
                          <span className='ml-auto'> {numberWithCommas(maxAmount)}</span>
                        </div>
                      </div>
                    </div>
                    {/* <div className='mobile-show'>
                      <a href='#' className='custom-button primary-bg-color calculator-started-button '>
                        Calculate
                      </a>
                    </div> */}
                  </div>
                </div>
                <div className='calculator-popups-right-col col-md-5'>
                  <div className='calculator-popups-right text-center'>
                    <p>You could earn between</p>
                    <h1 className='primary-color'>
                      <span>
                        <img src={images.login.nigeriaNairaCurrencyN} />
                      </span>
                      {numberWithCommas(minPrice.toFixed(2))}
                    </h1>
                    <h4>and</h4>
                    <h1 className='primary-color'>
                      <span>
                        <img src={images.login.nigeriaNairaCurrencyN} />
                      </span>
                      {numberWithCommas(maxPrice.toFixed(2))}
                    </h1>
                    <h4>Every month</h4>
                    <p className='mt-4 mb-4'>Based on estimate of between 1% and 5% of your followers subscribing</p>
                    <Link href='/register' to='/register'>
                      <a href='/register' className='custom-button primary-bg-color calculator-started-button mt-2'>
                        Get started
                      </a>
                    </Link>
                  </div>
                </div>
                <div className='col-12 text-center mt-4 desktop-show'>
                  <h5 className='red-color'>NB:Earnings figure does not include income from tips and Paid messages.</h5>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Modal>
    </section>
  )
}

export default MyPay
