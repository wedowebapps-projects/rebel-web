import React, { useState, useEffect } from 'react'
import images from 'src/config/images.js'
import moment from 'moment-timezone'
import * as Yup from 'yup'
import { Form, Button } from 'src/components/shared/index.js'
import Link from 'next/link'
import { socialLogin } from '../api/authApi'
import * as storageUtils from 'src/utils/storageUtils'
import { getFirebase } from 'src/utils/helpers'
import { useRouter } from 'next/router'
const firebase = getFirebase()

export default function RegisterForm(props) {
  const router = useRouter()
  const [errors, setErrors] = useState([])
  const [loading, setLoading] = useState(false)
  const [isChecked, setIsChecked] = useState(false)
  const [showPass, setShowPass] = useState(false)
  const [showConfirmPass, setShowConfirmPass] = useState(false)
  const [emailError, setEmailError] = useState('')
  const [socialErrors, setSocialErrors] = useState([])
  const [checkSocialLogin, setCheckSocialLogin] = useState(false)

  const defaultDate = new Date(
    moment()
      .subtract(18, 'years')
      .format('YYYY-MM-DD')
  )

  useEffect(() => {
    if (props.registerError.emailExists) {
      setLoading(false)
      setEmailError('This email is already registered!')
    } else {
      setEmailError('')
    }
  }, [props])

  const changePassType = () => {
    setShowPass(!showPass)
  }
  const changeConfirmPassType = () => {
    setShowConfirmPass(!showConfirmPass)
  }

  const doSignIn = provider => {
    return new Promise((resolve, reject) => {
      const socialProviderData = {}
      try {
        firebase.firebase
          .auth()
          .signInWithPopup(firebase[provider])
          .then(result => {
            var user = result.user
            socialProviderData.provider = provider
            socialProviderData.provider_id = user.uid
            socialProviderData.email_mobile = user.email || user.phoneNumber || `${user.uid}@${provider}.com`
            socialProviderData.email = user.email || `${user.uid}@${provider}.com`
            socialProviderData.first_name = user.displayName.split(' ')[0]
            socialProviderData.last_name = user.displayName.split(' ')[1]
            socialProviderData.avatar = user.photoURL

            // Set social login data.
            props.setSocialData(socialProviderData)
            resolve(socialProviderData)
          })
          .catch(async error => {
            console.log(error)
            if (error.code === 'auth/account-exists-with-different-credential') {
              try {
                const fetchSignInMethods = await firebase.firebase.auth().fetchSignInMethodsForEmail(error.email)
                reject(`Your account is already linked with Google. You can Sign In with ${fetchSignInMethods[0]}`)
              } catch (error) {
                reject(error.message)
              }
            } else {
              reject(error.message)
            }
          })
      } catch (error) {
        console.log(error)
      }
    })
  }

  const openSocialLoginPopup = async (e, provider) => {
    e.preventDefault()
    setSocialErrors('')
    try {
      // Handle login popup
      const result = await doSignIn(provider)
      const socialData = {
        provider,
        ...result,
        timezone: moment.tz.guess()
      }
      const query = router.query || undefined
      // Check if social account exist in database.
      try {
        setCheckSocialLogin(provider)
        const socialResult = await props.socialSignup(socialData)
        setCheckSocialLogin(false)
      } catch (error) {
        console.log(error)
        /* Redirect to signup, as user does not exist.*/
        setCheckSocialLogin(false)
        router.push({ pathname: '/register', query })
      }
      setCheckSocialLogin(false)
    } catch (error) {
      console.log('errorsss', error)
      setSocialErrors(error)
    }
  }

  return (
    <React.Fragment>
      <Form
        initialValues={{
          first_name: '',
          last_name: '',
          email: '',
          password: '',
          confirm_password: '',
          dob: defaultDate
        }}
        validationSchema={Yup.object().shape({
          first_name: Yup.string().required('First Name is required'),
          last_name: Yup.string().required('Last Name is required'),
          dob: Yup.date().test('dob', 'You must be 18 and above', value => {
            return moment().diff(moment(value), 'years') > 18
          }),
          email: Yup.string()
            .required('Email is required')
            .email('Invalid email'),
          password: Yup.string()
            .required('Password is required')
            .min(6, 'Your password must be longer than 6 characters.')
            .matches(/^(?=.*[a-zA-Z])(?=.*\d)[A-Za-z\d]{6,}/, 'Password must contain at least one Letter and one Number'),
            // .matches(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/, 'Password must contain one Uppercase, one Lowercase, One number and one special case character'),
          confirm_password: Yup.string()
            .oneOf([Yup.ref('password'), ''], 'Passwords must match')
            .required('Confirm Password is required')
        })}
        onSubmit={async (values) => {
          setErrors([])
          setLoading(true)
          await props.registerUser(values)
        }}>
        {props => (
          <form
            onSubmit={e => {
              e.preventDefault()
              props.submitForm()
              return false
            }}
            noValidate>
            <Form.Field.Input name='first_name' label='First Name' />
            <Form.Field.Input name='last_name' label='Last Name' />
            <Form.Field.Input name='email' errorMsg={emailError} label='Email Address' />
            <div className='position-relative'>
              <Form.Field.Input type={showPass ? 'text' : 'password'} name='password' label='Password' />
              {showPass ? (
                <img src={images.settings.Show} onClick={changePassType} className='position-absolute' style={{ right: '10px', top: '66px' }} />
              ) : (
                <img src={images.settings.Hide} onClick={changePassType} className='position-absolute' style={{ right: '10px', top: '66px' }} />
              )}
            </div>
            <div className='position-relative'>
              <Form.Field.Input type={showConfirmPass ? 'text' : 'password'} name='confirm_password' label='Confirm Password' />
              {showConfirmPass ? (
                <img src={images.settings.Show} onClick={changeConfirmPassType} className='position-absolute' style={{ right: '10px', top: '66px' }} />
              ) : (
                <img src={images.settings.Hide} onClick={changeConfirmPassType} className='position-absolute' style={{ right: '10px', top: '66px' }} />
              )}
            </div>
            <Form.Field.DatePicker name='dob' label='Date of birth' />
            <div className='d-flex my-3 check-terms'>
              <input
                type='checkbox'
                id='check-term'
                required
                onClick={e => setIsChecked(e.target.checked)}
                className='terms-checkbox'
                style={{ height: '17px', width: '17px', margin: '2px 5px 0 0' }}
                name='checked'
              />
              <label htmlFor='check-term'>
                By continuing you agree to our{' '}
                <Link href='/terms-conditions' to='/terms-conditions'>
                  <a href='/terms-conditions'>Terms of Service</a>
                </Link>{' '}
                and{' '}
                <Link href='/privacy-policy' to='/privacy-policy'>
                  <a href='/privacy-policy'>Privacy Policy</a>
                </Link>
              </label>
            </div>
            <Button type='submit' variant='primary' block isWorking={loading} disabled={!isChecked}>
              Sign up
            </Button>
            <Button variant='transparent' className='mt-5 mb-5' faIcon='fa-twitter' block isWorking={checkSocialLogin === 'twitter'} onClick={e => openSocialLoginPopup(e, 'twitter')}>
              Sign up with twitter
            </Button>
          </form>
        )}
      </Form>
    </React.Fragment>
  )
}
