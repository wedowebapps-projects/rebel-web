import React, { useState } from 'react'
import { Form, Button } from 'src/components/shared/index.js'

export default function ForgotPasswordForm(props) {
  const [errors, setErrors] = useState([])
  const [loading, setLoading] = useState(false)

  const error = errors && errors.length > 0 && errors[0].msg !== '' ? errors[0].msg : false

  return (
    <React.Fragment>
      {error && (
        <div className='alert alert-danger' role='alert'>
          {error}
        </div>
      )}
      <Form
        initialValues={{
          newPassword: '',
          confirm_password: '',
          otp: ''
        }}
        validations={{
          newPassword: [Form.is.required()],
          confirm_password: [Form.is.required()],
          otp: [Form.is.required()]
        }}
        onSubmit={async (values, form) => {
          values.email = props.email
          setLoading(true)
          setErrors([])
          await props
            .resetPassword(values)
            .then(res => {})
            .catch(err => {
              setErrors(err)
            })
          setLoading(false)
        }}>
        {props => (
          <React.Fragment>
            <Form.Field.OtpInput isInputNum numInputs={6} name='otp' label='OTP' />
            <div className='d-flex flex-row-reverse'>
              <Button
                medium
                variant='secondary'
                onClick={() => {
                  setLoading(true)
                  setErrors([])
                  props
                    .setFieldValue('otp', '')
                    .then(res => {})
                    .catch(err => {
                      setErrors(err)
                    })
                  setLoading(false)
                }}>
                Clear
              </Button>
            </div>
            <Form.Field.Input type="password" floating name='newPassword' label='Password' />
            <Form.Field.Input type="password" floating name='confirm_password' label='Confirm Password' />
            <Button variant='primary' block isWorking={loading} onClick={props.submitForm}>
              Submit
            </Button>
          </React.Fragment>
        )}
      </Form>
    </React.Fragment>
  )
}
