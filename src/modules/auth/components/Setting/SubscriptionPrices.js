import _, { cloneDeep } from 'lodash'
import React from 'react'
import { connect } from 'react-redux'
import { Button, Form, Spinner } from 'src/components/shared'
import images from 'src/config/images'
import { constants, numberWithCommas } from 'src/utils/helpers'
import * as Yup from 'yup'
import * as authActions from '../../actions/authActions'
import * as profileActions from '../../../profile/actions/profileActions'
import { Alert } from 'react-bootstrap'
import Link from 'next/link'
import { useRouter } from 'next/router'

const PriceBifurcation = ({ amount, commissionData }) => {
  const getFees = amount => {
    let newAmount = 0
    if (process.env.PAYMENT_METHOD === 'flutterwave') {
      newAmount = (amount * commissionData.flutterwave_fees) / 100 // for flutterwave payment
    } else {
      newAmount = (amount * commissionData.paystack_fees) / 100 // for paystack payment
    }
    return newAmount.toFixed(2)
  }
  const getCommssion = amount => {
    return amount && commissionData.commmission > 0 ? ((amount * commissionData.commmission) / 100).toFixed(2) : 0
  }
  return (
    <div style={{ fontSize: '12px' }}>
      <div className='text-right'>
        <div className='d-inline-block text-muted px-0'>Price:</div>
        <div className='d-inline-block ml-1' style={{ minWidth: 40, maxWidth: 150, flexGrow: 'unset' }}>
          <strong>
            {constants.NAIRA}&nbsp;{amount ? numberWithCommas(amount) : 0}
          </strong>
        </div>
      </div>
      {commissionData.commmission > 0 && (
        <div className='text-right'>
          <div className='d-inline-block text-muted px-0'>Rebel Commission ({commissionData.commmission}%):</div>
          <div className='d-inline-block ml-1' style={{ minWidth: 40, maxWidth: 150, flexGrow: 'unset' }}>
            <strong>
              {constants.NAIRA}&nbsp;{numberWithCommas(getCommssion(amount))}
            </strong>
          </div>
        </div>
      )}
      <div className='text-right'>
        <div className='d-inline-block text-muted px-0'>Transaction Charges({process.env.PAYMENT_METHOD === 'flutterwave' ? commissionData.flutterwave_fees : commissionData.paystack_fees}%):</div>
        <div className='d-inline-block ml-1' style={{ minWidth: 40, maxWidth: 150, flexGrow: 'unset' }}>
          <strong>
            {constants.NAIRA}&nbsp;{numberWithCommas(getFees(amount))}
          </strong>
        </div>
      </div>
      <div className='text-right'>
        <div className='d-inline-block text-muted px-0'>You&apos;ll get:</div>
        <div className='d-inline-block ml-1' style={{ minWidth: 40, maxWidth: 150, flexGrow: 'unset' }}>
          <strong>
            {constants.NAIRA}&nbsp;{numberWithCommas((amount - getCommssion(amount) - getFees(amount)).toFixed(2))}
          </strong>
        </div>
      </div>
    </div>
  )
}

const SubscriptionPrices = props => {
  const router = useRouter()
  const [showDropdown, setShowDropdown] = React.useState({ first: false, second: false, third: false })
  const [updateAlert, setUpdateAlert] = React.useState({ type: '', msg: '' })
  const [list, setList] = React.useState([])
  const [isSubmitting, setIsSubmitting] = React.useState(false)
  const [isLoading, setIsLoading] = React.useState(true)

  React.useEffect(() => {
    if (props.auth.user.personal_info_status !== 'approved') {
      router.push('/wallet/bank-application')
    } else {
      props
        .fetchSubscriptionPrice()
        .then(data => {
          let temp = data.map(value => {
            return { ...value, plan_data: value.plan_data ? JSON.parse(value.plan_data) : {} }
          })
          setList(temp)
          setIsLoading(false)
        })
        .catch(() => {
          setIsLoading(false)
        })
    }
  }, [])

  return (
    <div className='w-100 ac-content-area'>
      <div className='min-vh-100 profile-wrp my-4'>
        <h3 className='border-wrap-bottom'>
          <Link href='/settings' to='/settings'>
            <a href='/settings' className='go-back d-md-none back-arrow-round mr-3'>
              <img src={images.settings.acBackArrow} />
            </a>
          </Link>
          Set Pricing
        </h3>
        <div className='account-setting-area ml-auto'>
          <div className='pricing-row'>
            {isLoading ? (
              <div className='d-flex align-items-center justify-content-center'>
                <Spinner size={40} color='#187BCD' />
              </div>
            ) : (
              <Form
                initialValues={{
                  amount1:
                    process.env.PAYMENT_METHOD === 'flutterwave'
                      ? list?.find(a => a?.plan_data?.interval === 'monthly')?.plan_data?.amount || 0
                      : list?.find(a => a?.plan_data?.interval === 'monthly')?.plan_data?.amount / 100 || 0,
                  amount2:
                    process.env.PAYMENT_METHOD === 'flutterwave'
                      ? list?.find(a => a?.plan_data?.interval === 'quarterly')?.plan_data?.amount || 0
                      : list?.find(a => a?.plan_data?.interval === 'quarterly')?.plan_data?.amount / 100 || 0,
                  amount3:
                    process.env.PAYMENT_METHOD === 'flutterwave'
                      ? list?.find(a => a?.plan_data?.interval === 'annually')?.plan_data?.amount || 0
                      : list?.find(a => a?.plan_data?.interval === 'annually')?.plan_data?.amount / 100 || 0
                }}
                validationSchema={Yup.object().shape({
                  amount1: Yup.number()
                    .required()
                    .label('Monthly amount')
                    .test(value => value == 0 || (value >= props.app.commissions.min_subscription_amount && value <= props.app.commissions.max_subscription_amount)),
                  amount2: Yup.number()
                    .required()
                    .label('Quarterly amount')
                    .test(value => value == 0 || (value >= props.app.commissions.min_subscription_amount && value <= props.app.commissions.max_subscription_amount)),
                  amount3: Yup.number()
                    .required()
                    .label('Annually amount')
                    .test(value => value == 0 || (value >= props.app.commissions.min_subscription_amount && value <= props.app.commissions.max_subscription_amount))
                })}
                onSubmit={async values => {
                  setIsSubmitting(true)
                  let isUpdated = false

                  for (const val in values) {
                    let gap = val === 'amount1' ? 'monthly' : val === 'amount2' ? 'quarterly' : 'annually'
                    let selectedPlan = list.find(a => a.plan_data.interval === gap)
                    let amount = Number(values[val])

                    // If plan price is update
                    if (selectedPlan && selectedPlan?.plan_data.amount != amount) {
                      try {
                        await props.deletePlan(selectedPlan)
                        isUpdated = true
                      } catch (error) {
                        if (error.errors) {
                          setUpdateAlert({ msg: error.errors[0].msg, type: 'danger' })
                          setTimeout(() => {
                            setUpdateAlert({ msg: '', type: '' })
                          }, 4000)
                        }
                      }
                    }

                    // create new plan
                    if (amount > 0 && (!selectedPlan || selectedPlan?.plan_data.amount != amount)) {
                      try {
                        const result = await props.submitSubscriptionPrice({
                          amount: amount,
                          name: `${gap}-${new Date()}`,
                          interval: gap
                        })
                        setUpdateAlert({ msg: result.message, type: 'success' })
                        isUpdated = true
                      } catch (error) {
                        if (error.errors) {
                          setUpdateAlert({ msg: error.errors[0].msg, type: 'danger' })
                        }
                      }
                    }
                  }

                  if (!isUpdated) {
                    // show success message if no changes made
                    setUpdateAlert({ msg: 'Subscription created successfully', type: 'success' })
                  } else {
                    // update subscription list
                    props
                      .fetchSubscriptionPrice()
                      .then(data => {
                        let temp = data.map(value => {
                          return { ...value, plan_data: value.plan_data ? JSON.parse(value.plan_data) : {} }
                        })
                        setList(temp)
                        setIsLoading(false)
                      })
                      .catch(() => {
                        setIsLoading(false)
                      })
                  }
                  setTimeout(() => {
                    setUpdateAlert({ msg: '', type: '' })
                  }, 4000)

                  setIsSubmitting(false)
                }}>
                {prop => (
                  <form
                    className='pl-4 edit-profile-wrp'
                    onSubmit={e => {
                      e.preventDefault()
                      prop.submitForm()
                      return false
                    }}
                    noValidate>
                    <div className='row pricing-banner pb-3'>
                      <div className='col-sm-4 pricing-col'>
                        <div className='pricing-box'>
                          <h5 className='text-center primary-color'>Monthly</h5>
                          <ul>
                            <li>Access to posts monthly</li>
                            <li>Enable direct messaging</li>
                            <li>Cancel auto renewal at any time</li>
                          </ul>
                          <div className='privacy-button text-center'>
                            <span className='price-input'>
                              <Form.Field.Input name='amount1' type='number' placeholder='Enter Price' />
                              <div className='price-input-currency'>{constants.NAIRA}</div>
                            </span>
                          </div>
                        </div>
                        <a
                          onClick={() => setShowDropdown({ ...showDropdown, first: !showDropdown.first })}
                          className='d-flex justify-content-center my-1 cursor-pointer'
                          style={{ fontSize: '13px', margin: '0 auto', color: '#007bff' }}>
                          View dropdown {showDropdown.first ? <img src={images.settings.arrowFat} alt='arrow' /> : <img src={images.settings.arrowFat} alt='arrow' />}
                        </a>
                        {showDropdown.first && (
                          <PriceBifurcation
                            amount={prop.values.amount1}
                            commissionData={{
                              flutterwave_fees: props.app.commissions.flutterwave_fees,
                              paystack_fees: props.app.commissions.paystack_fees,
                              commmission: props.app.commissions.commmission
                            }}
                          />
                        )}
                      </div>
                      <div className='col-sm-4 pricing-col'>
                        <div className='pricing-box'>
                          <h5 className='text-center primary-color'>Quarterly</h5>
                          <ul>
                            <li>Access to posts quarterly</li>
                            <li>Enable direct messaging</li>
                            <li>Cancel auto renewal at any time</li>
                          </ul>
                          <div className='privacy-button text-center'>
                            <span className='price-input'>
                              <Form.Field.Input name='amount2' type='number' placeholder='Enter Price' />
                              <div className='price-input-currency'>{constants.NAIRA}</div>
                            </span>
                          </div>
                        </div>
                        <a
                          onClick={() => setShowDropdown({ ...showDropdown, second: !showDropdown.second })}
                          className='d-flex justify-content-center my-1 cursor-pointer'
                          style={{ fontSize: '13px', margin: '0 auto', color: '#007bff' }}>
                          View dropdown {showDropdown.second ? <img src={images.settings.arrowFat} alt='arrow' /> : <img src={images.settings.arrowFat} alt='arrow' />}
                        </a>
                        {showDropdown.second && (
                          <PriceBifurcation
                            amount={prop.values.amount2}
                            commissionData={{
                              flutterwave_fees: props.app.commissions.flutterwave_fees,
                              paystack_fees: props.app.commissions.paystack_fees,
                              commmission: props.app.commissions.commmission
                            }}
                          />
                        )}
                      </div>
                      <div className='col-sm-4 pricing-col'>
                        <div className='pricing-box'>
                          <h5 className='text-center primary-color'>Annually</h5>
                          <ul>
                            <li>Access to posts annually</li>
                            <li>Enable direct messaging</li>
                            <li>Cancel auto renewal at any time</li>
                          </ul>
                          <div className='privacy-button text-center'>
                            <span className='price-input'>
                              <Form.Field.Input name='amount3' type='number' placeholder='Enter Price' />
                              <div className='price-input-currency'>{constants.NAIRA}</div>
                            </span>
                          </div>
                        </div>
                        <a
                          onClick={() => setShowDropdown({ ...showDropdown, third: !showDropdown.third })}
                          className='d-flex justify-content-center my-1 cursor-pointer'
                          style={{ fontSize: '13px', margin: '0 auto', color: '#007bff' }}>
                          View dropdown {showDropdown.third ? <img src={images.settings.arrowFat} alt='arrow' /> : <img src={images.settings.arrowFat} alt='arrow' />}
                        </a>
                        {showDropdown.third && (
                          <PriceBifurcation
                            amount={prop.values.amount3}
                            commissionData={{
                              flutterwave_fees: props.app.commissions.flutterwave_fees,
                              paystack_fees: props.app.commissions.paystack_fees,
                              commmission: props.app.commissions.commmission
                            }}
                          />
                        )}
                      </div>
                    </div>
                    {updateAlert.type && (
                      <Alert variant={updateAlert.type} className='text-center rounded-0 mb-0'>
                        {updateAlert.msg}
                      </Alert>
                    )}
                    <div className='mt-4'>
                      <small className='float-right pb-2'>
                        Min allowed value {numberWithCommas(props?.app?.commissions?.min_subscription_amount)} & max allowed value {numberWithCommas(props?.app?.commissions?.max_subscription_amount)}
                      </small>
                      <Button type='submit' disabled={isSubmitting} variant='primary' className='w-100 primary-bg-color custom-button'>
                        Save Changes {isSubmitting && <Spinner color='#2A9DF4' size={40} />}
                      </Button>
                    </div>
                  </form>
                )}
              </Form>
            )}
          </div>
        </div>
      </div>
    </div>
  )
}

const mapStateToProps = state => ({
  auth: state.auth,
  app: state.app
})

const mapDispatchToProps = dispatch => {
  return {
    fetchSubscriptionPrice: () => new Promise((resolve, reject) => dispatch(authActions.fetchSubscriptionPricesAction(resolve, reject))),
    fetchProfile: username => dispatch(profileActions.fetchProfile(username)),
    deletePlan: data => new Promise((resolve, reject) => dispatch(authActions.deleteSubscriptionPriceAction(data, resolve, reject))),
    submitSubscriptionPrice: (data, uuid) => new Promise((resolve, reject) => dispatch(authActions.submitSubscriptionPriceAction(data, uuid, resolve, reject)))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SubscriptionPrices)
