import Link from 'next/link'
import React from 'react'
import { getCmsPages_Api } from 'src/api/appApis'
import images from 'src/config/images'

const PrivacyPolicy = () => {
  const [contentPage, setContentPage] = React.useState('<p></p>')
  const [slugImage, setSlugImage] = React.useState('')
  const [title, setTitle] = React.useState('')

  React.useEffect(() => {
    getCmsPages()
  }, [])

  const getCmsPages = async () => {
    try {
      const filters = { slug: 'privacy-policy' }
      const result = await getCmsPages_Api(filters)
      setContentPage(result.page.content)
      setTitle(result.page.title)
      setSlugImage(result.page.image)
    } catch (error) {
      console.log(error)
    }
  }
  return (
    <section className="min-vh-100">
      <div className='policy-bg' 
        style={{ 
          backgroundImage: slugImage 
            ? `url(https://rebelyus-prod.s3.amazonaws.com/${slugImage})` 
            : `url(${images.app.web_header})` 
        }}
      >
        <div className='container'>
          <div className='row'>
            <div className='col-12'>
              <div className='banner-box'>
                <div className='back-button'>
                  <Link href='/settings/accountPrivacy' to='/settings/accountPrivacy'>
                    <a>
                      <img src={images.settings.arrowBack} />
                    </a>
                  </Link>
                </div>
                <div className='banner-title-box mt-5 d-flex align-items-center'>
                  <div className='banner-title-icon mr-4'>
                    <img src={images.settings.titleIconLock} />
                  </div>
                  <div className='banner-title-text'>
                    <h2 className='color-white'>{title}</h2>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className='policy-info'>
        <div className='container'>
          <div className='row'>
            <div className='col-12 mt-5 mb-5'>
              <p dangerouslySetInnerHTML={{ __html: contentPage }}></p>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default PrivacyPolicy
