import Link from 'next/link'
import { useRouter } from 'next/router'
import React from 'react'
import { Alert } from 'react-bootstrap'
import { Button, Form, Spinner } from 'src/components/shared'
import images from 'src/config/images'
import * as Yup from 'yup'
import { changePassword_api } from '../../api/authApi'

const ChangePassword = () => {
  const router = useRouter()
  const [isFormSubmitting, setIsFormSubmitting] = React.useState(false)
  const [updateAlert, setUpdateAlert] = React.useState({})

  return (
    <div className='w-100 ac-content-area'>
      <div className='min-vh-100 profile-wrp my-md-4'>
        <h3 className='border-wrap-bottom d-flex align-item-center'>
          <Link href='/settings' to='/settings'>
            <a href='/settings' className='go-back d-md-none back-arrow-round mr-3'>
              <img src={images.settings.acBackArrow} />
            </a>
          </Link>
          Change Password
        </h3>
        <Form
          initialValues={{
            oldPassword: '',
            newPassword: '',
            confPassword: ''
          }}
          validationSchema={Yup.object().shape({
            oldPassword: Yup.string().required('Old Password is required'),
            newPassword: Yup.string().required('New Password is required'),
            confPassword: Yup.string()
              .required('Confirm Password is required')
              .oneOf([Yup.ref('newPassword'), null], 'Password must match')
          })}
          onSubmit={async values => {
            try {
              setIsFormSubmitting(true)
              const result = await changePassword_api(values)
              setUpdateAlert({ msg: result.message, type: 'success' })
              router.push('/login')
            } catch (error) {
              if (error.errors) {
                setUpdateAlert({ msg: error.errors[0].msg, type: 'danger' })
              }
            }
            setTimeout(() => {
              setUpdateAlert({ msg: '', type: '' })
            }, 3000)
            setIsFormSubmitting(false)
          }}>
          {props => (
            <form
              className='pl-4 edit-profile-wrp'
              onSubmit={e => {
                e.preventDefault()
                props.submitForm()
                return false
              }}
              noValidate>
              <div className='account-setting-area mr-auto pl-5 pr-5 mt-5 input-with-fix '>
                <Form.Field.Input type='password' name='oldPassword' label='Current Password' placeholder='**********' />
                <Form.Field.Input type='password' name='newPassword' label='New Password' placeholder='**********' />
                <Form.Field.Input type='password' name='confPassword' label='Confirm Password' placeholder='**********' />
                {updateAlert.type && (
                  <Alert variant={updateAlert.type} className='text-center rounded-0 mb-0'>
                    {updateAlert.msg}
                  </Alert>
                )}
                <Button variant='primary' disabled={isFormSubmitting} className='primary-bg-color custom-button mt-1 mb-3'>
                  Submit {isFormSubmitting && <Spinner color='#2A9DF4' size={36} />}
                </Button>
              </div>
            </form>
          )}
        </Form>
      </div>
    </div>
  )
}

export default ChangePassword
