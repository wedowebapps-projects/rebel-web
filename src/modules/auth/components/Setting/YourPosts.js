import Link from 'next/link'
import React from 'react'
import { Button } from 'src/components/shared'
import images from 'src/config/images'

const YourPosts = () => {
  return (
    <div className='w-100 ac-content-area'>
      <div className='profile-wrp my-4'>
        <h3 className='border-wrap-bottom'>
          <Link href='/settings/accountPrivacy' to='/settings/accountPrivacy'>
            <a href='/settings/accountPrivacy' title='' className='back-arrow-round mr-3'>
              <img src={images.settings.acBackArrow} alt='' />
            </a>
          </Link>
          Your posts
        </h3>
        <div className='account-setting-area your-post ml-auto mr-auto ml-5'>
          <ol>
            <li className='allow-sharing'>
              <h4 className='primary-color'>Allow sharing</h4>
              <label className='switch ml-3'>
                <input type='checkbox' checked='' />
                <span className='slider round'></span>
              </label>
            </li>
            <li className='d-flex flex-wrap align-items-center border-bottom-0 who-can-comment'>
              <p>Who can comment on your posts</p>
              <div className='more-settings-wrp comment-privacy'>
                <a href='#' className='primary-color'>
                  Everyone
                  <img src={images.settings.optionArrow} />
                </a>
                <div className='more-setings-link' style={{ display: 'none' }}>
                  <a href='#' title=''>
                    Everyone
                  </a>
                  <a href='#' title=''>
                    Subscribers
                  </a>
                </div>
              </div>
              <div className='w-100'>
                <Button variant='primary' className='primary-bg-color custom-button mt-1 mb-3 mt-5'>
                  Save Changes
                </Button>
              </div>
            </li>
          </ol>
        </div>
      </div>
    </div>
  )
}

export default YourPosts
