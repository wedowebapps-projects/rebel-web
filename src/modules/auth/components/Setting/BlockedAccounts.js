import _ from 'lodash'
import Link from 'next/link'
import React from 'react'
import InfiniteScroll from 'react-infinite-scroll-component'
import { connect } from 'react-redux'
import { fetchListNumbers_api } from 'src/api/appApis'
import { Spinner, UserMedia } from 'src/components/shared'
import images from 'src/config/images'
import { checkAndRedirectLogin } from 'src/utils/helpers'
import * as profileActions from '../../../profile/actions/profileActions'
import { unBlockedUserFromBlockList_api } from '../../api/authApi'

const BlockedAccounts = props => {
  const [listType, setListType] = React.useState([])
  const [postList, setPostList] = React.useState([])
  const [btnLoader, setBtnLoader] = React.useState(false)
  const [input, setInput] = React.useState('')

  var maxLength = props?.listType?.filter(l => l.type === 'blocked')?.count || 0

  React.useEffect(() => {
    if (props.auth.user.username) {
      props.fetchProfile(props.auth.user.username)
    }
  }, [props.auth.user.username])

  React.useEffect(() => {
    if (props.blocked.data) {
      setPostList(props.blocked.data)
    }
  }, [props.blocked.data])

  React.useEffect(() => {
    fetchListData()
    fetchPosts('blocked')
  }, [])

  const fetchListData = () => {
    fetchListNumbers_api().then(data => {
      if (data && data.lists) setListType(data.lists)
    })
  }

  const fetchPosts = type => {
    const state = props[type].state
    if (!state.end) {
      const params = {
        username: props.auth.user.username,
        type: type,
        page: state.page,
        perPage: state.perPage
      }
      props.fetchProfilePosts(params)
    }
  }

  const onUnblock = async data => {
    if (checkAndRedirectLogin()) {
      try {
        setBtnLoader(true)
        await unBlockedUserFromBlockList_api(data)
        location.reload()
      } catch (error) {
        console.log(error)
      }
      setBtnLoader(false)
    }
  }

  //search posts
  React.useEffect(() => {
    if (input) {
      searchInput()
    } else {
      setPostList(props.blocked.data)
    }
  }, [input])

  const searchInput = () => {
    let temp = _.filter(props.blocked.data, function(e) {
      return _.includes(_.lowerCase(`${e?.followedId?.first_name} ${e?.followedId?.last_name}`), _.lowerCase(input))
    })
    setPostList(temp)
  }

  return (
    <div className='w-100 ac-content-area'>
      <div className='min-vh-100 profile-wrp my-4'>
        <h3 className='border-wrap-bottom'>
          <Link href='/settings/accountPrivacy' to='/settings/accountPrivacy'>
            <a href='/settings/accountPrivacy' title='' className='back-arrow-round mr-3'>
              <img src={images.settings.acBackArrow} alt='' />
            </a>
          </Link>
          Blocked accounts <div className='blocked-account'>({listType.find(t => t.type === 'blocked')?.count || 0})</div>
        </h3>
        <div className='block-ac-list pr-2'>
          <div className='search-input-wrap'>
            <input type='search' name='' value={input} onChange={e => setInput(e.target.value)} placeholder='Search user' />
            <img src={images.app.search} />
          </div>
          <InfiniteScroll
            dataLength={postList.length} //This is important field to render the next data
            next={() => fetchPosts('blocked')}
            hasMore={!props.blocked.state.end}
            >
            {postList.map((user, i) => {
              return (
                <div className='account-setting-area mr-auto mt-5' key={`blocked-post-${user.followedId.id}-${i}`}>
                  <div className='d-flex align-items-center justify-content-between'>
                    <UserMedia user={user.followedId} size={64} />
                    <Link href='/[username]' as={`/${user.followedId.username}`}>
                      <a className='view-profile cursor-pointer'>View profile</a>
                    </Link>
                    <div
                      className='unblock'
                      onClick={e => {
                        onUnblock(user.followedId.id)
                      }}>
                      {btnLoader ? <Spinner color='#2A9DF4' size={20} /> : <span>Unblock</span>}
                    </div>
                  </div>
                </div>
              )
            })}
          </InfiniteScroll>
          {props.blocked.state.loading && (
            <div className='d-flex mt-5 align-items-center justify-content-center'>
              <Spinner color='#2A9DF4' size={40} />
            </div>
          )}
        </div>
        {!props.blocked.state.loading && postList.length === 0 && (
          <div className='text-center no-post-found'>
            <img src={images.app.emptyFeed} style={{ maxWidth: 300 }} />
            <h3>No blocked accounts!</h3>
          </div>
        )}
      </div>
    </div>
  )
}

const mapStateToProps = state => ({
  auth: state.auth,
  app: state.app,
  profile: state.profile.profile,
  isProfileLoading: state.profile.isProfileLoading,
  blocked: state.profile.blocked
})

const mapDispatchToProps = dispatch => {
  return {
    fetchProfile: username => dispatch(profileActions.fetchProfile(username)),
    fetchProfilePosts: data => dispatch(profileActions.fetchProfilePosts(data)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(BlockedAccounts)
