import Link from 'next/link'
import React from 'react'
import { fetchListNumbers_api } from 'src/api/appApis'
import images from 'src/config/images'

const AccountPrivacy = props => {
  const [listType, setListType] = React.useState([])

  React.useEffect(() => {
    fetchListNumbers_api().then(data => {
      if (data && data.lists) setListType(data.lists)
    })
  }, [])

  return (
    <div className='w-100 ac-content-area'>
      <div className='min-vh-100 profile-wrp my-4'>
        <h3 className='border-wrap-bottom'>
          <Link href='/settings' to='/settings'>
            <a href='/settings' className='go-back d-md-none back-arrow-round mr-3'>
              <img src={images.settings.acBackArrow} />
            </a>
          </Link>
          Account Privacy
        </h3>
        <div className='account-setting-area account-settings account-settings-mobile ml-auto mr-auto ml-5'>
          <ol>
            <Link href='/settings/accountPrivacy/your-posts' to='/settings/accountPrivacy/your-posts'>
              <li>
                <h4>Your posts</h4>
                <span>Manage your posts and audience who sees them</span>
                <span className='angle-right'>
                  <img src={images.settings.angleRight} alt='' />
                </span>
              </li>
            </Link>
            <Link href='/settings/accountPrivacy/blocked-accounts' to='/settings/accountPrivacy/blocked-accounts'>
              <li>
                <h4>
                  Blocked accounts <div className='blocked-account'>({listType.find(t => t.type === 'blocked')?.count || 0})</div>
                </h4>
                <span>Manage your posts and audience who sees them</span>
                <span className='angle-right'>
                  <img src={images.settings.angleRight} alt='' />
                </span>
              </li>
            </Link>
            <li>
              <h4>Messaging</h4>
              <span>Manage your posts and audience who sees them</span>
              <span className='angle-right'>
                <img src={images.settings.angleRight} alt='' />
              </span>
            </li>
          </ol>
          <div className='terms-policy-wrap'>
            <Link href='/terms-conditions' to='/terms-conditions'>
              <h4 className='d-flex justify-content-between cursor-pointer'>
                <a href='/terms-conditions' title=''>
                  Terms of use
                </a>
                <img src={images.settings.arrowUpRight} alt='' />
              </h4>
            </Link>
            <Link href='/privacy-policy' to='/privacy-policy'>
              <h4 className='d-flex justify-content-between cursor-pointer'>
                <a href='/privacy-policy' title=''>
                  Privacy policy
                </a>
                <img src={images.settings.arrowUpRight} alt='' />
              </h4>
            </Link>
            <Link href='/how-it-works' to='/how-it-works'>
              <h4 className='d-flex justify-content-between cursor-pointer'>
                <a href='/how-it-works' title='How it works'>
                  How it works
                </a>
                <img src={images.settings.arrowUpRight} alt='' />
              </h4>
            </Link>
          </div>
        </div>
      </div>
    </div>
  )
}

export default AccountPrivacy
