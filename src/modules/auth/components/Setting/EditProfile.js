import moment from 'moment'
import React from 'react'
import { connect } from 'react-redux'
import { Button, Form, Spinner } from 'src/components/shared'
import images from 'src/config/images'
import * as Yup from 'yup'
import * as profileTypes from '../../../profile/actions/profileTypes'
import { changeProfileMedia_api, deleteAccount_api } from 'src/modules/auth/api/authApi'
import { Alert } from 'react-bootstrap'
import { InterestList, GenderList } from 'src/config'
import { checkUsernameExistAction, verifyPasswordAction } from '../../actions/authActions'
import { Field } from 'formik'
import { Modal } from 'src/components/app'
import { upload } from 'src/utils/s3'
import { useRouter } from 'next/router'
import Link from 'next/link'

const EditProfile = ({ user, ...props }) => {
  const router = useRouter()
  var maxDateOfBirth
  const [updateAlert, setUpdateAlert] = React.useState({ type: '', msg: '' })
  const [passwordUpdateAlert, setPasswordUpdateAlert] = React.useState({ type: '', msg: '' })
  const [usernameErrors, setUsernameErrors] = React.useState([])
  const [isFormSubmitting, setIsFormSubmitting] = React.useState(false)
  const [isPasswordFormSubmitting, setIsPasswordFormSubmitting] = React.useState(false)
  const [submitLoader, setSubmitLoader] = React.useState(false)
  const [isUsernameCheck, setIsUsernameCheck] = React.useState(true)
  const [diactivateModalOpen, setDiactivateModalOpen] = React.useState(false)
  const [passwordModalOpen, setPasswordModalOpen] = React.useState(false)
  const [avatarPreview, setAvtarPreview] = React.useState('')
  const [coverImagePreview, setCoverImagePreview] = React.useState('')

  React.useEffect(() => {
    maxDateOfBirth = new Date(
      moment()
        .subtract(18, 'years')
        .format('YYYY-MM-DD')
    )
    if (user) {
      setAvtarPreview(user.avatar || '')
      setCoverImagePreview(user.cover_image || '')
    }
  }, [])

  const toggleDiactivateModal = value => setDiactivateModalOpen(value)
  const togglePasswordModal = value => setPasswordModalOpen(value)

  const onChangeAvatar = async e => {
    const files = e.target.files
    if (files) {
      try {
        const uploadedFiles = await upload([files[0]], user.username, 'profile_photos')
        const data = {
          avatar: uploadedFiles[0].location
        }
        const result = await changeProfileMedia_api(data)
        setAvtarPreview(result.user.avatar || '')
      } catch (error) {
        console.log(error)
      }
    }
  }
  const onChangeCover = async e => {
    const files = e.target.files
    if (files) {
      try {
        const uploadedFiles = await upload([files[0]], user.username, 'cover_photos')
        const data = {
          cover_image: uploadedFiles[0].location
        }
        const result = await changeProfileMedia_api(data)
        setCoverImagePreview(result.user.cover_image || '')
      } catch (error) {
        console.log(error)
      }
    }
  }

  const onRemoveMediaConfirm = async type => {
    try {
      setSubmitLoader(true)
      const result = await changeProfileMedia_api({ [type]: '' })
      setAvtarPreview(result.user.avatar)
      setCoverImagePreview(result.user.cover_image)
    } catch (error) {
      console.log(error)
    }
    setSubmitLoader(false)
  }

  const checkUserName = async (e, setFieldValue, setFieldError) => {
    try {
      setUsernameErrors([])
      setIsUsernameCheck(false)
      let result = await props.checkUsernameExist({
        username: e.target.value
      })
      setUsernameErrors([{ type: 'success', msg: result.message }])
      setIsUsernameCheck(true)
      setFieldValue('username', e.target.value)
      setTimeout(() => {
        setUsernameErrors([])
      }, 3000)
    } catch (error) {
      console.log(error)
      setFieldError('username', error.errors[0].msg)
      setUsernameErrors([{ type: 'error', msg: error.errors[0].msg }])
      setIsUsernameCheck(false)
    }
  }

  return (
    <>
      <div className='w-100'>
        <div className='profile-wrp my-md-4'>
          <h3 className='desktop-show'>Edit Profile</h3>
          <div
            className='profile-banner comman-bg d-flex justify-content-between position-relative'
            style={{ backgroundImage: `url(${coverImagePreview ? coverImagePreview : images.profile.profileBG})` }}>
            <div className='go-back go-back-edit-profile d-md-none'>
              <Link href='/settings' to='/settings'>
                <a href='/settings' className='back-arrow-round mr-3'>
                  <img src={images.profile.profileBack} />
                </a>
              </Link>
            </div>
            <div className='user-profile-avtar-wrp'>
              <div className='user-avtar avtar-64 cursor-pointer'>
                {avatarPreview && <img src={avatarPreview} />}
                <div>
                  <div
                    className='avtar-gradiant'
                    style={{ backgroundImage: `url(${avatarPreview ? images.app.cross : images.profile.profileImages})`, backgroundPosition: 'center', backgroundRepeat: 'no-repeat' }}
                    onClick={() => {
                      avatarPreview && onRemoveMediaConfirm('avatar')
                    }}>
                    {!avatarPreview && (
                      <>
                        <input type='file' style={{ display: 'none' }} id='new-avatar' name='avatar' className='avatar-file' accept='image/*' onChange={onChangeAvatar} />
                        <label style={{ width: '100%', height: '100%' }} htmlFor='new-avatar'></label>
                      </>
                    )}
                  </div>
                </div>
              </div>
            </div>
            <div></div>
            <div className='change-header-btn'>
              {coverImagePreview ? (
                <label
                  style={{ color: 'white' }}
                  onClick={() => {
                    coverImagePreview && onRemoveMediaConfirm('cover_image')
                  }}>
                  Remove Header
                </label>
              ) : (
                <>
                  <input type='file' style={{ display: 'none' }} id='new-cover_image' name='avatar' className='avatar-file' accept='image/*' onChange={onChangeCover} />
                  <label htmlFor='new-cover_image' style={{ color: 'white' }}>
                    Change Header
                  </label>
                </>
              )}
            </div>
          </div>
          <div className='d-flex'>
            <div className='col-md-10 col-lg-10 col-xl-6 pb-5'>
              <Form
                initialValues={{
                  first_name: user.first_name || '',
                  last_name: user.last_name || '',
                  gender: user.gender || '',
                  dob: new Date(user.dob) || '',
                  username: user.username || '',
                  email: user.email || '',
                  interest: user.interest ? user.interest : [],
                  bio: user.bio || '',
                  website: user.website || '',
                  wishlist: user.wishlist || '',
                  address: user.address || ''
                }}
                validationSchema={Yup.object().shape({
                  first_name: Yup.string().required('First Name is required'),
                  last_name: Yup.string().required('Last Name is required'),
                  gender: Yup.string().required('Gender is required'),
                  dob: Yup.date().test('dob', 'You must be 18 and above', value => {
                    return moment().diff(moment(value), 'years') > 18
                  }),
                  bio: Yup.string(),
                  interest: Yup.array(),
                  username: Yup.string()
                    .required('Username is required')
                    .min(4, 'Username must have 4 characters')
                    .max(16, 'Username should not exceed 16 characters')
                    .matches(/^[a-z0-9._-]{4,16}$/gim, 'Should contain alphabets, digits, ., _. -'),
                  website: Yup.string().url(),
                  wishlist: Yup.string(),
                  address: Yup.string()
                })}
                onSubmit={async values => {
                  try {
                    setIsFormSubmitting(true)
                    setUpdateAlert({ type: '', msg: '' })
                    const data = {
                      username: values.username,
                      email: values.email,
                      first_name: values.first_name,
                      last_name: values.last_name,
                      gender: values.gender,
                      dob: moment(values.dob).format('YYYY-MM-DD') || new Date(),
                      interest: values.interest,
                      bio: values.bio,
                      website: values.website,
                      wishlist: values.wishlist,
                      address: values.address
                    }
                    await props.updateProfile(data)
                    //   setUpdateAlert({ type: 'success', msg: 'Profile Updated!' })
                    setIsFormSubmitting(false)
                    setTimeout(() => {
                      setUpdateAlert({ type: '', msg: '' })
                    }, 3000)
                  } catch (error) {
                    console.log(error)
                    setIsFormSubmitting(false)
                    setUpdateAlert({ type: 'danger', msg: 'Something went wrong. Please try again later!' })
                  }
                }}>
                {prop => (
                  <form
                    className='mt-5 pt-3 pl-4 edit-profile-wrp'
                    onSubmit={e => {
                      e.preventDefault()
                      prop.submitForm()
                      return false
                    }}
                    noValidate>
                    <Form.Field.Input disabled name='email' label='Email' placeholder='John@yahoo.com' />
                    <Form.Field.Input name='first_name' label='First Name' placeholder='John' />
                    <Form.Field.Input name='last_name' label='Last Name' placeholder='Becodi' />
                    <div className='input-box'>
                      <label>Username</label>
                      <Field name='username'>
                        {({ field }) => (
                          <input
                            {...field}
                            type='text'
                            placeholder='Username'
                            className='form-control'
                            onChange={e => {
                              prop.handleChange(e)
                              checkUserName(e, prop.setFieldValue, prop.setFieldError)
                            }}
                          />
                        )}
                      </Field>
                      {usernameErrors.length > 0 &&
                        usernameErrors.map((err, i) => (
                          <p key={`error-${err.type}-${i}`} className={`${err.type === 'success' ? 'text-success' : 'text-danger'}`}>
                            {err.msg}
                          </p>
                        ))}
                    </div>
                    <Form.Field.Select name='gender' label='Gender' options={GenderList} />
                    <Form.Field.Select className='multi-select' name='interest' isMulti={true} label='Interests' options={InterestList} />
                    <Form.Field.Textarea name='bio' label='Bio' placeholder='Write a short description about yourself' />
                    <Form.Field.DatePicker name='dob' maxDate={maxDateOfBirth} label='Date Of Birth' />
                    <Form.Field.Input name='website' label='Website' placeholder='https://' />
                    <Form.Field.Input name='address' label='Location' placeholder='location' />
                    {updateAlert.type && (
                      <Alert variant={updateAlert.type} className='text-center rounded-0 mb-0'>
                        {updateAlert.msg}
                      </Alert>
                    )}
                    <Button type='submit' disabled={!prop.isValid || isFormSubmitting || !isUsernameCheck} variant='primary' className='primary-bg-color custom-button mt-3'>
                      Save {isFormSubmitting && <Spinner color='#2A9DF4' size={32} />}
                    </Button>
                    <a
                      className='error-color py-2 cursor-pointer'
                      onClick={e => {
                        e.preventDefault()
                        setDiactivateModalOpen(true)
                      }}>
                      Deactivate this account
                    </a>
                  </form>
                )}
              </Form>
              <p className='py-3'></p>
            </div>
          </div>
        </div>
      </div>
      <Modal keyboard={true} size='lg' show={diactivateModalOpen} onClose={() => toggleDiactivateModal(false)}>
        <div className='account-popups'>
          <div className='modal-wrp text-center'>
            <div className='modal-inner-wrap text-center'>
              <button
                className='popupclose'
                onClick={e => {
                  e.preventDefault()
                  toggleDiactivateModal(false)
                }}>
                <img src={images.app.popupClose} />
              </button>
              <h4>Are you sure you want to deactivate?</h4>
              <div className='deactivate-input-wrap mt-3'>
                <form>
                  <div className='input-box justify-content-center row row-cols-1 row-cols-lg-3'>
                    <button
                      className='custom-button error-bg deactivate-button mr-lg-3'
                      onClick={e => {
                        e.preventDefault()
                        togglePasswordModal(true)
                        toggleDiactivateModal(false)
                      }}>
                      Yes , Deactivate
                    </button>
                    <button
                      className='custom-button error-color cancel-button'
                      onClick={e => {
                        e.preventDefault()
                        toggleDiactivateModal(false)
                      }}>
                      Cancel
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </Modal>
      <Modal keyboard={true} size='lg' show={passwordModalOpen} onClose={() => togglePasswordModal(false)}>
        <div className='account-popups'>
          <div className='modal-wrp text-center'>
            <div className='modal-inner-wrap'>
              <button
                className='popupclose'
                onClick={e => {
                  e.preventDefault()
                  togglePasswordModal(false)
                  toggleDiactivateModal(true)
                }}>
                <img src={images.app.popupClose} />
              </button>
              <h4>Enter password to continue</h4>
              <div className='account-input-wrap mt-3'>
                <Form
                  initialValues={{
                    password: ''
                  }}
                  validationSchema={Yup.object().shape({
                    password: Yup.string().required('Password is required!')
                  })}
                  onSubmit={async values => {
                    try {
                      setIsPasswordFormSubmitting(true)
                      await props.verifyPassword(values)
                      const result = await deleteAccount_api()
                      setPasswordUpdateAlert({ msg: result.message, type: 'success' })
                      router.push('/login')
                    } catch (error) {
                      if (error.errors) {
                        setPasswordUpdateAlert({ msg: error.errors[0].msg, type: 'danger' })
                      }
                    }
                    setTimeout(() => {
                      setPasswordUpdateAlert({ msg: '', type: '' })
                    }, 3000)
                    setIsPasswordFormSubmitting(false)
                  }}>
                  {props => (
                    <form
                      className='edit-profile-wrp'
                      onSubmit={e => {
                        e.preventDefault()
                        props.submitForm()
                        return false
                      }}
                      noValidate>
                      <Form.Field.Input type='password' name='password' placeholder='**********' />
                      {passwordUpdateAlert.type && (
                        <Alert variant={passwordUpdateAlert.type} className='text-center rounded-0 mb-2'>
                          {passwordUpdateAlert.msg}
                        </Alert>
                      )}
                      <Button variant='primary' disabled={isPasswordFormSubmitting} className='w-100 primary-bg-color custom-button'>
                        Submit {isPasswordFormSubmitting && <Spinner color='#2A9DF4' size={36} />}
                      </Button>
                    </form>
                  )}
                </Form>
              </div>
            </div>
          </div>
        </div>
      </Modal>
    </>
  )
}

const mapStateToProps = state => ({
  auth: state.auth,
  app: state.app,
  user: state.auth.user
})

const mapDispatchToProps = dispatch => {
  return {
    updateProfile: data => {
      return new Promise((resolve, reject) => {
        dispatch({ type: profileTypes.UPDATE_PROFILE, data, resolve, reject })
      })
    },
    checkUsernameExist: data => {
      return new Promise((resolve, reject) => {
        dispatch(checkUsernameExistAction(data, resolve, reject))
      })
    },
    verifyPassword: data => {
      return new Promise((resolve, reject) => {
        dispatch(verifyPasswordAction(data, resolve, reject))
      })
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditProfile)
