import React, { useState } from 'react'
import images from 'src/config/images.js'
import * as Yup from 'yup'
import Link from 'next/link'

import { Form, Button } from 'src/components/shared/index.js'
import moment from 'moment-timezone'
import { getFirebase } from 'src/utils/helpers'
import * as storageUtils from 'src/utils/storageUtils'
import { useRouter } from 'next/router'
import { OptionsNoResults } from 'src/components/shared/Form/Select/styles'

const firebase = getFirebase()

export default function LoginForm(props) {
  const router = useRouter()
  const [errors, setErrors] = useState([])
  const [socialErrors, setSocialErrors] = useState([])
  const [loading, setLoading] = useState(false)
  const [showPass, setShowPass] = useState(false)
  const [checkSocialLogin, setCheckSocialLogin] = useState(false)

  const error = errors && errors.length > 0 && errors[0].msg !== '' ? errors[0].msg : false

  const changePassType = () => {
    setShowPass(!showPass)
  }

  const doSignIn = provider => {
    return new Promise((resolve, reject) => {
      const socialProviderData = {}
      try {
        firebase.firebase
          .auth()
          .signInWithPopup(firebase[provider])
          .then(result => {
            var user = result.user
            socialProviderData.provider = provider
            socialProviderData.provider_id = user.uid
            socialProviderData.email_mobile = user.email || user.phoneNumber || `${user.uid}@${provider}.com`
            socialProviderData.email = user.email || `${user.uid}@${provider}.com`
            socialProviderData.first_name = user.displayName.split(' ')[0]
            socialProviderData.last_name = user.displayName.split(' ')[1]
            socialProviderData.avatar = user.photoURL

            // Set social login data.
            props.setSocialData(socialProviderData)
            resolve(socialProviderData)
          })
          .catch(async error => {
            console.log(error)
            if (error.code === 'auth/account-exists-with-different-credential') {
              try {
                const fetchSignInMethods = await firebase.firebase.auth().fetchSignInMethodsForEmail(error.email)
                reject(`Your account is already linked with ${provider}. You can Sign In with ${fetchSignInMethods[0]}`)
              } catch (error) {
                reject(error.message)
              }
            } else {
              reject(error.message)
            }
          })
      } catch (error) {
        console.log(error)
      }
    })
  }

  const openSocialLoginPopup = async (e, provider) => {
    e.preventDefault()
    setSocialErrors('')
    try {
      // Handle login popup
      const result = await doSignIn(provider)
      const socialData = {
        provider,
        email_mobile: result.email || result.mobile,
        timezone: moment.tz.guess()
      }
      const query = router.query || undefined
      // Check if social account exist in database.
      try {
        setCheckSocialLogin(provider)
        const socialResult = await props.socialLogin(socialData)
        // Set token and login to app.
        router.push({ pathname: '/', query })
        setCheckSocialLogin(false)
      } catch (error) {
        console.log(error)
        /* Redirect to signup, as user does not exist.*/
        setCheckSocialLogin(false)
        router.push({ pathname: '/register', query })
      }
      setCheckSocialLogin(false)
    } catch (error) {
      setSocialErrors(error)
    }
  }

  return (
    <>
      <Form
        initialValues={{
          email: '',
          password: ''
        }}
        validationSchema={Yup.object().shape({
          email: Yup.string()
            .trim('This field cannot include trailing spaces')
            .strict(true)
            .required('Email or Username is required!'),
          password: Yup.string().required('Password is required!')
        })}
        onSubmit={async (values, form) => {
          form.prevent
          setLoading(true)
          setErrors([])
          let data = {
            typeValue: values.email,
            password: values.password,
            timezone: moment.tz.guess()
          }
          if (values.email.includes('@')) {
            data['type'] = 'email'
          } else {
            data['type'] = 'username'
          }
          await props
            .loginUser(data)
            .then(res => {
              props.restoreAuthentication()
              console.log(res)
            })
            .catch(err => {
              setErrors(err)
              console.log(err)
            })
          setLoading(false)
        }}>
        {props => {
          return (
            <form
              onSubmit={e => {
                e.preventDefault()
                props.submitForm()
                return false
              }}
              noValidate>
              {error && (
                <div className='alert alert-danger' role='alert'>
                  {error}
                </div>
              )}
              <Form.Field.Input floating name='email' label='Email or Username' />
              <div className='position-relative'>
                <Form.Field.Input type={showPass ? 'text' : 'password'} floating name='password' label='Password' />
                {showPass ? (
                  <img src={images.settings.Show} onClick={changePassType} className='position-absolute' style={{ right: '10px', top: '33px' }} />
                ) : (
                  <img src={images.settings.Hide} onClick={changePassType} className='position-absolute' style={{ right: '10px', top: '33px' }} />
                )}
              </div>
              <Link href='/login/reset-password'>
                <h5 className='forgot-password primary-color' style={{ cursor: 'pointer' }}>
                  Forgot password
                </h5>
              </Link>
              <Button variant='primary' type='submit' isWorking={loading} disabled={loading} block>
                Sign in
              </Button>
            </form>
          )
        }}
      </Form>
      <Button
        variant='transparent'
        isWorking={checkSocialLogin === 'twitter'}
        className='mt-5 mb-5'
        onClick={e => openSocialLoginPopup(e, 'twitter')}
        faIcon='fa-twitter'
        block
        isWorking={false}>
        Sign in with twitter
      </Button>
    </>
  )
}
