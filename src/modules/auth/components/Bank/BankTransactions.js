import React from 'react'
import moment from 'moment'
import images from 'src/config/images'
import { constants, numberWithCommas } from 'src/utils/helpers'
import { Avatar, Button, Spinner } from 'src/components/shared/index.js'
import { getTransactionList_api } from '../../api/authApi'
import { PriceBifurcation } from './PriceBifurcation'

function BankTransactions(props) {
  const [pages, setPages] = React.useState({ page: 1, perPage: 10 })
  const [list, setList] = React.useState([])
  const [isLoading, setIsLoading] = React.useState(false)

  React.useEffect(() => {
    getList()
  }, [])

  const getList = async () => {
    try {
      if (pages.page !== 'lastpage') {
        setIsLoading(true)
        let result = await getTransactionList_api(pages)
        if (result?.transactions.length > 0) {
          setList(lastList => [...lastList, ...result.transactions])
          setPages(lastData => {
            return { ...lastData, page: result.transactions.length < lastData.perPage ? 'lastpage' : lastData.page + 1 }
          })
          setIsLoading(false)
        } else {
          setPages({ ...pages, page: 'lastpage' })
          setIsLoading('noData')
        }
      }
    } catch (error) {
      console.log(error)
    }
  }

  const expandTransaction = t => {
    let temp = [...list]
    const i = temp.map(tr => tr.id).indexOf(t.id)
    if (i !== -1) {
      const tr = temp[i]
      tr.expanded = tr.expanded ? !tr.expanded : true
      temp[i] = tr
      setList(temp)
    }
  }

  return (
    <div className='transactions-tab-box'>
      <div className='transactions-tab-content mt-5 current'>
        {isLoading === 'noData' && list.length === 0 && (
          <div className='text-center pt-5 text-muted'>
            <i className='fas fa-images fa-3x mb-1'></i>
            <h3 className='text-muted'> No transactions found. </h3>
          </div>
        )}
        {list.map(transaction => {
          const isReceived = transaction.receiver_id === props.user.id
          const transactionData = transaction.transactionData === 'ByAdmin' ? { id: '-', account_number: '-' } : JSON.parse(transaction.transactionData)
          const commissionAndCharges = JSON.parse(transaction.commissionAndCharges)
          return (
            <React.Fragment key={`transaction-${transaction.uuid}`}>
              {/* Received amount */}
              {isReceived && transaction.trans_type !== 'withdraw' && transaction.trans_type !== 'withdraw-reversed' && (
                <>
                  <div
                    className='interests-box d-flex align-items-center cursor-pointer'
                    onClick={e => {
                      e.preventDefault()
                      e.stopPropagation()
                      isReceived && transaction.trans_type !== 'refund' && transaction.trans_type !== 'withdraw' && transaction.trans_type !== 'withdraw-reversed'
                        ? expandTransaction(transaction)
                        : null
                    }}>
                    <div className='interests-box-img'>
                      <img src={images.app.arrow_Down} />
                    </div>
                    <div className='interests-box-info'>
                      {transaction.trans_type === 'topup' && transaction.userId && <p> Add money </p>}
                      {transaction.trans_type === 'tips' && transaction.userId && (
                        <p>
                          Tip for post from <span className='primary-color'>@{transaction.userId.username}</span>
                        </p>
                      )}
                      {transaction.trans_type === 'user_tip' && transaction.userId && (
                        <p>
                          Tip from <span className='primary-color'>@{transaction.userId.username}</span>
                        </p>
                      )}
                      {transaction.trans_type === 'paid_post' && transaction.userId && (
                        <p>
                          Paid Post from <span className='primary-color'>@{transaction.userId.username}</span>
                        </p>
                      )}
                      {transaction.trans_type === 'paid_subscription' && transaction.userId && (
                        <p>
                          Subscription from <span className='primary-color'>@{transaction.userId.username}</span>
                        </p>
                      )}
                      {transaction.trans_type === 'refund' && transaction.userId && (
                        <p>
                          Refund from <span className='primary-color'>@{transaction.userId.username}</span>
                        </p>
                      )}
                      <h5>
                        {moment
                          .utc(transaction.createdAt)
                          .local()
                          .format('DD MMM, YY HH:mm')}
                      </h5>
                    </div>
                    {(transaction.trans_type === 'tips' || transaction.trans_type === 'user_tip' || transaction.trans_type === 'paid_subscription' || transaction.trans_type === 'paid_post') && (
                      <div className='user-avtar avtar-32'>
                        <Avatar name={transaction.userId?.first_name} avatarUrl={transaction.userId?.avatar} size={33} />
                        {/* <img src={transaction.userId.avatar} alt='User_Avtar' /> */}
                      </div>
                    )}
                    <div className='interests-box-price '>
                      <h5 className='green-color'>
                        +{`NGN ${numberWithCommas(transaction.price)}`}
                        {'  '}
                        {transaction.trans_type !== 'refund' && <img src={images.app.arrowDown}></img>}
                      </h5>
                    </div>
                  </div>
                  {transaction.expanded && (
                    <PriceBifurcation
                      amount={transaction.total}
                      commissionData={{
                        flutterwave_fees: commissionAndCharges.flutterwave_fees,
                        paystack_fees: commissionAndCharges.paystack_fees,
                        commmission: commissionAndCharges.commmission
                      }}
                    />
                  )}
                </>
              )}
              {/* Paid amount */}
              {!isReceived && transaction.trans_type !== 'withdraw' && (
                <div className='interests-box d-flex align-items-center'>
                  <div className='interests-box-img'>
                    <img src={images.app.arrowUp} />
                  </div>
                  <div className='interests-box-info'>
                    {transaction.trans_type === 'paid_post' && transaction.receiverId && (
                      <p>
                        Paid Post to <span className='primary-color'> @{transaction.receiverId?.username}</span>
                      </p>
                    )}
                    {transaction.trans_type === 'tips' && transaction.receiverId && (
                      <p>
                        Sent Tip for post to <span className='primary-color'>@{transaction.receiverId?.username}</span>
                      </p>
                    )}
                    {transaction.trans_type === 'user_tip' && transaction.receiverId && (
                      <p>
                        Sent Tip to <span className='primary-color'>@{transaction.receiverId?.username}</span>
                      </p>
                    )}
                    {transaction.trans_type === 'paid_subscription' && transaction.receiverId && (
                      <p>
                        Subscribed to <span className='primary-color'>@{transaction.receiverId?.username} </span>
                      </p>
                    )}
                    {transaction.trans_type === 'refund' && transaction.receiverId && (
                      <p>
                        Refunded to <span className='primary-color'>@{transaction.receiverId?.username}</span>
                      </p>
                    )}
                    <h5>
                      {moment
                        .utc(transaction.createdAt)
                        .local()
                        .format('DD MMM, YY HH:mm')}
                    </h5>
                  </div>
                  {(transaction.trans_type === 'tips' || transaction.trans_type === 'user_tip' || transaction.trans_type === 'paid_subscription' || transaction.trans_type === 'paid_post') && (
                    <div className='user-avtar avtar-32'>
                      <Avatar name={transaction.receiverId?.first_name} avatarUrl={transaction.receiverId?.avatar} size={33} />
                      {/* <img src={transaction.receiverId.avatar} alt='User_Avtar' /> */}
                    </div>
                  )}
                  <div className='interests-box-price'>
                    <h5 className='red-color'>
                      -{`NGN ${transaction.trans_type !== 'refund' ? numberWithCommas(transaction.total) : transaction.trans_type === 'refund' ? numberWithCommas(transaction.price) : null}`}
                    </h5>
                  </div>
                </div>
              )}
              {/* withdraw amount */}
              {(transaction.trans_type === 'withdraw' || transaction.trans_type === 'withdraw-reversed') && (
                <div className='interests-box d-flex align-items-center'>
                  <div className='interests-box-img'>{transaction.trans_type === 'withdraw' ? <img src={images.app.arrowUp} /> : <img src={images.app.arrow_Down} />}</div>
                  <div className='interests-box-info'>
                    {transaction.trans_type === 'withdraw' ? (
                      <p>
                        Withdrawn to {transactionData.account_number} - {transaction.transfer_status}
                      </p>
                    ) : (
                      <p>Withdrawal Reversal</p>
                    )}
                    <h5>
                      {moment
                        .utc(transaction.createdAt)
                        .local()
                        .format('DD MMM, YY HH:mm')}
                    </h5>
                  </div>
                  <div className='interests-box-price'>
                    {transaction.trans_type === 'withdraw' ? (
                      <h5 className='red-color'>-{`NGN ${numberWithCommas(transaction.total)}`}</h5>
                    ) : (
                      <h5 className='green-color'>+{`NGN ${numberWithCommas(transaction.total)}`}</h5>
                    )}
                  </div>
                </div>
              )}
            </React.Fragment>
          )
        })}
        {pages.page !== 'lastpage' && (
          <Button className='mt-5' disabled={isLoading} style={{ margin: 'auto' }} onClick={getList}>
            Load More {isLoading && <Spinner color='#2A9DF4' size={30} />}
          </Button>
        )}
      </div>
    </div>
  )
}

export default BankTransactions
