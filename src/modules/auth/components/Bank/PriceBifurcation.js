import React from 'react'
import { constants, numberWithCommas } from 'utils/helpers'

export const PriceBifurcation = props => {
  const { amount, commissionData } = props

  const getFees = amount => {
    // if (amount < 2500) {
    //   newAmount = (amount * commissionData.paystack_fees) / 100
    // } else {
    //   newAmount = (amount * commissionData.paystack_fees) / 100 + 100
    // }
    let newAmount = 0
    if (process.env.PAYMENT_METHOD === 'flutterwave') {
      newAmount = (amount * commissionData.flutterwave_fees) / 100 // for flutterwave payment
    } else {
      newAmount = (amount * commissionData.paystack_fees) / 100 // for paystack payment
    }
    return newAmount.toFixed(2)
  }
  const getCommssion = amount => {
    return amount && commissionData.commmission > 0 ? ((amount * commissionData.commmission) / 100).toFixed(2) : 0
  }
  return (
    <div className={props.className}>
      <div className='text-right mb-1'>
        <div className='d-inline-block text-muted px-0'>Price:</div>
        <div className='d-inline-block ml-3' style={{ minWidth: 75, maxWidth: 150, flexGrow: 'unset' }}>
          <strong>
            {constants.NAIRA}&nbsp;{amount ? numberWithCommas(amount) : 0}
          </strong>
        </div>
      </div>
      {commissionData.commmission > 0 && (
        <div className='text-right mb-1'>
          <div className='d-inline-block text-muted px-0'>Rebel Commission ({commissionData.commmission}%):</div>
          <div className='d-inline-block ml-3' style={{ minWidth: 75, maxWidth: 150, flexGrow: 'unset' }}>
            <strong>
              {constants.NAIRA}&nbsp;{numberWithCommas(getCommssion(amount))}
            </strong>
          </div>
        </div>
      )}
      <div className='text-right mb-1'>
        <div className='d-inline-block text-muted px-0'>Transaction Charges ({process.env.PAYMENT_METHOD === 'flutterwave' ? commissionData.flutterwave_fees : commissionData.paystack_fees}%):</div>
        <div className='d-inline-block ml-3' style={{ minWidth: 75, maxWidth: 150, flexGrow: 'unset' }}>
          <strong>
            {constants.NAIRA}&nbsp; {numberWithCommas(getFees(amount))}
          </strong>
        </div>
      </div>
      <div className='text-right mb-1'>
        <div className='d-inline-block text-muted px-0'>You&apos;ll get:</div>
        <div className='d-inline-block ml-3' style={{ minWidth: 75, maxWidth: 150, flexGrow: 'unset' }}>
          <strong>
            {constants.NAIRA}&nbsp;{numberWithCommas((amount - getCommssion(amount) - getFees(amount)).toFixed(2))}
          </strong>
        </div>
      </div>
    </div>
  )
}
