import moment from 'moment'
import React from 'react'
import images from 'src/config/images'
import { getTransactionMonths_api } from '../../api/authApi'
import { Spinner } from 'src/components/shared'

const BankAccountStatement = () => {
  const [isLoading, setIsLoading] = React.useState(false)
  const [transactionMonths, setTransactionMonth] = React.useState([])
  const [activeTransaction, setActiveTransaction] = React.useState({})

  React.useEffect(() => {
    getTransactionsMonth()
  }, [])

  const getTransactionsMonth = async () => {
    try {
      setIsLoading(true)
      const result = await getTransactionMonths_api()
      if (result.transactionresponse.length > 0) {
        let allData = result.transactionresponse.map(transaction => {
          return {
            label: `${moment()
              .month(transaction.month - 1)
              .format('MMMM')} ${transaction.year}`,
            ...transaction
          }
        })
        setTransactionMonth(allData)
        setActiveTransaction(allData[0])
        setIsLoading(false)
      } else if (result.errors) {
        setIsLoading(false)
      }
    } catch (error) {
      setActiveTransaction('noData')
      console.log(error)
    } finally {
      setIsLoading(false)
    }
  }

  //setActiveMonth
  const setActiveMonth = label => {
    let currentT = transactionMonths.filter(t => label === t.label)
    setActiveTransaction(currentT ? currentT[0] : {})
  }

  if (isLoading) {
    return (
      <div className='d-flex justify-content-center align-items-center mt-4'>
        <Spinner color='#2A9DF4' size={40} />
      </div>
    )
  }

  return (
    <>
      {activeTransaction === 'noData' ? (
        <div className='text-center pt-5 text-muted'>
          <i className='fas fa-images fa-3x mb-1'></i>
          <h3 className='text-muted'> No statements found. </h3>
        </div>
      ) : (
        <div className='transactions-tab-box'>
          <div className='transactions-tab-content current mt-5'>
            <div className='row align-items-center'>
              <div className='col-md-6'>
                <div className='custom-select-month input-box select-arrow-custom'>
                  <select name='months' id='months' onChange={e => setActiveMonth(e.target.value)}>
                    {transactionMonths.map((t, i) => (
                      <option value={t.label} keu={`months-${t.label}-${i}`}>
                        {t.label}
                      </option>
                    ))}
                  </select>
                </div>
              </div>
              <div className='col-md-6'>
                <div className='pdf-down-box ml-auto'>
                  <a href='#' className='pdf-down-box-button'>
                    <span>
                      <img src={images.app.paperNegative} />
                    </span>{' '}
                    Download pdf
                  </a>
                </div>
              </div>
            </div>
            <div className='wallet-box-info mt-5'>
              <h6>TOTAL EARNINGS</h6>
              <h2 className='primary-color'>
                <sup>
                  <img src={images.app.nigeriaCurrencyIconBlue} />
                </sup>{' '}
                {activeTransaction ? activeTransaction.total : 0}
              </h2>
            </div>
            <div className='row'>
              <div className='col-md-4'>
                <div className='earn-box mt-5'>
                  <h6>
                    <span>
                      <img src={images.app.earn01} />
                    </span>{' '}
                    Subscription earnings
                  </h6>
                  <h2>
                    <sup>
                      <img src={images.app.nigeriaCurrencyIconPurpal} />
                    </sup>
                    {activeTransaction ? activeTransaction.subscriptions : 0}
                  </h2>
                </div>
              </div>
              <div className='col-md-4'>
                <div className='earn-box mt-5'>
                  <h6>
                    <span>
                      <img src={images.app.earn02} />
                    </span>{' '}
                    Post earnings
                  </h6>
                  <h2>
                    <sup>
                      <img src={images.app.nigeriaCurrencyIconPurpal} />
                    </sup>
                    {activeTransaction ? activeTransaction.posts : 0}
                  </h2>
                </div>
              </div>
              <div className='col-md-4'>
                <div className='earn-box mt-5'>
                  <h6>
                    <span>
                      <img src={images.app.earn03} />
                    </span>{' '}
                    Total tips earned
                  </h6>
                  <h2>
                    <sup>
                      <img src={images.app.nigeriaCurrencyIconPurpal} />
                    </sup>
                    {activeTransaction ? activeTransaction.tips : 0}
                  </h2>
                </div>
              </div>
            </div>
            <div className='earn-total-list mt-5'>
              <h4>TOTAL</h4>
              <h4>GROSS: ₦{activeTransaction ? activeTransaction.gross : 0}</h4>
              <h4>NET: ₦{activeTransaction ? activeTransaction.nettotal : 0}</h4>
            </div>
          </div>
        </div>
      )}
    </>
  )
}

export default BankAccountStatement
