import Link from 'next/link'
import React from 'react'
import images from 'src/config/images'

const NoBankAddedWallet = props => {
  return (
    <div className='Wallet-contain tab-content current'>
      <form>
        <div className='wallet-accessed-box text-center'>
          <div className='wallet-accessed-box-img'>
            <img src={images.app.bookmarkWallet} />
          </div>
          <div className='wallet-accessed-box-info mt-3'>
            <h3>Wallet can only be accessed by creators</h3>
            <p>Add your bank details to become a creator and access your wallet</p>
          </div>
          <div className='wallet-accessed-box-button mt-5'>
            <Link href={props.allRoutes.BankApplication}>
              <button className='custom-button primary-bg-color m-auto'>Add bank details</button>
            </Link>
          </div>
        </div>
      </form>
    </div>
  )
}

export default NoBankAddedWallet
