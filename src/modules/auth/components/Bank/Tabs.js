import Link from 'next/link'
import { useRouter } from 'next/router'
import React from 'react'

function Tabs(props) {
  const router = useRouter()

  return (
    <div className='card-option'>
      <ul className='tabs'>
        <Link href={props.allRoutes.BankApplication}>
          <li className={[props.allRoutes.BankApplication, props.allRoutes.BankDetail].includes(router.asPath) ? 'current' : ''}>
            Add Bank <span>To Earn 💰</span>
          </li>
        </Link>
        <Link href={props.allRoutes.Transaction}>
          <li className={[props.allRoutes.Transaction, props.allRoutes.Summary].includes(router.asPath) ? 'current' : ''}>Wallet</li>
        </Link>
      </ul>
    </div>
  )
}

export default Tabs
