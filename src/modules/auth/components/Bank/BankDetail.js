import { cloneDeep } from 'lodash'
import { useRouter } from 'next/router'
import React from 'react'
import { Form, Button, Spinner } from 'src/components/shared/index.js'
import * as Yup from 'yup'

const BankDetail = props => {
  const router = useRouter()

  const [errors, setErrors] = React.useState({})
  const [loading, setLoading] = React.useState(false)

  React.useEffect(() => {
    if (props.user.personal_info_status !== 'approved') {
      router.push('/wallet/[page]', props.allRoutes.BankApplication)
    }
  }, [])

  if (!props?.banks[0]) {
    return (
      <div className='d-flex justify-content-center align-items-center mt-4'>
        <Spinner color='#2A9DF4' size={40} />
      </div>
    )
  }

  const banks = props.banks ? [...props.banks] : []

  return (
    <div>
      <div className='add-bank-contain tab-content current pt-4'>
        <Form
          initialValues={{
            account_bank: props?.user_personal_info?.bank ? props.user_personal_info.bank.account_bank : '',
            account_number: props?.user_personal_info?.bank ? props.user_personal_info.bank.account_number : '',
            beneficiary_name: props?.user_personal_info?.bank ? props.user_personal_info.bank.beneficiary_name : ''
          }}
          validationSchema={Yup.object().shape({
            account_bank: Yup.string().required(),
            account_number: Yup.string().required(),
            beneficiary_name: Yup.string().required()
          })}
          onSubmit={async values => {
            try {
              setLoading(true)
              const data = cloneDeep(values)
              data.currency = 'NGN'
              await props.submitBankDetails(data)
              setErrors({ msg: 'Bank details added successfully', type: 'success' })
            } catch (error) {
              if (error.errors) {
                setErrors({ msg: error.errors[0].msg, type: 'danger' })
              }
            }
            setTimeout(() => {
              setErrors({ msg: '', type: '' })
            }, 4000)
            setLoading(false)
          }}>
          {prop => (
            <React.Fragment>
              {errors && errors.msg && (
                <div className={`alert alert-${errors.type}`} role='alert'>
                  {errors.msg}
                </div>
              )}
              <div className='input-with-fix'>
                <div className='bank-information-msg'>
                  <p className=''>Add bank information to enable Wallet feature.</p>
                </div>
                <Form.Field.Select floating name='account_bank' options={banks} label='Your Bank' />
                <Form.Field.Input floating type='number' name='account_number' label='Account Number' />
                <Form.Field.Input floating type='text' name='beneficiary_name' label='Beneficiary Name' />
              </div>
              <Button
                type='submit'
                variant='primary'
                className='primary-bg-color custom-button hours-popups-button mt-3'
                disabled={props?.user_personal_info?.bank?.account_number || loading}
                isWorking={loading}
                onClick={prop.submitForm}>
                Submit
              </Button>
              <div className='bank-information-msg mt-4'>
                <h5 className='red-color'>NB: Details cannot be changed once submitted </h5>
              </div>
            </React.Fragment>
          )}
        </Form>
      </div>
    </div>
  )
}

export default BankDetail
