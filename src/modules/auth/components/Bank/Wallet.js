import Link from 'next/link'
import React, { useState } from 'react'
import images from 'src/config/images'
import BankAccountStatement from './BankAccountStatement'
import BankTransactions from './BankTransactions'
import NoBankAddedWallet from './NoBankAddedWallet'
import { useRouter } from 'next/router'
import WithdrawModal from './WithdrawModal'
import DepositeModal from './DepositeModal'
import { numberWithCommas } from 'src/utils/helpers'

const Wallet = props => {
  const [withdrawModal, setWithdrawModal] = useState(false)
  const [depositeModal, setDepositeModal] = useState(false)
  const router = useRouter()

  return props.user_personal_info.bank ? (
    <div className='wallet-contain tab-content current'>
      <WithdrawModal {...props} setWithdrawModal={setWithdrawModal} withdrawModal={withdrawModal} />
      <DepositeModal {...props} setDepositeModal={setDepositeModal} depositeModal={depositeModal} />
      <div className='d-flex wallet-row pt-5 pb-5'>
        <div className='wallet-box'>
          <div className='wallet-box-inner'>
            <div className='wallet-box-info'>
              <h6>AVAILABLE BALANCE</h6>
              <h2 className='primary-color'>
                <sup className="naira-currency">₦</sup>{' '}
                {numberWithCommas(parseFloat(props?.user?.wallet?.available).toFixed(2))}
              </h2>
            </div>
            <div className='balance-button-group d-flex'>
              <button className='custom-button withdraw-button' disabled={!props?.user_personal_info?.bank} onClick={() => setWithdrawModal(true)}>
                Withdraw
              </button>
              <button className='custom-button deposit-button' disabled={!props?.user_personal_info?.bank} onClick={() => setDepositeModal(true)}>
                Deposit
              </button>
            </div>
          </div>
        </div>
        <div className='wallet-box'>
          <div className='wallet-box-inner'>
            <div className='wallet-box-info'>
              <h6>LEDGER BALANCE</h6>
              <h2 className='primary-color'>
                <sup className="naira-currency">₦</sup>{' '}
                {numberWithCommas(parseFloat(props?.user?.wallet?.delegate).toFixed(2))}
              </h2>
            </div>
            <div className='wallet-box-message'>
              <h5>Funds stay here for 7 days before moving to Available Balance</h5>
            </div>
          </div>
        </div>
      </div>
      <div className='pt-3'>
        <div className='transactions-option-col'>
          <div className='transactions-option'>
            <ul className='transactions-tabs'>
              <Link href={props.allRoutes.Transaction}>
                <li data-tab='tab-21' className={router.asPath === props.allRoutes.Transaction ? 'current' : ''}>
                  Transactions
                </li>
              </Link>
              <Link href={props.allRoutes.Summary}>
                <li data-tab='tab-22' className={router.asPath === props.allRoutes.Summary ? 'current' : ''}>
                  <span>
                    <img src={images.bank.documentSvg} />
                  </span>{' '}
                  Account statement
                </li>
              </Link>
            </ul>
          </div>
          {router.asPath === props.allRoutes.Transaction && <BankTransactions {...props} />}
          {router.asPath === props.allRoutes.Summary && <BankAccountStatement {...props} />}
        </div>
      </div>
    </div>
  ) : (
    <NoBankAddedWallet {...props} />
  )
}

export default Wallet
