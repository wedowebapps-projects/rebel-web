import React from 'react'
import { Alert } from 'react-bootstrap'
import { Modal } from 'src/components/app'
import { Spinner } from 'src/components/shared'
import images from 'src/config/images'
import { isValidPrice } from 'src/utils/helpers'
import { getWithdrawalFees_api, withdraw_api } from '../../api/authApi'

const WithdrawModal = props => {
  const [loading, setLoading] = React.useState(false)
  const [loadingWithdrawalFee, setLoadingWithdrawalFee] = React.useState(false)
  const [withdrawalFee, setWithdrawalFee] = React.useState(null)
  const [withdrawalAmount, setWithdrawalAmount] = React.useState(props?.commissions?.flutterwave_minimum_withdraw)
  const [errors, setErrors] = React.useState([])

  const closeModal = () => props.setWithdrawModal(false)

  React.useEffect(() => {
    getWithdrawalFees()
  }, [])

  const changePrice = e => {
    let debounce = null
    if (e.target.value >= 1 && e.target.value <= props.user.wallet.available) {
      setErrors([])
      clearTimeout(debounce)
      setWithdrawalAmount(e.target.value)
      debounce = setTimeout(() => {
        if (debounce) {
          clearTimeout(debounce)
          debounce = null
          getWithdrawalFees()
        }
      }, 400)
    }
  }

  const getWithdrawalFees = async () => {
    try {
      setLoadingWithdrawalFee(true)
      const result = await getWithdrawalFees_api(withdrawalAmount)
      setWithdrawalFee(result.data[0].fee)
    } catch (error) {
      console.log(error)
    }
    setLoadingWithdrawalFee(false)
  }

  const submitWdRequest = async e => {
    e.preventDefault()
    setErrors([])
    if (withdrawalAmount >= props.commissions.flutterwave_minimum_withdraw) {
      try {
        setLoading(true)
        await withdraw_api({ amount: withdrawalAmount })
        location.reload()
      } catch (error) {
        console.log(error)
        setErrors(error.errors)
      }
      setLoading(false)
    } else {
      setErrors([{ msg: `Minimum amount you can withdraw is ${props.commissions.flutterwave_minimum_withdraw}.` }])
    }
  }
  return (
    <Modal keyboard={true} size='lg' show={props.withdrawModal} onClose={closeModal}>
      <div id='sendTipModal' className='send-tip-popups visible'>
        <div className='modal-wrp text-center'>
          <div className='modal-inner-wrap'>
            <div className='colse-welcome-modal' onClick={closeModal}>
              <img src={images.app.cross} alt='' />
            </div>
            <h2>Withdraw Money</h2>
            <p>
              Enter amount to <strong> withdraw </strong>below
            </p>
            {errors.length > 0 &&
              errors.map((error, i) => (
                <Alert key={i} variant='danger'>
                  {error?.msg}
                </Alert>
              ))}
            <div className='tip-input-wrap'>
              <span>₦</span>
              <input type='number' name='Price' value={withdrawalAmount} onChange={changePrice} />
            </div>
            <a
              title=''
              disabled={!isValidPrice(withdrawalAmount) || withdrawalAmount < props.commissions.flutterwave_minimum_withdraw || loadingWithdrawalFee || loading}
              className='send-tip-btn'
              onClick={submitWdRequest}>
              Withdraw Money {loading && <Spinner color='#2A9DF4' size={30} />}
            </a>
            <p>
              Bank charge of{' '}
              <strong>
                <u>NGN {withdrawalFee}</u>
              </strong>{' '}
              as banking fees applicable
            </p>
          </div>
        </div>
      </div>
    </Modal>
  )
}

export default WithdrawModal
