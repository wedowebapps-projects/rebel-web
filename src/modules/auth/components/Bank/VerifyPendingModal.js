import React from 'react'
import { Modal } from 'src/components/app'
import images from 'src/config/images'

const VerifyPendingModal = props => {
  const closeModal = () => props.setVerifyModal(false)

  return (
    <Modal keyboard={true} size='lg' show={props.verifyModal} onClose={closeModal}>
      <div className='hours-popups'>
        <div className='modal-wrp'>
          <div className='modal-inner-wrap'>
            <div className='send-broadcast-popups-header'>
              <div className='colse-welcome-modal' onClick={closeModal}>
                <img src={images.app.cross} alt='' />
              </div>
              <div className='round-logo text-center'>
                <img src={images.app.logo} />
              </div>
            </div>
            <div className='hours-popups-inner'>
              <p>Your application has been sent and is now under review. We will get back to you in the next 24-48 hours</p>
            </div>
          </div>
        </div>
      </div>
    </Modal>
  )
}

export default VerifyPendingModal
