import React from 'react'
import { FlutterWaveButton } from 'flutterwave-react-v3'
import { PaystackButton } from 'react-paystack'
import { Modal } from 'src/components/app'
import images from 'src/config/images'
import { handleExtraChargeAmount, isValidPrice } from 'src/utils/helpers'
import { v4 as uuidv4 } from 'uuid'
import { addMoneyToWallet_api } from '../../api/authApi'

const DepositeModal = props => {
  const [depositeAmmount, setDepositeAmmount] = React.useState(0)
  const closeModal = () => props.setDepositeModal(false)

  const config = {
    publicKey: process.env.PAYSTACK_publicKey,
    tx_ref: `topup-${props.user.id}-${uuidv4()}`,
    amount: depositeAmmount * 100,
    email: props.user.email,
    onSuccess: reference => handlePaystackSuccessAction(reference),
    onClose: () => {
      setDepositeAmmount(0)
      closeModal()
    }
  }
  const flutterwaveConfig = {
    public_key: process.env.FLUTTERWAVE_SECRET,
    tx_ref: `topup-${props.user.id}-${uuidv4()}`,
    amount: depositeAmmount,
    currency: 'NGN',
    payment_options: 'card',
    customer: {
      email: props.user.email,
      name: `${props.user.first_name} ${props.user.last_name}`
    },
    customizations: {
      title: 'Rebelyus.com',
      description: 'Deposite money to wallet',
      logo: images.app.logo
    },
    text: 'Pay with Flutterwave!',
    callback: async response => {
      try {
        const data = {
          transaction_id: response.transaction_id,
          tx_ref: response.tx_ref,
          amount: depositeAmmount
        }
        await addMoneyToWallet_api(data)
      } catch (error) {
        console.log(error)
      }
    },
    onClose: () => {
      setDepositeAmmount(0)
      closeModal()
    }
  }

  const handlePaystackSuccessAction = async response => {
    try {
      const data = {
        transaction_id: response.trxref,
        tx_ref: response.transaction,
        amount: depositeAmmount
      }
      try {
        await addMoneyToWallet_api(data)
        location.reload()
      } catch (error) {
        console.log(error)
      }
    } catch (error) {
      console.log(error)
    }
  }

  return (
    <Modal keyboard={true} size='lg' show={props.depositeModal} onClose={closeModal}>
      <div id='sendTipModal' className='send-tip-popups visible'>
        <div className='modal-wrp text-center'>
          <div className='modal-inner-wrap'>
            <div className='colse-welcome-modal' onClick={closeModal}>
              <img src={images.app.cross} alt='' />
            </div>
            <h2>TopUp Wallet</h2>
            <p>
              Enter amount to <strong> TopUp </strong>below
            </p>
            <div className='tip-input-wrap'>
              <span>₦</span>
              <input
                type='number'
                name='Price'
                value={depositeAmmount}
                onChange={e => {
                  if (e.target.value >= 1 && e.target.value <= 99999) {
                    setDepositeAmmount(e.target.value)
                  }
                }}
              />
            </div>
            {process.env.PAYMENT_METHOD === 'flutterwave' ? (
              <FlutterWaveButton
                {...flutterwaveConfig}
                text={'TopUp Wallet'}
                className='btn btn-success send-tip-btn rounded px-4'
                disabled={!isValidPrice(depositeAmmount) || depositeAmmount === 0}
              />
            ) : (
              <PaystackButton {...config} text={'TopUp Wallet'} className='btn btn-success send-tip-btn rounded px-4' disabled={!isValidPrice(depositeAmmount) || depositeAmmount === 0} />
            )}
            <p>
              Bank charge of{' '}
              <strong>
                <u>NGN {handleExtraChargeAmount(depositeAmmount)}</u>
              </strong>{' '}
              as banking fees applicable
            </p>
          </div>
        </div>
      </div>
    </Modal>
  )
}

export default DepositeModal
