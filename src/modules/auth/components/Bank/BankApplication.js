import React, { useState } from 'react'
import { Form, Button, Spinner } from 'src/components/shared/index.js'
import * as Yup from 'yup'
import { docTypes } from 'src/config'
import { upload } from 'src/utils/s3'
import images from 'src/config/images'
import { useRouter } from 'next/router'
import VerifyPendingModal from './VerifyPendingModal'

export default function ForgotPasswordForm(props) {
  const router = useRouter()

  const [verifyModal, setVerifyModal] = useState(false)
  const [errors, setErrors] = useState([])
  const [loading, setLoading] = useState(false)

  const [identityPreviewLoading, setIdentityPreviewLoading] = useState(false)
  const [identityPreview, setIdentityPreview] = useState('')

  const [selfiePreviewLoading, setSelfiePreviewLoading] = useState(false)
  const [selfiePreview, setSelfiePreview] = useState('')

  React.useEffect(() => {
    if (props.user.personal_info_status === 'approved' && props.user_personal_info) {
      router.push('/wallet/[page]', props.allRoutes.BankDetail)
    } else {
      setIdentityPreview(props.user_personal_info.identity)
      setSelfiePreview(props.user_personal_info.identityWithSelfie)
    }
    if (props.user.personal_info_status !== 'approved' && props.user_personal_info) {
      setVerifyModal(true)
    }
  }, [])

  const countries = []
  props.countries.map(country => {
    countries.push({
      label: country.country_name,
      value: country.country_name
    })
  })

  const onUpload = async (e, uploadType) => {
    const files = e.target.files
    if (files) {
      try {
        if (uploadType === 'identities') {
          setIdentityPreviewLoading(true)
          const uploadedFiles = await upload([files[0]], props.user.username, 'bank_application/identities')
          setIdentityPreview(uploadedFiles[0].location)
        }
        if (uploadType === 'selfies') {
          setSelfiePreviewLoading(true)
          const uploadedFiles = await upload([files[0]], props.user.username, 'bank_application/selfies')
          setSelfiePreview(uploadedFiles[0].location)
        }
      } catch (error) {
        console.log(error)
      } finally {
        setSelfiePreviewLoading(false)
        setIdentityPreviewLoading(false)
      }
    }
  }

  return (
    <div>
      {verifyModal && <VerifyPendingModal {...props} verifyModal={verifyModal} setVerifyModal={setVerifyModal} />}
      {props.user.personal_info_status !== 'approved' && props.user_personal_info ? <button className='approval-button'>Approval pending</button> : null}
      <div className='bank-information-msg'>
        <p className>Add bank information to enable Wallet feature.</p>
      </div>
      <Form
        initialValues={{
          first_name: props.user_personal_info.first_name || '',
          last_name: props.user_personal_info.last_name || '',
          dob: props.user_personal_info.dob || new Date(),
          country: props.user_personal_info.country || '',
          city: props.user_personal_info.city || '',
          docType: props.user_personal_info.docType || '',
          identity: '',
          identityWithSelfie: ''
        }}
        validationSchema={Yup.object().shape({
          first_name: Yup.string().required('First name is required!'),
          last_name: Yup.string().required('Last name is required!'),
          dob: Yup.string().required('Date of birth is required!'),
          country: Yup.string().required('Country is required!'),
          city: Yup.string().required('City is required!'),
          docType: Yup.string().required()
          //identity: Yup.string().required('ID document is required!'),
          //identityWithSelfie: Yup.string().required('ID document with selfie is required!')
        })}
        onSubmit={async (values, form) => {
          if (identityPreview && selfiePreview) {
            try {
              setLoading(true)
              values.currency = 'NGN'
              values.identity = identityPreview
              values.identityWithSelfie = selfiePreview
              await props.submitBankApplication(values)
              console.log('Submitted')
              setErrors({ msg: 'Bank application submited successfully', type: 'success' })
              //router.push('/wallet/[page]', props.allRoutes.BankDetail)
            } catch (error) {
              console.log(error)
              if (error.errors) {
                setErrors({ msg: error.errors[0].msg, type: 'danger' })
              }
            }
            setTimeout(() => {
              setErrors({ msg: '', type: '' })
            }, 4000)
            setLoading(false)
          }
        }}>
        {prop => (
          <React.Fragment>
            {errors && errors.msg && (
              <div className={`alert alert-${errors.type}`} role='alert'>
                {errors.msg}
              </div>
            )}
            <div className='input-with-fix'>
              <Form.Field.Input floating name='first_name' label='First Name' />
              <Form.Field.Input floating name='last_name' label='Last Name' />
              <Form.Field.Select floating name='country' options={countries} label='Country' />
              <Form.Field.Input floating name='city' label='City' />
              <Form.Field.DatePicker floating name='dob' label='Date of birth' />
              <Form.Field.Select floating name='docType' options={docTypes} label='Document Type' />
            </div>
            <div className='input-col-one upload-col'>
              <label htmlFor='identity'>
                {identityPreview ? (
                  <div className='input-box'>
                    <img disabled src={identityPreview} />
                    <span className='zoom-file-icon'>
                      <img src={images.bank.coolIcon} />
                    </span>
                  </div>
                ) : (
                  <>
                    <input
                      type='file'
                      id='identity'
                      name='identity'
                      className='avatar-file'
                      value={identityPreview}
                      accept='image/*'
                      onChange={e => onUpload(e, 'identities')}
                      style={{ display: 'none' }}
                    />
                    <div className='input-box'>
                      <input type='text' name='upload-document' className='file-upload' />
                      {identityPreviewLoading ? (
                        <span>
                          <Spinner color='#2A9DF4' size={40} />
                        </span>
                      ) : (
                        <span>
                          <span className='upload-img-icon upload-button'>
                            <img src={images.bank.uploadDoc} />
                          </span>
                          <a href='#' className='custom-link-button'>
                            Browse
                          </a>
                          to upload document type
                        </span>
                      )}
                    </div>
                  </>
                )}
              </label>

              <label htmlFor='identityWithSelfie'>
                {selfiePreview ? (
                  <div className='input-box mx-4'>
                    <img disabled src={selfiePreview} />
                    <span className='zoom-file-icon'>
                      <img src={images.bank.coolIcon} />
                    </span>
                  </div>
                ) : (
                  <>
                    <input
                      type='file'
                      id='identityWithSelfie'
                      name='identityWithSelfie'
                      className='avatar-file'
                      value={selfiePreview}
                      accept='image/*'
                      capture
                      onChange={e => onUpload(e, 'selfies')}
                      style={{ display: 'none' }}
                    />
                    <div className='input-box mx-4'>
                      <input type='text' name='upload-photo' className='photo-upload' />
                      {selfiePreviewLoading ? (
                        <span>
                          <Spinner color='#2A9DF4' size={40} />
                        </span>
                      ) : (
                        <span>
                          <span className='upload-img-icon'>
                            <img src={images.bank.uploadCam} />
                          </span>
                          Take a picture of you holding your document
                        </span>
                      )}
                    </div>
                  </>
                )}
              </label>
            </div>
            <Button
              variant='primary'
              disabled={props.user_personal_info || !identityPreview || !selfiePreview || !prop.isValid || loading}
              className='btn mt-4'
              isWorking={loading}
              onClick={prop.submitForm}>
              Continue
            </Button>
          </React.Fragment>
        )}
      </Form>
    </div>
  )
}
