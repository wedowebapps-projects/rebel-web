import React, { useState } from 'react'
import { Form, Button } from 'src/components/shared/index.js'

export default function ForgotPasswordForm(props) {
  const [errors, setErrors] = useState([])
  const [loading, setLoading] = useState(false)

  const error = errors && errors.length > 0 > 0 && errors[0].msg !== '' ? errors[0].msg : false

  return (
    <Form
      initialValues={{
        email: ''
      }}
      validations={{
        email: [Form.is.required('Email is required'), Form.is.email('Please enter valid email address')]
      }}
      onSubmit={async (values, form) => {
        setLoading(true)
        setErrors([])
        await props
          .requestOTP(values)
          .then(res => {})
          .catch(err => {
            setErrors(err)
          })
        setLoading(false)
      }}>
      {props => (
        <React.Fragment>
          {error && (
            <div className='alert alert-danger' role='alert'>
              {error}
            </div>
          )}
          <Form.Field.Input floating name='email' label='Email Address' />
          <Button variant='primary' className='mt-4' block isWorking={loading} onClick={props.submitForm}>
            Continue
          </Button>
        </React.Fragment>
      )}
    </Form>
  )
}
