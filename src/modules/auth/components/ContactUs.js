import Link from 'next/link'
import React from 'react'
import { getCmsPages_Api } from 'src/api/appApis'
import { Button, Form } from 'src/components/shared'
import images from 'src/config/images'
import * as Yup from 'yup'

const ContactUs = props => {
  const [contentPage, setContentPage] = React.useState('<p></p>')
  const [slugImage, setSlugImage] = React.useState('')
  const [isFormSubmitting, setIsFormSubmitting] = React.useState(false)
  const [title, setTitle] = React.useState('')
  const [error, setError] = React.useState('')
  const [success, setSuccess] = React.useState('')

  React.useEffect(() => {
    getCmsPages()
  }, [])

  const getCmsPages = async () => {
    try {
      const filters = { slug: 'contact-us' }
      const result = await getCmsPages_Api(filters)
      setContentPage(result.page.content)
      setTitle(result.page.title)
      setSlugImage(result.page.image)
    } catch (error) {
      console.log(error)
    }
  }
  return (
    <section className='min-vh-100'>
      <div className='pay-calculator-bg' style={{ backgroundImage: `url(https://rebelyus-prod.s3.amazonaws.com/${slugImage})` }}>
        <div className='container'>
          <div className='row'>
            <div className='col-12'>
              <div className='banner-box'>
                <div className='back-button'>
                  <Link href='/' to='/'>
                    <a href='/'>
                      <img src={images.settings.arrowBack} />
                    </a>
                  </Link>
                </div>
                <div className='banner-title-box mt-5 d-flex align-items-center'>
                  <div className='banner-title-icon mr-4'>
                    <img src={images.settings.titleIconLock} />
                  </div>
                  <div className='banner-title-text'>
                    <h2 className='color-white'>{title}</h2>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className='pay-calculator-info'>
        <div className='container'>
          <div className='row'>
            <div className='col-md-12 mt-5'>
              <div className='pay-calculator-left'>
                <p dangerouslySetInnerHTML={{ __html: contentPage }}></p>
              </div>
            </div>
            <div className='col-md-12'>
              <div>
                <h2 className='text-center'>Contact Us</h2>
                <Form
                  enableReinitialize
                  initialValues={{
                    name: props.user ? props.user.full_name : '',
                    email: props.user ? props.user.email : '',
                    phone_number: '',
                    message: ''
                  }}
                  validationSchema={Yup.object().shape({
                    name: Yup.string().required('Name is required'),
                    email: Yup.string()
                      .required('Email is required')
                      .email('Invalid email'),
                    phone_number: Yup.number()
                      .required('Phone number is required')
                      .typeError('Invalid Phone number'),
                    message: Yup.string().required('Message is required')
                  })}
                  onSubmit={async (values, actions) => {
                    try {
                      setIsFormSubmitting(true)
                      const data = {
                        name: values.name,
                        phone_number: values.phone_number,
                        email: values.email,
                        message: values.message
                      }
                      let result = await props.sendContactUs(data)
                      setSuccess(result.message)
                      setIsFormSubmitting(false)
                      setTimeout(() => {
                        actions.resetForm({
                          values: {
                            name: '',
                            phone_number: '',
                            email: '',
                            message: ''
                          }
                        })
                      }, 1000)
                      setTimeout(() => {
                        setSuccess('')
                      }, 5000)
                    } catch (error) {
                      console.log(error)
                      setError(error.errors)
                      setIsFormSubmitting(false)
                    }
                  }}>
                  {prop => (
                    <form onSubmit={prop.handleSubmit} className='form-input-wrap container-sm'>
                      <div className='input-wrap'>
                        <Form.Field.Input name='name' label='Name' placeholder='Name' />
                      </div>
                      <div className='input-wrap'>
                        <Form.Field.Input name='email' label='Email' placeholder='Email' />
                      </div>
                      <div className='input-wrap'>
                        <Form.Field.Input name='phone_number' label='Phone Number' placeholder='Phone number' />
                      </div>
                      <div className='input-wrap'>
                        <Form.Field.Textarea name='message' label='Message' maxLength='500' minRows={3} placeholder='Write your message...' />
                      </div>
                      {error &&
                        error.map(err => {
                          return (
                            <div className='alert alert-danger' role='alert'>
                              {err?.message}
                            </div>
                          )
                        })}
                      {success && (
                        <div className='alert alert-success' role='alert'>
                          {success}
                        </div>
                      )}
                      <div className='sticky-submit-btn my-0 mt-lg-5 d-flex justify-content-center'>
                        <Button type='submit' isWorking={isFormSubmitting} variant='primary' disabled={!prop.isValid || isFormSubmitting}>
                          Submit
                        </Button>
                      </div>
                    </form>
                  )}
                </Form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className='overlay'></div>
    </section>
  )
}

export default ContactUs
