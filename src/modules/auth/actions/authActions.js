import * as authTypes from './authTypes'

export const restoreAuthentication = () => {
  return { type: authTypes.RESTORE_SESSION }
}

export const setAuthentication = (user, token) => {
  return { type: authTypes.SET_AUTHENTICATION, user, token }
}

export const registerUser = data => {
  return { type: authTypes.REGISTER_USER, data }
}

export const setRegisterData = data => {
  return { type: authTypes.SET_REGISTER_DATA, data }
}

export const registerOtpSubmit = data => {
  return { type: authTypes.REGISTER_OTP_SUBMIT, data }
}

export const setRegisterError = data => {
  return { type: authTypes.SET_REGISTER_ERROR, data }
}

export const patchUser = (data, resolve, reject) => {
  return { type: authTypes.PATCH_USER, data, resolve, reject }
}

export const loginUser = (data, resolve, reject) => {
  return { type: authTypes.LOGIN_USER, data, resolve, reject }
}

export const logoutUser = (resolve, reject) => {
  return { type: authTypes.LOGOUT, resolve, reject }
}

export const forgotPassword = (data, resolve, reject) => {
  return { type: authTypes.FORGOT_PASSWORD, data, resolve, reject }
}

export const resetPassword = (data, resolve, reject) => {
  return { type: authTypes.RESET_PASSWORD, data, resolve, reject }
}

export const setMyProfileAction = data => {
  return {
    type: authTypes.SET_MY_PROFILE,
    data
  }
}

//bank actions
export const submitBankApplicationAction = (data, resolve, reject) => {
  return {
    type: authTypes.SUBMIT_BANK_APPLICATION,
    data,
    resolve,
    reject
  }
}
export const verifyPasswordAction = (data, resolve, reject) => {
  return {
    type: authTypes.VERIFY_PASSWORD,
    data,
    resolve,
    reject
  }
}

export const setBankApplicationStatusAction = data => {
  return {
    type: authTypes.SET_BANK_APPLICATION,
    data
  }
}
export const submitBankDetailsAction = (data, resolve, reject) => {
  return {
    type: authTypes.SUBMIT_BANK_DETAILS,
    data,
    resolve,
    reject
  }
}

//CHECK USERNAME EXIST
export const checkUsernameExistAction = (data, resolve, reject) => {
  return {
    type: authTypes.CHECK_USERNAME_EXIST,
    data,
    resolve,
    reject
  }
}

// Subscription price
export const submitSubscriptionPriceAction = (data, uuid, resolve, reject) => ({
  type: authTypes.SUBMIT_SUBSCRIPTION_PRICE,
  data,
  uuid,
  resolve,
  reject
})
export const setSubscriptionPriceAction = (data, reset) => ({
  type: authTypes.SET_SUBSCRIPTION_PRICE,
  data,
  reset
})
export const updateSubscriptionPriceAction = data => ({
  type: authTypes.UPDATE_SUBSCRIPTION_PRICE,
  data
})
export const deleteSubscriptionPriceAction = (data, resolve, reject) => ({
  type: authTypes.DELETE_SUBSCRIPTION_PRICE,
  data,
  resolve,
  reject
})
export const fetchSubscriptionPricesAction = (resolve, reject) => ({
  type: authTypes.FETCH_SUBSCRIPTION_PRICES,
  resolve,
  reject
})

export const setSocialDataAction = data => {
  return {
    type: authTypes.SET_SOCIAL_DATA,
    data
  }
}

export const storeAuthenticationAction = data => {
  return {
    type: authTypes.STORE_AUTHENTICATION,
    data
  }
}
export const socialSignupAction = (data, resolve, reject) => {
  return {
    type: authTypes.SOCIAL_SIGNUP,
    data,
    resolve,
    reject
  }
}
export const socialLoginAction = (data, resolve, reject) => {
  return {
    type: authTypes.SOCIAL_LOGIN,
    data,
    resolve,
    reject
  }
}