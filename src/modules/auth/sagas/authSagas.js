import 'firebase/messaging'
import { takeLatest, all, fork, call, put, select, delay } from 'redux-saga/effects'
import * as authTypes from '../actions/authTypes'
import * as authActions from '../actions/authActions'
import * as appActions from 'src/actions/appActions'
import * as authApi from '../api/authApi'
import * as storageUtils from 'src/utils/storageUtils'
import Router from 'next/router'
import { authPages } from 'src/config'
import toast from 'src/utils/toast'

export const getRegisterData = state => state.auth.registerData
export const getUserToken = state => state.auth.token

function* patchUser({ data, resolve, reject }) {
  let token = yield select(getUserToken)
  try {
    const update = yield call(authApi.patchUser, data, token)
    resolve(update)
  } catch (error) {
    reject(error.errors)
  }
}

function* loginUser({ data, resolve, reject }) {
  try {
    const login = yield call(authApi.login, data)
    yield call(storageUtils.setUserToken, login.token)
    yield call(storageUtils.setUserData, login.user)
    yield put(authActions.setAuthentication(login.user, `Bearer ${login.token}`))
    if (Router.router && Router.router?.query?.return) {
      yield call(Router.replace, Router.router?.query?.return)
    } else {
      yield call(Router.replace, '/')
    }
    resolve(login)
  } catch (error) {
    console.log(error)
    reject(error.errors)
  }
}

function* registerUser({ data }) {
  let errData = {
    emailExists: false,
    errors: []
  }
  try {
    yield call(storageUtils.setRegisterData, data)
    yield put(authActions.setRegisterData(data))
    yield put(authActions.setRegisterError(errData))
    const data1 = {
      type: 'email',
      typeValue: data.email
    }
    const checkUser = yield call(authApi.checkEmailUsername, data1)
    if (checkUser.isExist !== true) {
      const data2 = {
        email: data.email
      }
      const senOtp = yield call(authApi.sendOtpToEmail, data2)
      yield call(Router.push, '/register/verify')
    } else {
      errData.emailExists = true
      yield put(authActions.setRegisterError(errData))
    }
  } catch (error) {
    console.log('error : ', error)
    errData.errors = error.errors
    yield put(authActions.setRegisterError(errData))
  }
}

function* registerOtpSubmit({ data }) {
  let errData = {
    errors: [],
    invalidOtp: false
  }
  try {
    yield put(authActions.setRegisterError(errData))
    const verify = yield call(authApi.verifyOTP, data)
    if (verify.verified) {
      let registerData = yield select(getRegisterData)
      const register = yield call(authApi.signup, registerData)
      yield call(storageUtils.removeRegisterData)
      yield call(storageUtils.setUserToken, register.token)
      yield call(storageUtils.setUserData, register.user)
      yield call(storageUtils.showWelcomeModal.set, true)
      yield put(authActions.setAuthentication(register.user, `Bearer ${register.token}`))
      yield call(Router.replace, 'username')
    } else {
      errData.invalidOtp = true
      yield put(authActions.setRegisterError(errData))
    }
  } catch (error) {
    console.log('error : ', error)
    errData.errors = error.errors
    errData.invalidOtp = true
    yield put(authActions.setRegisterError(errData))
  }
}

function* forgotPassword({ data, resolve, reject }) {
  try {
    const forgot = yield call(authApi.requestOtp, data)
    resolve(forgot)
  } catch (error) {
    reject(error.errors)
  }
}

function* resetPassword({ data, resolve, reject }) {
  try {
    const reset = yield call(authApi.resetPassword, data)
    yield call(storageUtils.setUserToken, reset.token)
    yield call(storageUtils.setUserData, reset.user)
    yield put(authActions.setAuthentication(reset.user, `Bearer ${reset.token}`))
    yield call(Router.replace, '/')
    resolve(reset)
  } catch (error) {
    console.log(error)
    reject(error.errors)
  }
}

function* restoreSession() {
  yield put(appActions.appLoading(true))
  try {
    const token = yield call(storageUtils.getUserToken)
    if (token) {
      const user = yield call(authApi.getMyProfile, token)
      yield call(storageUtils.setUserData, user.user)
      yield put(authActions.setAuthentication(user.user, token))
      if (authPages.includes(Router.asPath)) {
        yield call(Router.replace, '/')
      }
    }
  } catch (error) {
    yield call(storageUtils.unsetUserData)
    yield call(storageUtils.unsetUserToken)
    // if (!authPages.includes(Router.asPath)) {
    //   yield call(Router.replace, '/login')
    // }
  }
  yield put(appActions.appLoading(false))
}

function* logoutUser({ resolve, reject }) {
  try {
    yield call(authApi.logout_api)
    yield call(storageUtils.unsetUserData)
    yield call(storageUtils.unsetUserToken)
    yield put(authActions.setAuthentication({}, ''))
    yield put(appActions.closeAllDrawers())
    resolve()
  } catch (error) {
    console.log(error)
    reject()
  }
}

//wallet sagas
function* submitBankApplicationSaga({ data, resolve, reject }) {
  try {
    const result = yield call(authApi.submitBankApplication_api, data)
    yield put(authActions.setBankApplicationStatusAction(JSON.stringify(result.personal_info)))
    resolve()
  } catch (error) {
    reject(error)
  }
}
function* submitBankDetailsSaga({ data, resolve, reject }) {
  try {
    const result = yield call(authApi.submitBankAccount_api, data)
    yield put(authActions.setBankApplicationStatusAction(JSON.stringify(result.personal_info)))
    resolve()
  } catch (error) {
    reject(error)
  }
}

//CHECK USER NAME EXIST
function* checkUsernameExistSaga({ data, resolve, reject }) {
  try {
    const result = yield call(authApi.checkUsernameExist_api, data)
    resolve(result)
  } catch (error) {
    reject(error)
  }
}

function* submitSubscriptionPriceSaga({ data, uuid, resolve, reject }) {
  try {
    let result
    if (uuid) {
      //update
      result = yield call(authApi.updateSubscriptionPrice_api, data, uuid)
      yield put(authActions.updateSubscriptionPriceAction(result.subscriptions))
    } else {
      // Add
      result = yield call(authApi.addSubscriptionPrice_api, data)
      yield put(authActions.setSubscriptionPriceAction([result.subscriptions], false))
    }
    resolve(result)
  } catch (error) {
    console.log(error)
    reject(error)
  }
}
function* fetchSubscriptionPricesSaga({ resolve, reject }) {
  try {
    const result = yield call(authApi.fetchSubscriptionPrice_api)
    resolve(result.subscriptions)
    yield put(authActions.setSubscriptionPriceAction(result.subscriptions, true))
  } catch (error) {
    reject()
    console.log(error)
  }
}
function* deleteSubscriptionPriceSaga({ data, resolve, reject }) {
  try {
    yield call(authApi.deleteSubscriptionPrice_api, data.uuid, { id: data.plan_data.id })
    const subscriptionPrices = yield select(state => state.auth.subscriptionPrices)
    const i = subscriptionPrices.map(p => p.uuid).indexOf(data.uuid)
    subscriptionPrices.splice(i, 1)
    yield put(authActions.setSubscriptionPriceAction(subscriptionPrices, true))
    resolve()
  } catch (error) {
    reject(error)
  }
}

function* udpateProfileSaga({ data, resolve, reject }) {
  try {
    const result = yield call(authApi.updateProfileApi, data)
    yield put(authActions.setMyProfileAction(result.user))
    resolve(result)
  } catch (error) {
    // reject(error)
  }
}

function* verifyPasswordSaga({ data, resolve, reject }) {
  try {
    yield call(authApi.verifyPassword_api, data)
    resolve()
  } catch (error) {
    reject(error)
  }
}

function* socialSignupSaga(action) {
  try {
    const result = yield call(authApi.socialSignupApi, action.data)
    yield put(authActions.setAuthentication(result.user, `Bearer ${result.token}`))
    yield call(storageUtils.setUserToken, result.token)
    yield call(storageUtils.setUserData, result.user)
    toast.success(result.message)
    action.resolve(result)
    yield call(Router.replace, 'register/username')
  } catch (error) {
    console.log('social sign up', error)
    if (error.errors.length) {
      error.errors.map(e => toast.error(e.msg))
    }
    action.reject(error)
  }
}

function* socialLoginSaga(action) {
  try {
    const result = yield call(authApi.socialLogin, action.data)
    yield put(authActions.setAuthentication(result.user, `Bearer ${result.token}`))
    yield call(storageUtils.setUserToken, result.token)
    yield call(storageUtils.setUserData, result.user)
    toast.success(result.message)
    action.resolve(result)
  } catch (error) {
    console.log('social login', error)
    if (error.errors.length) {
      error.errors.map(e => toast.error(e.msg))
    }
    action.reject(error)
  }
}

export function* watchSagas() {
  yield takeLatest(authTypes.LOGIN_USER, loginUser)
  yield takeLatest(authTypes.LOGOUT, logoutUser)
  yield takeLatest(authTypes.REGISTER_USER, registerUser)
  yield takeLatest(authTypes.REGISTER_OTP_SUBMIT, registerOtpSubmit)
  yield takeLatest(authTypes.RESTORE_SESSION, restoreSession)
  yield takeLatest(authTypes.FORGOT_PASSWORD, forgotPassword)
  yield takeLatest(authTypes.RESET_PASSWORD, resetPassword)
  yield takeLatest(authTypes.PATCH_USER, patchUser)

  yield takeLatest(authTypes.SUBMIT_BANK_APPLICATION, submitBankApplicationSaga)
  yield takeLatest(authTypes.SUBMIT_BANK_DETAILS, submitBankDetailsSaga)

  yield takeLatest(authTypes.CHECK_USERNAME_EXIST, checkUsernameExistSaga)
  yield takeLatest(authTypes.UPDATE_PROFILE, udpateProfileSaga)
  yield takeLatest(authTypes.VERIFY_PASSWORD, verifyPasswordSaga)

  yield takeLatest(authTypes.FETCH_SUBSCRIPTION_PRICES, fetchSubscriptionPricesSaga)
  yield takeLatest(authTypes.SUBMIT_SUBSCRIPTION_PRICE, submitSubscriptionPriceSaga)
  yield takeLatest(authTypes.DELETE_SUBSCRIPTION_PRICE, deleteSubscriptionPriceSaga)
  yield takeLatest(authTypes.SOCIAL_SIGNUP, socialSignupSaga)
  yield takeLatest(authTypes.SOCIAL_LOGIN, socialLoginSaga)
}

export default function* runSagas() {
  yield all([fork(watchSagas)])
}
