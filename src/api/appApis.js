import api from './index'
import { convertObjectToQuerystring } from 'src/utils/helpers'
import { getUserToken } from 'src/utils/storageUtils'
import { isEmpty } from 'lodash'

export const getHomeFeaturedAccounts_Api = filters => {
  let filter = ''
  if (!isEmpty(filters)) {
    filter = `${convertObjectToQuerystring(filters)}`
  }

  return api(`/open/user/featured?${filter}`, null, 'get')
}

// Fetch announcements data
export const fetchAnnouncements_api = () => {
  return api(`/open/announcements`, null, 'GET')
}

export const loadProfileDetail_api = (username, token) => {
  if (token) {
    return api(`/user/profile/${username}`, null, 'get', token)
  } else {
    return api(`/open/user/profile/${username}`, null, 'get')
  }
}

export const followUser_api = (id, data) => {
  const token = getUserToken()
  return api(`/user/follow/${id}`, data, 'post', token)
}
export const unFollowUser_api = id => {
  const token = getUserToken()
  return api(`/user/unfollow/${id}`, null, 'post', token)
}
export const favoriteProfile_api = data => {
  const token = getUserToken()
  return api(`/user/lists/add`, data, 'post', token)
}
export const addComment_api = (id, data) => {
  const token = getUserToken()
  return api(`/posts/${id}/comment`, data, 'post', token)
}
export const deleteComment_api = (id, comment_id) => {
  const token = getUserToken()
  return api(`/posts/${id}/comment/${comment_id}`, null, 'delete', token)
}

// LIST apis
export const getSubscriptions = (type, filters) => {
  let filter = ''
  if (!isEmpty(filters)) {
    filter = `${convertObjectToQuerystring(filters)}`
  }
  const token = getUserToken()
  return api(`/user/lists/${type}?${filter}`, null, 'GET', token)
}
export const fetchListNumbers_api = () => {
  const token = getUserToken()
  return api(`/user/lists`, null, 'GET', token)
}
export const searchUser = (data, filters) => {
  let filter = ''
  if (!isEmpty(filters)) {
    filter = `${convertObjectToQuerystring(filters)}`
  }
  const token = getUserToken()
  if (token) {
    return api(`/user/search?${filter}`, data, 'POST', token)
  } else {
    return api(`/open/user/search?${filter}`, data, 'POST')
  }
}

// GET REPORT REASONS API
export const fetchReportReasons_api = () => {
  const token = getUserToken()
  return api(`/reasons`, null, 'GET', token)
}

// Country list for bank application
export const getCountryList_api = () => {
  return api(`/countries`, null, 'GET')
}

// Fetch commission data
export const fetchCommission_api = () => {
  return api(`/stripe/fees`, null, 'GET')
}

//notification
export const fetchNotifications_api = () => {
  const token = getUserToken()
  return api(`/user/notifications`, null, 'GET', token)
}
export const fetchTopBarNotifications_api = () => {
  const token = getUserToken()
  return api(`/user/notifications/top-bar`, null, 'GET', token)
}
export const clearhNotifications_api = () => {
  const token = getUserToken()
  return api(`/user/notifications`, null, 'delete', token)
}
export const readNotifications_api = () => {
  const token = getUserToken()
  return api(`/user/notifications`, null, 'put', token)
}

// cms pages api
export const getCmsPages_Api = filters => {
  let filter = ''
  if (!_.isEmpty(filters)) {
    filter = `${convertObjectToQuerystring(filters)}`
  }
  return api(`/pageBySlug?${filter}`, null, 'get')
}

export const sendContactUs_api = data => {
  return api(`/contact_us`, data, 'post')
}
