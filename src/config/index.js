export const authPages = [
  '/login',
  '/login/reset-password',
  '/signup',
  '/register',
  '/register/verify',
  '/register/username',
  '/register/image',
  '/forgot-password',
  '/otp-verify',
  '/reset-password',
  '/page/[...page]',
  '/mypay',
  '/privacy-policy',
  '/how-it-works',
  '/terms-conditions'
]

export const docTypes = [
  {
    label: 'Passport',
    value: 'Passport'
  },
  {
    label: 'ID card',
    value: 'ID card'
  },
  {
    label: 'Driving License',
    value: 'Driving License'
  }
]

export const textPostColors = [
  {
    color: '#AA076B-#61045F',
    isDark: true
  },
  {
    color: '#A1FFCE-#FAFFD1',
    isDark: false
  },
  {
    color: '#A1FFCE-#F9D423',
    isDark: false
  },
  {
    color: '#A1FFCE-#FBC7D4',
    isDark: false
  },
  {
    color: '#A1FFCE-#799F0C',
    isDark: false
  },
  {
    color: '#A1FFCE-#928DAB',
    isDark: false
  },
  {
    color: '#A1FFCE-#F15F79',
    isDark: false
  },
  {
    color: '#A1FFCE-#00D9F5',
    isDark: false
  }
]

export const AllowedMediaTypes = ['video/mp4', 'video/x-m4v', 'video/*', 'image/*']
export const VideoMimes = ['video/webm', 'video/mp4', 'video/mpeg', 'video/avi', 'video/quicktime']
export const ImageMimes = ['image/jpeg', 'image/bmp', 'image/jpg', 'image/gif', 'image/webp', 'image/png']

export const InterestList = [
  { label: 'Health', value: 'Health' },
  { label: 'Fitness', value: 'Fitness' },
  { label: 'Food', value: 'Food' },
  { label: 'Entertainment', value: 'Entertainment' },
  { label: 'Music', value: 'Music' },
  { label: 'Chess', value: 'Chess' },
  { label: 'Writing', value: 'Writing' },
  { label: 'Painting', value: 'Painting' },
  { label: 'Dancing', value: 'Dancing' },
  { label: 'Gardening', value: 'Gardening' }
]

export const GenderList = [
  { label: 'Male', value: 'male' },
  { label: 'Female', value: 'female' }
]

export const pollExpiry = [
  {
    label: '1 Day',
    value: 1
  },
  {
    label: '3 Days',
    value: 3
  },
  {
    label: '7 Days',
    value: 7
  },
  {
    label: '14 Days',
    value: 14
  },
  {
    label: '30 Days',
    value: 30
  }
]
