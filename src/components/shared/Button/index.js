import React, { forwardRef } from 'react'
import PropTypes from 'prop-types'

import { color } from '../../../config/styles'
import Icon from '../Icon'

import { StyledSpinner, Text } from './styles'

const propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
  variant: PropTypes.oneOf(['primary', 'secondary', 'transparent', 'success', 'danger', 'light']),
  icon: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  faIcon: PropTypes.string,
  iconSize: PropTypes.number,
  block: PropTypes.bool,
  link: PropTypes.bool,
  medium: PropTypes.bool,
  small: PropTypes.bool,
  rounded: PropTypes.bool,
  disabled: PropTypes.bool,
  isWorking: PropTypes.bool,
  onClick: PropTypes.func
}

const defaultProps = {
  className: undefined,
  children: undefined,
  variant: 'secondary',
  icon: undefined,
  faIcon: undefined,
  iconSize: 18,
  block: false,
  link: false,
  medium: false,
  small: false,
  rounded: false,
  disabled: false,
  isWorking: false,
  onClick: () => {}
}

const Button = forwardRef(({ children, className, variant, icon, faIcon, iconSize, disabled, block, link, medium, small, rounded, isWorking, onClick, ...buttonProps }, ref) => {
  const handleClick = e => {
    if (!disabled && !isWorking) {
      onClick(e)
    }
  }

  
  let classes = 'btn custom-button'
  classes += variant ? ` button-${variant}` : ''
  classes += block ? ' w-100' : ''
  classes += link ? ' button-link' : ''
  classes += className ? ` ${className}` : ''
  classes += rounded ? ' button-rounded' : ''
  classes += medium ? ' button-md' : ''
  classes += small ? ' button-sm' : ''

  return (
    <button className={classes} {...buttonProps} onClick={handleClick} variant={variant} disabled={disabled || isWorking} ref={ref}>
      {isWorking && <StyledSpinner size={26} color={getIconColor(variant)} />}
      {faIcon && <i className={`fa ${faIcon}`}></i>}
      {!isWorking && icon && typeof icon === 'string' ? <Icon type={icon} size={iconSize} color={getIconColor(variant)} /> : icon}
      {children && <Text withPadding={isWorking || icon}>{children}</Text>}
    </button>
  )
})

const getIconColor = variant => (['secondary', 'transparent'].includes(variant) ? '#187BCD' : '#fff')

Button.propTypes = propTypes
Button.defaultProps = defaultProps

export default Button
