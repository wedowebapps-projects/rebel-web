import styled, { css } from 'styled-components';
import Spinner from '../Spinner';

// const buttonVariants = {
//   primary: primary,
//   secondary: secondary,
//   transparent: transparent,
//   rounded: rounded,
//   inline: inline,
//   success: success,
//   danger: danger,
// };

export const StyledSpinner = styled(Spinner)`
  position: relative;
  top: 1px;
`;

export const Text = styled.div`
  padding-left: ${props => (props.withPadding ? 7 : 0)}px;
`;