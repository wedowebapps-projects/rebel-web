import moment from 'moment'
import Link from 'next/link'
import React from 'react'
import { Button } from 'src/components/shared'
import images from 'src/config/images'
import { trim } from 'src/utils/helpers'

const NotificationDrawer = ({ isNotificationVisible, ...props }) => {
  const [isClearing, setIsClearing] = React.useState(false)

  const clearNotification = async e => {
    e.preventDefault()
    try {
      setIsClearing(true)
      await props.clearNotifications()
    } catch (error) {
      console.log(error)
    }
    setIsClearing(false)
    props.toggleNotification(false)
  }

  const getNotificationTime = createdAt => {
    let messageTime = ''
    var duration = moment.duration(
      moment().diff(
        moment
          .utc(createdAt)
          .local()
          .format()
      )
    )
    const mins = parseInt(duration.asMinutes())
    const hours = duration.asHours()
    const days = duration.asDays()
    if (days > 1) {
      messageTime = moment
        .utc(createdAt)
        .local()
        .format('DD MMM')
    } else if (hours > 1) {
      // messageTime = moment
      //   .utc(createdAt)
      //   .local()
      //   .format('hh:mm a')
      messageTime = `${parseInt(hours)} ${parseInt(hours) > 1 ? 'hours' : 'hour'} ago`
    } else if (mins > 1) {
      messageTime = `${mins} ${mins > 1 ? 'mins' : 'min'} ago`
    } else {
      messageTime = 'just now'
    }
    return messageTime
  }
  return (
    <div className='notifications-list' style={{ display: 'block' }}>
      <div className='d-flex justify-content-between align-items-center'>
        <span className='notification-title m-0 mr-5'>NOTIFICATIONS</span>
        <Button small variant='secondary' disabled={isClearing} onClick={clearNotification} className='m-0'>
          Clear
        </Button>
      </div>
      {props?.topBarNotifications?.rows.length === 0 && (
        <div className='py-1'>
          <h6>No Recent Notifications For today.</h6>
        </div>
      )}
      <ul>
        {props?.topBarNotifications?.rows?.map((noti, i) => {
          const content = JSON.parse(noti.content)
          return (
            <React.Fragment key={`notification-drawer-${noti.id}-${i}`}>
              {noti.type === 'follow' && (
                <Link href='/[username]' as={`/${noti.userId.username}`}>
                  <li className='text-wrap'>
                    <div className='notifications-msg' style={{ position: 'static', marginRight: '20px' }}>
                      <span className='red-bg' style={{ opacity: '1', paddingLeft: '7px' }}>
                        <img src={images.app.notificationIcon} />
                      </span>
                    </div>
                    {/* <img src={images.app.notificationIcon} /> */}
                    <div>
                      <a href={`/${noti.userId.username}`}>{noti.userId.username} started following you.</a>
                      <span>{getNotificationTime(noti.createdAt)}</span>
                    </div>
                  </li>
                </Link>
              )}
              {noti.type === 'like' && (
                <Link href='/[username]' as={`/${noti.userId.username}`}>
                  <li className='text-wrap'>
                    <img src={images.app.notificationHeart} />
                    <div>
                      <a href={`/${noti.userId.username}`}>{noti.userId.username} liked your post</a>
                      <span>{getNotificationTime(noti.createdAt)}</span>
                    </div>
                  </li>
                </Link>
              )}
              {noti.type === 'comment' && (
                <Link href='/[username]' as={`/${noti.userId.username}`}>
                  <li className='text-wrap'>
                    <img src={images.app.notificationChat} />
                    <div>
                      <a href={`/${noti.userId.username}`}>{noti.userId.username} commented your post</a>
                      <span>{getNotificationTime(noti.createdAt)}</span>
                    </div>
                  </li>
                </Link>
              )}
              {noti.type === 'post' && (
                <Link href='/[username]' as={`/${noti.userId.username}`}>
                  <li className='text-wrap'>
                    <div className='notifications-msg' style={{ position: 'static', marginRight: '20px' }}>
                      <span className='red-bg' style={{ opacity: '1', paddingLeft: '7px' }}>
                        <img src={images.app.notificationIcon} />
                      </span>
                    </div>
                    <div>
                      <a href={`/${noti.userId.username}`}>
                        {noti.userId.username} has added a new post {trim(content?.post?.title, 40)}
                      </a>
                      <span>{getNotificationTime(noti.createdAt)}</span>
                    </div>
                  </li>
                </Link>
              )}
              {noti.type === 'subscribe' && (
                <Link href='/[username]' as={`/${noti.userId.username}`}>
                  <li className='text-wrap'>
                    <div className='notifications-msg' style={{ position: 'static', marginRight: '20px' }}>
                      <span className='red-bg' style={{ opacity: '1', paddingLeft: '7px' }}>
                        <img src={images.app.notificationIcon} />
                      </span>
                    </div>
                    <div>
                      <a href={`/${noti.userId.username}`}>
                        {noti.userId.username} subscribed to your post {trim(content?.post?.title, 40)}
                      </a>
                      <span>{getNotificationTime(noti.createdAt)}</span>
                    </div>
                  </li>
                </Link>
              )}
              {noti.type === 'tip' && (
                <Link href='/[username]' as={`/${noti.userId.username}`}>
                  <li className='text-wrap'>
                    <img src={images.app.notificationCurrency} />
                    <div>
                      <a href={`/${noti.userId.username}`}>
                        {noti.userId.username} sent tip for your post of NGN {content?.amount}
                      </a>
                      <span>{getNotificationTime(noti.createdAt)}</span>
                    </div>
                  </li>
                </Link>
              )}
              {noti.type === 'user_tip' && (
                <Link href='/[username]' as={`/${noti.userId.username}`}>
                  <li className='text-wrap'>
                    <img src={images.app.notificationCurrency} />
                    <div>
                      <a href={`/${noti.userId.username}`}>
                        {noti.userId.username} sent you a tip of NGN {content?.amount}
                      </a>
                      <span>{getNotificationTime(noti.createdAt)}</span>
                    </div>
                  </li>
                </Link>
              )}
            </React.Fragment>
          )
        })}
      </ul>
      <Link href='/notifications' to='/notifications'>
        <a href='/notifications' onClick={() => props.toggleNotification(false)} className='alpha40 all-notification'>
          View all notifications
        </a>
      </Link>
    </div>
  )
}

export default NotificationDrawer
