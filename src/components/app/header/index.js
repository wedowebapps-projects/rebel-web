import React, { useEffect, useState } from 'react'
import images from 'src/config/images'
import UserMenu from './UserMenu'
import { Avatar, Button, UserMedia } from 'src/components/shared'
import Link from 'next/link'
import { searchUser } from 'store/apis/appApis'
import NotificationDrawer from './NotificationDrawer'
import { Dropdown } from 'react-bootstrap'
import { getUserToken } from 'src/utils/storageUtils'

function Header(props) {
  const token = getUserToken()
  const [isDark, setIsDark] = useState(false)
  const [isMenuVisible, setIsMenuVisible] = useState(false)
  const [isNotificationVisible, setIsNotificationVisible] = useState(false)
  const [isNotificationRead, setIsNotificationRead] = useState(false)
  const [showSearchBox, setShowSearchBox] = useState(false)
  const [showSearchLoading, setShowSearchLoading] = useState(false)
  const [searchResults, setSearchResults] = useState([])
  const [searchText, setSearchText] = useState('')

  useEffect(() => {
    setIsDark(props.theme === 'dark')
  }, [props])

  const toggleMenu = () => {
    setIsMenuVisible(!isMenuVisible)
  }

  const toggleNotification = () => {
    setIsNotificationVisible(!isNotificationVisible)
    if (props?.topBarNotifications.unreadNotification > 0) {
      props.readNotifications()
    }
  }

  const user = props.user

  const handleSearchInput = async e => {
    if (e.target.value !== '') {
      setShowSearchBox(true)
      setShowSearchLoading(true)
      const result = await searchUser({ searchTerm: e.target.value }, {})
      setShowSearchLoading(false)
      setSearchResults(result.data)
    } else {
      setShowSearchBox(false)
      setSearchResults([])
      setSearchText('')
    }
  }

  const NotificationIcon = React.forwardRef(({ onClick }, ref) => (
    <div
      className='notification-wrap mr-2 cursor-pointer'
      onClick={e => {
        e.preventDefault()
        onClick(e)
        setIsNotificationRead(true)
      }}
      ref={ref}>
      {props?.topBarNotifications.unreadNotification > 0 && !isNotificationRead && <div className='new-notification' />}
      <img src={images.app.notification} alt='Day' />
    </div>
  ))

  return (
    <header id='header' className='bg-color-white'>
      <div className='container'>
        <div className='col-12'>
          <div className='row'>
            <div className='col-md-6 d-flex align-items-center pl-0'>
              <Link href='/' as='/'>
                <a href='#'>
                  <img src={images.app.rebelLock} alt='Rebel_Logo' style={{ width: '50px' }} />
                </a>
              </Link>
              <div className='search-input-container ml-3'>
                <div className='search-input-wrap'>
                  <input type='search' placeholder='Search' onKeyUp={handleSearchInput} value={searchText} onChange={e => setSearchText(e.target.value)} />
                  <img src={images.app.search} />
                </div>
                {showSearchBox && (
                  <div className='p-2 search-results-wrap'>
                    {showSearchLoading ? (
                      <h6 className='text-center text-muted'>Searching Profiles...</h6>
                    ) : searchResults.length > 0 ? (
                      <ul>
                        {searchResults.map(user => (
                          <li
                            className='p-1'
                            onClick={() => {
                              setShowSearchBox(false)
                              setSearchResults([])
                              setSearchText('')
                            }}>
                            <UserMedia show='username' user={user} size={39} />
                          </li>
                        ))}
                      </ul>
                    ) : (
                      <h6 className='text-center text-muted'>No Accounts found for this search term</h6>
                    )}
                  </div>
                )}
              </div>
            </div>
            <div className='col-md-6 d-flex justify-content-end align-items-center pr-0'>
              {props.user.personal_info_status === 'approved' && (
                <Button variant='primary' medium className='create-post mb-0' disabled={props.user.personal_info_status !== 'approved'} onClick={props.toggleNewPostModal}>
                  <img src={images.app.plusSquare} /> New post
                </Button>
              )}
              <div className='d-flex align-items-center mode-option pr-5'>
                <div>
                  <img src={images.app.day} alt='Day' />
                </div>
                <div>
                  <input
                    type='checkbox'
                    id='toggle-mode-cb'
                    checked={isDark}
                    onChange={() => {
                      props.toggleTheme()
                    }}
                  />
                  <div id='mode-wrapper'>
                    <label id='toggle-mode' htmlFor='toggle-mode-cb'>
                      <span className='toggle-border'>
                        <span className='toggle-indicator' />
                      </span>
                    </label>
                  </div>
                </div>
                <div>
                  <img src={images.app.night} alt='Night_Mode' />
                </div>
              </div>
              {token && (
                <div className='d-flex align-items-center'>
                  <Dropdown>
                    <Dropdown.Toggle as={NotificationIcon} id={`dropdown-custom-components`} />
                    <Dropdown.Menu className='p-0 border-0'>
                      <Dropdown.Item eventKey='1' className='p-0'>
                        <NotificationDrawer {...props} isNotificationVisible={isNotificationVisible} toggleNotification={toggleNotification} />
                      </Dropdown.Item>
                    </Dropdown.Menu>
                  </Dropdown>
                  <div className='d-flex align-items-center header-profile-wrp' onClick={toggleMenu}>
                    <Avatar name={user.full_name} avatarUrl={user.avatar} size={48} className='mr-3' />
                    <img src={images.app.arrowDown} alt='Arrow_Down' />
                    <UserMenu {...props} isMenuVisible={isMenuVisible} logoutUser={props.logoutUser} />
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </header>
  )
}

export default Header
