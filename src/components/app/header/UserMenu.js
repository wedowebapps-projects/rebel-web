import Link from 'next/link'
import { useRouter } from 'next/router'
import React from 'react'
import images from 'src/config/images'

function UserMenu(props) {
  const { isMenuVisible } = props
  const router = useRouter()

  return (
    <div className='profile' style={{ display: isMenuVisible ? 'block' : 'none' }}>
      <ul>
        <Link href='/account' to='/account'>
          <li className={router.asPath.includes('/account') ? 'active' : ''}>
            <img src={images.app.profileSidemenu} className='alpha50' />
            <div>
              <a href='/account'>Profile</a>
              {/* <span>View your profile details</span> */}
            </div>
          </li>
        </Link>
        {props.user.personal_info_status === 'approved' && (
          <Link href='/creator-dashboard' to='/creator-dashboard'>
            <li className={router.asPath.includes('/creator-dashboard') ? 'active' : ''}>
              <img src={images.app.combinedShape} className='alpha50 alpha100' />
              <div className='d-flex mr-2'>
                <a href='/creator-dashboard'>Dashboard</a>
                <button>Creator 🔒</button>
              </div>
            </li>
          </Link>
        )}
        <Link href='/contact-us' to='/contact-us'>
          <li>
            <img src={images.app.support} alt='Profile' className='alpha50' />
            <div>
              <a href='/contact-us'>Support</a>
            </div>
          </li>
        </Link>
        <li>
          <hr />
        </li>
        <Link href='/settings/your-account' to='/settings/your-account'>
          <li className={router.asPath.includes('/settings') ? 'active' : ''}>
            <img src={images.app.Subtract} alt='Profile' className='alpha50' />
            <div>
              <a href='/settings/your-account'>Setting</a>
            </div>
          </li>
        </Link>
        <li
          onClick={() => {
            props.changeLogoutToggle(true)
          }}>
          <img src={images.app.Logout} alt='Profile' className='alpha50 alpha100' />
          <div>
            <a
              href=''
              onClick={e => {
                e.preventDefault()
                props.changeLogoutToggle(true)
              }}>
              Logout
            </a>
          </div>
        </li>
      </ul>
    </div>
  )
}

export default UserMenu
