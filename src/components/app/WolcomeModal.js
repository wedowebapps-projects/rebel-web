import React from 'react'
import { showWelcomeModal } from 'src/utils/storageUtils'
import { Modal } from './index'
import images from 'src/config/images'

function WelcomeModal() {
  const [showModal, setShowModal] = React.useState(false)

  React.useEffect(() => {
    setShowModal(showWelcomeModal.get() == 'true' ? true : false)
  }, [])

  const onclose = e => {
    e.preventDefault()
    showWelcomeModal.set(false)
    setShowModal(false)
  }

  if (!showModal) return null

  return (
    <Modal show={showModal} keyboard={true} size='lg' onClose={onclose}>
      <div className='welcome-modal'>
        <div className='text-center'>
          <div className='say-hello'>
            <img src={images.app.hand} alt='' />
          </div>
          <div className='colse-welcome-modal' onClick={onclose}>
            <img src={images.app.cross} alt='' />
          </div>
          <p>Welcome to Rebelyus! you can find amazing content here you can also add bank details to become a content creator.</p>
        </div>
      </div>
    </Modal>
  )
}

export default WelcomeModal
