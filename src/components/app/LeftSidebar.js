import React from 'react'
import { useRouter } from 'next/router'
import images from 'src/config/images'
import Link from 'next/link'
import { getUserToken } from 'src/utils/storageUtils'

function LeftSidebar(props) {
  const token = getUserToken()
  const router = useRouter()
  const path = router.asPath

  return (
    <div className='min-vh-100 p-0 left-sidebar-wrap bg-color-white sticky-side'>
      <div className='left-sidebar'>
        <nav>
          <ul>
            <li className={`${path === '/' ? 'active' : ''}`}>
              <Link href={`${token ? '/' : '/login'}`} as={`${token ? '/' : '/login'}`}>
                <a>
                  <span className='menu-icon-group'>
                    <img src={images.sidebar.feed} alt='Feed_icon' />
                    <img src={images.sidebar.feedActive} alt='Message_icon' />
                  </span>
                  Feed
                </a>
              </Link>
            </li>
            <li className={`${path.includes('/chat') ? 'active' : ''}`}>
              <Link href={`${token ? '/chat' : '/login'}`} as={`${token ? '/chat' : '/login'}`}>
                <a>
                  <span className='menu-icon-group'>
                    <img src={images.sidebar.message} alt='Message_icon' />
                    <img src={images.sidebar.messageActive} alt='Message_icon' />
                  </span>
                  Messages
                  {props?.chatUnreadCount > 0 && <span className='badge-custom'>{props.chatUnreadCount}</span>}
                </a>
              </Link>
            </li>
            <li className={`${path.includes('/wallet/') ? 'active' : ''}`}>
              <Link href={`${token ? '/wallet' : '/login'}`} as={`${token ? '/wallet/bank-application' : '/login'}`}>
                <a>
                  <span className='menu-icon-group'>
                    <img src={images.sidebar.wallet} alt='Wallet_icon' />
                    <img src={images.sidebar.walletActive} alt='Wallet_icon' />
                  </span>
                  Bank
                </a>
              </Link>
            </li>
            <li className={`${path === '/bookmarks' ? 'active' : ''}`}>
              <Link href={`${token ? '/bookmarks' : '/login'}`} as={`${token ? '/bookmarks' : '/login'}`}>
                <a>
                  <span className='menu-icon-group'>
                    <img src={images.sidebar.bookmark} alt='Bookmark_icon' />
                    <img src={images.sidebar.bookmarkActive} alt='Bookmark_icon' />
                  </span>
                  Bookmarks
                </a>
              </Link>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  )
}

export default LeftSidebar
