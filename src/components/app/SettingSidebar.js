import Link from 'next/link'
import { useRouter } from 'next/router'
import React from 'react'
import images from 'src/config/images'

const SettingSidebar = () => {
  const router = useRouter()

  return (
    <>
      <div className={`min-vh-100 w-100 d-none d-sm-none d-md-none d-lg-none d-xl-none ${router.asPath === '/settings' ? 'account-settings  d-sm-block' : ''} account-settings-mobile`}>
        <div className='settings-col border-wrap m-4'>
          <div className='h-100'>
            <div className='topper-title'>
              <div className='settings-menu mt-4 ml-4'>
                <ul className='tabs'>
                  <Link href='/settings/your-account' to='/settings/your-account'>
                    <li className={router.asPath.includes('/settings/your-account') ? 'current' : ''} data-tab='tab-1'>
                      <a href='/settings/your-account'>
                        Your account
                        <span className='angle-right'>
                          <img src={images.settings.angleRight} alt='' />
                        </span>
                      </a>
                    </li>
                  </Link>
                  <Link href='/settings/change-password' to='/settings/change-password'>
                    <li className={router.asPath.includes('/settings/change-password') ? 'current' : ''} data-tab='tab-2'>
                      <a href='/settings/change-password'>
                        Change password
                        <span className='angle-right'>
                          <img src={images.settings.angleRight} alt='' />
                        </span>
                      </a>
                    </li>
                  </Link>
                  <Link href='/settings/accountPrivacy' to='/settings/accountPrivacy'>
                    <li className={router.asPath.includes('/settings/accountPrivacy') ? 'current' : ''} data-tab='tab-3'>
                      <a href='/settings/accountPrivacy'>
                        Account privacy
                        <span className='angle-right'>
                          <img src={images.settings.angleRight} alt='' />
                        </span>
                      </a>
                    </li>
                  </Link>
                  <Link href='/settings/subscription-prices' to='/settings/subscription-prices'>
                    <li className={router.asPath.includes('/settings/subscription-prices') ? 'current' : ''} data-tab='tab-7'>
                      <a href='/settings/subscription-prices'>
                        Set pricing
                        <span className='angle-right'>
                          <img src={images.settings.angleRight} alt='' />
                        </span>
                      </a>
                    </li>
                  </Link>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className='min-vh-100 w-100 settings-col border-wrap m-4 sticky-side d-none d-sm-none d-md-block d-lg-block d-xl-block'>
        <div className='h-100'>
          <div className='topper-title border-wrap-bottom '>
            <div className='settings-title'>
              <h3 className='desktop-show'>Settings</h3>
            </div>
          </div>
          <div className='topper-title'>
            <div className='settings-menu mt-4 ml-4'>
              <ul className='tabs'>
                <Link href='/settings/your-account' to='/settings/your-account'>
                  <li className={router.asPath.includes('/settings/your-account') ? 'current' : ''} data-tab='tab-1'>
                    <a href='/settings/your-account'>Your account</a>
                  </li>
                </Link>
                <Link href='/settings/change-password' to='/settings/change-password'>
                  <li className={router.asPath.includes('/settings/change-password') ? 'current' : ''} data-tab='tab-2'>
                    <a href='/settings/change-password'>Change password</a>
                  </li>
                </Link>
                <Link href='/settings/accountPrivacy' to='/settings/accountPrivacy'>
                  <li className={router.asPath.includes('/settings/accountPrivacy') ? 'current' : ''} data-tab='tab-3'>
                    <a href='/settings/accountPrivacy'>Account privacy</a>
                  </li>
                </Link>
                <Link href='/settings/subscription-prices' to='/settings/subscription-prices'>
                  <li className={router.asPath.includes('/settings/subscription-prices') ? 'current' : ''} data-tab='tab-7'>
                    <a href='/settings/subscription-prices'>Set pricing</a>
                  </li>
                </Link>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default SettingSidebar
