import { useRouter } from 'next/router'
import React from 'react'
import images from 'src/config/images'
import { Modal } from '.'
import { Spinner } from '../shared'

const LogoutModal = props => {
  const router = useRouter()
  const [loading, setLoading] = React.useState(false)
  const clickLogout = () => {
    setLoading(true)
    props
      .logoutUser()
      .then(() => {
        // setLoading(false)
        router.push('/login')
      })
      .catch(err => {
        setLoading(false)
      })
  }
  return (
    <Modal
      keyboard={true}
      size='lg'
      show={props.isLogoutModalOpen}
      onClose={() => {
        !loading && props.changeLogoutToggle()
      }}>
      <div className='account-popups'>
        <div className='modal-wrp text-center'>
          <div className='modal-inner-wrap text-center'>
            <button
              className='popupclose'
              onClick={() => {
                !loading && props.changeLogoutToggle(false)
              }}>
              <img src={images.app.popupClose} />
            </button>
            <h4>Are you sure you want to logout?</h4>
            <div className='logout-input-wrap mt-3'>
              <div className='input-box'>
                <button className='custom-button primary-bg-color logout-button mr-1' disabled={loading} onClick={clickLogout}>
                  Yes, logout {loading && <Spinner color='#2A9DF4' size={30} />}
                </button>
                <button
                  className='custom-button cancel-button ml-1'
                  disabled={loading}
                  onClick={e => {
                    e.preventDefault()
                    props.changeLogoutToggle(false)
                  }}>
                  Cancel
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Modal>
  )
}

export default LogoutModal
