import styled from 'styled-components';


export const Container = styled.div`
  z-index: 999;
  position: fixed;
  right: 30px;
  top: 50px;
`;

export const StyledToast = styled.div`
  position: relative;
  margin-bottom: 5px;
  width: 300px;
  padding: 15px 20px;
  border-radius: 3px;
  color: #fff;
  ${props => props.type === 'success' && `background: #558B2F;`}
  ${props => props.type === 'error' && `background: #D84315;`}
  ${props => props.type === 'info' && `background: #187BCD;`}
  cursor: pointer;
  transition: all 0.15s;

  &.jira-toast-enter,
  &.jira-toast-exit.jira-toast-exit-active {
    opacity: 0;
    right: -10px;
  }

  &.jira-toast-exit,
  &.jira-toast-enter.jira-toast-enter-active {
    opacity: 1;
    right: 0;
  }
`;

export const CloseIcon = styled.div`
  position: absolute;
  top: 13px;
  right: 14px;
  font-size: 22px;
  cursor: pointer;
  color: #fff;
`;

export const Title = styled.div`
  padding-right: 22px;
  font-size: 16px;
  font-weight: bold;
`;

export const Message = styled.div`
  padding: 8px 10px 0 0;
  white-space: pre-wrap;
  font-size: 14px;
  font-weight: 500;
`;
