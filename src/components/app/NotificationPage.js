import moment from 'moment'
import Link from 'next/link'
import React from 'react'
import { connect } from 'react-redux'
import { clearNotificationsAction, fetchNotificationsAction, readNotificationsAction } from 'src/actions/appActions'
import images from 'src/config/images'
import { Button } from '../shared'

const NotificationPage = props => {
  const [isClearing, setIsClearing] = React.useState(false)
  React.useEffect(() => {
    props.fetchNotifications(),
    props.readNotifications()
  }, [])

  const clearNotification = async e => {
    e.preventDefault()
    try {
      setIsClearing(true)
      await props.clearNotifications()
    } catch (error) {
      console.log(error)
    }
    setIsClearing(false)
  }

  const getNotificationTime = createdAt => {
    let messageTime = ''
    var duration = moment.duration(
      moment().diff(
        moment
          .utc(createdAt)
          .local()
          .format()
      )
    )
    const mins = parseInt(duration.asMinutes())
    const hours = duration.asHours()
    const days = duration.asDays()
    if (days > 1) {
      messageTime = moment
        .utc(createdAt)
        .local()
        .format('DD MMM')
    } else if (hours > 1) {
      messageTime = `${parseInt(hours)} ${parseInt(hours) > 1 ? 'hours' : 'hour'} ago`
    } else if (mins > 1) {
      messageTime = `${mins} ${mins > 1 ? 'mins' : 'min'} ago`
    } else {
      messageTime = 'just now'
    }
    return messageTime
  }
  return (
    <div className='w-100 mt-4 mb-4 ml-4 border-wrap  ac-notifications-col'>
      <div className='notifications-wrap'>
        <div className='border-wrap-bottom'>
          <div className='ac-with-fix notifications-title d-flex align-items-center'>
            <h3>Notifications</h3>
            <Button medium className='ml-auto my-auto' onClick={clearNotification}>
              Clear
            </Button>
          </div>
        </div>
        <div className='ac-with-fix ac-notifications-list-col mb-5'>
          {props?.notifications?.length === 0 && (
            <div className='mt-5 d-flex justify-content-center'>
              <h3>No Older Notifications.</h3>
            </div>
          )}
          {props?.notifications?.map((noti, i) => {
            const content = JSON.parse(noti.content)
            return (
              <React.Fragment key={`notifications-${noti.id}-${i}`}>
                {noti.type === 'follow' && (
                  <Link href='/[username]' as={`/${noti.userId.username}`}>
                    <div className='ac-notifications-list d-flex align-items-center mt-5'>
                      <div className='user-avtar-box mr-5'>
                        <div className='user-avtar avtar-48'>
                          <img src={noti.userId.avatar} alt='User_Avtar' />
                        </div>
                        <div className='notifications-msg'>
                          <span className='red-bg'>
                            <img src={images.app.notificationIcon} />
                          </span>
                        </div>
                      </div>
                      <div className='notifications-list-info'>
                        <p>
                          {noti.userId.username} started following you. <span className='notifications-time ml-4'>{getNotificationTime(noti.createdAt)}</span>
                        </p>
                      </div>
                    </div>
                  </Link>
                )}
                {noti.type === 'like' && (
                  <Link href='/[username]' as={`/${noti.userId.username}`}>
                    <div className='ac-notifications-list d-flex align-items-center mt-5'>
                      <div className='user-avtar-box mr-5'>
                        <div className='user-avtar avtar-48'>
                          <img src={noti.userId.avatar} alt='User_Avtar' />
                        </div>
                        <div className='notifications-msg'>
                          <span className='purple-bg'>
                            <img src={images.app.notificationHeart} />
                          </span>
                        </div>
                      </div>
                      <div className='notifications-list-info'>
                        <p>
                          {noti.userId.username} liked your post <span className='notifications-time ml-4'>{getNotificationTime(noti.createdAt)}</span>
                        </p>
                      </div>
                      {content?.post?.post_media[0] && (
                        <div className='notifications-list-img ml-auto'>
                          <img src={content?.post?.post_media[0]?.media_path} alt='notifications-img' />
                        </div>
                      )}
                    </div>
                  </Link>
                )}
                {noti.type === 'comment' && (
                  <Link href='/[username]' as={`/${noti.userId.username}`}>
                    <div className='ac-notifications-list d-flex align-items-center mt-5'>
                      <div className='user-avtar-box mr-5'>
                        <div className='user-avtar avtar-48'>
                          <img src={noti.userId.avatar} alt='User_Avtar' />
                        </div>
                        <div className='notifications-msg'>
                          <span className='purple-bg'>
                            <img src={images.app.notificationChat} />
                          </span>
                        </div>
                      </div>
                      <div className='notifications-list-info'>
                        <p>
                          {noti.userId.username} commented your post <span className='notifications-time ml-4'>{getNotificationTime(noti.createdAt)}</span>
                        </p>
                      </div>
                      {content?.post?.post_media[0] && (
                        <div className='notifications-list-img ml-auto'>
                          <img src={content?.post?.post_media[0]?.media_path} alt='notifications-img' />
                        </div>
                      )}
                    </div>
                  </Link>
                )}
                {noti.type === 'post' && (
                  <Link href='/[username]' as={`/${noti.userId.username}`}>
                    <div className='ac-notifications-list d-flex align-items-center mt-5'>
                      <div className='user-avtar-box mr-5'>
                        <div className='user-avtar avtar-48'>
                          <img src={noti.userId.avatar} alt='User_Avtar' />
                        </div>
                        <div className='notifications-msg'>
                          <span className='purple-bg'>
                            <img src={images.app.notificationIcon} />
                          </span>
                        </div>
                      </div>
                      <div className='notifications-list-info'>
                        <p>
                          {noti.userId.username} added a new post <span className='notifications-time ml-4'>{getNotificationTime(noti.createdAt)}</span>
                        </p>
                      </div>
                      {content?.post?.post_media[0] && (
                        <div className='notifications-list-img ml-auto'>
                          <img src={content?.post?.post_media[0]?.media_path} alt='notifications-img' />
                        </div>
                      )}
                    </div>
                  </Link>
                )}
                {noti.type === 'subscribe' && (
                  <Link href='/[username]' as={`/${noti.userId.username}`}>
                    <div className='ac-notifications-list d-flex align-items-center mt-5'>
                      <div className='user-avtar-box mr-5'>
                        <div className='user-avtar avtar-48'>
                          <img src={noti.userId.avatar} alt='User_Avtar' />
                        </div>
                        <div className='notifications-msg'>
                          <span className='purple-bg'>
                            <img src={images.app.notificationIcon} />
                          </span>
                        </div>
                      </div>
                      <div className='notifications-list-info'>
                        <p>
                          {noti.userId.username} subscribed to your post <span className='notifications-time ml-4'>{getNotificationTime(noti.createdAt)}</span>
                        </p>
                      </div>
                      {content?.post?.post_media[0] && (
                        <div className='notifications-list-img ml-auto'>
                          <img src={content?.post?.post_media[0]?.media_path} alt='notifications-img' />
                        </div>
                      )}
                    </div>
                  </Link>
                )}
                {noti.type === 'tip' && (
                  <Link href='/[username]' as={`/${noti.userId.username}`}>
                    <div className='ac-notifications-list d-flex align-items-center mt-5'>
                      <div className='user-avtar-box mr-5'>
                        <div className='user-avtar avtar-48'>
                          <img src={noti.userId.avatar} alt='User_Avtar' />
                        </div>
                        <div className='notifications-msg'>
                          <span className='purple-bg'>
                            <img src={images.app.notificationCurrency} />
                          </span>
                        </div>
                      </div>
                      <div className='notifications-list-info'>
                        <p>
                          {noti.userId.username} sent tip for your post of <span className='green-color'>NGN {content?.amount}</span> <span className='notifications-time ml-4'>{getNotificationTime(noti.createdAt)}</span>
                        </p>
                      </div>
                      {content?.post?.post_media[0] && (
                        <div className='notifications-list-img ml-auto'>
                          <img src={content?.post?.post_media[0]?.media_path} alt='notifications-img' />
                        </div>
                      )}
                    </div>
                  </Link>
                )}
                {noti.type === 'user_tip' && (
                  <Link href='/[username]' as={`/${noti.userId.username}`}>
                    <div className='ac-notifications-list d-flex align-items-center mt-5'>
                      <div className='user-avtar-box mr-5'>
                        <div className='user-avtar avtar-48'>
                          <img src={noti.userId.avatar} alt='User_Avtar' />
                        </div>
                        <div className='notifications-msg'>
                          <span className='purple-bg'>
                            <img src={images.app.notificationCurrency} />
                          </span>
                        </div>
                      </div>
                      <div className='notifications-list-info'>
                        <p>
                          {noti.userId.username} sent you a tip of <span className='green-color'>NGN {content?.amount}</span> <span className='notifications-time ml-4'>{getNotificationTime(noti.createdAt)}</span>
                        </p>
                      </div>
                      {content?.post?.post_media[0] && (
                        <div className='notifications-list-img ml-auto'>
                          <img src={content?.post?.post_media[0]?.media_path} alt='notifications-img' />
                        </div>
                      )}
                    </div>
                  </Link>
                )}
              </React.Fragment>
            )
          })}
        </div>
      </div>
    </div>
  )
}

const mapStateToProps = state => ({
  user: state.auth.user,
  notifications: state.app.notifications,
})

const mapDispatchToProps = dispatch => {
  return {
    fetchNotifications: () => dispatch(fetchNotificationsAction()),
    clearNotifications: () => new Promise((resolve, reject) => dispatch(clearNotificationsAction(resolve, reject))),
    readNotifications: () => dispatch(readNotificationsAction())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NotificationPage)
