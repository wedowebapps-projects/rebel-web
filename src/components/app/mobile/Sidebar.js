import React from 'react'
import images from 'src/config/images'

import { UserMedia } from 'src/components/shared'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { getUserToken } from 'src/utils/storageUtils'

function Sidebar(props) {
  const token = getUserToken()
  const router = useRouter()
  const [isDark, setIsDark] = React.useState(false)

  React.useEffect(() => {
    setIsDark(props.theme === 'dark')
  }, [props])

  return (
    <div className='mobile-sidemenu-wrp bg-color-white' style={{ left: props.is_MenuDrawerVisible ? 0 : '-100%' }}>
      <div className='sidebar-menu-header'>
        {token && <UserMedia user={props.user} size={64} />}

        <div className='close-sidemenu' onClick={props.toggleMenu}>
          <img src={images.app.bankCloseIcon} />
        </div>
      </div>
      <ul>
        {token && (
          <Link href='/account' to='/account'>
            <li className={router.asPath.includes('/account') ? 'active' : ''} onClick={() => props.toggleMenu()}>
              <a title='Feed' href='/account'>
                <span className='menu-icon-group'>
                  <img src={images.app.profileSidemenu} alt='Profile' />
                </span>
                <p>Profile</p>
              </a>
            </li>
          </Link>
        )}
        {props.user.personal_info_status === 'approved' && (
          <Link href='/creator-dashboard' to='/creator-dashboard'>
            <li className={router.asPath.includes('/creator-dashboard') ? 'active' : ''} onClick={() => props.toggleMenu()}>
              <a title='Feed' href='/account'>
                <span className='menu-icon-group'>
                  <img src={images.app.combinedShape} alt='Profile' />
                </span>
                <p>Dashboard</p>
              </a>
            </li>
          </Link>
        )}
        <Link href='/contact-us' to='/contact-us'>
          <li>
            <a title='Feed' href='/contact-us'>
              <span className='menu-icon-group'>
                <img src={images.app.support} alt='Support' />
              </span>
              <p>Support</p>
            </a>
          </li>
        </Link>
        {token && (
          <Link href='/settings' to='/settings'>
            <li className={router.asPath.includes('/settings') ? 'active' : ''} onClick={() => props.toggleMenu()}>
              <a title='Feed' href='/settings'>
                <span className='menu-icon-group'>
                  <img src={images.app.Subtract} alt='Settings' />
                </span>
                <p>Settings</p>
              </a>
            </li>
          </Link>
        )}
        <li className='active'>
          <a title='Feed'>
            <span className='menu-icon-group'></span>
            <p />
          </a>
        </li>
        <li className='active'>
          <a title='Feed'>
            <span className='menu-icon-group'></span>
            <p />
          </a>
        </li>
        {token && (
          <li>
            <a
              title='Feed'
              onClick={() => {
                props.toggleMenu()
                props.changeLogoutToggle(true)
              }}>
              <span className='menu-icon-group'>
                <img src={images.app.Logout} alt='Logout' />
              </span>
              <p>Logout</p>
            </a>
          </li>
        )}
        <li>
          <div className='d-flex align-items-center mode-option'>
            <div>
              <img src={images.app.day} alt='Day' />
            </div>
            <div>
              {/* <input type='checkbox' id='toggle-mode-cb' /> */}
              <input
                type='checkbox'
                id='toggle-mode-cb'
                checked={isDark}
                onChange={() => {
                  props.toggleTheme()
                }}
              />
              <div id='mode-wrapper'>
                <label id='toggle-mode' htmlFor='toggle-mode-cb'>
                  <span className='toggle-border'>
                    <span className='toggle-indicator' />
                  </span>
                </label>
              </div>
            </div>
            <div>
              <img src={images.app.night} alt='Night_Mode' />
            </div>
          </div>
        </li>
      </ul>
    </div>
  )
}

export default Sidebar
