import React from 'react'
import { useRouter } from 'next/router'
import Link from 'next/link'
import images from 'src/config/images'
import { getUserToken } from 'src/utils/storageUtils'

function Footer(props) {
  const router = useRouter()
  const token = getUserToken()
  const path = router.asPath

  return (
    <nav className='mobile-footer-menu bg-color-white'>
      <ul>
        <li className={`${path === '/' ? 'active' : ''}`}>
          <Link href={`${token ? '/' : '/login'}`} as={`${token ? '/' : '/login'}`}>
            <a>
              <span className='menu-icon-group'>
                <img src={images.sidebar.feed} alt='Feed_icon' />
                <img src={images.sidebar.feedActive} />
              </span>
            </a>
          </Link>
        </li>
        <li className={`${path.includes('/chat') ? 'active' : ''}`}>
          <Link href={`${token ? '/chat' : '/login'}`} as={`${token ? '/chat' : '/login'}`}>
            <a>
              <span className='menu-icon-group'>
                <img src={images.sidebar.message} />
                <img src={images.sidebar.messageActive} />
              </span>
              {props?.chatUnreadCount > 0 && <span className='badge-custom'>{props.chatUnreadCount}</span>}
            </a>
          </Link>
        </li>
        {props.user.personal_info_status === 'approved' && (
          <li className='play-plus-mobile'>
            <a
              title='Feed'
              onClick={e => {
                e.preventDefault()
                if (props.user.personal_info_status === 'approved') props.toggleNewPostModal()
              }}>
              <span className='menu-icon-group'>
                <img src={images.app.plusRound} />
                <img src={images.app.plusRound} />
              </span>
            </a>
          </li>
        )}
        <li className={`${path === '/wallet' ? 'active' : ''}`}>
          <Link href={`${token ? '/wallet' : '/login'}`} as={`${token ? '/wallet/' : '/login'}`}>
            <a>
              <span className='menu-icon-group'>
                <img src={images.sidebar.wallet} />
                <img src={images.sidebar.walletActive} />
              </span>
            </a>
          </Link>
        </li>
        <li className={`${path === '/bookmarks' ? 'active' : ''}`}>
          <Link href={`${token ? '/bookmarks' : '/login'}`} as={`${token ? '/bookmarks' : '/login'}`}>
            <a>
              <span className='menu-icon-group'>
                <img src={images.sidebar.bookmark} />
                <img src={images.sidebar.bookmarkActive} />
              </span>
            </a>
          </Link>
        </li>
      </ul>
    </nav>
  )
}

export default Footer
