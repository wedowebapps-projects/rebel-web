import React, { useState } from 'react'
import images from 'src/config/images'
import Link from 'next/link'
import { searchUser } from 'store/apis/appApis'
import { Avatar, Button, Form, UserMedia, Spinner } from 'src/components/shared'
import NotificationDrawer from '../header/NotificationDrawer'
import {Dropdown} from 'react-bootstrap'

function MobileHeader(props) {
  const [showSearch, setShowSearch] = useState(false)
  const [isNotificationVisible, setIsNotificationVisible] = useState(false)
  const [isNotificationRead, setIsNotificationRead] = useState(false)
  const [showSearchBox, setShowSearchBox] = useState(false)
  const [showSearchLoading, setShowSearchLoading] = useState(false)
  const [searchResults, setSearchResults] = useState([])
  const [searchText, setSearchText] = useState('')

  const toggleNotification = () => {
    setIsNotificationVisible(!isNotificationVisible)
    if (props?.topBarNotifications.unreadNotification > 0) {
      props.readNotifications()
    }
  }

  const handleSearchInput = async e => {
    if (e.target.value !== '') {
      setShowSearchBox(true)
      setShowSearchLoading(true)
      const result = await searchUser({ searchTerm: e.target.value }, {})
      setShowSearchLoading(false)
      setSearchResults(result.data)
    }
    else {
      setShowSearchBox(false)
      setSearchResults([])
      setSearchText('')
    }
  }

  const NotificationIcon = React.forwardRef(({ onClick }, ref) => (
    <div className='notification-wrap mr-2 cursor-pointer' onClick={(e)=> {e.preventDefault(); onClick(e); setIsNotificationRead(true)}} ref={ref}>
      {props?.topBarNotifications.unreadNotification > 0 && !isNotificationRead && <div className='new-notification' />}
      <img src={images.app.notification} alt='Day' />
    </div>
  ));

  return (
    <header className='mobile-header bg-color-white'>
      <div className='d-flex align-items-center'>
        <div className='menu-icon' onClick={props.toggleMenu}>
          <a href='javascript:;'>
            <img src={images.app.mobileMenu} />
          </a>
        </div>
        <div className='menu-logo' >
          <Link href='/' as='/'>
            <img src={images.app.mobileLogo} style={{ width:'100px' }} />
          </Link>
        </div>
      </div>
      <div className='d-flex'>
        <div className='search-icon'>
          <a
            href='javascript:;'
            onClick={e => {
              e.preventDefault()
              setShowSearch(!showSearch)
            }}>
            <img src={images.app.searchMobile} />
          </a>
        </div>
        <Dropdown>
          <Dropdown.Toggle as={NotificationIcon} id={`dropdown-custom-components`} />
          <Dropdown.Menu className='p-0 border-0'>
            <Dropdown.Item eventKey="1" className='p-0'>
              <NotificationDrawer {...props} isNotificationVisible={isNotificationVisible} toggleNotification={toggleNotification} />
            </Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
      </div>

      {showSearch && (
        <div className='mobile-search bg-color-white' style={{ display: 'block' }}>
          {/* <input type='search' placeholder='Search' /> */}
          <div className='search-input-container'>
            <div className='search-input-wrap'>
              <input type='search' placeholder='Search' onKeyUp={handleSearchInput} value={searchText} onChange={(e)=> setSearchText(e.target.value)}/>
              <img src={images.app.search} />
            </div>
            {showSearchBox && (
              <div className='p-2 search-results-wrap'>
                {showSearchLoading ? (
                  <h6 className='text-center text-muted'>Searching Profiles...</h6>
                ) : searchResults.length > 0 ? (
                  <ul>
                    {searchResults.map(user => (
                      <li className='p-1' onClick={()=>{ setShowSearchBox(false); setSearchResults([]); setSearchText('') }}>
                        <UserMedia show='username' user={user} size={39} />
                      </li>
                    ))}
                  </ul>
                ) : (
                  <h6 className='text-center text-muted'>No Accounts found for this search term</h6>
                )}
              </div>
            )}
          </div>
        </div>
      )}
    </header>
  )
}

export default MobileHeader
