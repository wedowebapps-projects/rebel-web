import React from 'react'
import images from 'src/config/images'
import { addBankCta } from 'src/utils/storageUtils'
import Link from 'next/link'

function AddBankCta() {
  const [showCta, setShowCta] = React.useState(false)

  React.useEffect(() => {
    setShowCta(addBankCta.get() == 'false' ? false : true)
  }, [])

  const onclose = e => {
    e.preventDefault()
    addBankCta.set(false)
    setShowCta(false)
  }

  if (!showCta) return null

  return (
    <div className='add-bank-wrp mb-4'>
      <img src={images.app.addBankImage} />
      <div className='add-bank-content'>
        <h6>
          Add your bank details to <br /> earn as a content creator.
        </h6>
        <Link href='/wallet/bank-application'>
          <a href='#'>
            Get Started
            <img src={images.app.bankArrRight} />
          </a>
        </Link>
      </div>
      <div className='close-bank-info' onClick={e => onclose(e)}>
        <img src={images.app.bankCloseIcon} />
      </div>
    </div>
  )
}

export default AddBankCta
