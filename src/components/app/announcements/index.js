import React from 'react'
import { connect } from 'react-redux'
import { Modal } from 'react-bootstrap'
import { setAnnouncements } from 'store/actions/app/appActions'
import { announcementStorage } from 'utils/storageUtils'

export const Announcements = props => {
  const [dialog, setDialog] = React.useState(false)
  const [announcement, setAnnouncement] = React.useState({})

  React.useEffect(() => {
    const getAnnouncements = async () => {
      if (props.announcements && props.announcements.length) {
        const closedAns = await announcementStorage.get()
        const ans = props.announcements[0]
        if (!closedAns.includes(ans.id)) {
          setDialog(true)
          setAnnouncement(props.announcements[0])
        }
      }
    }
    getAnnouncements()
  }, [props])

  const closeDialog = async () => {
    await announcementStorage.set(announcement.id)
    setDialog(false)
  }

  return (
    <div>
      <Modal show={dialog} size='lg' onHide={closeDialog} centered className='announcement-modal announcement'>
        <Modal.Header className='announcement__header' closeButton>
          <h3 className='announcement__title'>{announcement.title}</h3>
        </Modal.Header>
        <Modal.Body>
          <div className='d-flex mb-4 justify-content-between align-items-center flex-column'>
            {announcement.image_path && announcement.image_path != '' && (
              <div className='mb-4 w-100 announcement__image'>
                <img src={announcement.image_path} className='announcement__image__img' />
              </div>
            )}
            <div className='announcement__desc' dangerouslySetInnerHTML={{ __html: announcement.description }}></div>
          </div>
        </Modal.Body>
      </Modal>
    </div>
  )
}

const mapStateToProps = state => ({
  announcements: state.app.announcements
})

const mapDispatchToProps = {}

export default connect(mapStateToProps, mapDispatchToProps)(Announcements)
