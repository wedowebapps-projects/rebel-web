import React from 'react'
import { announcementStorage } from 'src/utils/storageUtils'
import images from 'src/config/images'
import { Modal } from '.'

const AnnouncementModal = props => {
  const [dialog, setDialog] = React.useState(false)
  const [announcement, setAnnouncement] = React.useState({})

  React.useEffect(() => {
    const getAnnouncements = async () => {
      if (props.announcements && props.announcements.length) {
        const closedAns = await announcementStorage.get()
        const ans = props.announcements[0]
        if (!closedAns.includes(ans.id)) {
          setDialog(true)
          setAnnouncement(props.announcements[0])
        }
      }
    }
    getAnnouncements()
  }, [props])

  const closeDialog = async () => {
    await announcementStorage.set(announcement.id)
    setDialog(false)
  }

  return (
    <Modal show={dialog} size='lg' backdrop='static' onClose={closeDialog}>
      <div className='px-5 py-3'>
        <div className='text-center'>
          <div className='say-hello'>
            <img src={images.app.rebelLock} alt='Rebel' style={{ width:'50px' }} />
          </div>
          <div className='colse-welcome-modal' onClick={closeDialog}>
            <img src={images.app.cross} alt='' />
          </div>
          <h3 className='pb-3'>{announcement.title}</h3>
          <p dangerouslySetInnerHTML={{ __html: announcement.description }}></p>
        </div>
      </div>
    </Modal>
  )
}

export default AnnouncementModal
