import React from 'react'
import images from 'src/config/images.js'

function AppLoading(props) {
  return (
    <div className={props.theme == 'dark' ? 'dark-mode' : ''}>
      <div className='wrapper'>
        <div className='page-content'>
          <section className='desktop-front-bg'>
            <div className='container'>
              <div className='row'>
                <div className='col-12'>
                  <div className='desktop-front-content'>
                    <div className='desktop-front-content-inner'>
                      <div className='desktop-front-content-logo-box'>
                        <img src={images.app.rebelLogo} style={{ width:'70px' }} />
                      </div>
                      {/* <div className='desktop-front-content-progress-box'>
                        <div className='progress-bar-box-inner'>
                          <span />
                        </div>
                      </div> */}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  )
}

export default AppLoading
