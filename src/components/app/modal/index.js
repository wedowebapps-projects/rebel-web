import React from 'react'
import { Modal } from 'react-bootstrap'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

const AppModal = props => {
  // let modalClass = props.theme == 'dark' ? 'dark-mode' : ''
  let modalClass = props.className ? ` ${props.className}` : ''
  let dialogClassName = props.theme == 'dark' ? 'dark-mode' : ''

  return (
    <Modal {...props} size={props.size} onHide={props.onClose} centered className={modalClass} dialogClassName={dialogClassName}>
      {props.showHeader &&
        <Modal.Header closeButton>
          <Modal.Title>{props.modalTitle}</Modal.Title>
        </Modal.Header>
      }
      <Modal.Body>{props.children}</Modal.Body>
      {/* <Modal.Footer>
        <button onClick={props.onClose}>Close</button>
      </Modal.Footer> */}
    </Modal>
  )
}

AppModal.propTypes = {
  size: PropTypes.string,
  onClose: PropTypes.func,
  className: PropTypes.string,
  showHeader: PropTypes.bool,
  modalTitle: PropTypes.modalTitle,
}

AppModal.defaultProps = {
  size: 'lg',
  onClose: () => {},
  className: undefined
}

const mapStateToProps = state => ({
  theme: state.app.theme
})

const mapDispatchToProps = {}

export default connect(mapStateToProps, mapDispatchToProps)(AppModal)
