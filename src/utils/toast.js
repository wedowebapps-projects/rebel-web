import pubsub from 'sweet-pubsub';
import { get } from 'lodash';

const show = toast => {pubsub.emit('toast', toast);}

const success = msg => show({
  type: 'success',
  title: 'Success', 
  message: get(msg, 'message', msg),
  duration: 3,
});

const error = msg => {
  show({
    type: 'error',
    title: 'Error',
    message: get(msg, 'message', msg),
    duration: 3,
  });
};

const info = msg => {
  show({
    type: 'info',
    title: 'Info',
    message: get(msg, 'message', msg),
    duration: 3,
  });
};

export default { show, error, success, info };
