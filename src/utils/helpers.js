import * as firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/messaging'
import AWS from 'aws-sdk'
import Router from 'next/router'
import { getUserToken } from './storageUtils'
import store from 'store'
import { setFCMToken } from 'store/actions/auth/authActions'
import { updateFCMToken_api } from 'store/apis/authApis'
import toast from 'utils/toast'

export const getFirebase = () => {
  try {
    const firebaseConfig = {
      apiKey: process.env.FIREBASE_apiKey,
      authDomain: process.env.FIREBASE_authDomain,
      databaseURL: process.env.FIREBASE_databaseURL,
      projectId: process.env.FIREBASE_projectId,
      storageBucket: process.env.FIREBASE_storageBucket,
      messagingSenderId: process.env.FIREBASE_messagingSenderId,
      measurementId: process.env.FIREBASE_measurementId,
      appId: process.env.FIREBASE_appId
    }
    // Initialize Firebase
    if (!firebase.apps.length) {
      firebase.initializeApp(firebaseConfig)
      if (firebase.messaging.isSupported()) {
        const messaging = firebase.messaging()
        messaging.usePublicVapidKey(process.env.FIREBASE_vapidKey)
      }
    }
    const googleProvider = new firebase.auth.GoogleAuthProvider()
    const facebookProvider = new firebase.auth.FacebookAuthProvider()
    const twitterProvider = new firebase.auth.TwitterAuthProvider()
    return {
      firebase,
      google: googleProvider,
      facebook: facebookProvider,
      twitter: twitterProvider
    }
  } catch (error) {
    console.log(error)
  }
}

export const initialiseAWSConfig = () => {
  AWS.config.update({
    region: process.env.AWS_bucketRegion,
    accessKeyId: process.env.AWS_UserAccessKey,
    secretAccessKey: process.env.AWS_UserSecret
  })
}

// Push Notification
export const getToken = messaging => {
  return new Promise(async (resolve, reject) => {
    try {
      const currentToken = await messaging.getToken()
      if (currentToken) {
        resolve(currentToken)
      } else {
        reject()
      }
    } catch (error) {
      reject(error)
    }
  })
}
export const setupFCM = async () => {
  try {
    const { firebase } = getFirebase()
    const messaging = firebase.messaging()
    var currentToken = await messaging.getToken()
    if (currentToken) {
      store.dispatch(setFCMToken(currentToken))
      updateFCMToken_api({ fcm_token: currentToken })
    }
    messaging.onTokenRefresh(async () => {
      currentToken = await messaging.getToken()
      if (currentToken) {
        store.dispatch(setFCMToken(currentToken))
        updateFCMToken_api({ fcm_token: currentToken })
      }
    })
    messaging.onMessage(payload => {
      console.log('Message received. ', payload)
    })
  } catch (error) {
    console.log(error)
  }
}
// End. Push notification

export const AllowedMediaTypes = ['video/mp4', 'video/x-m4v', 'video/*', 'image/*']
export const VideoMimes = ['video/webm', 'video/mp4', 'video/mpeg', 'video/avi', 'video/quicktime']
export const ImageMimes = ['image/jpeg', 'image/bmp', 'image/jpg', 'image/gif', 'image/webp', 'image/png']
export const constants = {
  COMMENTS_PER_PAGE: 3,
  POSTS_PER_PAGE: 4,
  CHATS_PER_PAGE: 15,
  MOBILE_WIDTH: 576,
  INTERVALS: ['monthly', 'quarterly', 'annually'],
  NAIRA: '₦',
  FETCH_CHAT_LIST_INTERVAL: 25000,
  MIN_WALLET_BALANCE: 45
}

export const convertObjectToQuerystring = obj => {
  return Object.keys(obj)
    .map(key => `${key}=${obj[key]}`)
    .join('&')
}
export const trim = (content, length, suffix = '...') => {
  if (!length || !content) {
    return content
  }
  if (content.length <= length) {
    return content
  }
  return `${content.substr(0, length)}${suffix}`
}
export const scrollToTop = scrollTo => {
  window.scrollTo(0, scrollTo || 0)
  document.body.scrollTo(0, scrollTo || 0)
}

export const fullnameInitials = (first_name, last_name) => {
  const first = first_name.substr(0, 1)
  const last = last_name.substr(0, 1)
  return `${first}${last}`.toUpperCase()
}

export const pageTitles = {
  '/null': '',
  '/': 'Feed',
  '/[username]': 'Profile',
  '/new-post': 'Create Post',
  '/account': 'My Profile',
  '/bookmarked': 'Bookmarked Posts',
  '/chat': 'Chat',
  '/chat/[roomId]': 'Chat',
  editProfile: 'Edit Profile',
  'bank-application': 'Bank Application',
  'bank-detail': 'Connected Bank',
  'subscription-prices': 'Subscription Plans',
  wallet: 'My Wallet',
  changePassword: 'Change Password',
  deleteAccount: 'Delete Account'
}

export const checkAndRedirectLogin = () => {
  const token = getUserToken()
  if (!token) {
    if (Router.router.pathname !== '/[username]' && Router.router.pathname !== '/mypay' && Router.router.pathname !== '/page/[...page]') {
      Router.push(`/login${Router.asPath !== '/login' && Router.asPath !== '/' ? `?return=${Router.asPath}` : ''}`)
    }
    return false
  } else {
    return true
  }
}

export const isValidPrice = price => {
  const pattern = new RegExp(/\d/)
  return pattern.test(price)
}

export const copyToClipboard = url => {
  var textField = document.createElement('textarea')
  textField.innerText = url
  document.body.appendChild(textField)
  textField.select()
  document.execCommand('copy')
  textField.remove()
  toast.info('Copied successfully.')
}

export const numberWithCommas = x => {
  if (x) {
    // return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ',')
    // Integer.parse(x).toString("##,##,###")
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
  } else {
    return 0
  }
}

export const postBgColors = [
  { value: '#E13C3C', label: '#E13C3C' },
  { value: '#b56900', label: '#b56900' },
  { value: '#3C64B1', label: '#3C64B1' },
  { value: '#266d03', label: '#266d03' },
  { value: '#6d035b', label: '#6d035b' },
  { value: '#035b6d', label: '#035b6d' }
]

export const handleExtraChargeAmount = topupAmount => {
  let extraCharge = 0
  if (topupAmount < 2500) {
    extraCharge = (topupAmount * 1.5) / 100
  } else {
    extraCharge = (topupAmount * 1.5) / 100 + 100
  }
  return extraCharge.toFixed(2)
}
