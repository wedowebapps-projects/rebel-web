export const setUserData = data => {
  localStorage.setItem('userData', JSON.stringify(data))
}
export const setUserToken = token => {
  localStorage.setItem('token', `Bearer ${token}`)
}
export const getUserToken = () => {
  return localStorage.getItem('token')
}
export const getUserData = () => {
  return JSON.parse(localStorage.getItem('userData'))
}

export const unsetUserData = () => {
  localStorage.removeItem('userData')
}
export const unsetUserToken = () => {
  localStorage.removeItem('token')
}

export const setRegisterData = data => {
  localStorage.setItem('registerData', JSON.stringify(data))
}
export const getRegisterData = () => {
  return JSON.parse(localStorage.getItem('registerData'))
}
export const removeRegisterData = () => {
  localStorage.removeItem('registerData')
}

export const setTheme = theme => {
  localStorage.setItem('theme', theme)
}
export const getTheme = () => {
  return localStorage.getItem('theme')
}
export const removeTheme = () => {
  localStorage.removeItem('theme')
}

export const addBankCta = {
  set: val => {
    localStorage.setItem('addBankCta', val)
  },
  get: () => {
    return localStorage.getItem('addBankCta')
  }
}

export const showWelcomeModal = {
  set: val => {
    localStorage.setItem('welcomeModal', val)
  },
  get: () => {
    return localStorage.getItem('welcomeModal')
  }
}

export const announcementStorage = {
  set: async val => {
    let data = await announcementStorage.get()
    if(!data.includes(val)) {
      data.push(val)
      localStorage.setItem('announcements', JSON.stringify(data))
    }
  },
  get: () => {
    return localStorage.getItem('announcements') ? JSON.parse(localStorage.getItem('announcements')) : []
  }
}