import * as appTypes from './appTypes'

export const fetchFeaturedCreators = filters => ({ type: appTypes.FETCH_FEATURED_CREATORS, filters })
export const setFeaturedCreators = data => ({ type: appTypes.SET_FEATURED_CREATORS, data })

export const fetchCountries = () => ({ type: appTypes.FETCH_COUNTRIES })
export const setCountries = data => ({ type: appTypes.SET_COUNTRIES, data })

export const fetchCommissions = () => ({ type: appTypes.FETCH_COMMISSIONS })
export const setCommissions = data => ({ type: appTypes.SET_COMMISSIONS, data })

export const toggleNewPostModal = () => ({ type: appTypes.TOGGLE_NEW_POST_MODAL })
export const toggleTheme = () => ({ type: appTypes.TOGGLE_THEME })
export const setTheme = theme => ({ type: appTypes.SET_THEME, theme })
export const toggleMenu = () => ({ type: appTypes.TOGGLE_MENU_DRAWER })
export const appLoading = loading => ({ type: appTypes.SET_APP_LOADING, loading })
export const closeAllDrawers = () => ({ type: appTypes.CLOSE_DRAWERS })

// export const fetchCommissionAction = () => ({
//   type: appTypes.FETCH_COMMISSION
// })
// export const setCommissionAction = data => ({
//   type: appTypes.SET_COMMISSION,
//   data
// })

export const fetchReportReasons = () => ({
  type: appTypes.FETCH_REPORT_REASONS
})
export const setReportReasons = reasons => ({
  type: appTypes.SET_REPORT_REASONS,
  reasons
})

// Notification
export const toggleNotificationDrawerAction = () => ({ type: appTypes.TOGGLE_NOTIFICATION_DRAWER })

export const fetchNotificationsAction = () => ({
  type: appTypes.FETCH_NOTIFICATION
})
export const setNotificationsAction = data => ({
  type: appTypes.SET_NOTIFICATION,
  data
})
export const fetchTopBarNotificationsAction = () => ({
  type: appTypes.FETCH_TOP_BAR_NOTIFICATION
})
export const setTopBarNotificationsAction = data => ({
  type: appTypes.SET_TOP_BAR_NOTIFICATION,
  data
})
export const clearNotificationsAction = (resolve, reject) => ({
  type: appTypes.CLEAR_NOTIFICATION,
  resolve,
  reject
})
export const readNotificationsAction = () => ({
  type: appTypes.READ_NOTIFICATION
})
export const setReadNotificationsAction = () => ({
  type: appTypes.SET_READ_NOTIFICATION
})

// CONTACT US
export const sendContactUsAction = (data, resolve, reject) => {
  return {
    type: appTypes.SEND_CONTACT_US,
    data,
    resolve,
    reject
  }
}

export const fetchAnnouncements = () => ({
  type: appTypes.FETCH_ANNOUNCEMENTS
})
export const setAnnouncements = data => ({
  type: appTypes.SET_ANNOUNCEMENTS,
  data
})
