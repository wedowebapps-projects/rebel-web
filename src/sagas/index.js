import { all } from 'redux-saga/effects'
import authSagas from 'src/modules/auth/sagas/authSagas'
import AppSagas from './AppSagas'
import postSagas from 'src/modules/post/sagas/postSagas'
import profileSagas from 'src/modules/profile/sagas/profileSagas'
import chatSagas from 'src/modules/chat/sagas/chatSagas'
import bookmarkSagas from 'src/modules/profile/sagas/bookmarkSagas'
import dashboardSagas from 'src/modules/profile/sagas/dashboardSagas'

function* rootSaga() {
  yield all([AppSagas(), authSagas(), postSagas(), profileSagas(), bookmarkSagas(), chatSagas(), dashboardSagas()])
}

export default rootSaga
