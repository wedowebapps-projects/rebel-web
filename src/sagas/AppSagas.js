import { takeLatest, all, fork, call, put, select } from 'redux-saga/effects'
import * as appTypes from '../actions/appTypes'
import * as appActions from '../actions/appActions'
import * as appApis from '../api/appApis'
import { setTheme } from 'src/utils/storageUtils'

export const getTheme = state => state.app.theme

function* toggleTheme() {
  try {
    let theme = yield select(getTheme)
    yield call(setTheme, theme)
  } catch (error) {
    console.log('Errors', error)
  }
}

function* fetchFeaturedCreators({ filters }) {
  try {
    const data = yield call(appApis.getHomeFeaturedAccounts_Api, filters)
    yield put(appActions.setFeaturedCreators(data.users))
  } catch (error) {
    console.log('Error', error)
  }
}

function* fetchCommissions() {
  try {
    const data = yield call(appApis.fetchCommission_api)
    yield put(appActions.setCommissions(data.fees))
  } catch (error) {
    console.log('Error', error)
  }
}

function* fetchCountries() {
  try {
    const data = yield call(appApis.getCountryList_api)
    yield put(appActions.setCountries(data.data))
  } catch (error) {
    console.log('Error', error)
  }
}

function* fetchReportReasons() {
  try {
    const result = yield call(appApis.fetchReportReasons_api)
    yield put(appActions.setReportReasons(result.reasons))
  } catch (error) {
    console.log('Errors', error)
  }
  yield {}
}
//notification
function* fetchNotificationsSaga() {
  try {
    const response = yield call(appApis.fetchNotifications_api)
    yield put(appActions.setNotificationsAction(response.notifications))
  } catch (error) {
    console.log(error)
  }
}
function* fetchTopBarNotificationsSaga() {
  try {
    const response = yield call(appApis.fetchTopBarNotifications_api)
    yield put(appActions.setTopBarNotificationsAction(response.data))
  } catch (error) {
    console.log(error)
  }
}
function* clearNotificationsSaga({ resolve, reject }) {
  try {
    yield call(appApis.clearhNotifications_api)
    yield put(appActions.setNotificationsAction([]))
    yield put(appActions.setTopBarNotificationsAction({
      rows: [],
      unreadNotification: 0,
    }))
    resolve()
  } catch (error) {
    console.log(error)
    reject()
  }
}
function* readNotificationsSaga() {
  try {
    yield call(appApis.readNotifications_api)
    yield put(appActions.setReadNotificationsAction())
  } catch (error) {
    console.log(error)
  }
}

function* contactUsSaga({ data, resolve, reject }) {
  try {
    const result = yield call(appApis.sendContactUs_api, data)
    resolve(result)
  } catch (error) {
    reject(error)
  }
}

function* fetchAnnouncements() {
  try {
    const result = yield call(appApis.fetchAnnouncements_api)
    yield put(appActions.setAnnouncements(result.announcements))
  } catch (error) {
    console.log('Errors', error)
  }
}

export function* watchSagas() {
  yield takeLatest(appTypes.TOGGLE_THEME, toggleTheme)
  yield takeLatest(appTypes.FETCH_FEATURED_CREATORS, fetchFeaturedCreators)
  yield takeLatest(appTypes.FETCH_REPORT_REASONS, fetchReportReasons)
  // yield takeLatest(appTypes.FETCH_COMMISSION, fetchCommissionSaga)
  yield takeLatest(appTypes.FETCH_COMMISSIONS, fetchCommissions)
  yield takeLatest(appTypes.FETCH_COUNTRIES, fetchCountries)
  yield takeLatest(appTypes.FETCH_NOTIFICATION, fetchNotificationsSaga)
  yield takeLatest(appTypes.FETCH_TOP_BAR_NOTIFICATION, fetchTopBarNotificationsSaga)
  yield takeLatest(appTypes.READ_NOTIFICATION, readNotificationsSaga)
  yield takeLatest(appTypes.CLEAR_NOTIFICATION, clearNotificationsSaga)
  yield takeLatest(appTypes.SEND_CONTACT_US, contactUsSaga)
  yield takeLatest(appTypes.FETCH_ANNOUNCEMENTS, fetchAnnouncements)
}

export default function* runSagas() {
  yield all([fork(watchSagas)])
}
