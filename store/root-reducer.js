import { combineReducers } from 'redux'
import authReducer from 'store/reducer/auth/authReducer'
import appReducer from 'store/reducer/app/appReducer'
import postReducer from 'store/reducer/post/postReducer'
import profileReducer from 'store/reducer/profile/profileReducer'
import chatReducer from 'store/reducer/chat/chatReducer'

export default combineReducers({
  auth: authReducer,
  app: appReducer,
  posts: postReducer,
  profile: profileReducer,
  chat: chatReducer,
})
