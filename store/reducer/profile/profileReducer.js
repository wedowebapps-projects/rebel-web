import * as types from 'store/actions/profile/profileTypes'
import { constants } from 'utils/helpers'
const initialState = {
  profilePosts: {
    showLoader: false,
    list: [],
    perPage: 4,
    page: 1
  }
}
const profileReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.TOGGLE_PROFILE_POSTS_LOADER: {
      return {
        ...state,
        profilePosts: {
          ...state.profilePosts,
          showLoader: action.status
        }
      }
    }
    case types.SET_PROFILE_POSTS: {
      const list = state.profilePosts.list
      const oldPostsIDs = list.map(post => post.id)
      const newPosts = action.data.filter(post => !oldPostsIDs.includes(post.id))
      // Add pagination on each post
      for (const i in newPosts) {
        newPosts[i].commentsFilter = {
          page: 1,
          perPage: constants.COMMENTS_PER_PAGE,
          showLoadMoreButton: false
        }
      }
      return {
        ...state,
        profilePosts: {
          ...state.profilePosts,
          list: [...list, ...newPosts],
          showLoader: false,
          page: newPosts.length > 0 ? state.profilePosts.page + 1 : state.profilePosts.page
        }
      }
    }
    case types.RESET_PROFILE_POSTS: {
      return {
        ...state,
        profilePosts: initialState.profilePosts
      }
    }
    case types.SET_LIKE_UNLIKE_COMMENT_PROFILE_POST: {
      const list = state.profilePosts.list
      const postIndex = list.map(post => post.id).indexOf(action.data.postId)
      const post = list[postIndex]
      if (!action.data.parent) {
        // Comment:
        const commentIndex = post.comments.map(c => c.id).indexOf(action.data.commentId)
        post.comments[commentIndex].isLiked = action.data.isLiked
        const likesCount = post.comments[commentIndex].likesCount
        post.comments[commentIndex].likesCount = action.data.isLiked ? likesCount + 1 : likesCount - 1
      } else {
        // Reply:
        const commentIndex = post.comments.map(c => c.id).indexOf(action.data.parent)
        const replyIndex = post.comments[commentIndex].replies.map(c => c.id).indexOf(action.data.commentId)
        post.comments[commentIndex].replies[replyIndex].isLiked = action.data.isLiked
        const likesCount = post.comments[commentIndex].replies[replyIndex].likesCount
        post.comments[commentIndex].replies[replyIndex].likesCount = action.data.isLiked ? likesCount + 1 : likesCount - 1
      }
      list[postIndex] = post
      return {
        ...state,
        profilePosts: {
          ...state.profilePosts,
          list
        }
      }
    }
    case types.LIKE_BOOKMARK_PROFILE_POST: {
      const list = state.profilePosts.list
      const postIndex = list.map(post => post.id).indexOf(action.post.id)
      list[postIndex] = action.post
      return {
        ...state,
        profilePosts: {
          ...state.profilePosts,
          list
        }
      }
    }
    case types.ADD_COMMENT_TO_PROFILE_POST: {
      const list = state.profilePosts.list
      const postIndex = list.map(post => post.id).indexOf(action.data.post_id)
      const post = list[postIndex]
      if (action.data.parent_id === 0) {
        // Main comment:
        post.comments.unshift(action.data)
        post.commentsCount = post.commentsCount + 1
      } else {
        // Reply:
        const commentIndex = post.comments.map(c => c.id).indexOf(action.data.parent_id)
        post.comments[commentIndex].replies.push(action.data)
        post.comments[commentIndex].replyCount = post.comments[commentIndex].replyCount + 1
      }
      list[postIndex] = post
      return {
        ...state,
        profilePosts: {
          ...state.profilePosts,
          list
        }
      }
    }
    case types.DELETE_COMMENT_TO_PROFILE_POST: {
      const list = state.profilePosts.list
      const postIndex = list.map(post => post.id).indexOf(action.post_id)
      const post = list[postIndex]
      if (!action.parent_id) {
        const commentIndex = post.comments.map(c => c.id).indexOf(action.comment_id)
        post.comments.splice(commentIndex, 1)
      } else {
        const commentIndex = post.comments.map(c => c.id).indexOf(action.parent_id)
        const replies = post.comments[commentIndex].replies
        const replyIndex = replies.map(c => c.id).indexOf(action.comment_id)
        replies.splice(replyIndex, 1)
        post.comments[commentIndex].replies = replies
      }
      list[postIndex] = post
      return {
        ...state,
        profilePosts: {
          ...state.profilePosts,
          list
        }
      }
    }
    case types.SET_PROFILE_POST_COMMENTS: {
      const list = state.profilePosts.list
      const postIndex = list.map(post => post.id).indexOf(action.post_id)
      list[postIndex].commentsCount = action.totalComments
      if (list[postIndex].commentsFilter.page === 1) {
        list[postIndex].comments = []
      }
      for (const c of action.data) {
        // Add reply pagination filters to each comment
        c.commentsFilter = {
          page: c.replyCount > constants.COMMENTS_PER_PAGE ? 2 : 1,
          perPage: constants.COMMENTS_PER_PAGE,
          showLoadMoreButton: c.replyCount > constants.COMMENTS_PER_PAGE
        }
        list[postIndex].comments.push(c)
      }
      const { commentsFilter } = list[postIndex]
      list[postIndex].commentsFilter = {
        ...commentsFilter,
        page: action.data.length > 0 ? commentsFilter.page + 1 : commentsFilter.page,
        showLoadMoreButton: action.data.length === commentsFilter.perPage
      }

      return {
        ...state,
        profilePosts: {
          ...state.profilePosts,
          list
        }
      }
    }
    case types.RESET_COMMENTS_PAGINATION_PROFILE: {
      const list = state.profilePosts.list
      const postIndex = list.map(post => post.id).indexOf(action.post_id)
      list[postIndex].comments = list[postIndex].comments.filter((p, i) => i < 3)
      list[postIndex].commentsFilter = {
        page: 1,
        perPage: constants.COMMENTS_PER_PAGE,
        showLoadMoreButton: false
      }
      return {
        ...state,
        profilePosts: {
          ...state.profilePosts,
          list
        }
      }
    }
    case types.SET_REPLIES_PROFILE: {
      const list = state.profilePosts.list
      const postIndex = list.map(post => post.id).indexOf(action.post_id)
      const post = list[postIndex]
      const commentIndex = post.comments.map(c => c.id).indexOf(action.comment_id)
      const comment = post.comments[commentIndex]
      if (comment.commentsFilter.page === 1) {
        comment.replies = []
      }
      for (const c of action.data) {
        comment.replies.push(c)
      }
      const { commentsFilter } = comment
      comment.commentsFilter = {
        ...commentsFilter,
        page: action.data.length > 0 ? commentsFilter.page + 1 : commentsFilter.page,
        showLoadMoreButton: action.data.length === commentsFilter.perPage
      }

      comment.replyCount = action.totalComments

      post.comments[commentIndex] = comment
      list[postIndex] = post

      return {
        ...state,
        profilePosts: {
          ...state.profilePosts,
          list
        }
      }
    }
    case types.SET_PURCHASE_POST_PROFILE: {
      const list = state.profilePosts.list
      const postIndex = list.map(post => post.id).indexOf(action.post_id)
      list[postIndex].isPaid = true
      return {
        ...state,
        profilePosts: {
          ...state.profilePosts,
          list
        }
      }
    }
    case types.SET_SENDTIP_POST_PROFILE: {
      const list = state.profilePosts.list
      const postIndex = list.map(post => post.id).indexOf(action.post_id)
      list[postIndex].isTipsent = true
      return {
        ...state,
        profilePosts: {
          ...state.profilePosts,
          list
        }
      }
    }
    case types.SET_DELETED_PROFILE_POSTS: {
      const list = state.profilePosts.list
      const postIndex = list.map(post => post.id).indexOf(action.post_id)
      list.splice(postIndex, 1)
      return {
        ...state,
        profilePosts: {
          ...state.profilePosts,
          list
        }
      }
    }
    default:
      return state
  }
}

export default profileReducer
