import * as authTypes from 'store/actions/auth/authTypes'
import moment from 'moment'

const initialState = {
  userProfile: {
    is_fcm_loading: false,
    fcm_token: null
  },
  isLoggedIn: false,
  authEmail: null,
  token: null,
  socialData: null,
  subscriptionPrices: [],
}

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case authTypes.STORE_AUTH_EMAIL:
      return {
        ...state,
        authEmail: action.email
      }
    case authTypes.STORE_AUTHENTICATION:
      return {
        ...state,
        authEmail: null,
        isLoggedIn: true,
        userProfile: {
          ...state.userProfile,
          ...action.data.user
        },
        token: action.data.token
      }
    case authTypes.SET_SOCIAL_DATA:
      return {
        ...state,
        socialData: action.data
      }
    case authTypes.SET_MY_PROFILE:
      return {
        ...state,
        userProfile: {
          ...state.userProfile,
          ...action.data
        }
      }
    case authTypes.SET_BANK_APPLICATION:
      return {
        ...state,
        userProfile: {
          ...state.userProfile,
          personal_info: action.data
        }
      }
    case authTypes.SET_SUBSCRIPTION_PRICE:
      return {
        ...state,
        subscriptionPrices: action.reset ? action.data : [...state.subscriptionPrices, ...action.data]
      }
    case authTypes.UPDATE_SUBSCRIPTION_PRICE: {
      const subscriptionPrices = state.subscriptionPrices
      const planIndex = subscriptionPrices.map(p => p.id).indexOf(action.data.id)
      subscriptionPrices[planIndex] = action.data
      return {
        ...state,
        subscriptionPrices
      }
    }
    case authTypes.SET_FCM_LOADED: {
      return {
        ...state,
        userProfile: {
          ...state.userProfile,
          is_fcm_loaded: action.status
        }
      }
    }
    case authTypes.SET_FCM_TOKEN: {
      return {
        ...state,
        userProfile: {
          ...state.userProfile,
          fcm_token: action.token
        }
      }
    }
    case authTypes.RESET_AUTHENTICATION:
      return initialState
    default:
      return state
  }
}

export default authReducer
