import * as types from 'store/actions/post/postTypes'
import { constants } from 'utils/helpers'

const initialState = {
  notification: {
    show: false
  },
  showLoader: false,
  list: [],
  perPage: 4,
  page: 1,
  purchaseModal: {
    post: {},
    visible: false,
    username: null
  },
  sendTipModal: {
    post: {},
    visible: false,
    username: null
  }
}

const postReducer = (state = initialState, action) => {
  const list = state.list
  switch (action.type) {
    case types.SET_POST_ALERT:
      return {
        ...state,
        notification: { ...state.notification, ...action.data }
      }
    case types.ADD_CREATED_POST:
      // Add pagination on each post
      action.data.commentsFilter = {
        page: 1,
        perPage: constants.COMMENTS_PER_PAGE,
        showLoadMoreButton: false
      }
      return {
        ...state,
        list: [...list, action.data]
      }
    case types.TOGGLE_POSTS_LOADER:
      return {
        ...state,
        showLoader: action.status
      }
    case types.SET_POSTS: {
      const oldPostsIDs = list.map(post => post.id)
      const newPosts = action.data.filter(post => !oldPostsIDs.includes(post.id))
      // Add pagination on each post
      for (const i in newPosts) {
        newPosts[i].commentsFilter = {
          page: 1,
          perPage: constants.COMMENTS_PER_PAGE,
          showLoadMoreButton: false
        }
      }
      return {
        ...state,
        showLoader: state.page === 1 && action.data.length === 0 ? 'noData' : false,
        list: [...list, ...newPosts],
        page: newPosts.length > 0 ? state.page + 1 : state.page
      }
    }
    case types.RESET_POSTS_PAGINATION: {
      return {
        ...state,
        showLoader: false,
        page: 1,
        list: []
      }
    }
    case types.SET_SINGLE_POST: {
      // Add pagination on each post
      action.data.commentsFilter = {
        page: 1,
        perPage: constants.COMMENTS_PER_PAGE,
        showLoadMoreButton: false
      }
      list.push(action.data)
      return {
        ...state,
        list
      }
    }
    case types.SET_LIKE_UNLIKE_COMMENT: {
      const postIndex = list.map(post => post.id).indexOf(action.data.postId)
      const post = list[postIndex]
      if (!action.data.parent) {
        // Comment:
        const commentIndex = post.comments.map(c => c.id).indexOf(action.data.commentId)
        post.comments[commentIndex].isLiked = action.data.isLiked
        const likesCount = post.comments[commentIndex].likesCount
        post.comments[commentIndex].likesCount = action.data.isLiked ? likesCount + 1 : likesCount - 1
      } else {
        // Reply:
        const commentIndex = post.comments.map(c => c.id).indexOf(action.data.parent)
        const replyIndex = post.comments[commentIndex].replies.map(c => c.id).indexOf(action.data.commentId)
        post.comments[commentIndex].replies[replyIndex].isLiked = action.data.isLiked
        const likesCount = post.comments[commentIndex].replies[replyIndex].likesCount
        post.comments[commentIndex].replies[replyIndex].likesCount = action.data.isLiked ? likesCount + 1 : likesCount - 1
      }
      list[postIndex] = post
      return {
        ...state,
        list
      }
    }
    case types.LIKE_BOOKMARK_POST: {
      const postIndex = list.map(post => post.id).indexOf(action.post.id)
      list[postIndex] = action.post
      return {
        ...state,
        list
      }
    }
    case types.ADD_COMMENT_TO_POST: {
      const postIndex = list.map(post => post.id).indexOf(action.data.post_id)
      const post = list[postIndex]
      if (action.data.parent_id === 0) {
        // Main comment:
        post.comments.unshift(action.data)
        post.commentsCount = post.commentsCount + 1
      } else {
        // Reply:
        const commentIndex = post.comments.map(c => c.id).indexOf(action.data.parent_id)
        post.comments[commentIndex].replies.push(action.data)
        post.comments[commentIndex].replyCount = post.comments[commentIndex].replyCount + 1
      }
      list[postIndex] = post
      return {
        ...state,
        list
      }
    }
    case types.DELETE_COMMENT_TO_POST: {
      const postIndex = list.map(post => post.id).indexOf(action.post_id)
      const post = list[postIndex]
      if (!action.parent_id) {
        const commentIndex = post.comments.map(c => c.id).indexOf(action.comment_id)
        post.comments.splice(commentIndex, 1)
      } else {
        const commentIndex = post.comments.map(c => c.id).indexOf(action.parent_id)
        const replies = post.comments[commentIndex].replies;
        const replyIndex = replies.map(c => c.id).indexOf(action.comment_id);
        replies.splice(replyIndex, 1)
        post.comments[commentIndex].replies = replies;
      }
      list[postIndex] = post
      return {
        ...state,
        list
      }
    }
    case types.SET_POST_COMMENTS: {
      const postIndex = list.map(post => post.id).indexOf(action.post_id)
      list[postIndex].commentsCount = action.totalComments
      if (list[postIndex].commentsFilter.page === 1) {
        list[postIndex].comments = []
      }
      for (const c of action.data) {
        // Add reply pagination filters to each comment
        c.commentsFilter = {
          page: c.replyCount > constants.COMMENTS_PER_PAGE ? 2 : 1,
          perPage: constants.COMMENTS_PER_PAGE,
          showLoadMoreButton: c.replyCount > constants.COMMENTS_PER_PAGE
        }
        list[postIndex].comments.push(c)
      }
      const { commentsFilter } = list[postIndex]
      list[postIndex].commentsFilter = {
        ...commentsFilter,
        page: action.data.length > 0 ? commentsFilter.page + 1 : commentsFilter.page,
        showLoadMoreButton: action.data.length === commentsFilter.perPage
      }
      return {
        ...state,
        list
      }
    }
    case types.RESET_COMMENTS_PAGINATION: {
      const postIndex = list.map(post => post.id).indexOf(action.post_id)
      if (postIndex !== -1) {
        list[postIndex].comments = list[postIndex].comments.filter((p, i) => i < constants.COMMENTS_PER_PAGE)
        list[postIndex].commentsFilter = {
          page: 1,
          perPage: constants.COMMENTS_PER_PAGE,
          showLoadMoreButton: false
        }
      }
      return {
        ...state,
        list
      }
    }
    case types.SET_REPLIES: {
      const postIndex = list.map(post => post.id).indexOf(action.post_id)
      const post = list[postIndex]
      const commentIndex = post.comments.map(c => c.id).indexOf(action.comment_id)
      const comment = post.comments[commentIndex]
      if (comment.commentsFilter.page === 1) {
        comment.replies = []
      }
      for (const c of action.data) {
        comment.replies.push(c)
      }
      const { commentsFilter } = comment
      comment.commentsFilter = {
        ...commentsFilter,
        page: action.data.length > 0 ? commentsFilter.page + 1 : commentsFilter.page,
        showLoadMoreButton: action.data.length === commentsFilter.perPage
      }

      comment.replyCount = action.totalComments

      post.comments[commentIndex] = comment
      list[postIndex] = post

      return {
        ...state,
        list
      }
    }
    case types.TOGGLE_PURCHASE_POST_MODAL: {
      return {
        ...state,
        purchaseModal: {
          post: action.post || {},
          visible: action.visible,
          username: action.username || null
        }
      }
    }
    case types.SET_PURCHASE_POST: {
      const postIndex = list.map(post => post.id).indexOf(action.post_id)
      list[postIndex].isPaid = true
      return {
        ...state,
        list
      }
    }
    case types.TOGGLE_TIP_POST_MODAL: {
      return {
        ...state,
        sendTipModal: {
          post: action.post || {},
          visible: action.visible,
          username: action.username || null
        }
      }
    }
    case types.SET_SENDTIP_POST: {
      const postIndex = list.map(post => post.id).indexOf(action.post_id)
      list[postIndex].isTipsent = true
      return {
        ...state,
        list
      }
    }
    case types.SET_DELETED_POST: {
      const postIndex = list.map(post => post.id).indexOf(action.post_id)
      list.splice(postIndex, 1)
      return {
        ...state,
        list
      }
    }
    default:
      return state
  }
}
export default postReducer
