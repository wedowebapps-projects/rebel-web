import * as types from 'store/actions/app/appTypes'

const initialState = {
  is_NotificationDrawerVisible: false,
  is_MenuDrawerVisible: false,
  is_SearchDrawerVisible: false,
  featured: [],
  reportReasons: [],
  commission: {},
  announcements: []
}

const appReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_ANNOUNCEMENTS:
      return {
        ...state,
        announcements: action.data
      }
    case types.SET_REPORT_REASONS:
      return {
        ...state,
        reportReasons: action.reasons
      }
    case types.TOGGLE_NOTIFICATION_DRAWER:
      return {
        ...state,
        is_NotificationDrawerVisible: !state.is_NotificationDrawerVisible,
        is_MenuDrawerVisible: false,
        is_SearchDrawerVisible: false
      }
    case types.TOGGLE_MENU_DRAWER:
      return {
        ...state,
        is_MenuDrawerVisible: !state.is_MenuDrawerVisible,
        is_NotificationDrawerVisible: false,
        is_SearchDrawerVisible: false
      }
    case types.TOGGLE_SEARCH_DRAWER:
      return {
        ...state,
        is_SearchDrawerVisible: !state.is_SearchDrawerVisible,
        is_NotificationDrawerVisible: false,
        is_MenuDrawerVisible: false
      }
    case types.CLOSE_DRAWERS:
      return {
        ...state,
        is_NotificationDrawerVisible: false,
        is_MenuDrawerVisible: false,
        is_SearchDrawerVisible: false
      }
    case types.SET_HOME_FEATURED_ACCOUNTS:
      return {
        ...state,
        featured: action.data
      }
    case types.SET_COMMISSION:
      return {
        ...state,
        commission: action.data
      }
    default:
      return state
  }
}
export default appReducer
