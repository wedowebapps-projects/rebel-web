import { all } from 'redux-saga/effects';
import authSagas from 'store/sagas/authSagas'
import appSagas from 'store/sagas/appSagas'
import postSagas from 'store/sagas/postSagas'
import profileSagas from 'store/sagas/profileSagas'
import chatSagas from 'store/sagas/chatSagas'

function* rootSaga() {
  yield all([authSagas(), appSagas(), postSagas(), profileSagas(), chatSagas()])
}

export default rootSaga
