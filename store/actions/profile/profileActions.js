import * as types from './profileTypes'

export const toggleProfilePostsLoader = status => ({
  type: types.TOGGLE_PROFILE_POSTS_LOADER,
  status
})
export const fetchProfilePostsAction = (filters, resolve, reject) => ({
  type: types.FETCH_PROFILE_POSTS,
  filters,
  resolve,
  reject
})
export const setProfilePostsAction = data => ({
  type: types.SET_PROFILE_POSTS,
  data
})
export const resetProfilePostsAction = () => ({
  type: types.RESET_PROFILE_POSTS
})
export const likeBookmarkProfilePostAction = post => ({
  type: types.LIKE_BOOKMARK_PROFILE_POST,
  post
})
export const likeCommentProfilePostAction = data => ({
  type: types.LIKE_UNLIKE_COMMENT_PROFILE_POST,
  data
})
export const setLikeUnlikeCommentProfilePostAction = data => ({
  type: types.SET_LIKE_UNLIKE_COMMENT_PROFILE_POST,
  data
})
export const setDeletedPostAction_profile = post_id => ({
  type: types.SET_DELETED_PROFILE_POSTS,
  post_id
})

// Comments
export const addCommentToProfilePostAction = data => ({
  type: types.ADD_COMMENT_TO_PROFILE_POST,
  data
})
export const deleteCommentToProfilePostAction = (post_id, comment_id, parent_id) => ({
  type: types.DELETE_COMMENT_TO_PROFILE_POST,
  post_id,
  comment_id,
  parent_id
})

export const fetchProfilePostCommentsAction = (post_id, filters, resolve, reject) => ({
  type: types.FETCH_PROFILE_POST_COMMENTS,
  post_id,
  filters,
  resolve,
  reject
})
export const setProfilePostCommentsAction = (post_id, totalComments, data) => ({
  type: types.SET_PROFILE_POST_COMMENTS,
  post_id,
  totalComments,
  data
})
export const resetCommentPaginationProfileAction = post_id => ({
  type: types.RESET_COMMENTS_PAGINATION_PROFILE,
  post_id
})

// Replies
export const fetchRepliesActionProfile = (post_id, comment_id, filters, resolve, reject) => ({
  type: types.FETCH_REPLIES_PROFILE,
  post_id,
  comment_id,
  filters,
  resolve,
  reject
})
export const setRepliesActionProfile = (post_id, comment_id, totalComments, data) => ({
  type: types.SET_REPLIES_PROFILE,
  post_id,
  comment_id,
  totalComments,
  data
})

// Purchase Post
export const setPurchasePostAction_profile = post_id => ({
  type: types.SET_PURCHASE_POST_PROFILE,
  post_id
})
export const setSendTipPostAction_profile = post_id => ({
  type: types.SET_SENDTIP_POST_PROFILE,
  post_id
})
