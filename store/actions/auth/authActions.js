import * as authTypes from './authTypes'

export const registrationAction = (data, resolve, reject) => {
  return {
    type: authTypes.REGISTER,
    data,
    resolve,
    reject
  }
}
export const socialSignupAction = (data, resolve, reject) => {
  return {
    type: authTypes.SOCIAL_SIGNUP,
    data,
    resolve,
    reject
  }
}
export const storeAuthEmailAction = (email, resolve, reject) => {
  return {
    type: authTypes.STORE_AUTH_EMAIL,
    email,
    resolve,
    reject
  }
}
export const verifyOTPAction = (data, resolve, reject) => {
  return {
    type: authTypes.VERIFY_OTP,
    data,
    resolve,
    reject
  }
}
export const resetPasswordAction = (data, resolve, reject) => {
  return {
    type: authTypes.RESET_PASSWORD,
    data,
    resolve,
    reject
  }
}
export const storeAuthenticationAction = data => {
  return {
    type: authTypes.STORE_AUTHENTICATION,
    data
  }
}
export const checkCredentialsAction = (data, resolve, reject) => {
  return {
    type: authTypes.CHECK_CREDENTIALS,
    data,
    resolve,
    reject
  }
}
export const logoutAction = () => {
  return {
    type: authTypes.LOGOUT
  }
}
export const resetAuthentication = () => {
  return {
    type: authTypes.RESET_AUTHENTICATION
  }
}
export const restoreSessionAction = () => {
  return {
    type: authTypes.RESTORE_SESSION
  }
}

export const setSocialDataAction = data => {
  return {
    type: authTypes.SET_SOCIAL_DATA,
    data
  }
}

export const getMyProfileAction = () => {
  return {
    type: authTypes.GET_MY_PROFILE
  }
}
export const setMyProfileAction = data => {
  return {
    type: authTypes.SET_MY_PROFILE,
    data
  }
}
export const updateProfileAction = (data, resolve, reject) => {
  return {
    type: authTypes.UPDATE_PROFILE,
    data,
    resolve,
    reject
  }
}
export const submitBankApplicationAction = (data, resolve, reject) => {
  return {
    type: authTypes.SUBMIT_BANK_APPLICATION,
    data,
    resolve,
    reject
  }
}
export const setBankApplicationStatusAction = data => {
  return {
    type: authTypes.SET_BANK_APPLICATION,
    data
  }
}
export const submitBankDetailsAction = (data, resolve, reject) => {
  return {
    type: authTypes.SUBMIT_BANK_DETAILS,
    data,
    resolve,
    reject
  }
}

// Subscription price
export const submitSubscriptionPriceAction = (data, uuid, resolve, reject) => ({
  type: authTypes.SUBMIT_SUBSCRIPTION_PRICE,
  data,
  uuid,
  resolve,
  reject
})
export const setSubscriptionPriceAction = (data, reset) => ({
  type: authTypes.SET_SUBSCRIPTION_PRICE,
  data,
  reset
})
export const updateSubscriptionPriceAction = data => ({
  type: authTypes.UPDATE_SUBSCRIPTION_PRICE,
  data
})
export const deleteSubscriptionPriceAction = (data, resolve, reject) => ({
  type: authTypes.DELETE_SUBSCRIPTION_PRICE,
  data,
  resolve,
  reject
})
export const fetchSubscriptionPricesAction = () => ({
  type: authTypes.FETCH_SUBSCRIPTION_PRICES
})

export const getFCMToken = () => ({
  type: authTypes.GET_FCM_TOKEN
})
export const setFCMToken = token => ({
  type: authTypes.SET_FCM_TOKEN,
  token
})
export const setFCMloaded = status => ({
  type: authTypes.SET_FCM_LOADED,
  status
})
export const clearNotificationsAction = (resolve, reject) => {
  return {
    type: authTypes.CLEAR_NOTIFICATION,
    resolve,
    reject
  }
}

// CONTACT US
export const sendContactUsAction = (data, resolve, reject) => {
  return {
    type: authTypes.SEND_CONTACT_US,
    data,
    resolve,
    reject
  }
}

//CHECK USERNAME EXIST
export const checkUsernameExistAction = (data, resolve, reject) => {
  return {
    type: authTypes.CHECK_USERNAME_EXIST,
    data,
    resolve,
    reject
  }
}