export const SET_POST_ALERT = 'SET_POST_ALERT';
export const CREATE_POST = 'CREATE_POST';
export const ADD_CREATED_POST = 'ADD_CREATED_POST';
export const FETCH_POSTS = 'FETCH_POSTS';
export const RESET_POSTS_PAGINATION = 'RESET_POSTS_PAGINATION';
export const SET_POSTS = 'SET_POSTS';
export const DELETE_POST = 'DELETE_POST';
export const SET_DELETED_POST = 'SET_DELETED_POST';
export const TOGGLE_POSTS_LOADER = 'TOGGLE_POSTS_LOADER';
export const LIKE_UNLIKE_COMMENT = 'LIKE_UNLIKE_COMMENT';
export const SET_LIKE_UNLIKE_COMMENT = 'SET_LIKE_UNLIKE_COMMENT';
export const FETCH_SINGLE_POST = 'FETCH_SINGLE_POST';
export const SET_SINGLE_POST = 'SET_SINGLE_POST';
export const LIKE_BOOKMARK_POST = 'LIKE_BOOKMARK_POST';
export const REPORT_POST = "REPORT_POST";

// Comments
export const ADD_COMMENT_TO_POST = 'ADD_COMMENT_TO_POST';
export const DELETE_COMMENT_TO_POST = 'DELETE_COMMENT_TO_POST';
export const FETCH_POST_COMMENTS = 'FETCH_POST_COMMENTS';
export const SET_POST_COMMENTS = 'SET_POST_COMMENTS';
export const RESET_COMMENTS_PAGINATION = 'RESET_COMMENTS_PAGINATION';

// Replies
export const FETCH_REPLIES = 'FETCH_REPLIES';
export const SET_REPLIES = 'SET_REPLIES';

// Purchase post
export const TOGGLE_PURCHASE_POST_MODAL = 'TOGGLE_PURCHASE_POST_MODAL';
export const SUBMIT_PURCHASE_TRANSACTION_POST = 'SUBMIT_PURCHASE_TRANSACTION_POST';
export const SET_PURCHASE_POST = 'SET_PURCHASE_POST';

// Send Tip
export const TOGGLE_TIP_POST_MODAL = 'TOGGLE_TIP_POST_MODAL';
export const SUBMIT_SENDTIP_TRANSACTION_POST = 'SUBMIT_SENDTIP_TRANSACTION_POST';
export const SET_SENDTIP_POST = 'SET_SENDTIP_POST';

