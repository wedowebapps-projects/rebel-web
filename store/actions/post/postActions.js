import * as types from './postTypes'

export const reportPost = (postid, data) => ({
  type: types.REPORT_POST,
  postid,
  data
})

export const createPostAction = (data, resolve, reject) => ({
  type: types.CREATE_POST,
  data,
  resolve,
  reject
})
export const postAlertAction = data => ({
  type: types.SET_POST_ALERT,
  data
})
export const addCreatedPostAction = data => ({
  type: types.ADD_CREATED_POST,
  data
})
export const fetchPostsAction = (filters, resolve, reject) => ({
  type: types.FETCH_POSTS,
  filters,
  resolve,
  reject
})
export const setPostsAction = data => ({
  type: types.SET_POSTS,
  data
})
export const resetPostsPaginationAction = resolve => ({
  type: types.RESET_POSTS_PAGINATION,
  resolve
})

export const deletePostAction = (post_id, resolve, reject, username) => ({
  type: types.DELETE_POST,
  post_id,
  resolve,
  reject,
  username
})

export const setDeletedPostAction = post_id => ({
  type: types.SET_DELETED_POST,
  post_id
})

export const togglePostsLoader = status => ({
  type: types.TOGGLE_POSTS_LOADER,
  status
})
export const likeCommentAction = data => ({
  type: types.LIKE_UNLIKE_COMMENT,
  data
})
export const setLikeUnlikeCommentAction = data => ({
  type: types.SET_LIKE_UNLIKE_COMMENT,
  data
})
export const addCommentToPostAction = data => ({
  type: types.ADD_COMMENT_TO_POST,
  data
})
export const deleteCommentToPostAction = (post_id, comment_id, parent_id) => ({
  type: types.DELETE_COMMENT_TO_POST,
  post_id,
  comment_id,
  parent_id
})
export const fetchSinglePostAction = (uuid, resolve, reject) => ({
  type: types.FETCH_SINGLE_POST,
  uuid,
  resolve,
  reject
})
export const setSinglePostAction = data => ({
  type: types.SET_SINGLE_POST,
  data
})
export const likeBookmarkPostAction = post => ({
  type: types.LIKE_BOOKMARK_POST,
  post
})

// Comments
export const fetchPostCommentsAction = (post_id, filters, resolve, reject) => ({
  type: types.FETCH_POST_COMMENTS,
  post_id,
  filters,
  resolve,
  reject
})
export const setPostCommentsAction = (post_id, totalComments, data) => ({
  type: types.SET_POST_COMMENTS,
  post_id,
  totalComments,
  data
})
export const resetCommentPaginationAction = post_id => ({
  type: types.RESET_COMMENTS_PAGINATION,
  post_id
})

// Replies
export const fetchRepliesAction = (post_id, comment_id, filters, resolve, reject) => ({
  type: types.FETCH_REPLIES,
  post_id,
  comment_id,
  filters,
  resolve,
  reject
})
export const setRepliesAction = (post_id, comment_id, totalComments, data) => ({
  type: types.SET_REPLIES,
  post_id,
  comment_id,
  totalComments,
  data
})

// Purchase Post
export const togglePurchasePostModalAction = (visible, post, username) => ({
  type: types.TOGGLE_PURCHASE_POST_MODAL,
  post,
  visible,
  username
})
export const setPurchasePostAction = post_id => ({
  type: types.SET_PURCHASE_POST,
  post_id
})
export const submitTransactionAction = (post_id, data, isProfile, resolve, reject) => ({
  type: types.SUBMIT_PURCHASE_TRANSACTION_POST,
  post_id,
  data,
  resolve,
  reject,
  isProfile
})

// Send Tip
export const toggleTipPostModalAction = (visible, post, username) => ({
  type: types.TOGGLE_TIP_POST_MODAL,
  post,
  visible,
  username
})
export const submitTipTransactionAction = (post_id, data, isProfile, resolve, reject) => ({
  type: types.SUBMIT_SENDTIP_TRANSACTION_POST,
  post_id,
  data,
  resolve,
  reject,
  isProfile
})
export const setSendTipPostAction = post_id => ({
  type: types.SET_SENDTIP_POST,
  post_id
})
