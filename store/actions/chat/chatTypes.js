export const SET_CHAT_LOADER = 'SET_CHAT_LOADER';
export const FETCH_CHAT_LIST = 'FETCH_CHAT_LIST';
export const SET_CHAT_LIST = 'SET_CHAT_LIST';

export const SET_ACTIVE_CHAT = 'SET_ACTIVE_CHAT';

export const SET_PAGE = 'SET_PAGE';
export const FETCH_CHAT_ROOM = 'FETCH_CHAT_ROOM';
export const SET_CHAT_ROOM = 'SET_CHAT_ROOM';

export const APPEND_CHAT = 'APPEND_CHAT';
export const DELETE_CHAT = 'DELETE_CHAT';
export const SET_DELETED_CHAT = 'SET_DELETED_CHAT';