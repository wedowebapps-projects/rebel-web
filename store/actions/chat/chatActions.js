import * as chatTypes from './chatTypes'

export const setChatLoader = data => {
  return {
    type: chatTypes.SET_CHAT_LOADER,
    data
  }
}
export const fetchChatList = () => {
  return {
    type: chatTypes.FETCH_CHAT_LIST
  }
}

export const setChatList = data => {
  return {
    type: chatTypes.SET_CHAT_LIST,
    data
  }
}

export const setActiveChat = data => {
  return {
    type: chatTypes.SET_ACTIVE_CHAT,
    data
  }
}

export const appendChat = data => {
  return {
    type: chatTypes.APPEND_CHAT,
    data
  }
}

export const setChatPageAction = page => ({
  type: chatTypes.SET_PAGE,
  page
})

export const fetchChatRoomAction = (roomId, filters, resolve, reject) => {
  return {
    type: chatTypes.FETCH_CHAT_ROOM,
    roomId,
    filters,
    resolve,
    reject
  }
}

export const setChatRoom = chats => {
  return {
    type: chatTypes.SET_CHAT_ROOM,
    chats
  }
}

export const deleteChat_action = (chat_id, resolve, reject) => ({
  type: chatTypes.DELETE_CHAT,
  chat_id,
  resolve,
  reject
})
export const setDeletedChat_action = chat_id => ({
  type: chatTypes.SET_DELETED_CHAT,
  chat_id
})
