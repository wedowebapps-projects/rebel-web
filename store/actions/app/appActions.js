import * as appTypes from './appTypes'

export const toggleNotificationDrawerAction = () => ({
  type: appTypes.TOGGLE_NOTIFICATION_DRAWER
})
export const toggleMenuDrawerAction = () => ({
  type: appTypes.TOGGLE_MENU_DRAWER
})
export const toggleSearchDrawerAction = () => ({
  type: appTypes.TOGGLE_SEARCH_DRAWER
})
export const closeAllDrawersAction = () => ({
  type: appTypes.CLOSE_DRAWERS
})

export const getHomeFeaturedAccountsAction = filters => ({
  type: appTypes.GET_HOME_FEATURED_ACCOUNTS,
  filters
})
export const setHomeFeaturedAccountsAction = data => ({
  type: appTypes.SET_HOME_FEATURED_ACCOUNTS,
  data
})

export const fetchReportReasons = () => ({
  type: appTypes.FETCH_REPORT_REASONS
})
export const setReportReasons = reasons => ({
  type: appTypes.SET_REPORT_REASONS,
  reasons
})
export const fetchCommissionAction = () => ({
  type: appTypes.FETCH_COMMISSION
})
export const setCommissionAction = data => ({
  type: appTypes.SET_COMMISSION,
  data
})

export const fetchAnnouncements = () => ({
  type: appTypes.FETCH_ANNOUNCEMENTS
})
export const setAnnouncements = data => ({
  type: appTypes.SET_ANNOUNCEMENTS,
  data
})