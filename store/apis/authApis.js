import { isEmpty } from 'lodash'
import api from 'utils/api'
import { convertObjectToQuerystring } from 'utils/helpers'
import { getUserToken } from 'utils/storageUtils'

export const changeProfileMedia_api = data => {
  const token = getUserToken()
  return api('/user/updateProfile/media', data, 'put', token)
}

export const getBankList_api = () => {
  return api('/banks', null, 'get')
}
export const submitBankApplication_api = data => {
  const token = getUserToken()
  return api('/user/application', data, 'post', token)
}
export const submitBankAccount_api = data => {
  const token = getUserToken()
  return api('/user/bank', data, 'post', token)
}
export const addSubscriptionPrice_api = data => {
  const token = getUserToken()
  return api('/user/subscriptions', data, 'post', token)
}
export const updateSubscriptionPrice_api = (data, uuid) => {
  const token = getUserToken()
  return api(`/user/subscriptions/${uuid}`, data, 'put', token)
}
export const fetchSubscriptionPrice_api = () => {
  const token = getUserToken()
  return api('/user/subscriptions', null, 'get', token)
}
export const deleteSubscriptionPrice_api = (uuid, data) => {
  const token = getUserToken()
  return api(`/user/subscriptions/${uuid}`, data, 'delete', token)
}
export const submitSubscriptionTransaction_api = (profile_id, data) => {
  const token = getUserToken()
  if (process.env.PAYMENT_METHOD === 'flutterwave') {
    return api(`/user/follow/${profile_id}/paid`, data, 'post', token)
  } else {
    return api(`/user/follow/${profile_id}/paidpaystack`, data, 'post', token)
  }
}
export const getTransactionList_api = filters => {
  let filter = ''
  if (!isEmpty(filters)) {
    filter = `${convertObjectToQuerystring(filters)}`
  }
  const token = getUserToken()
  return api(`/user/transactions?${filter}`, null, 'get', token)
}
export const getTransactionMonths_api = () => {
  const token = getUserToken()
  return api(`/user/transactionsmonthly`, null, 'get', token)
}
export const addMoneyToWallet_api = data => {
  const token = getUserToken()
  if (process.env.PAYMENT_METHOD === 'flutterwave') {
    return api(`/user/wallet/add`, data, 'post', token)
  } else {
    return api(`/user/walletPayStack/add`, data, 'post', token)
  }
}
export const withdraw_api = data => {
  const token = getUserToken()
  if (process.env.PAYMENT_METHOD === 'flutterwave') {
    return api(`/user/wallet/withdraw`, data, 'post', token)
  } else {
    return api(`/user/walletPayStack/withdraw`, data, 'post', token)
  }
}
export const getWithdrawalFees_api = amount => {
  return api(`/transfer/fee/${amount}`, null, 'get')
}

export const blockUser_api = id => {
  const token = getUserToken()
  if (process.env.PAYMENT_METHOD === 'flutterwave') {
    return api(`/user/blocked/${id}`, null, 'post', token)
  } else {
    return api(`/user/blockedpaystack/${id}`, null, 'post', token)
  }
}
export const unBlockUser_api = id => {
  const token = getUserToken()
  return api(`/user/blocked/${id}`, null, 'post', token)
}
export const removeSubscription_api = id => {
  const token = getUserToken()

  if (process.env.PAYMENT_METHOD === 'flutterwave') {
    return api(`/user/remove/subscription/${id}`, null, 'post', token)
  } else {
    return api(`/user/remove/subscriptionpaystack/${id}`, null, 'post', token)
  }
}
export const changePassword_api = data => {
  const token = getUserToken()
  return api(`/user/changePassword`, data, 'put', token)
}
export const deleteAccount_api = () => {
  const token = getUserToken()
  return api(`/user/remove/account`, null, 'delete', token)
}

export const updateFCMToken_api = data => {
  const token = getUserToken()
  return api(`/user/updateFcmToken`, data, 'put', token)
}

export const sendContactUs_api = data => {
  return api(`/contact_us`, data, 'post')
}

export const checkUsernameExist_api = data => {
  const token = getUserToken()
  return api(`/user/checkUsername`, data, 'put', token)
}

export const unBlockedUserFromBlockList_api = id => {
  const token = getUserToken()
  return api(`/user/unblocked/${id}`, null, 'post', token)
}
