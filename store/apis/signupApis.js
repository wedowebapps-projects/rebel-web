import api from 'utils/api'
import { getUserToken } from 'utils/storageUtils'

export const signup = data => {
  return api('/auth/register', data, 'post')
}

export const login = data => {
  return api('/auth/login', data, 'post')
}

export const verifyOTP = data => {
  return api('/auth/confirm', data, 'post')
}

export const logout_api = () => {
  const token = getUserToken()
  return api('/user/logout', null, 'get', token)
}
export const resendOTP = data => {
  return api('/auth/resend/confirm', data, 'post')
}

export const resetPassword = data => {
  return api('/auth/resetPassword', data, 'post')
}

export const socialSignupApi = data => {
  return api('/auth/socialSignUp', data, 'post')
}

export const getMyProfileApi = token => {
  return api('/user/me', null, 'get', token)
}

export const updateProfileApi = data => {
  const token = getUserToken()
  return api('/user/updateProfile', data, 'put', token)
}
