import api from 'utils/api'
import { convertObjectToQuerystring } from 'utils/helpers'

// Login Start
export const checkEmailUsername = data => {
  return api('/auth/check/exists', data, 'post')
}

export const resetPasswordOTP = data => {
  return api('/auth/requestOTP', data, 'post')
}

export const socialLogin = data => {
  return api('/auth/socialLogin', data, 'post')
}

export const sendOtpToEmail = data =>{
  return api('/auth/resendotp', data, 'post')
}

// cms pages api
export const getCmsPages_Api = filters => {
  let filter = ''
  if (!_.isEmpty(filters)) {
    filter = `${convertObjectToQuerystring(filters)}`
  }
  return api(`/pageBySlug?${filter}`, null, 'get')
}
