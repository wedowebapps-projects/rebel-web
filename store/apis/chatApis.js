import api from 'utils/api'
const _ = require('lodash')
import { convertObjectToQuerystring } from 'utils/helpers'
import { getUserToken } from 'utils/storageUtils'

export const initiateChatApi = data => {
  const token = getUserToken()
  return api(`/user/initiate/chat`, data, 'post', token)
}

export const fetchChatsApi = () => {
  const token = getUserToken()
  return api(`/user/chats`, '', 'get', token)
}

export const fetchChatRoomApi = (roomId, filters) => {
  let filter = ''
  if (!_.isEmpty(filters)) {
    filter = `${convertObjectToQuerystring(filters)}`
  }
  const token = getUserToken()
  return api(`/user/chats/${roomId}?${filter}`, '', 'get', token)
}

export const deleteChat_api = chat_id => {
  const token = getUserToken()
  return api(`/user/chats/${chat_id}`, null, 'delete', token)
}
