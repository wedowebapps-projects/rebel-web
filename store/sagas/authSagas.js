import 'firebase/messaging'
import { takeLatest, all, fork, call, put, select, delay } from 'redux-saga/effects'
import * as types from 'store/actions/auth/authTypes'
import { signup, verifyOTP, login, resetPassword, socialSignupApi, getMyProfileApi, updateProfileApi, logout_api } from 'store/apis/signupApis'
import {
  storeAuthEmailAction,
  storeAuthenticationAction,
  resetAuthentication,
  setMyProfileAction,
  setBankApplicationStatusAction,
  setSubscriptionPriceAction,
  updateSubscriptionPriceAction
} from 'store/actions/auth/authActions'
import * as storageUtils from 'utils/storageUtils'
import { closeAllDrawersAction } from 'store/actions/app/appActions'
import {
  submitBankApplication_api,
  submitBankAccount_api,
  addSubscriptionPrice_api,
  updateSubscriptionPrice_api,
  fetchSubscriptionPrice_api,
  deleteSubscriptionPrice_api,
  sendContactUs_api,
  checkUsernameExist_api
} from 'store/apis/authApis'
import Router from 'next/router'

function* setAuthentication(result, action) {
  yield call(storageUtils.setUserToken, result.token)
  yield call(storageUtils.setUserData, result.user)
  yield put(storeAuthenticationAction(result))

  if (action && action.resolve) {
    action.resolve()
  }
}

function* registerSaga({ data, resolve, reject }) {
  try {
    const result = yield call(signup, data)
    yield put(storeAuthEmailAction(data.email))
    yield setAuthentication(result, { resolve, reject })
    resolve(result)
  } catch (error) {
    reject(error)
  }
}

function* socialSignupSaga(action) {
  try {
    const result = yield call(socialSignupApi, action.data)
    yield setAuthentication(result, action)
    action.resolve(result)
  } catch (error) {
    action.reject(error)
  }
}

function* verifyOTPSaga(action) {
  try {
    const result = yield call(verifyOTP, action.data)
    action.resolve()
    // yield setAuthentication(result, action)
  } catch (error) {
    action.reject(error)
  }
}

function* logoutSaga() {
  try {
    yield call(logout_api)
  } catch (error) {
    console.log(error)
  }
  yield call(storageUtils.unsetUserData)
  yield call(storageUtils.unsetUserToken)
  yield put(resetAuthentication())
  yield put(closeAllDrawersAction())
  Router.push(`/login${Router.asPath !== '/login' && Router.asPath !== '/' ? `?return=${Router.asPath}` : ''}`)
}

function* restoreSessionSaga() {
  try {
    const token = yield call(storageUtils.getUserToken)
    const userData = yield call(storageUtils.getUserData)
    if (token && userData) {
      yield setAuthentication({ token: token.replace('Bearer ', ''), user: userData })
    }

    const result = yield call(getMyProfileApi, token)
    if (token && result.user) {
      yield setAuthentication({ token: token.replace('Bearer ', ''), user: result.user })
    }
  } catch (error) {
    console.log(error)
  }
}

function* checkCredentialsSaga(action) {
  try {
    const result = yield call(login, action.data)
    yield setAuthentication(result, action)
  } catch (error) {
    action.reject(error)
  }
}
function* resetPasswordSaga(action) {
  try {
    const result = yield call(resetPassword, action.data)
    yield setAuthentication(result, action)
  } catch (error) {
    action.reject(error)
  }
}
function* getMyProfileSaga() {
  try {
    const token = yield call(storageUtils.getUserToken)
    const result = yield call(getMyProfileApi, token)
    yield put(setMyProfileAction(result.user))
  } catch (error) {
    console.log(error)
  }
}

function* udpateProfileSaga({ data, resolve, reject }) {
  try {
    const result = yield call(updateProfileApi, data)
    yield put(setMyProfileAction(result.user))
    resolve(result)
  } catch (error) {
    reject(error)
  }
}

function* submitBankApplicationSaga({ data, resolve, reject }) {
  try {
    const result = yield call(submitBankApplication_api, data)
    yield put(setBankApplicationStatusAction(JSON.stringify(result.personal_info)))
    resolve()
  } catch (error) {
    reject(error)
  }
}
function* submitBankDetailsSaga({ data, resolve, reject }) {
  try {
    const result = yield call(submitBankAccount_api, data)
    yield put(setBankApplicationStatusAction(JSON.stringify(result.personal_info)))
    resolve()
  } catch (error) {
    reject(error)
  }
}

function* submitSubscriptionPriceSaga({ data, uuid, resolve, reject }) {
  try {
    let result
    if (!uuid) {
      // Add
      result = yield call(addSubscriptionPrice_api, data)
      yield put(setSubscriptionPriceAction([result.subscriptions], false))
    } else {
      result = yield call(updateSubscriptionPrice_api, data, uuid)
      yield put(updateSubscriptionPriceAction(result.subscriptions))
    }
    resolve(result)
  } catch (error) {
    console.log(error)
    reject(error)
  }
}
function* fetchSubscriptionPricesSaga() {
  try {
    const result = yield call(fetchSubscriptionPrice_api)
    yield put(setSubscriptionPriceAction(result.subscriptions, true))
  } catch (error) {
    console.log(error)
  }
}
function* deleteSubscriptionPriceSaga({ data, resolve, reject }) {
  try {
    yield call(deleteSubscriptionPrice_api, data.uuid, { id: data.plan_data.id })
    const subscriptionPrices = yield select(state => state.auth.subscriptionPrices)
    const i = subscriptionPrices.map(p => p.uuid).indexOf(data.uuid)
    subscriptionPrices.splice(i, 1)
    yield put(setSubscriptionPriceAction(subscriptionPrices, true))
    resolve()
  } catch (error) {
    reject(error)
  }
}

function* contactUsSaga({ data, resolve, reject }) {
  try {
    const result = yield call(sendContactUs_api, data)
    resolve(result)
  } catch (error) {
    reject(error)
  }
}

//CHECK USER NAME EXIST
function* checkUsernameExistSaga({ data, resolve, reject }) {
  try {
    const result = yield call(checkUsernameExist_api, data)
    resolve(result)
  } catch (error) {
    reject(error)
  }
}

export function* watchSagas() {
  yield takeLatest(types.REGISTER, registerSaga)
  yield takeLatest(types.VERIFY_OTP, verifyOTPSaga)
  yield takeLatest(types.LOGOUT, logoutSaga)
  yield takeLatest(types.RESTORE_SESSION, restoreSessionSaga)
  yield takeLatest(types.CHECK_CREDENTIALS, checkCredentialsSaga)
  yield takeLatest(types.RESET_PASSWORD, resetPasswordSaga)
  yield takeLatest(types.SOCIAL_SIGNUP, socialSignupSaga)
  yield takeLatest(types.GET_MY_PROFILE, getMyProfileSaga)
  yield takeLatest(types.UPDATE_PROFILE, udpateProfileSaga)
  yield takeLatest(types.SUBMIT_BANK_APPLICATION, submitBankApplicationSaga)
  yield takeLatest(types.SUBMIT_BANK_DETAILS, submitBankDetailsSaga)
  yield takeLatest(types.FETCH_SUBSCRIPTION_PRICES, fetchSubscriptionPricesSaga)
  yield takeLatest(types.SUBMIT_SUBSCRIPTION_PRICE, submitSubscriptionPriceSaga)
  yield takeLatest(types.DELETE_SUBSCRIPTION_PRICE, deleteSubscriptionPriceSaga)
  yield takeLatest(types.SEND_CONTACT_US, contactUsSaga)
  yield takeLatest(types.CHECK_USERNAME_EXIST, checkUsernameExistSaga)
}

export default function* runSagas() {
  yield all([fork(watchSagas)])
}
