import { takeLatest, all, fork, call, put } from 'redux-saga/effects'
import * as chatTypes from '../../store/actions/chat/chatTypes'
import { setChatList, setChatRoom, setActiveChat, setChatLoader, setDeletedChat_action } from '../../store/actions/chat/chatActions'
import { fetchChatsApi, fetchChatRoomApi, deleteChat_api } from 'store/apis/chatApis'
import { constants } from 'utils/helpers'

function* fetchChatList() {
  const response = yield call(fetchChatsApi)
  yield put(setChatList(response.chats))
}

function* fetchChatRoom(action) {
  const roomId = action.roomId
  const filters = action.filters
  try {
    yield put(setChatLoader(true))
    filters.perPage = constants.CHATS_PER_PAGE
    const response = yield call(fetchChatRoomApi, roomId, filters)
    yield put(setActiveChat(response.data))
    yield put(setChatRoom(response.chats))
    action.resolve()
  } catch (error) {
    yield put(setChatRoom([]))
    action.reject(error)
  }
}

function* deleteChatSaga({ chat_id, resolve, reject }) {
  try {
    yield call(deleteChat_api, chat_id);
    yield put(setDeletedChat_action(chat_id))
    resolve();
  } catch (error) {
    console.log(error);
    reject(error)
  }
}

export function* watchSagas() {
  yield takeLatest(chatTypes.FETCH_CHAT_LIST, fetchChatList)
  yield takeLatest(chatTypes.FETCH_CHAT_ROOM, fetchChatRoom)
  yield takeLatest(chatTypes.DELETE_CHAT, deleteChatSaga)
}

export default function* runSagas() {
  yield all([fork(watchSagas)])
}
