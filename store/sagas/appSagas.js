import { takeLatest, all, fork, call, put } from 'redux-saga/effects'
import * as appTypes from 'store/actions/app/appTypes'
import { getHomeFeaturedAccounts_Api, fetchReportReasons_api, fetchCommission_api, fetchAnnouncements_api } from 'store/apis/appApis'
import { setCommissionAction, setHomeFeaturedAccountsAction, setReportReasons, setAnnouncements } from 'store/actions/app/appActions'

function* getHomeFeaturedAccounts({ filters }) {
  try {
    const result = yield call(getHomeFeaturedAccounts_Api, filters)
    yield put(setHomeFeaturedAccountsAction(result.users))
  } catch (error) {
    console.log('Errors', error)
  }
  yield {}
}

function* fetchReportReasons() {
  try {
    const result = yield call(fetchReportReasons_api)
    yield put(setReportReasons(result.reasons))
  } catch (error) {
    console.log('Errors', error)
  }
  yield {}
}

function* fetchCommissionSaga() {
  try {
    const result = yield call(fetchCommission_api)
    yield put(setCommissionAction(result.fees))
  } catch (error) {
    console.log('Errors', error)
  }
  yield {}
}

function* fetchAnnouncements() {
  try {
    const result = yield call(fetchAnnouncements_api)
    yield put(setAnnouncements(result.announcements))
  } catch (error) {
    console.log('Errors', error)
  }
}

export function* watchSagas() {
  yield takeLatest(appTypes.GET_HOME_FEATURED_ACCOUNTS, getHomeFeaturedAccounts)
  yield takeLatest(appTypes.FETCH_REPORT_REASONS, fetchReportReasons)
  yield takeLatest(appTypes.FETCH_COMMISSION, fetchCommissionSaga)
  yield takeLatest(appTypes.FETCH_ANNOUNCEMENTS, fetchAnnouncements)
}

export default function* runSagas() {
  yield all([fork(watchSagas)])
}
