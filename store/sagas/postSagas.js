import { takeLatest, all, fork, call, put, delay } from 'redux-saga/effects'
import * as types from 'store/actions/post/postTypes'
import * as storageUtils from 'utils/storageUtils'
import {
  createPostApi,
  fetchOpenPosts,
  fetchProtectedPosts,
  likeUnlikComment_api,
  fetchPostDetail_api,
  fetchPostComments_api,
  fetchReplies_api,
  reportPost_api,
  submitPurchaseTransaction_api, submitSendTipTransaction_api, deletePost_api
} from 'store/apis/postApis'
import {
  addCreatedPostAction,
  postAlertAction,
  setPostsAction,
  togglePostsLoader,
  setLikeUnlikeCommentAction,
  setSinglePostAction,
  setPostCommentsAction,
  setRepliesAction,
  setPurchasePostAction, setSendTipPostAction, setDeletedPostAction
} from 'store/actions/post/postActions'
import toast from 'utils/toast'
import { setDeletedPostAction_profile, setPurchasePostAction_profile, setSendTipPostAction_profile } from 'store/actions/profile/profileActions'

function* createPostSaga({ data, resolve, reject }) {
  try {
    const token = yield call(storageUtils.getUserToken)
    const result = yield call(createPostApi, { data, token })

    const post = result.post
    if (data.post_type === 'post') {
      post.mediaFiles = data.mediaFiles
    } else if (data.post_type === 'poll') {
      post.pollOptions = data.pollOptions
    }
    yield put(addCreatedPostAction(post))

    resolve(result)

    yield put(
      postAlertAction({
        show: true,
        type: 'success',
        text: 'Your post is uploaded'
      })
    )
    yield delay(5000)
    yield put(
      postAlertAction({
        show: false
      })
    )
  } catch (error) {
    reject && reject(error)
  }
}

function* fetchPostsSaga({ filters, resolve, reject }) {
  try {
    yield put(togglePostsLoader(true))
    const token = yield call(storageUtils.getUserToken)
    let result
    if (token)
    {
      result = yield call(fetchProtectedPosts, { filters, token })
      yield put(setPostsAction(result.posts))
    } else {
      // If not logged, call open api
      // result = yield call(fetchOpenPosts, filters)
    }    
    // yield put(setPostsAction(result.posts))
    resolve()
  } catch (error) {
    console.log(error)
    reject(error)
  }
}

function* deletePostSaga({ post_id, resolve, reject, username }) {
  try {
    yield call(deletePost_api, post_id);
    if (!username) {
      // From feed list
      yield put(setDeletedPostAction(post_id))
    } else {
      // from My account
      yield put(setDeletedPostAction_profile(post_id))
    }
    resolve();
  } catch (error) {
    console.log(error);
    reject(error)
  }
}

function* likeUnlikeCommentSaga({ data }) {
  try {
    yield call(likeUnlikComment_api, data.postId, data.commentId, { type: data.type })
    yield put(setLikeUnlikeCommentAction({ ...data, isLiked: data.type === 'like' ? true : false }))
  } catch (error) {
    console.log(error)
  }
}

function* fetchSinglePostSaga({ uuid, resolve, reject }) {
  try {
    const result = yield call(fetchPostDetail_api, uuid)
    yield put(setSinglePostAction(result.posts[0]))
    resolve()
  } catch (error) {
    console.log(error)
    reject(error)
  }
}

function* fetchPostCommentsSaga({ post_id, filters, resolve, reject }) {
  try {
    const result = yield call(fetchPostComments_api, post_id, filters)
    if (result.totalComments > 0) {
      yield put(setPostCommentsAction(post_id, result.totalComments, result.comments))
    }
    resolve()
  } catch (error) {
    console.log(error)
    reject(error)
  }
}

function* fetchRepliessSaga({ post_id, comment_id, filters, resolve, reject }) {
  try {
    const result = yield call(fetchReplies_api, post_id, comment_id, filters)
    yield put(setRepliesAction(post_id, comment_id, result.totalComments, result.comments))
    resolve()
  } catch (error) {
    console.log(error)
    reject(error)
  }
}

function* reportPost({ postid, data }) {
  try {
    const result = yield call(reportPost_api, postid, data)
    toast.success(result.message)
  } catch (error) {
    console.log(error)
    let errorMsg = error.errors[0]
    errorMsg = errorMsg.msg
    toast.error(errorMsg)
  }
}

function* submitTransactionSaga({ post_id, data, resolve, reject, isProfile }) {
  try {
    const result = yield call(submitPurchaseTransaction_api, post_id, data)
    if (!isProfile) {
      yield put(setPurchasePostAction(post_id))
    } else {
      yield put(setPurchasePostAction_profile(post_id))
    }
    resolve(result)
  } catch (error) {
    reject(error)
  }
}

function* submitSendTipTransactionSaga({ post_id, data, resolve, reject, isProfile }) {
  try {
    const result = yield call(submitSendTipTransaction_api, post_id, data)
    if (!isProfile) {
      yield put(setSendTipPostAction(post_id))
    } else {
      yield put(setSendTipPostAction_profile(post_id))
    }
    resolve(result)
  } catch (error) {
    reject(error)
  }
}

export function* watchSagas() {
  yield takeLatest(types.CREATE_POST, createPostSaga)
  yield takeLatest(types.FETCH_POSTS, fetchPostsSaga)
  yield takeLatest(types.DELETE_POST, deletePostSaga)
  yield takeLatest(types.LIKE_UNLIKE_COMMENT, likeUnlikeCommentSaga)
  yield takeLatest(types.FETCH_SINGLE_POST, fetchSinglePostSaga)
  yield takeLatest(types.FETCH_POST_COMMENTS, fetchPostCommentsSaga)
  yield takeLatest(types.FETCH_REPLIES, fetchRepliessSaga)
  yield takeLatest(types.REPORT_POST, reportPost)
  yield takeLatest(types.SUBMIT_PURCHASE_TRANSACTION_POST, submitTransactionSaga)
  yield takeLatest(types.SUBMIT_SENDTIP_TRANSACTION_POST, submitSendTipTransactionSaga)
}

export default function* runSagas() {
  yield all([fork(watchSagas)])
}
