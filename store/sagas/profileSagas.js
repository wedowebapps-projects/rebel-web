import { takeLatest, all, fork, call, put } from 'redux-saga/effects'
import * as types from 'store/actions/profile/profileTypes'
import { toggleProfilePostsLoader, setProfilePostsAction, setLikeUnlikeCommentProfilePostAction, setProfilePostCommentsAction, setRepliesActionProfile } from 'store/actions/profile/profileActions'
import { fetchPostsByUsername, likeUnlikComment_api, fetchPostComments_api, fetchReplies_api } from 'store/apis/postApis'

function* fetchProfilePostsSaga({ filters, resolve, reject }) {
  try {
    yield put(toggleProfilePostsLoader(true))
    const username = filters.username
    delete filters.username
    const result = yield call(fetchPostsByUsername, filters, username)
    if(result.posts)
    {
      yield put(setProfilePostsAction(result.posts))
    }
    else
    {
      yield put(setProfilePostsAction(result.data.rows))
    }
    resolve()
  } catch (error) {
    console.log(error)
    reject(error)
  }
}

function* likeUnlikeCommentSaga({ data }) {
  try {
    yield call(likeUnlikComment_api, data.postId, data.commentId, { type: data.type })
    yield put(setLikeUnlikeCommentProfilePostAction({ ...data, isLiked: data.type === 'like' ? true : false }))
  } catch (error) {
    console.log(error)
  }
}

function* fetchProfilePostCommentsSaga({ post_id, filters, resolve, reject }) {
  try {
    const result = yield call(fetchPostComments_api, post_id, filters)
    yield put(setProfilePostCommentsAction(post_id, result.totalComments, result.comments))
    resolve()
  } catch (error) {
    console.log(error)
    reject(error)
  }
}

function* fetchRepliessSaga({ post_id, comment_id, filters, resolve, reject }) {
  try {
    const result = yield call(fetchReplies_api, post_id, comment_id, filters)
    yield put(setRepliesActionProfile(post_id, comment_id, result.totalComments, result.comments))
    resolve()
  } catch (error) {
    console.log(error)
    reject(error)
  }
}

export function* watchSagas() {
  yield takeLatest(types.FETCH_PROFILE_POSTS, fetchProfilePostsSaga)
  yield takeLatest(types.LIKE_UNLIKE_COMMENT_PROFILE_POST, likeUnlikeCommentSaga)
  yield takeLatest(types.FETCH_PROFILE_POST_COMMENTS, fetchProfilePostCommentsSaga)
  yield takeLatest(types.FETCH_REPLIES_PROFILE, fetchRepliessSaga)
}

export default function* runSagas() {
  yield all([fork(watchSagas)])
}
