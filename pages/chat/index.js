import Head from 'next/head'
import React from 'react'
import ChatContainer from 'src/modules/chat/container'

const index = () => (
  <>
    <Head>
      <title>Chat - Rebelyus</title>
    </Head>
    <ChatContainer />
  </>
)

export default index
