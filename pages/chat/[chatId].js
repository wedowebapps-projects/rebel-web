import Head from 'next/head'
import React, { Component } from 'react'
import ChatContainer from 'src/modules/chat/container'

export class Chat extends Component {
  render() {
    return (
      <>
        <Head>
          <title>Chat - Rebelyus</title>
        </Head>
        <ChatContainer />
      </>
    )
  }
}

export default Chat
