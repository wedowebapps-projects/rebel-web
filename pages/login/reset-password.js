import React from 'react'
import ResetPassword from 'src/modules/auth/container/resetPassword';

function resetPassword() {
  return (
    <ResetPassword />
  )
}

export default resetPassword
