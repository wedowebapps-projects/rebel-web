import React from 'react'
import BookmarksContainer from 'src/modules/profile/container/bookmarks'

const bookmarks = () => <BookmarksContainer />

export default bookmarks
