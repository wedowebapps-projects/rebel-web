import React from 'react'
import Head from 'next/head'
import store from 'src/store/index.js'
import withRedux from 'next-redux-wrapper'
import { Provider } from 'react-redux'

import { restoreAuthentication } from 'src/modules/auth/actions/authActions'

import 'bootstrap/dist/css/bootstrap.min.css'
import 'src/assets/css/font-awesome.min.css'
import 'swiper/swiper.scss'
import 'swiper/components/navigation/navigation.scss'
import 'swiper/components/pagination/pagination.scss'
import 'src/assets/css/styles.css'
import 'emoji-mart/css/emoji-mart.css'
import 'react-google-flight-datepicker/dist/main.css'
import 'react-image-lightbox/style.css'

import { authPages } from 'src/config'
import { checkAndRedirectLogin, initialiseAWSConfig } from 'utils/helpers'

import AppLayout from 'src/layouts/AppLayout'
import AuthLayout from 'src/layouts/AuthLayout'
import Toast from 'src/components/app/Toast'

export const title = 'Rebelyus'
const description = 'Sign up to monetize your influence'

class RebelApp extends React.Component {
  componentDidMount() {
    if (process.env.NODE_ENV === 'production' && 'serviceWorker' in navigator) {
      navigator.serviceWorker
        .register('/service-worker.js')
        .then(registration => {
          console.log('service worker registration successful')
        })
        .catch(err => {
          console.warn('service worker registration failed', err.message)
        })
    }
    initialiseAWSConfig()
    store.dispatch(restoreAuthentication())
  }

  static async getInitialProps({ Component, ctx }) {
    let pageProps = {}
    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx)
    }

    return { pageProps }
  }

  render() {
    const { Component, pageProps, router } = this.props

    return (
      <>
        <Head>
          <meta charSet='utf-8' />
          <meta httpEquiv='X-UA-Compatible' content='IE=edge' />
          <meta name='description' content={description} />
          <meta name='keywords' content='Keywords' />
          <meta name='viewport' content='width=device-width, initial-scale=1.0, viewport-fit=cover'></meta>

          {/* Android  */}
          <meta name='theme-color' content='#187bcd' />
          <meta name='mobile-web-app-capable' content='yes' />
          {/* iOS */}
          <meta name='apple-mobile-web-app-title' content={title} />
          <meta name='apple-mobile-web-app-capable' content='yes' />
          <meta name='apple-mobile-web-app-status-bar-style' content='default' />

          {/* Pinned Sites  */}
          <meta name='application-name' content={title} />
          <meta name='msapplication-tooltip' content={title} />
          <meta name='msapplication-starturl' content='/' />
          {/* Tap highlighting  */}
          <meta name='msapplication-tap-highlight' content='no' />
          {/* UC Mobile Browser  */}
          <meta name='full-screen' content='yes' />
          <meta name='browsermode' content='application' />
          {/* Disable night mode for this page  */}
          <meta name='nightmode' content='enable/disable' />
          {/* Layout mode */}
          <meta name='layoutmode' content='fitscreen/standard' />
          {/* imagemode - show image even in text only mode  */}
          <meta name='imagemode' content='force' />
          {/* Orientation  */}
          <meta name='screen-orientation' content='portrait' />

          {/* Main Link Tags  */}
          <link href='/public/icons/icon-16x16.png' rel='icon' type='image/png' sizes='16x16' />
          <link href='/public/icons/icon-32x32.png' rel='icon' type='image/png' sizes='32x32' />
          <link href='/public/icons/icon-96x96.png' rel='icon' type='image/png' sizes='96x96' />
          {/* iOS  */}
          <link href='/public/icons/icon-114x114.png' rel='apple-touch-icon' sizes='114x114' />
          <link href='/public/icons/icon-76x76.png' rel='apple-touch-icon' sizes='76x76' />
          <link href='/public/icons/icon-120x120.png' rel='apple-touch-icon' sizes='120x120' />
          <link href='/public/icons/icon-152x152.png' rel='apple-touch-icon' sizes='152x152' />
          {/* Startup Image  */}
          <link href='/public/icons/splash-8.png' rel='apple-touch-startup-image' />
          {/* Pinned Tab  */}
          <link href='/public/icons/icon-16x16.png' rel='mask-icon' size='any' color='#187bcd' />
          {/* Android  */}
          <link href='/public/icons/icon-192x192.png' rel='icon' sizes='192x192' />
          <link href='/public/icons/icon-120x120.png' rel='icon' sizes='120x120' />
          {/* Others */}
          <link href='/public/favicon.ico' rel='shortcut icon' type='image/x-icon' />

          {/* Manifest.json  */}
          <link href='/manifest.json' rel='manifest' />
        </Head>
        <React.Fragment>
          <Provider store={store}>
            <Toast />
            {authPages.includes(router.asPath) || router.asPath.includes('?return') ? (
              <AuthLayout>
                <Component {...pageProps} />
              </AuthLayout>
            ) : (
              <AppLayout>
                <Component {...pageProps} />
              </AppLayout>
            )}
          </Provider>
        </React.Fragment>
      </>
    )
  }
}

const makeStore = () => store

export default withRedux(makeStore)(RebelApp)
