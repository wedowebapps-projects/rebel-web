import React from 'react'
import SettingContainer from 'src/modules/auth/container/setting'

const index = () => <SettingContainer />

export default index
