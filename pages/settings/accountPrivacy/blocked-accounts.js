import React from 'react'
import { BlockedAccount } from 'src/modules/auth/components/Setting'

const blockedAccounts = () => <BlockedAccount />
export default blockedAccounts
