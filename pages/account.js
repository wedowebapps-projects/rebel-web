import React from 'react'
import MyProfileContainer from 'src/modules/profile/container/myProfile'

const index = () => {
  return <MyProfileContainer />
}

export default index
