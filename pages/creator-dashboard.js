import React from 'react'
import DashboardContainer from 'src/modules/profile/container/creatorDashboard'

const creatorDashboard = () => {
  return <DashboardContainer />
}

export default creatorDashboard
