import React from 'react'
import NotificationPage from 'src/components/app/NotificationPage'

const notifications = () => <NotificationPage />

export default notifications
