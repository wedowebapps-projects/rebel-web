import React from 'react'

import Register from 'src/modules/auth/container/register'

function index() {
  return <Register />
}

export default index
