import React from 'react'
import { TermsConditions } from 'src/modules/auth/components/Setting'

const termsConditions = () => <TermsConditions/>

export default termsConditions
