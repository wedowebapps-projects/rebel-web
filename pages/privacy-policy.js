import React from 'react'
import { PrivacyPolicy } from 'src/modules/auth/components/Setting'

const privacyPolicy = () => <PrivacyPolicy />

export default privacyPolicy
