importScripts('https://www.gstatic.com/firebasejs/7.17.1/firebase-app.js')
importScripts('https://www.gstatic.com/firebasejs/7.17.1/firebase-messaging.js')
firebase.initializeApp({
  apiKey: "AIzaSyCeWNX8OBNrK7-78_N2HDVVFLh_hW9Udzw",
  authDomain: "rebelyus-5c3eb.firebaseapp.com",
  databaseURL: "https://rebelyus-5c3eb.firebaseio.com",
  projectId: "rebelyus-5c3eb",
  storageBucket: "rebelyus-5c3eb.appspot.com",
  messagingSenderId: "239123804960",
  appId: "1:239123804960:web:ab1909a5ccefbc42811d45",
  measurementId: "G-HC7RKMEQZ8"
})

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging()
